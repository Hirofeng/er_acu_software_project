/************************************************************************
*				        ESEC UESTC
* 	 Copyright (C) 2005-2011 ESEC UESTC. All Rights Reserved.
***********************************************************************/

/**
 * Log:  
 * Revision 1.0 2011-5-6����08:36:11 stanley
 * description: create
 *
 */

/**
 * @file 	Dem_Types.h
 * @brief
 *	<li>Function�� definitions for types of DEM module</li>
 *  <li>Design Points��</li>
 *  <p>
 *     Definition for types, including standard types, configuration types, runtime types
 *  </p>
 * @author 	stanley
 * @date 	2011-5-6
 * 
 */

#ifndef DEM_TYPES_H_
#define DEM_TYPES_H_

/****************************** references *********************************/
#include "Std_Types.h"
#if (DEM_NEED_NVM_SUPPORT == STD_ON)
#include "NvM.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif  /* __cplusplus */

/****************************** declarations *********************************/

/****************************** definitions *********************************/
/** @req DEM-FUNR-001[Dem153], @req DEM-FUNR-002[Dem154] Internal identifier of a diagnostic event. 0 is not a valid value. */
typedef uint16 Dem_EventIdType;

/* 0 is not a valid event identifier */
#define DEM_EVENT_ID_INVALID 0x0000

/** @req DEM-CFGR-012[Dem678_Conf] DTC value of the selected group of DTC */
typedef uint32 Dem_DTCGroupType;

/* selects all DTCs */
#define DEM_DTC_GROUP_ALL_DTCS          (uint32)0xFFFFFF

/* selects group of OBD-relevant DTCs */
#define DEM_DTC_GROUP_EMISSION_REL_DTCS 0x000000

/** @req DEM-CFGR-018[Dem644_Conf] kind of DTC */
typedef uint8 Dem_DTCKindType;

/* select all DTCs */
#define DEM_DTC_KIND_ALL_DTCS          0x01U

/* select OBD-relevant DTCs */
#define DEM_DTC_KIND_EMISSION_REL_DTCS 0x02U

/** @req DEM-FUNR-014[Dem010], @req DEM-FUNR-015[Dem548] This type is used to define the location of the events. The definition and use of the different memory types is OEM-specific */
typedef uint8 Dem_DTCOriginType;

/**@req DEM-FUNR-015[Dem548]*/
/* event iformation located in the primary memory */
#define DEM_DTC_ORIGIN_PRIMARY_MEMORY   0x01U

/* event information located in the mirror memory */
#define DEM_DTC_ORIGIN_MIRROR_MEMORY    0x02U

/* event information is lcated in the permanent memory */
#define DEM_DTC_ORIGIN_PERMANENT_MEMORY 0x03U

/* event information located in the secondary memory */
#define DEM_DTC_ORIGIN_SECONDARY_MEMORY 0x04U

/**@req DEM-FUNR-025[Dem006]*/
/** @req DEM-FUNR-025[Dem006] In this data-byte each bit has an individual meaning. The bit is set to 1 when the condition holds. For example, if the 2nd bit(0x02) is set to 1, this means that the test failed this operation cycle. If the bit is set to 0, it has not yet failed this cycle. */
/**@req DEM-FUNR-246[Dem014]*/
typedef uint8 Dem_EventStatusExtendedType;

/*status mask for set filter for all event*/
#define DEM_DTC_STATUS_MASK_ALL                              0x00

/* UDS DTC status bit 0: test failed */
#define DEM_EVENT_STATUS_TEST_FAILED                         0x01U

/* UDS DTC status bit 1: test failed this operation cycle */
#define DEM_EVENT_STATUS_TEST_FAILED_THIS_OPCYC              0x02U

/* UDS DTC status bit 2: Pending DTC */
#define DEM_EVENT_STATUS_PENDING_DTC                         0x04U

/* UDS DTC status bit 3: confirmed DTC */
#define DEM_EVENT_STATUS_CONFIRMED_DTC                       0x08U

/* UDS DTC status bit 4: test not completed since last clear */
#define DEM_EVENT_STATUS_TEST_NOT_COMPLETED_SINCE_LAST_CLEAR 0x10U

/* UDS DTC status bit 5: test failed since last clear */
#define DEM_EVENT_STATUS_TEST_FAILED_SINCE_LAST_CLEAR        0x20U

/* UDS DTC status bit 6: test bit completed this operation cyle */
#define DEM_EVENT_STATUS_TEST_NOT_COMPLETED_THIS_OPCYC       0x40U

/* UDS DTC status 7: warning indicator requested */
#define DEM_EVENT_STATUS_WARNING_INDICATOR_REQUESTED         0x80U

/* type Dem_DTCRequestType */
typedef uint8 Dem_DTCRequestType;

/* first failed DTC requested */
#define DEM_FIRST_FAILED_DTC           0x01U

/* most recent failed DTC requested */
#define DEM_MOST_RECENT_FAILED_DTC     0x02U

/* first detected confirmed DTC requested */
#define DEM_FIRST_DET_CONFIRMED_DTC    0x03U

/* most recently detected confirmed DTC requested */
#define DEM_MOST_REC_DET_CONFIRMED_DTC 0x04U

/** @req DEM-CFGR-018[Dem645_Conf] severity of DTC */
typedef uint8 Dem_DTCSeverityType;

/* no severity information available */
#define DEM_SEVERITY_NO_SEVERITY        0x00

/* maitanance required */
#define DEM_SEVERITY_MAINTENANCE_ONLY   0x20U

/* check at next halt */
#define DEM_SEVERITY_CHECK_AT_NEXT_HALT 0x40U

/* check immediately */
#define DEM_SEVERITY_CHECK_IMMEDIATELY  0x80U

/*type Dem_FilterForFDCType */
typedef uint8 Dem_FilterForFDCType;

/* fault detection counter information used */
#define DEM_FILTER_FOR_FDC_YES 0x00

/* fault detection counter information not used */
#define DEM_FILTER_FOR_FDC_NO  0x01U

/* type Dem_FilterWithSeverityType */
typedef uint8 Dem_FilterWithSeverityType;

/* severity information used */
#define DEM_FILTER_WITH_SEVERITY_YES 0x00U

/* severity information not used */
#define DEM_FILTER_WITH_SEVERITY_NO  0x01U

/*## type Dem_RatioIdType */
typedef uint8 Dem_RatioIdType;

/* used to return the status of updating the DTC filter */
typedef uint8 Dem_ReturnSetDTCFilterType;

/* filter was accepted */
#define DEM_FILTER_ACCEPTED 0x00

/* wrong filter selected */
#define DEM_WRONG_FILTER    0x01U

/* used to return the status of Dem_GetStatusOfDTC */
typedef uint8 Dem_ReturnGetStatusOfDTCType;

/* status of DTC is OK */
#define DEM_STATUS_OK              0x00

/* wrong DTC */
#define DEM_STATUS_WRONG_DTC       0x01U

/* wrong DTC origin */
#define DEM_STATUS_WRONG_DTCORIGIN 0x02U

/* DTC kind wrong */
#define DEM_STATUS_WRONG_DTCKIND   0x03U

/* DTC failed */
#define DEM_STATUS_FAILED          0x04U

/************************************************/
/* used to return the status of Dem_ClearDTC */
typedef uint8 Dem_ReturnClearDTCType;

/* DTC successfully cleared */
#define DEM_CLEAR_OK              0x00

/* wrong DTC */
#define DEM_CLEAR_WRONG_DTC       0x01U

/* wrong DTC origin */
#define DEM_CLEAR_WRONG_DTCORIGIN 0x02U

/* DTC kind wrong */
#define DEM_CLEAR_WRONG_DTCKIND   0x03U

/* DTC not cleared */
#define DEM_CLEAR_FAILED          0x04U

/* clearing of DTC is pending */
#define DEM_CLEAR_PENDING         0x05U

/************************************************/
/** @req DEM-APIR-902, @req DEM-APIR-903, @req DEM-APIR-904, @req DEM-APIR-905 Used to return the status of Dem_DisableDTCStorage and Dem_EnableDTCStorage, Dem_DisableEventStatusUpdate and Dem_EnableEventStatusUpdate */
typedef uint8 Dem_ReturnControlDTCStorageType;

/* DTC storage control successful */
#define DEM_CONTROL_DTC_STORAGE_OK   0x00

/* DTC storage control not successful */
#define DEM_CONTROL_DTC_STORAGE_N_OK 0x01U

/* DTC setting control not successful because group of DTC war wrong */
#define DEM_CONTROL_DTC_WRONG_DTCGROUP 0x02U

/* status of the operation Dem_GetDTCByOccurrenceTime */
typedef uint8 Dem_ReturnGetDTCByOccurrenceTimeType;

/* status of DTC was OK */
#define DEM_OCCURR_OK            0x00

/* DTC kind wrong */
#define DEM_OCCURR_WRONG_DTCKIND 0x01U

/* DTC failed */
#define DEM_OCCURR_FAILED        0x02U

/* used to return the status of Dem_GetDTCOfFreezeFrameRecord */
typedef uint8 Dem_ReturnGetDTCOfFreezeFrameRecordType;

/* DTC successfully returned */
#define DEM_GET_DTCOFFF_OK                0x00

/* wong record */
#define DEM_GET_DTCOFFF_WRONG_RECORD      0x01U

/* No DTC for record available */
#define DEM_GET_DTCOFFF_NO_DTC_FOR_RECORD 0x02U

/* DTC kind wrong */
#define DEM_GET_DTCOFFF_WRONG_DTCKIND     0x03U

/* used to return the status of Dem_GetExtendedDataRecordByDTC */
typedef uint8 Dem_ReturnGetExtendedDataRecordByDTCType;

/* extended data record successfully returned */
#define DEM_RECORD_OK               0x00

/* wrong DTC */
#define DEM_RECORD_WRONG_DTC        0x01U

/* origin wrong */
#define DEM_RECORD_WRONG_DTCORIGIN  0x02U

/* DTC kind wrong */
#define DEM_RECORD_WRONG_DTCKIND    0x03U

/* record number wrong */
#define DEM_RECORD_WRONG_NUMBER     0x04U

/* provided buffer too small */
#define DEM_RECORD_WRONG_BUFFERSIZE 0x05U

/* the requested value is currently not available. The caller can retry later */
#define DEM_RECORD_PENDING          0x06U

/* used to return the status of Dem_GetFreezeFrameDataByDTC */
typedef uint8 Dem_ReturnGetFreezeFrameDataByDTCType;

/* freeze frame data successfully returned */
#define DEM_GET_FFDATABYDTC_OK                 0x00

/* wrong DTC */
#define DEM_GET_FFDATABYDTC_WRONG_DTC          0x01U

/* wrong DTC origin */
#define DEM_GET_FFDATABYDTC_WRONG_DTCORIGIN    0x02U

/* DTC kind wrong */
#define DEM_GET_FFDATABYDTC_WRONG_DTCKIND      0x03U

/* wrong record number */
#define DEM_GET_FFDATABYDTC_WRONG_RECORDNUMBER 0x04U

/* provided buffer size too small */
#define DEM_GET_FFDATABYDTC_WRONG_BUFFERSIZE   0x05U

/* the requested value is currently not available. The caller can retry later */
#define DEM_GET_FFDATABYDTC_PENDING            0x06U

/* used to return the status of Dem_GetNextFilteredDTC */
typedef uint8 Dem_ReturnGetNextFilteredDTCType;

/* returned next filtered DTC */
#define DEM_FILTERED_OK              0x00

/* No DTC matched */
#define DEM_FILTERED_NO_MATCHING_DTC 0x01U

/* DTC kind wrong */
#define DEM_FILTERED_WRONG_DTCKIND   0x02U

/* requested value is currently not available. the caller can retry later */
#define DEM_FILTERED_PENDING         0x03U

/* used to return the status of Dem_GetNumberOfFilteredDTC */
typedef uint8 Dem_ReturnGetNumberOfFilteredDTCType;

/* get number of DTC was successful */
#define DEM_NUMBER_OK      0x00

/* get number of DTC failed */
#define DEM_NUMBER_FAILED  0x01U

/* get number of DTC is pending */
#define DEM_NUMBER_PENDING 0x02U

/* used to return the status of Dem_GetSeverityOfDTC */
typedef uint8 Dem_ReturnGetSeverityOfDTCType;

/* severity successfully returned */
#define DEM_GET_SEVERITYOFDTC_OK              0x00

/* wrong DTC */
#define DEM_GET_SEVERITYOFDTC_WRONG_DTC       0x01U

/* wrong DTC origin */
#define DEM_GET_SEVERITYOFDTC_WRONG_DTCORIGIN 0x02U

/* severity information is not available */
#define DEM_GET_SEVERITYOFDTC_NOSEVERITY      0x03U

/* used to return the status of Dem_GetSizeOfExtendedDataRecordByDTC */
typedef uint8 Dem_ReturnGetSizeOfExtendedDataRecordByDTCType;

/* size successfully returned */
#define DEM_GET_SIZEOFEDRBYDTC_OK      0x00

/* wrong DTC */
#define DEM_GET_SIZEOFEDRBYDTC_W_DTC   0x01U

/* wrong DTC origin */
#define DEM_GET_SIZEOFEDRBYDTC_W_DTCOR 0x02U

/* DTC kind wrong */
#define DEM_GET_SIZEOFEDRBYDTC_W_DTCKI 0x03U

/* wrong record number */
#define DEM_GET_SIZEOFEDRBYDTC_W_RNUM  0x04U

/* the requested value is currently not available. The caller can retry later */
#define DEM_GET_SIZEOFEDRBYDTC_PENDING 0x05U

/* used to return the status of Dem_GetSizeOfFreezeFrame */
typedef uint8 Dem_ReturnGetSizeOfFreezeFrameType;

/* size successfully returned */
#define DEM_GET_SIZEOFFF_OK            0x00

/* wrong DTC */
#define DEM_GET_SIZEOFFF_WRONG_DTC     0x01U

/* wrong DTC origin */
#define DEM_GET_SIZEOFFF_WRONG_DTCOR   0x02U

/* DTC kind wrong */
#define DEM_GET_SIZEOFFF_WRONG_DTCKIND 0x03U

/* wrong record number */
#define DEM_GET_SIZEOFFF_WRONG_RNUM    0x04U

/* the requested value is currently not available. The caller can retry later */
#define DEM_GET_SIZEOFFF_PENDING       0x05U

/** @req DEM-APIR-901[Dem237] used to return the status of Dem_GetFreezeFrameDataIdentifierByDTC */
typedef uint8 Dem_ReturnGetFreezeFrameDataIdentifierByDTCType;

/* freeze frame data identifier successfully returned */
#define DEM_GET_ID_OK              0x00

/* wrong DTC */
#define DEM_GET_ID_WRONG_DTC       0x01U

/* wrong DTC origin */
#define DEM_GET_ID_WRONG_DTCORIGIN 0x02U

/* DTC kind wrong */
#define DEM_GET_ID_WRONG_DTCKIND   0x03U

/* Freeze frame type wrong */
#define DEM_GET_ID_WRONG_FF_TYPE   0x04U

/* type Dem_CallbackClearEventAllowedFncType */
typedef Std_ReturnType (*Dem_CallbackClearEventAllowedFncType)(boolean* Allowed);

/** @req DEM-APIR-069[Dem260] triggers on change of the UDS DTC status byte */
typedef Std_ReturnType (*Dem_CallbackDTCStatusChangedFncType)(uint32 DTC, uint8 DTCStatusOld, uint8 DTCStatusNew);

/* type Dem_CallbackEventDataChangedFncType */
typedef void (*Dem_CallbackEventDataChangedFncType)(Dem_EventIdType EventId);

/* type Dem_CallbackEventStatusChangedFncType */
typedef void (*Dem_CallbackEventStatusChangedFncType)(Dem_EventIdType EventId, Dem_EventStatusExtendedType EventStatusOld, Dem_EventStatusExtendedType EventStatusNew);

/* type Dem_CallbackGetFDCFncType */
typedef Std_ReturnType (*Dem_CallbackGetFDCFncType)(sint8* FaultDetectionCounter);

/* type Dem_CallbackInitMForEFncType */
typedef Std_ReturnType (*Dem_CallbackInitMForEFncType)(uint8 InitMonitorKind);

/** @req DEM-APIR-067[Dem258] InitMonitorForFunction */
typedef Std_ReturnType (*Dem_CallbackInitMForFFncType)(void);

/* function to read data element data */
typedef Std_ReturnType (*Dem_DataElementReadFncType)(uint8* Buffer);

/* DEM_EVENT_STATUS_PASSED */
#define DEM_EVENT_STATUS_PASSED    0x0

/* DEM_EVENT_STATUS_FAILED */
#define DEM_EVENT_STATUS_FAILED    0x01U

/* DEM_EVENT_STATUS_PREPASSED */
#define DEM_EVENT_STATUS_PREPASSED 0x02U

/* DEM_EVENT_STATUS_PREFAILED */
#define DEM_EVENT_STATUS_PREFAILED 0x03U

/* type Dem_DebounceAlgorithmTypeType */
typedef enum Dem_DebounceAlgorithmTypeType
{
    DEM_DEBOUNCE_ALGORITHM_TYPE_CTRBASED, /* counter based algorithm */
    DEM_DEBOUNCE_ALGORITHM_TYPE_TIMEBASED, /* time based algorithm */
    DEM_DEBOUNCE_ALGORITHM_TYPE_MONITOR_INTERNAL /* monitor internal algorithm */
} Dem_DebounceAlgorithmTypeType;

/* type of supported DTC format */
typedef uint8 Dem_DTCTypeSupportedType;

/**@req DEM-FUNR-009[Dem013]*/
/* DEM_ISO15031_6 */
#define DEM_ISO15031_6 0x00

/* DEM_ISO14229_1 */
#define DEM_ISO14229_1 0x01U

/* DEM_ISO11992_4 */
#define DEM_ISO11992_4 0x02U

/** @req DEM-CFGR-019[Dem660_Conf] event kind to distinguish between SW-C and BSW events. */
typedef enum Dem_EventKindType
{
    DEM_EVENT_KIND_BSW, /* the event is assigned to a BSW module */
    DEM_EVENT_KIND_SWC  /* the event is assigned a SW-C */
} Dem_EventKindType;

/** @req DEM-CFGR-021[Dem682_Conf] behavoir of the linked indicator */
typedef uint8 Dem_IndicatorStatusType;

/* DEM_INDICATOR_OFF */
#define DEM_INDICATOR_OFF        0x00

/* DEM_INDICATOR_CONTINUOUS */
#define DEM_INDICATOR_CONTINUOUS 0x01U

/* DEM_INDICATOR_BLINKING */
#define DEM_INDICATOR_BLINKING   0x02U

/* DEM_INDICATOR_BLINK_CONT */
#define DEM_INDICATOR_BLINK_CONT 0x03U

/* DEM_CYCLE_STATE_START */
#define DEM_CYCLE_STATE_START 0x00

/* operation cycle is ended */
#define DEM_CYCLE_STATE_END   0x01U

/* DEM_INIT_MONITOR_CLEAR */
#define DEM_INIT_MONITOR_CLEAR   0x01U

/* DEM_INIT_MONITOR_RESTART */
#define DEM_INIT_MONITOR_RESTART 0x02U

/* no DTC assigned to the event */
#define E_NO_DTC_AVAILABLE 0x02U

typedef enum Dem_OperationCycleType
{
    DEM_OPCYC_IGNITION,
    DEM_OPCYC_OBD_DCY,
    DEM_OPCYC_OTHER,
    DEM_OPCYC_POWER,
    DEM_OPCYC_TIME,
    DEM_OPCYC_WARMUP
} Dem_OperationCycleType;

#if (DEM_ENABLE_CONDITION_SUPPORT == STD_ON)
/** @req DEM-CFGR-005[Dem653_Conf] configuration for enable conditions */
typedef struct Dem_EnableConditionType_T
{
    /** @req DEM-CFGR-005[Dem654_Conf] a unique enable condition Id.Range 0..255 */
    const uint8 DemEnableConditionId;
    /** @req DEM-CFGR-005[Dem656_Conf] initial status for enable or disable of acceptance of event reports of a diagnostic event */
    const boolean DemEnableConditionStatus;
} Dem_EnableConditionType;

/** @req DEM-CFGR-006[Dem745_Conf] configuration for enable condition groups */
typedef struct Dem_EnableConditionGroupType_T
{
    /** number of enable condition in this group, range 1..255 */
    const uint8 DemNumberOfEnableConditionRef;
    /** @req DEM-CFGR-006[Dem655_Conf] pointer to the array of  references to the enable conditions in this group */
    const uint8 * DemEnableConditionRef;
} Dem_EnableConditionGroupType;
#endif

#if (DEM_STORAGE_CONDITION_SUPPORT == STD_ON)
/** @req DEM-CFGR-007[Dem728_Conf] configuration for storage conditions */
typedef struct Dem_StorageConditionType_T
{
    /** @req DEM-CFGR-007[Dem730_Conf] unique storage condition Id. Range 0..255, beginning with 0 and no gaps in between. */
    const uint8 DemStorageConditionId;
    /** @req DEM-CFGR-007[Dem731_Conf] the initial status for enable or disable of storage of a diagnostic event */
    const boolean DemStorageConditionStatus;
} Dem_StorageConditionType;

/** @req DEM-CFGR-008[Dem773_Conf] configuration for storage condition groups */
typedef struct Dem_StorageConditionGroupType_T
{
    /* number of storage conditions in this group, range 1..255 */
    const uint8 DemNumberOfStorageConditionRef;
    /** @req DEM-CFGR-008[Dem768_Conf] pointer to the array of references to storage conditions in this group */
    const uint8 * DemStorageConditionRef;
} Dem_StorageConditionGroupType;
#endif

#if (DEM_INDICATOR_SUPPORT == STD_ON)
/** @req DEM-CFGR-009[Dem680_Conf] configuration for indicators */
/**@req DEM-FUNR-181[Dem509]*/
typedef struct Dem_IndicatorType_T
{
    /** @req DEM-CFGR-009[Dem683_Conf] unique identifier of an indicator */
    const uint8 DemIndicatorID;
} Dem_IndicatorType;
#endif

#if (DEM_OBD_SUPPORT == STD_ON)
/** @req DEM-CFGR-010[Dem693_Conf] configuration of monitor groups according ISO/SAE. This is required only for emission-related ECUs */
typedef struct Dem_MonitorGroupType_T
{
    /** @req DEM-CFGR-010[Dem694_Conf] name of the monitor groups according ISO/SAE */
    const uint8* DemMonitorGroupName;
} Dem_MonitorGroupType;
#endif
#if (DEM_NEED_NVM_SUPPORT == STD_ON)
/** @req DEM-CFGR-011[Dem696_Conf] configuration for a non-volatile memory block, which is used from the DEM */
typedef struct Dem_NvRamBlockIdType_T
{
    /** @req DEM-CFGR-011[Dem697_Conf] reference contains the link to a non-volatile memory block. Here is the block Id to the NvM Block Descriptor */
    const NvM_BlockIdType DemNvRamBlockIdRef;
} Dem_NvRamBlockIdType;
#endif

/** @req DEM-CFGR-014[Dem626_Conf] DTC status changed callback */
typedef struct Dem_CallbackDTCStatusChangedType_T
{
    /** @req DEM-CFGR-014[Dem627_Conf] function of DTC status changed */
    const Dem_CallbackDTCStatusChangedFncType DemCallbackDTCStatusChangedFnc;
} Dem_CallbackDTCStatusChangedType;

/** @req DEM-CFGR-015[Dem600_Conf] InitMonitorForFunction callback */
typedef struct Dem_CallbackInitMForFType_T
{
    /** @req DEM-CFGR-015[Dem633_Conf] function of InitMonitorForFunction */
    const Dem_CallbackInitMForFFncType DemCallbackInitMForFFnc;
} Dem_CallbackInitMForFType;

/** @req DEM-CFGR-026[Dem630_Conf] configuration for callback function for GetFaultDetectionCounter */
typedef struct Dem_CallbackGetFDCType_T
{
    /** @req DEM-CFGR-026[Dem631_Conf] function for GetFaultDetectionCounter */
    const Dem_CallbackGetFDCFncType DemCallbackGetFDCFnc;
} Dem_CallbackGetFDCType;

/** @req DEM-CFGR-027[Dem607_Conf] callback configuration for ClearEventAllowed */
typedef struct Dem_CallbackClearEventAllowedType_T
{
    /** @req DEM-CFGR-027[Dem609_Conf] function for ClearEventAllowed */
    const Dem_CallbackClearEventAllowedFncType DemCallbackClearEventAllowedFnc;
} Dem_CallbackClearEventAllowedType;

/** @req DEM-CFGR-028[Dem606_Conf] callback configuration for EventDataChanged */
typedef struct Dem_CallbackEventDataChangedType_T
{
    /** @req DEM-CFGR-028[Dem608_Conf] function for EventDataChanged */
    const Dem_CallbackEventDataChangedFncType DemCallbackEventDataChangedFcn;
} Dem_CallbackEventDataChangedType;

/** @req DEM-CFGR-029[Dem628_Conf] callback configuration for EventStatusChanged */
typedef struct Dem_CallbackEventStatusChangedType_T
{
    /** @req DEM-CFGR-029[Dem629_Conf] function for EventStatusChanged */
    const Dem_CallbackEventStatusChangedFncType DemCallbackEventStatusChangedFnc;
} Dem_CallbackEventStatusChangedType;

/** @req DEM-CFGR-030[Dem632_Conf] callback configuration for InitMForEvent */
typedef struct Dem_CallbackInitMForEType_T
{
    /** @req DEM-CFGR-030[Dem601_Conf] function for InitMForEvent */
    const Dem_CallbackInitMForEFncType DemCallbackInitMForEFnc;
} Dem_CallbackInitMForEType;

#if ((DEM_MAX_NUMBER_EX_DATA_RECORD > 0) || (DEM_MAX_NUMBER_FF_DATA_RECORD > 0))
/** @req DEM-CFGR-019[Dem663_Conf,Dem672_Conf] capture point in time for data collection of extended data and freeze frame */
typedef enum Dem_EventDataCaptureType_E
{
    DEM_TRIGGER_CONFIRMEDDTC, /* triggers the collection of data if the UDS DTC status bit 3(confirmedDTC) changes from 0 to 1 */
    DEM_TRIGGER_PENDINGDTC, /* triggers the collection of data if the UDS DTC status bit 2(pendingDTC) changes from 0 to 1 */
    DEM_TRIGGER_TESTFAILED  /* triggers the collection fo data if the UDS DTC status bit 0(TestedFailed) changes from 0 to 1 */
} Dem_EventDataCaptureType;
#endif

#if ((DEM_MAX_NUMBER_EX_DATA_RECORD > 0) || (DEM_MAX_NUMBER_FF_DATA_RECORD > 0) || (DEM_OBD_SUPPORT == STD_ON))
/* types of data element */
typedef enum Dem_DataElementTypeType_E
{
    DEM_DATA_ELEMENT_TYPE_EXTERNAL_CS,
    DEM_DATA_ELEMENT_TYPE_EXTERNAL_SR,
    DEM_DATA_ELEMENT_TYPE_INTERNAL
} Dem_DataElementTypeType;

/** @req DEM-CFGR-036[Dem610_Conf] configuration for an internal/external data element class. Choice container */
typedef struct Dem_DataElementClassType_T
{
    /* type of this data element */
    const Dem_DataElementTypeType DemDataElementType;
    /* pointer to the configuration of external CS data element class */
    /* pointer to the configuration of external SR Data element class */
    /* pointer to configuration of internal data element class */
    const void * DemDataElementClassCfgPtr;
} Dem_DataElementClassType;

/* type Dem_InternalDataElementType */
/**@req DEM-FUNR-146[Dem469]*/
typedef enum Dem_InternalDataElementType_T
{
    DEM_AGINGCTR,
    DEM_OCCCTR,
    DEM_OVFLIND
} Dem_InternalDataElementType;

/** @req DEM-CFGR-037[Dem684_Conf] configuration for an internal data element class */
typedef struct Dem_InternalDataElementClassType_T
{
    /** @req DEM-CFGR-037[Dem614_Conf] the size of the data elment in bytes */
    const uint8 DemDataElementDataSize;
    /** @req DEM-CFGR-037[Dem616_Conf] defines the DEM-internal data value which is mapped to the data element */
    const Dem_InternalDataElementType DemInternalDataElment;
} Dem_InternalDataElementClassType;

/** @req DEM-CFGR-038[Dem668_Conf] configuration for an external client/server based data element class */
typedef struct Dem_ExternalCSDataElementClassType_T
{
    /** @req DEM-CFGR-038[Dem646_Conf] the size of the data element in bytes */
    const uint8 DemDataElementDataSize;
    /** @req DEM-CFGR-038[Dem618_Conf] function for read date element */
    /**@req DEM-FUNR-131[Dem261]*/
    const Dem_DataElementReadFncType DemDataElementReadFnc;
    /** @req DEM-CFGR-038[Dem647_Conf] whether a R-port is generated */
    const boolean DemDataElementUsePort;
} Dem_ExternalCSDataElementClassType;

/** @req DEM-CFGR-039[Dem669_Conf] configuration for external sender/receiver based data element class. */
typedef struct Dem_ExternalSRDataElementClassType_T
{
    /** @req DEM-CFGR-039[Dem615_Conf] size of data element in bits */
    const uint8 DemDataElementDataSize;
    /** @req DEM-CFGR-039[Dem770_Conf] instance reference to the actual operation prototype which shall be traced */
    uint8 DemDataElmentInstanceRef;
} Dem_ExternalSRDataElementClassType;
#endif

#if (DEM_MAX_NUMBER_EX_DATA_RECORD > 0)
/** @req DEM-CFGR-035[Dem621_Conf] define the case when the extended data record is stored/updated */
typedef enum Dem_ExtendedDataRecordUpdateType_E
{
    DEM_UPDATE_RECORD_NO, /* extended data record is only captured for new event memory entries */
    DEM_UPDATE_RECORD_YES /* extended data record is captured every time */
} Dem_ExtendedDataRecordUpdateType;

/** @req DEM-CFGR-034[Dem664_Conf] configuration for the combination of extended data records for an extended data class */
typedef struct Dem_ExtendedDataClassType_T
{
    /** number of extended data record for this extended data class */
    const uint8 DemNumberOfExDataRecordClassRef;
    /** @req DEM-CFGR-034[Dem774_Conf] pointer to array of the references of extended record configuration classes */
    /**@req DEM-FUNR-141[Dem041]*/
    const uint8 * DemExtendedDataRecordClassRef;
} Dem_ExtendedDataClassType;

/** @req DEM-CFGR-035[Dem665_Conf] configuration for an extended data record class. It is assembled out of one or several data elements */
typedef struct Dem_ExtendedDataRecordClassType_T
{
    /** @req DEM-CFGR-035[Dem666_Conf] unique identifier for an extended data record */
    const uint8 DemExtendedDataRecordNumber;
    /** @req DEM-CFGR-035[Dem621_Conf] define the case when the extended data record is stored/updated */
    /**@req DEM-FUNR-142[Dem466]*/
    const Dem_ExtendedDataRecordUpdateType DemExtendedDataRecordUpdate;
    /* number of data elment configuration classes in the extended data record */
    const uint8 DemNumberOfDataElementClassRef;
    /** @req DEM-CFGR-035[Dem771_Conf] pointer to the array of references to data element classes */
    /**@req DEM-FUNR-144[Dem282]*/
    const uint8 * DemDataElementClassRef;
} Dem_ExtendedDataRecordClassType;
#endif

#if (DEM_MAX_NUMBER_FF_DATA_RECORD > 0)
/** @req DEM-CFGR-032[Dem706_Conf] configuraion for a data Id class. It is assembled out of one or several data elements */
typedef struct Dem_DidClassType_T
{
    /** @req DEM-CFGR-032[Dem650_Conf] identifier of the Data ID */
    const uint16 DemDidIdentifier;
    /** number of data elment in this Data ID */
    const uint8 DemNumberOfDidDataElementClassRef;
    /** @req DEM-CFGR-032[Dem617_Conf] pointer to the array of references to the data element configuration classes in this Data ID. */
    const uint8 * DemDidDataElementClassRef;
} Dem_DidClassType;

/** @req DEM-CFGR-031[Dem673_Conf] configuration for the combinations of DIDs for a non OBD relevant freeze frame class */
/**@req DEM-FUNR-125[Dem040]*/
typedef struct Dem_FreezeFrameClassType_T
{
    const uint8 DemNumberOfDidClassRef;
    /** @req DEM-CFGR-031[Dem707] pointer to the array of DID configuration classes of this freeze frame. */
    const uint16 * DemDidClassRef;
} Dem_FreezeFrameClassType;

#if (DEM_TYPE_OF_FREEZE_FRAME_RECORD_NUMERATION == DEM_FF_RECNUM_CONFIGURED)
/** @req DEM-CFGR-033[Dem775_Conf] configuration for freeze frame record number. It contains a list of dedicated, different freeze frame record numbers assigned to an event. It is used only record number is configured. */
typedef struct Dem_FreezeFrameRecNumClassType_T
{
    /* number of record in this freeze frame */
    /**@req DEM-FUNR-128[Dem582]*/
    const uint8 DemNumberOfFFRecNumber;
    /** @req DEM-CFGR-033[Dem777_Conf] pointer to array of record numbers for a freeze frame */
    /**@req DEM-FUNR-129[Dem583]*/
    const uint8 * DemFreezeFrameRecordNumber;
} Dem_FreezeFrameRecNumClassType;
#endif

#endif

#if (DEM_OBD_SUPPORT == STD_ON)
/** @req DEM-CFGR-017[Dem729_Conf] configuration for different PIDs for OBD relevant freeze frame class */
typedef struct Dem_PidClassType_T
{
    /** @req DEM-CFGR-017[Dem705_Conf] identifier of the PID */
    const uint8 DemPidIdentifier;
    /* number of data elements in this PID */
    const uint8 DemNumberOfPidDataElementClass;
    /** @req DEM-CFGR-017[Dem733_Conf] pointer to the array of references to data elment configuration class in this PID */
    const uint8 * DemPidDataElementClassRef;
} Dem_PidClassType;


/** @req DEM-CFGR-020[Dem755_Conf] the event OBD readiness group for PID $01 and PID $41 computation */
typedef enum Dem_EventOBDReadinessGroupType_E
{
    DEM_OBD_RDY_AC,
    DEM_OBD_RDY_BOOSTPR,
    DEM_OBD_RDY_CAT,
    DEM_OBD_RDY_CMPRCMPT,
    DEM_OBD_RDY_EGSENS,
    DEM_OBD_RDY_ERG,
    DEM_OBD_RDY_EVAP,
    DEM_OBD_RDY_FLSYS,
    DEM_OBD_RDY_HCCAT,
    DEM_OBD_RDY_HTCAT,
    DEM_OBD_RDY_MISF,
    DEM_OBD_RDY_NONE,
    DEM_OBD_RDY_NOXCAT,
    DEM_OBD_RDY_O2SENS,
    DEM_OBD_RDY_O2SENSHT,
    DEM_OBD_RDY_PMFLT,
    DEM_OBD_RDY_SECAIR
} Dem_EventOBDReadinessGroupType;
#endif

/** @req DEM-CFGR-018[Dem641_Conf] configuration for DTC class */
typedef struct Dem_DTCClassType_T
{
    /** @req DEM-CFGR-018[Dem640_Conf] diagnostic trouble code value */
    /**@req DEM-FUNR-011[Dem277]*/
    const uint32 DemDTC;
    /** @req DEM-CFGR-018[Dem643_Conf] 1-byte value which identifies the corresponding basic vehicle/system function which reports the DTC */
    const uint8 DemDTCFunctionalUnit;
    /** @req DEM-CFGR-018[Dem644_Conf] whether the DTC is OBD relevant or not */
    const Dem_DTCKindType DemDTCKind;
    /** @req DEM-CFGR-018[Dem645_Conf] severity of this DTC */
    /** @req DEM-FUNR-013[Dem033]*/
    const Dem_DTCSeverityType DemDTCSeverity;
#if (DEM_IMMEDIATE_NV_STORAGE_LIMIT > 0)
    /** @req DEM-CFGR-018[Dem739_Conf] switch to enable immediate storage triggering of an according event memory entry persistently to NVRAM */
    /**@req DEM-FUNR-291[Dem550]*/
    const boolean DemImmediateNvStorage;
#endif
    /* number of callback function InitMForFunction of this DTC */
    /**@req DEM-FUNR-205[Dem335]*/
    const uint8 DemNumberOfCbkInitMForF;
    /* pointer to array of callback InitMForFunction of this DTC */
    const Dem_CallbackInitMForFType* DemCallbackInitMForF;
    uint32 DemDTCGroup;
} Dem_DTCClassType;

/** @req DEM-CFGR-022[Dem604_Conf] Debounce algorithm class, choice container. The pointer of the tree types of algorithms uses void pointer in the union */
/**@req DEM-FUNR-060[Dem413]*/
typedef struct Dem_DebounceAlgorithmClassType_T
{
    /* type of algorithm used for this algorithm class */
    const Dem_DebounceAlgorithmTypeType DemDebounceAlgorithmType;
    /** @req DEM-CFGR-022 configuration for counter based debouncing. Void pointer pointing to the type of Dem_DebounceCounterBasedType */
    /** @req DEM-CFGR-022 configuration for time based debouncing. void pointer pointing to type of Dem_DebounceTimeBasedType */
    /** @req DEM-CFGR-022 configuration for monitor internal debouncing. Void pointer pointing to type of Dem_DebounceMonitorInternal */
    const void * DemDebounceConfigPtr;
} Dem_DebounceAlgorithmClassType;

#if (DEM_DEBOUNCE_COUNTER_BASED_SUPPORT == STD_ON)
/** @req DEM-CFGR-023[Dem711_Conf] configuration for counter based debouncing */
typedef struct Dem_DebounceCounterBasedType_T
{
    /** @req DEM-CFGR-023[Dem635_Conf] the step size for decrementation of the internal fault detection counter(PREPASSED) */
    const sint16 DemDebounceCounterDecrementStepSize;
    /** @req DEM-CFGR-023[Dem636_Conf] the value of the internal fault detection counter which indicates the passed status */
    /**@req DEM-FUNR-065[Dem417]*/
    const sint16 DemDebounceCounterPassedThreshold;
    /** @req DEM-CFGR-023[Dem637_Conf] the step size for incrementation of the internal fault detection counter(PREFAILED) */
    const sint16 DemDebounceCounterIncrementStepSize;
    /** @req DEM-CFGR-023[Dem618_Conf] the value of the internal fault detection counter which indicate the failed status */
    /**@req DEM-FUNR-064[Dem416]*/
    const sint16 DemDebounceCounterFailedThreshold;
    /** @req DEM-CFGR-023[Dem685_Conf] swithc for the activation of Jump-Down */
    /**@req DEM-FUNR-070[Dem422]*/
    const boolean DemDebounceCounterJumpDown;
    /** @req DEM-CFGR-023[Dem638_Conf] Jump-Down value of the interanl fault detectin counter */
    /**@req DEM-FUNR-071[Dem423]*/
    const sint16 DemDebounceCounterJumpDownValue;
    /** @req DEM-CFGR-023[Dem686_Conf] switch for the acitvation of Jump-Up */
    /**@req DEM-FUNR-072[Dem424]*/
    const boolean DemDebounceCounterJumpUp;
    /** @req DEM-CFGR-023[Dem639_Conf] Jump-Up value of the internal fault detection counter */
    /**@req DEM-FUNR-073[Dem425]*/
    const sint16 DemDebounceCounterJumpUpValue;
} Dem_DebounceCounterBasedType;
#endif

#if (DEM_DEBOUNCE_TIME_BASED_SUPPORT == STD_ON)
/** @req DEM-CFGR-024[Dem713_Conf] configuration for time based debouncing */
typedef struct Dem_DebounceTimeBasedType_T
{
    /** @req DEM-CFGR-024[Dem716_Conf] time out duration for Event Failed qualification */
    /**@req DEM-FUNR-079[Dem430]*/
    const uint32 DemDebounceTimeFailedThreshold;
    /** @req DEM-CFGR-024[Dem717_Conf] the time out duration for Event Passed qualification */
    /**@req DEM-FUNR-083[Dem434]*/
    const uint32 DemDebounceTimePassedThreshold;
} Dem_DebounceTimeBasedType;
#endif

/** @req DEM-CFGR-025[Dem712_Conf] configuratioin for monitor internal debouncing */
typedef struct Dem_DebounceMonitorInternalType_T
{
    /** @req DEM-CFGR-025 callback function for GetFaultDetectionCounter used to obtain the value of the fault detection counter */
    const Dem_CallbackGetFDCType DemCallbackGetFDC;
} Dem_DebounceMonitorInternalType;

#if (DEM_INDICATOR_SUPPORT == STD_ON)
/** @req DEM-CFGR-021[Dem747_Conf] which failure cycle is used for the WarningzIndicatorOnCritia handling */
typedef enum Dem_IndicatorFailureCycleSourceType_E
{
    DEM_FAILURE_CYCLE_EVENT, /* event based failure cycle is used */
    DEM_FAILURE_CYCLE_INDICATOR /* indicatore based failure cycle is used */
} Dem_IndicatorFailureCycleSourceType;

/** @req DEM-CFGR-021[Dem681_Conf] event specific configuration of indicators */
typedef struct Dem_IndicatorAttributeType_T
{
    /** @req DEM-CFGR-021[Dem682_Conf] behavior of thie linked indicator */
    /**@req DEM-FUNR-183[Dem511]*/
    const Dem_IndicatorStatusType DemIndicatorBehaviour;
    /** @req DEM-CFGR-021[Dem750_Conf] the number of failure cycles for the WarningIndicatorOnCriteria, if the failure cycle source is indicator based */
    /**@req DEM-FUNR-185[Dem500]*/
    const uint8 DemIndicatorFailureCycleCounterThreshold;
    /** @req DEM-CFGR-021[Dem751_Conf] kind of failure cycle for the indicator controlled by the according event used for WarningIndicatorOnCriteria, if the failure cycle soure is indicatore based */
    /**@req DEM-FUNR-186[Dem504]*/
    const uint8 DemIndicatorFailureCycleRef;
    /** @req DEM-CFGR-021[Dem747_Conf] which failure cycle is used for the WarningzIndicatorOnCritia handling */
    /**@req DEM-FUNR-188[Dem568]*/
    const Dem_IndicatorFailureCycleSourceType DemIndicatorFailureCycleSource;
    /** @req DEM-CFGR-021[Dem748_Conf] the number of healing cycles for the WarningIndicatorOffCriteria */
    /**@req DEM-FUNR-189[Dem502]*/
    const uint8 DemIndicatorHealingCycleCounterThreshold;
    /** @req DEM-CFGR-021[Dem749_Conf] kind of healingcycle for the indicator controlled by the according event used for the WarningIndicatorOffCriteria */
    /**@req DEM-FUNR-190[Dem505]*/
    const uint8 DemIndicatorHealingCycleRef;
    /** @req DEM-CFGR-021[Dem687_Conf] reference to the used indicator */
    const Dem_IndicatorType* DemIndicatorRef;
} Dem_IndicatorAttributeType;
#endif

/** @req DEM-CFGR-020[Dem657_Conf] configuration for EventClass */
typedef struct Dem_EventClassType_T
{
    /** @req DEM-CFGR-020[Dem622_Conf] switch to allow aging/unlearning of the event or not */
    const boolean DemAgingAllowed;
    /** @req DEM-CFGR-020[Dem623_Conf] number of aging cycles needed to unlearn/delete the event, range:1..256 */
    /**@req DEM-FUNR-173[Dem493]*/
    const uint16 DemAgingCycleCounterThreshold;
#if (DEM_PTO_SUPPORT == STD_ON)
    /** @req DEM-CFGR-020[Dem602_Conf] whether the event is affected by the DEM PTO handling */
    const boolean DemConsiderPtoStatus;
#endif
    /** @req DEM-CFGR-020[Dem658_Conf] the event destination assigns events to none,one or multiple origins. */
    const Dem_DTCOriginType DemEventDestination[4];
    /** @req DEM-CFGR-020[Dem753_Conf] the number of failure cycles for the event based fault confiramtion. If the event does not handle fault confirmatioin, it is not used. */
    /**@req DEM-FUNR-093[Dem528]*/
    const uint16 DemEventFailureCycleCounterThreshold;
#if (DEM_OBD_SUPPORT == STD_ON)
    /** @req DEM-CFGR-020[Dem755_Conf] the event OBD readiness group for PID $01 and PID $41 computation */
    const Dem_EventOBDReadinessGroupType DemEventOBDReadinessGroup;
#endif
    /** @req DEM-CFGR-020[Dem662_Conf] priority of the event */
    /** @req DEM-FUNR-003[Dem382]*/
    const uint16 DemEventPriority;
#if ((DEM_MAX_NUMBER_PRESTORED_FF > 0) && (DEM_MAX_NUMBER_FF_DATA_RECORD > 0))
    /** @req DEM-CFGR-020[Dem671_Conf] whether the prestorage of freeze frame is supported by the assigned event */
    /**@req DEM-FUNR-135[Dem002]*/
    const boolean DemFFPrestorageSupported;
#endif
    /** @req DEM-CFGR-020[Dem624_Conf] reference to the cycle used for aging of the event. It is used only when aging allowed, dependency: DemAgingAllowed */
    /**@req DEM-FUNR-174[Dem494]*/
    const uint8 DemAgingCycleRef;
#if (DEM_ENABLE_CONDITION_SUPPORT == STD_ON)
    /** @req DEM-CFGR-020[Dem746_Conf] reference to enable condition group of this event */
    /**@req DEM-FUNR-113[Dem446]*/
    const Dem_EnableConditionGroupType* DemEnableConditionGroupRef;
#endif
    /** @req DEM-CFGR-020[Dem752_Conf] kind of failure cycle for the event based fault confirmation. It is only used if the event support fault confirmation */
    /**@req DEM-FUNR-094[Dem529]*/
    const uint8 DemEventFailureCycleRef;
#if (DEM_OBD_SUPPORT == STD_ON)
    /** @req DEM-CFGR-020[Dem695_Conf] reference to the monitor group according ISO/SAT of this event */
    const Dem_MonitorGroupType* DemMonitorGroupRef;
#endif
    /** @req DEM-CFGR-020[Dem702_Conf] kind of operation cycle for the event */
    const uint8 DemOperationCycleRef;
#if (DEM_STORAGE_CONDITION_SUPPORT == STD_ON)
    /** @req DEM-CFGR-020[Dem769_Conf] reference to a storage condition group of this event */
    /**@req DEM-FUNR-119[Dem453]*/
    const Dem_StorageConditionGroupType* DemStorageConditionGroupref;
#endif
#if (DEM_INDICATOR_SUPPORT == STD_ON)
    /* number of event specific configuration of indicators */
    /**@req DEM-FUNR-182[Dem510]*/
    const uint8 DemNumberOfIndicatorAttribute;
    /** @req DEM-CFGR-020 pointer to array of event specific configuration of indicators, rang 0.. 255. */
    const Dem_IndicatorAttributeType* DemIndicatorAttribute;
#endif
    /** @req DEM-CFGR-020 debounce algorithm class */
    const Dem_DebounceAlgorithmClassType DemDebounceAlgorithmClass;
} Dem_EventClassType;

/** @req DEM-CFGR-019[Dem661_Conf] configuration for events */
typedef struct Dem_EventParameterType_T
{
    /** @req DEM-CFGR-019[Dem659_Conf] unique identifier of a diagnostic event */
    const Dem_EventIdType DemEventId;
    /** @req DEM-CFGR-019[Dem660_Conf] event kind to distinguish between SW-C and BSW events */
    const Dem_EventKindType DemEventKind;
#if (DEM_MAX_NUMBER_EX_DATA_RECORD > 0)
    /** @req DEM-CFGR-019[Dem663_Conf] the point in time when the extended data collection is done for the initial event memory entry */
    /**@req DEM-FUNR-143[Dem467]*/
    const Dem_EventDataCaptureType DemExtendedDataCapture;
#endif
#if (DEM_MAX_NUMBER_FF_DATA_RECORD > 0)
    /** @req DEM-CFGR-019[Dem672_Conf] the point in time when the freeze frame data collection is done for the initial event memory entry */
    /**@req DEM-FUNR-130[Dem461]*/
    const Dem_EventDataCaptureType DemFreezeFrameCapture;
#endif
#if ((DEM_MAX_NUMBER_FF_DATA_RECORD > 0) && (DEM_TYPE_OF_FREEZE_FRAME_RECORD_NUMERATION == DEM_FF_RECNUM_CALCULATED))
    /* start of the record number for this event, this only required for calculated record numeration. This can be used to translate Event-unique(relative) record number to ECU-unique(absolute) record number. */
    /**@req DEM-FUNR-127[Dem581]*/
    const uint8 DemFreezeFrameRecordNumStart;
    /** @req DEM-CFGR-019[Dem605_Conf] number of according freeze frame records which can be maximal be stored for this event. Only used for calculated record numberation. dependency: DemTypeOfFreezeFrameRecordNumeration */
    /**@req DEM-FUNR-126[Dem337]*/
    const uint8 DemMaxNumberFreezeFrameRecords;
#endif
    /** @req DEM-CFGR-019[Dem642_Conf] DTC configuration associated with the diagnostic event */
    /**@req DEM-FUNR-123[Dem460]*/
    const Dem_DTCClassType* DemDTCClassRef;
#if (DEM_MAX_NUMBER_EX_DATA_RECORD > 0)
    /** @req DEM-CFGR-019[Dem667_Conf] reference to an extended data configuratin class */
    const Dem_ExtendedDataClassType* DemExtendedDataClassRef;
#endif
#if (DEM_MAX_NUMBER_FF_DATA_RECORD > 0)
    /** @req DEM-CFGR-019[Dem674_Conf] reference to the freeze frame configuration class */
    /**@req DEM-FUNR-124[Dem039]*/
    const Dem_FreezeFrameClassType* DemFreezeFrameClassRef;
#endif
#if ((DEM_MAX_NUMBER_FF_DATA_RECORD > 0) && (DEM_TYPE_OF_FREEZE_FRAME_RECORD_NUMERATION == DEM_FF_RECNUM_CONFIGURED))
    /** @req DEM-CFGR-019[Dem776_Conf] reference to the configuration class for freeze frame record number. It defines the list of dedicated freeze frame record numbers associated with the diagnostic event. Only required for configured record numberation */
    const Dem_FreezeFrameRecNumClassType* DemFreezeFrameRecNumClassRef;
#endif
    /* callback functiono ClearEventAllowed for this event */
    const Dem_CallbackClearEventAllowedType DemCallbackClearEventAllowed;
    /* callback function EventDataChanged of this event */
    const Dem_CallbackEventDataChangedType DemCallbackEventDataChanged;
    /* number of callback function EventStatusChanged of this event */
    const uint8 DemNumberOfCbkEventStatusChanged;
    /* pointer to the array of callback EventStatusChanged of this event */
    const Dem_CallbackEventStatusChangedType* DemCallbackEventStatusChanged;
    /* callback InitMForEvent of this event */
    /**@req DEM-FUNR-203[Dem003]*/
    const Dem_CallbackInitMForEType DemCallbackInitMForE;
    /* configuration of event class for this event */
    const Dem_EventClassType DemEventClass;
} Dem_EventParameterType;

#if (DEM_OBD_SUPPORT == STD_ON)
/** @req DEM-CFGR-013[Dem737_Conf] IUMPR group type */
typedef enum Dem_IUMPRGroupType_E
{
    DEM_IUMPR_BOOSTPRS,
    DEM_IUMPR_CAT1,
    DEM_IUMPR_CAT2,
    DEM_IUMPR_EGR,
    DEM_IUMPR_EGSENSOR,
    DEM_IUMPR_EVAP,
    DEM_IUMPR_NMHCCAT,
    DEM_IUMPR_NOXADSORB,
    DEM_IUMPR_NOXCAT,
    DEM_IUMPR_OXS1,
    DEM_IUMPR_OXS2,
    DEM_IUMPR_PMFILTER,
    DEM_IUMPR_PRIVATE,
    DEM_IUMPR_SAIR,
    DEM_IUMPR_SECOXS1,
    DEM_IUMPR_SECOXS2
} Dem_IUMPRGroupType;

/** @req DEM-CFGR-013[Dem741_Conf] whether the ratio Id will be calculated API or observer based */
typedef enum Dem_RatioIdTypeType_E
{
    DEM_RATIO_API, /* API based ratio Id */
    DEM_RATIO_OBSERVER /* Observer based ratio Id */
} Dem_RatioIdTypeType;

/** @req DEM-CFGR-013[Dem734_Conf] configuration for OBD specific ratio Id */
typedef struct Dem_RatioIdClassType_T
{
    /** @req DEM-CFGR-013[Dem735_Conf] Reference to DemEventParameter */
    const Dem_EventParameterType* DemDiagnosticEventRef;
    /** @req DEM-CFGR-013[Dem736_Conf] reference to FiMID */
    const uint16 DemFunctionIdRef;
    /** @req DEM-CFGR-013[Dem737_Conf] the assigned IUMPR group of the ratio Id */
    const Dem_IUMPRGroupType DemIUMPRGroup;
    /** @req DEM-CFGR-013[Dem741_Conf] whether the ratio Id will be calculated API or observer based */
    const Dem_RatioIdTypeType DemRatioIdType;
} Dem_RatioIdClassType;
#endif

/** @req DEM-CFGR-016[Dem634_Conf] configuration parameters and containers of DEM */
typedef struct Dem_ConfigSetType_T
{
    /* number of DTC classes in this DEM module */
    const uint16 DemNumberOfDTCClass;
    /* pointer to the array of all the DTC configuration classes in the DEM module */
    const Dem_DTCClassType* DemDTCClass;
#if (DEM_OBD_SUPPORT == STD_ON)
    /* number of PID configuration classes in this DEM module */
    const uint8 DemNumberOfPidClass;
    /* pointer to the array of all the PID configuration classes in this DEM module */
    const Dem_PidClassType* DemPidClass;
#endif
    /* number of event parameter configurations in this DEM module */
    const uint16 DemNumberOfEventParameters;
    /* pointer to the array of all the diagnostice event parameter configurations in the DEM module */
    const Dem_EventParameterType* DemEventParameter;
} Dem_ConfigSetType;


#if (DEM_OBD_SUPPORT == STD_ON)
/** @req DEM-CFGR-003[Dem756_Conf] general OBD-specific configuration */
typedef struct Dem_GeneralOBDType_T
{
    /** @req DEM-CFGR-003 [Dem763_Conf] input variable for the accelerator paddle information */
    const Dem_DataElementClassType* DemOBDInputAccceleratorPaddleInformation;
    /** @req DEM-CFGR-003[Dem762_Conf] input variable for the ambient pressure */
    const Dem_DataElementClassType* DemOBDInputAmbientPressure;
    /** @req DEM-CFGR-003[Dem761_Conf] input variable for the ambient temperature */
    const Dem_DataElementClassType* DemOBDInputAmbientTemperature;
    /** @req DEM-CFGR-003[Dem759_Conf] input variable for the distance information */
    const Dem_DataElementClassType* DemOBDInputDistanceInformation;
    /** @req DEM-CFGR-003[Dem757_Conf] input variable for the engine speed */
    const Dem_DataElementClassType* DemOBDInputEngineSpeed;
    /** @req DEM-CFGR-003[Dem772_Conf] input variable for the engine temperature */
    const Dem_DataElementClassType* DemOBDInputEngineTemperature;
    /** @req DEM-CFGR-003[Dem760_Conf] input variable for the programming event */
    const Dem_DataElementClassType* DemOBDInputProgrammingEvent;
    /** @req DEM-CFGR-003[Dem758_Conf] input variable for the vehicle speed */
    const Dem_DataElementClassType* DemOBDInputVehicleSpeed;
} Dem_GeneralOBDType;
#endif

/** @req DEM-CFGR-002[Dem677_Conf] configuration of the BSW DEM */
typedef struct Dem_GeneralType_T
{
    /** @req DEM-CFGR-002[Dem652_Conf] mask for the supported DTC status bits by the DEM. This is used by UDS service 0x19 */
    const uint8 DemDtcStatusAvailabilityMask;
    /** @req DEM-CFGR-002[Dem715_Conf] task time for Dem main function */
    const uint32 DemTaskTime;
    /** @req DEM-CFGR-002[Dem720_Conf] the format returned by Dem_GetTranslation type */
    const Dem_DTCTypeSupportedType DemTypeOfDTCSupported;
#if ((DEM_OBD_SUPPORT == STD_ON) && (DEM_INDICATOR_SUPPORT == STD_ON))
    /** @req DEM-CFGR-002[Dem723_Conf] @req DEM-FUNR-194[Dem546] defines the indicator representing the MIL */
    const Dem_IndicatorType* DemMILIndicatorRef;
#endif
    /* number of all callback functions for DTC status changed notification in the DEM module */
    const uint8 DemNumberOfCallbackDTCStatusChanged;
    /* pointer to the array of all callback function pointers for DTC status changed notification in the DEM module */
    const Dem_CallbackDTCStatusChangedType* DemCallbackDTCStatusChanged;
#if ((DEM_MAX_NUMBER_EX_DATA_RECORD > 0) || (DEM_MAX_NUMBER_FF_DATA_RECORD > 0) || (DEM_OBD_SUPPORT == STD_ON))
    /* number of all the data elment configuration class in the configure */
    const uint8 DemNumberOfDataElementClass;
    /* pointer to the array of all the data elment configuration class in the DEM module configuration */
    const Dem_DataElementClassType* DemDataElementClass;
#endif

#if (DEM_MAX_NUMBER_FF_DATA_RECORD > 0)
    /* number of all the Did configuration class in the DEM configuration */
    const uint8 DemNumberOfDidClass;
    /* pointer to the array of all the Did configuration class in the DEM module  configuration */
    const Dem_DidClassType* DemDidClass;
#endif
#if (DEM_ENABLE_CONDITION_SUPPORT == STD_ON)
    /* number of all the enable condition configuration class in DEM module configuration */
    const uint8 DemNumberOfEnableCondition;
    /* pointer to the array of all the enable condition configuration class in DEM module configuration */
    const Dem_EnableConditionType* DemEnableCondition;
    /* number of all the enable condition group configuration class in DEM module configuration */
    const uint8 DemNumberOfEnableConditionGroup;
    /* point to the array of all the enable condition group configuation class in DEM configuration */
    const Dem_EnableConditionGroupType* DemEnableConditionGroup;
#endif
#if (DEM_MAX_NUMBER_EX_DATA_RECORD > 0)
    /* number of all the extended data configuration class in DEM module configuration */
    const uint8 DemNumberOfExtendedDataClass;
    /* pointer to the array of all the extended data configuration classes in the DEM module configuration */
    const Dem_ExtendedDataClassType* DemExtendedDataClass;
    /* number of all the extended data record configuration classes in the DEM module configuration */
    const uint8 DemNumberOfExtendedDataRecordClass;
    /* pointer to the array of all the extended data record configuration classes in DEM module configuration */
    const Dem_ExtendedDataRecordClassType* DemExtendedDataRecordClass;
#endif
#if (DEM_MAX_NUMBER_FF_DATA_RECORD > 0)
    /* number of all the freeze frame configuration classes in DEM module configuration */
    const uint8 DemNumberOfFreezeFrameClass;
    /* pointer to the array of all the freeze frame configuration classes in DEM module configuration */
    const Dem_FreezeFrameClassType* DemFreezeFrameClass;
#endif
#if ((DEM_MAX_NUMBER_FF_DATA_RECORD > 0) && (DEM_TYPE_OF_FREEZE_FRAME_RECORD_NUMERATION == DEM_FF_RECNUM_CONFIGURED))
    /* number of all the freeze frame record number configuration classes in DEM module configuration if the record number is dedicated and configured */
    const uint8 DemNumberOfFreezeFrameRecNumClass;
    /* pointer to the array of all the freeze frame record number configuration classes in the DEM module configuration */
    const Dem_FreezeFrameRecNumClassType* DemFreezeFrameRecNumClass;
#endif
#if (DEM_OBD_SUPPORT == STD_ON)
    const Dem_GeneralOBDType DemGeneralOBD;
#endif
    /* number of all the DTC groups in DEM module configuration */
    const uint8 DemNumberOfDTCGroup;
    /** @req DEM-CFGR-012[Dem679_Conf] pointer to the array of all the DTC groups in DEM module configuration */
    const Dem_DTCGroupType * DemGroupOfDTC;
#if (DEM_INDICATOR_SUPPORT == STD_ON)
    /* number of all the indicator configuration classes in the DEM module configuration */
    const uint8 DemNumberOfIndicator;
    /* pointer to the array of all the indiactor configuration classes in DEM module configuration */
    const Dem_IndicatorType* DemIndicator;
#endif
#if (DEM_OBD_SUPPORT == STD_ON)
    /* number of all the monitor group configuration classes in DEM module configuration */
    const uint8 DemNumberOfMonitorGroup;
    /* pointer to the array of all the monitor group configuration classes in DEM module configuration */
    const Dem_MonitorGroupType* DemMonitorGroup;
#endif
#if (DEM_NEED_NVM_SUPPORT == STD_ON)
    /* number of NvRam block Id configuration classes in DEM configuration */
    const uint8 DemNumberOfNvRamBlockId;
    /* pointer to the array of all the NvRam blockId configuration classess in DEM module configuration .
     * The configuration must make sure that the first block is used for event storage*/
    const Dem_NvRamBlockIdType* DemNvRamBlockId;
#endif
#if (DEM_OBD_SUPPORT == STD_ON)
    /* number of all the ratio Id configuration classes in DEM configuration */
    const uint8 DemNumberOfRatioIdClass;
    /* pointer to the array of all the ratio ID configuration classes in DEM module configuration */
    const Dem_RatioIdClassType* DemRatioId;
#endif
#if (DEM_STORAGE_CONDITION_SUPPORT == STD_ON)
    /* number of all the storage condition configuration classes in DEM module configuration */
    const uint8 DemNumberOfStorageCondition;
    /* poiner to the array of all the storage condition configuration classes in DEM configuration */
    const Dem_StorageConditionType* DemStorageCondition;
    /* number of all the storage condition group configuration classes in DEM configuration */
    const uint8 DemNumberOfStorageConditionGroup;
    /* pointer to the array of all the storage condition group configuration classes in DEM configuration */
    const Dem_StorageConditionGroupType* DemStorageConditionGroup;
#endif
    /* number of all the operation cycles in the DEM module  configuration */
    const uint8 DemNumberOfOperationCycle;
    /** @req DEM-CFGR-004[Dem701_Conf] pointer to array of all the operation cycle in DEM configuration */
    const Dem_OperationCycleType * DemOperationCycle;
} Dem_GeneralType;

/** @req DEM-CFGR-001 */
typedef  struct Dem_ConfigType_T
{
    const Dem_ConfigSetType DemConfigSet;    /* DemConfigSet */
    const Dem_GeneralType 	DemGeneral;      /* DemGeneral */
} Dem_ConfigType;

#ifdef __cplusplus
}
#endif  /* __cplusplus */

#endif /* DEM_TYPES_H_ */
