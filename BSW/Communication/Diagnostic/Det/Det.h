/************************************************************************
*				        ESEC UESTC
* 	 Copyright (C) 2005-2010 ESEC UESTC. All Rights Reserved.
***********************************************************************/

/**
 * Log:  
 * Revision 1.0 2010-5-13����10:48:55 stanley
 * description: create
 *
 */

/**
 * @file 	Det.h
 * @brief
 *	<li>Function�� </li>
 *  <li>Design Points��</li>
 *  <p>
 *  
 *  </p>
 * @author 	stanley
 * @date 	2010-5-13
 * 
 */

#ifndef DET_H
#define DET_H

/****************************** references *********************************/
#include "Std_Types.h"

#ifdef __cplusplus
extern "C" {
#endif  /* __cplusplus */

#define DET_H_AR_MAJOR_VERSION  2
#define DET_H_AR_MINOR_VERSION  2
/****************************** declarations *********************************/
#define Det_ReportError(ModuleId, InstanceId, ApiId, ErrorId);

/****************************** definitions *********************************/

#ifdef __cplusplus
}
#endif  /* __cplusplus */

#endif /* DET_H */
