/*
 * Copyright (c) 2013 - 2016, Freescale Semiconductor, Inc.
 * Copyright 2016-2018 NXP
 * All rights reserved.
 *
 * THIS SOFTWARE IS PROVIDED BY NXP "AS IS" AND ANY EXPRESSED OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL NXP OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SRX_IRQ_H
#define SRX_IRQ_H

#include <stddef.h>
#include "device_registers.h"

/*******************************************************************************
 * API
 ******************************************************************************/

#if defined(__cplusplus)
extern "C" {
#endif

/**
 * @brief Driver side interrupt
 *
 * Fast Rx Event.
 * Gets called from the low level handler
 * with instance and channel as parameter
 *
 * @param[in] instance Peripheral instance
 * @param[in] channel Communication channel
 */
void SRX_DRV_IRQ_FastInstNChanNHandler(uint32_t instance, uint32_t channel);

/**
 * @brief Driver side interrupt
 *
 * Slow Rx Event.
 * Gets called from the low level handler
 * with instance and channel as parameter
 *
 * @param[in] instance Peripheral instance
 * @param[in] channel Communication channel
 */
void SRX_DRV_IRQ_SlowInstNChanNHandler(uint32_t instance, uint32_t channel);

/**
 * @brief Driver side interrupt
 *
 * Rx Error Event.
 * Gets called from the low level handler
 * with instance and channel as parameter
 *
 * @param[in] instance Peripheral instance
 * @param[in] channel Communication channel
 */
void SRX_DRV_IRQ_RxErrInstNChanNHandler(uint32_t instance, uint32_t channel);

#if defined(__cplusplus)
}
#endif

#endif /* SRX_IRQ_H */
/*******************************************************************************
 * EOF
 ******************************************************************************/
