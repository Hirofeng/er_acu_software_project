/*===========================================================================*/
/* Project			= AUTOSAR MCAL Components								 */
/* Module			= Public HeadFile										 */
/* Version			= 1.0													 */
/* Date				= 07.04.2013											 */
/*===========================================================================*/
/*					COPYRIGHT												 */
/*===========================================================================*/
/* Copyright (c) 2012 by  Infrastructure software Co.,LTD.              */
/*																			 */
/* All rights are reserved. Reproduction in whole or in part is				 */
/* prohibited without the prior written consent of the copyright			 */
/* owner. The information presented in this document does not				 */
/* form part of any quotation or contract, is believed to be				 */
/* accurate and reliable and may be changed without notice.					 */
/* No liability will be accepted by the publisher for any					 */
/* consequence of its use. Publication thereof does not convey				 */
/* nor imply any license under patent- or other industrial or				 */
/* intellectual property rights.											 */
/*===========================================================================*/
/*					Revision Control History								 */
/*===========================================================================*/
/*																			 */
/* <revision 1.0>:	<07.04.2013>		<Eric>    Initial version		     */
/*																			 */
/*===========================================================================*/
#ifndef STD_EXTTYPES_H
#define STD_EXTTYPES_H

/* Define all bits. */
#define 	BIT0		((uint32)1 << 0)
#define 	BIT1		((uint32)1 << 1)
#define 	BIT2		((uint32)1 << 2)
#define 	BIT3		((uint32)1 << 3)
#define 	BIT4		((uint32)1 << 4)
#define 	BIT5		((uint32)1 << 5)
#define 	BIT6		((uint32)1 << 6)
#define 	BIT7		((uint32)1 << 7)
#define 	BIT8		((uint32)1 << 8)
#define 	BIT9		((uint32)1 << 9)
#define 	BIT10		((uint32)1 << 10)
#define 	BIT11		((uint32)1 << 11)
#define 	BIT12		((uint32)1 << 12)
#define 	BIT13		((uint32)1 << 13)
#define 	BIT14		((uint32)1 << 14)
#define 	BIT15		((uint32)1 << 15)
#define 	BIT16		((uint32)1 << 16)
#define 	BIT17		((uint32)1 << 17)
#define 	BIT18		((uint32)1 << 18)
#define 	BIT19		((uint32)1 << 19)
#define 	BIT20		((uint32)1 << 20)
#define 	BIT21		((uint32)1 << 21)
#define 	BIT22		((uint32)1 << 22)
#define 	BIT23		((uint32)1 << 23)
#define 	BIT24		((uint32)1 << 24)
#define 	BIT25		((uint32)1 << 25)
#define 	BIT26		((uint32)1 << 26)
#define 	BIT27		((uint32)1 << 27)
#define 	BIT28		((uint32)1 << 28)
#define 	BIT29		((uint32)1 << 29)
#define 	BIT30		((uint32)1 << 30)
#define 	BIT31		((uint32)1 << 31)

/* Define some numbers' Macro. */
#define 	ZERO	(0)
#define		ONE		(1)
#define		TWO		(2)
#define		THREE   (3)
#define 	FOUR	(4)
#define		FIVE	(5)
#define		SIX		(6)
#define		SEVEN   (7)
#define		EIGHT	(8)
#define		NINE    (9)

#define		DISABLE_GLOBAL_INTERRUPT	asm volatile (" wrteei 0");
#define  	ENABLE_GLOBAL_INTERRUPT 	asm volatile (" wrteei 1");


#endif
