/*============================================================================*/
/*  Copyright (C) 2009-2011,  INFRASTRUCTURE SOFTWARE CO.,LTD.
 *  
 *  All rights reserved. This software is  property. Duplication
 *  or disclosure without  written authorization is prohibited.
 *  
 *  
 *  @file       <Can.h>
 *  @brief      <Can Registers define>
 *  
 *  <Compiler: CodeWarrior V2.7    MCU:MPC55XX>
 *  
 *  @author     <bo.zeng>
 *  @date       <15-07-2013>
 */
/*============================================================================*/
#ifndef  CAN_REGS_H
#define  CAN_REGS_H
/*=======[R E V I S I O N   H I S T O R Y]====================================*/
/*  <VERSION>    <DATE>    <AUTHOR>    <REVISION LOG>
 *  V1.0.0      20130715    bo.zeng    Initial version
 *  V1.0.1       20131101  jianan.liu  1. Clear buffer when controller of CAN goes
 *                                        to STOP;
 *                                     2. Make the code more united in the same
 *                                        series of chips
 */
/*============================================================================*/

/*=======[V E R S I O N  I N F O R M A T I O N]===============================*/
#define CAN_REG_H_AR_MAJOR_VERSION  2
#define CAN_REG_H_AR_MINOR_VERSION  4
#define CAN_REG_H_AR_PATCH_VERSION  0
#define CAN_REG_H_SW_MAJOR_VERSION  1
#define CAN_REG_H_SW_MINOR_VERSION  0
#define CAN_REG_H_SW_PATCH_VERSION  1

/*=======[I N C L U D E S]====================================================*/
#include "ComStack_Types.h"

/*=======[M A C R O S]========================================================*/
#define CAN_DATA_LENGTH     8

/*
 *  MBCS Register
 */
#define CAN_MBCS_CODE         0x0F000000U        /* MB code */ 
#define CAN_MBCS_CODETX       0x08000000U        /* MB code: Tx INACTIVE */ 
#define CAN_MBCS_CODETXEN     0x04000000U        /* MB code: Tx Pending */ 
#define CAN_MBCS_CODETXRTR    0x02000000U        /* MB code: transmit the message whenever 
                                                    a remote frame with a matching ID is received */ 
#define CAN_MBCS_CODETXABORT  0x01000000U        /* MB code: Abort transmission */ 
#define CAN_MBCS_CODETXINACTIVE  0xf8ffffffU     /* MB code: INACTIVE transmission */ 
#define CAN_MBCS_CODERXINACTIVE  0xf0ffffffU     /* MB code: INACTIVE transmission */

#define CAN_MBCS_CODERX       0x00000000U        /* MB code: Rx MB */ 
#define CAN_MBCS_CODERXEMPTY  0x04000000U        /* MB code: Rx MB active and empty */ 
#define CAN_MBCS_CODERXFULL   0x02000000U        /* MB code: Rx MB is full */
#define CAN_MBCS_CODERXOVRR   0x06000000U        /* MB code: Rx message buffer overrun (a new 
                                                    message arrived before the previous one 
                                                    was picked-up) */ 
#define CAN_MBCS_CODERXBUSY   0x01000000U        /* MB code: MB busy and cannot be read */ 

#define CAN_MBCS_SRR          0x00400000U        /* SRR bit, must be written as '1' in any Tx MB */
#define CAN_MBCS_IDE          0x00200000U        /* IDE bit, 0=standard ID, 1=extended ID */
#define CAN_MBCS_RTR          0x00100000U        /* RTR bit, 0=data frame, 1=remote frame */
#define CAN_MBCS_LENGTH       0x000F0000U        /* DLC (data length code) */
#define CAN_MBCS_TIMESTAMP    0x0000FFFFU        /* time stamp of Rx and Tx frames (timer value 
                                                    captured at the time the beginning of the ID 
                                                    appears on the bus) */
/*
 *  MBID Register
 */
#define CAN_MBID_PRIO         0xE0000000U        /* local Tx priority (added in front of the ID 
                                                    to determine Tx priority) */
#define CAN_MBID_ID_EXTENDED  0x1FFFFFFFU        /* extended (bits 0-28) ID */
#define CAN_MBID_ID_STANDARD  0x1FFC0000U        /* standard (bits 18-28) ID */

/* 
 * MCR Register
 */



//#define CAN_MCR_MDIS          0x80000000U        /* Module Disable */
//#define CAN_MCR_FRZ           0x40000000U        /* Freeze enable */
//#define CAN_MCR_FEN           0x20000000U        /* FIFO mode enable */
//#define CAN_MCR_HALT          0x10000000U        /* Halt FlexCAN */
//#define CAN_MCR_NOTRDY        0x08000000U        /* FlexCAN Not Ready */
#define CAN_MCR_WAK_MSK       0x04000000U        /* Wake Up Interrupt Mask */
//#define CAN_MCR_SOFTRST       0x02000000U        /* Soft Reset */
//#define CAN_MCR_FRZACK        0x01000000U        /* Freeze Mode Acknowledge */
#define CAN_MCR_SLF_WAK       0x00400000U        /* Self Wake Up */
//#define CAN_MCR_WRNEN         0x00200000U        /* Warning interrupt enable */
//#define CAN_MCR_LPMACK        0x00100000U        /* Module Disable Acknowledge */
#define CAN_MCR_WAK_SRC       0x00080000U        /* Wake up Source */
//#define CAN_MCR_DOZE          0x00040000U        /* Doze Module */
//#define CAN_MCR_SRXDIS        0x00020000U        /* Self Rx Disable */
#define CAN_MCR_BCC           0x00010000U        /* Backwards Compatibility Configuration */
//#define CAN_MCR_LPRIOEN       0x00002000U        /* Local Priority Enable */ 
//#define CAN_MCR_AEN           0x00001000U        /* Abort Enable */
//#define CAN_MCR_IDAM          0x00000300U        /* ID acceptance mode for the FIFO mode */
#define CAN_MCR_IDAM32        0x00000000U        /* IDAM: one full ID standard or extended ID */
#define CAN_MCR_IDAM16        0x00000100U        /* IDAM: two standard or partial extended ID */
#define CAN_MCR_IDAM08        0x00000200U        /* IDAM: four partial standard or extended IDs */
#define CAN_MCR_MAXMB         0x0000003FU        /* Maximum number of MBs in use */

/* 
 * CTRL Register
 */
#define CAN_CTRL_PRESDIV      0xFF000000U        /* Prescaler Divider Factor */
#define CAN_CTRL_RJW          0x00C00000U        /* resynchronization jump width */
#define CAN_CTRL_PSEG1        0x00380000U        /* phase segment 1 */
#define CAN_CTRL_PSEG2        0x00070000U        /* phase segment 2 */
#define CAN_CTRL_BOFFMSK      0x00008000U        /* Bus Off Mask */
#define CAN_CTRL_ERRMSK       0x00004000U        /* Error Mask  */
#define CAN_CTRL_CLKSRC       0x00002000U        /* Clock source selection 
                                                    (0=oscillator clock, 1=system clock) */
#define CAN_CTRL_LPB          0x00001000U        /* Loop-Back mode */
#define CAN_CTRL_TWRNMSK      0x00000800U        /* Tx Warning interrupt enable */
#define CAN_CTRL_RWRNMSK      0x00000400U        /* Rx Warning interrupt enable */
#define CAN_CTRL_SMP          0x00000080U        /* Sampling mode (0=1 sample, 1=3 samples) */
#define CAN_CTRL_BOFFREC      0x00000040U        /* Bus off recovery mode (0=automatic, 1=manual) */
#define CAN_CTRL_TSYN         0x00000020U        /* Timer Synchronization (when set the 
                                                    free-running timer is reset each time 
                                                    a message is received into MB0) */
#define CAN_CTRL_LBUF         0x00000010U        /* Lowest Buffer First (0=highest priority buffer 
                                                    transmitted first, 1=lowest numbered buffer 
                                                    transmitted first) */
#define CAN_CTRL_LOM          0x00000008U        /* Listen only mode */
#define CAN_CTRL_PROPSEG      0x00000007U        /* Propagation Segment (length of the segment is 
                                                    field value plus 1 time quantum) */

/* 
 * ESR Register
 */
#define CAN_ESR_TWRNINT       0x00020000U        /* Tx error warning interrupt flag */
#define CAN_ESR_RWRNINT       0x00010000U        /* Rx error warning interrupt flag */
#define CAN_ESR_BIT1ERR       0x00008000U        /* Recessive bit error */
#define CAN_ESR_BIT0ERR       0x00004000U        /* Dominant bit error  */
#define CAN_ESR_ACKERR        0x00002000U        /* Acknowledge error   */
#define CAN_ESR_CRCERR        0x00001000U        /* CRC error detected  */
#define CAN_ESR_FRMERR        0x00000800U        /* Form error detected */
#define CAN_ESR_STFERR        0x00000400U        /* Stuffing error detected */
#define CAN_ESR_TXWRN         0x00000200U        /* Tx error warning flag 
                                                    (set when Tx error counter >=96) */
#define CAN_ESR_RXWRN         0x00000100U        /* Rx error warning flag 
                                                    (set when Rx error counter >=96) */
#define CAN_ESR_IDLE          0x00000080U        /* Can bus IDLE */
#define CAN_ESR_TXRX          0x00000040U        /* Current FlexCAN status */
#define CAN_ESR_FLTCONF       0x00000030U        /* Fault state (00=error active, 
                                                            01=error passive, 1x=bus off) */
#define CAN_ESR_BOFFINT       0x00000004U        /* Bus off interrupt flag */
#define CAN_ESR_ERRINT        0x00000002U        /* Error interrupt flag */
#define CAN_ESR_WAK_INT       0x00000001U        /* Wake-Up Interrupt */

/* IMASK */
#define CAN_IMASK2_INDEX      0
#define CAN_IMASK1_INDEX      1

/* IFLAG */
#define CAN_IFLAG2_INDEX      0
#define CAN_IFLAG1_INDEX      1

/* software interrupt flag */
#define SWF_WAK_MASK          0x01U
#define SWF_BOFF_MASK         0x02U
#define SWF_ERR_MASK          0x04U
#define SWF_TWRN_MASK         0x08U
#define SWF_RWRN_MASK         0x10U
#define SWF_RX_MASK           0x20U
#define SWF_TX_MASK           0x40U

#define CAN_LOW_MB_NUMBER                 32U

typedef struct 
{
    uint32  cs;                     /* control and status */
    uint32  id;                     /* identifier */
    uint8   data[CAN_DATA_LENGTH];  /* data */
} Can_MBRegType;

typedef struct 
{
    uint32  mcr;                    /* module Configuration */
    uint32  ctrl;                   /* control Register */
    uint32  timer;                  /* 16-bit free running timer value */
    uint32  _reserved1;
    uint32  rxgmask;                /* rx Global Mask */
    uint32  rx14mask;               /* rx buffer 14 Mask */
    uint32  rx15mask;               /* rx buffer 15 Mask */
    uint32  ecr;                    /* error counter register */
    uint32  esr;                    /* error and status register */
    uint32  imask[2];               /* interrupt masks (from MB63..MB32([0]) and MB31..MB0([1])) */
    uint32  iflag[2];               /* interrupt flags (from MB63..MB32([0]) and MB31..MB0([1])) */
    
    uint32 CTRL2;                                  /*!< Control 2 Register, offset: 0x34 */
    uint32 ESR2;                                   /*!< Error and Status 2 Register, offset: 0x38 */
	uint32 IMEUR;                                  /*!< Individual Matching Elements Update Register, offset: 0x3C */
	uint32 LRFR;                                   /*!< Lost Rx Frames Register, offset: 0x40 */
	uint32 CRCR;                                   /*!< CRC Register, offset: 0x44 */
	uint32 RXFGMASK;                               /*!< Rx FIFO Global Mask Register, offset: 0x48 */
	uint32 RXFIR;                                  /*!< Rx FIFO Information Register, offset: 0x4C */
	uint8  RESERVED_1[48];
		  
    Can_MBRegType  mb[16];          /* message buffers */
    uint8 RESERVED_2[1792];
    uint32  rximr[16];              /* individual rx mask registers */
} Can_FlexCanRegType;

#endif  /* #ifndef  CAN_REGS_H */

/*=======[E N D   O F   F I L E]==============================================*/



