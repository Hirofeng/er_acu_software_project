/*!
 * @defgroup flexpwm Flexible Pulse Width Modulator (FlexPWM)
 * @brief FlexPWM Driver
 *
 * ## Hardware background ##
 * The pulse width modulator module (FlexPWM) contains one or more PWM submodules,
 * each capable of controlling a single half-bridge power stage. One or more fault channels
 * may also be included.
 * This PWM is capable of controlling most motor types: AC induction motors (ACIM),
 * Permanent Magnet AC motors (PMAC), both brushless (BLDC) and brush DC motors
 * (BDC), switched (SRM) and variable reluctance motors (VRM), and stepper motors.
 *
 * Module features:
 * - 16 bits of resolution for center, edge aligned, and asymmetrical PWMs
 * - PWM outputs capable of operating as complementary pairs or independent channels
 * - Can accept signed numbers for PWM generation
 * - Independent control of both edges of each PWM output
 * - Synchronization to external hardware or other PWM supported
 *  - Double buffered PWM registers
 *  - Integral reload rates from 1 to 16
 * - Half cycle reload capability
 * - Multiple output trigger events can be generated per PWM cycle via hardware
 * - Support for double switching PWM outputs
 * - Fault inputs can be assigned to control multiple PWM outputs
 * - Programmable filters for fault inputs
 * - Independently programmable PWM output polarity
 * - Independent top and bottom deadtime insertion
 * - Each complementary pair can operate with its own PWM frequency and deadtime values
 * - Individual software control for each PWM output
 * - All outputs can be programmed to change simultaneously via a "Force Out" event
 * - PWMX pin can optionally output a third PWM signal from each submodule
 * - PWMX channels not used for PWM generation can be used for buffered output compare functions
 * - Channels not used for PWM generation can be used for input capture functions
 * - Capture is supported only for X channels not A and B channels
 * - Enhanced dual edge capture functionality
 * - The option to supply the source for each complementary PWM signal pair from any of the following:
 *  - External digital pin
 *  - Internal timer channel
 *  - External ADC input, taking into account values set in ADC high and low limit registers
 *
 * ## How to use the FlexPWM driver in your application ##
 * In order to be able to use the FlexPWM driver in your application, the first thing to do is
 * initializing it with the desired configuration. This is done by calling the <b>FLEXPWM_DRV_SetupPwm</b> or
 * <b>FLEXPWM_DRV_ConfigureInputCapture</b> depending on which functionality you want to use.
 *
 * ### Important Notes ###
 * <p>
 *   - Before using the FlexPWM driver the module clock must be configured.
 *   - The user must enable the interrupts for the corresponding FlexPWM module if he wants to use any.
 *   - If the user wants to use the output compare functionalities then these can be achieved using
 *     the PWM capabilities as specified in section <b>40.4.2.5</b> in the Reference Manual.
 *
 *
 * </p>
 @}
 */
