/*
 * Copyright 2017 NXP
 * All rights reserved.
 *
 * THIS SOFTWARE IS PROVIDED BY NXP "AS IS" AND ANY EXPRESSED OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL NXP OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * @page misra_violations MISRA-C:2012 violations
 *
 * @section [global]
 * Violates MISRA 2012 Advisory Rule 8.7, External could be made static.
 * Function is defined for usage by application code.
 *
 * @section [global]
 * Violates MISRA 2012 Advisory Rule 8.13, Pointer parameter could be declared as pointing to const
 * This is a pointer to the driver context structure which is for internal use only, and the application
 * must make no assumption about the content of this structure. Therefore it is irrelevant for the application
 * whether the structure is changed in the function or not. The fact that in a particular implementation of some
 * functions there is no write in the context structure is an implementation detail and there is no reason to
 * propagate it in the interface. That would compromise the stability of the interface, if this implementation
 * detail is changed in later releases or on other platforms.
 *
 * @section [global]
 * Violates MISRA 2012 Required Rule 1.3, Taking address of near auto variable.
 * The code is not dynamically linked. An absolute stack address is obtained
 * when taking the address of the near auto variable. Also, the called functions
 * do not store the address into variables with lifetime longer then its own call.
 *
 * @section [global]
 * Violates MISRA 2012 Required Rule 10.3, Expression assigned to a narrower or different essential type.
 * The cast is required to perform a conversion between an unsigned integer and an struct type with many values.
 *
 * @section [global]
 * Violates MISRA 2012 Required Rule 11.3, cast performed between a pointer to
 * object type and a pointer to a different object type.
 * The cast is used for casting a pointer to a struct in order to optimize code.
 *
 * @section [global]
 * Violates MISRA 2012 Required Rule 2.2, Last value assigned to variable
 * These parameters are assigned after declared to avoid return a unknown value after they are initialized.
 *
 * @section [global]
 * Violates MISRA 2012 Advisory Rule 11.5, conversion from pointer to void to pointer to other type
 * This is required for working with the common initialize function in initialized counter bus function.
 * Counter bus can run with many different mode.
 *
 */

#include "emios_pwm_driver.h"
#include "emios_mc_driver.h"
#include "emios_hw_access.h"

/*******************************************************************************
* Static variables define
******************************************************************************/
/*!
* @brief OPWFM configuration parameters structure
*/
typedef struct
{
    emios_pwm_mode_config_t         mode;                     /*!< Sub-mode selected */
    emios_clock_internal_ps_t       internalPrescaler;        /*!< Internal prescaler, pre-scale channel clock by internalPrescaler +1 */
    bool                            internalPrescalerEn;      /*!< Internal prescaler Enable */
    emios_pulse_polarity_mode_t     outputActiveMode;         /*!< Output active value, Choose active low or high level */
    uint32_t                        periodCount;              /*!< Period count */
    uint32_t                        dutyCycleCount;           /*!< Duty cycle count */
} emios_opwfm_param_t;

/*!
* @brief OPWMB configuration parameters structure
*/
typedef struct
{
    emios_pwm_mode_config_t         mode;                     /*!< Sub-mode selected */
    emios_clock_internal_ps_t       internalPrescaler;        /*!< Internal prescaler, pre-scale channel clock by internalPrescaler +1 */
    bool                            internalPrescalerEn;      /*!< Internal prescaler Enable */
    emios_pulse_polarity_mode_t     outputActiveMode;         /*!< Output active value, Choose active low or high level */
    uint32_t                        leadingEdgePlacement;     /*!< Leading edge placement */
    uint32_t                        trailingEdgePlacement;    /*!< Trailing edge placement */
    emios_bus_select_t              timebase;                 /*!< Counter bus selected */
} emios_opwmb_param_t;

/*!
* @brief OPWMCB configuration parameters structure
*/
typedef struct
{
    emios_pwm_mode_config_t         mode;                     /*!< Sub-mode selected */
    emios_clock_internal_ps_t       internalPrescaler;        /*!< Internal prescaler, pre-scale channel clock by internalPrescaler +1 */
    bool                            internalPrescalerEn;      /*!< Internal prescaler Enable */
    emios_pulse_polarity_mode_t     outputActiveMode;         /*!< Output active value, Choose active low or high level */
    uint32_t                        idealDutyCycle;           /*!< Ideal duty cycle of the PWM signal using to
                                                              compare with the selected time base */
    uint32_t                        deadTime;                 /*!< The dead time value and is compared against the internal counter */
    emios_bus_select_t              timebase;                 /*!< Counter bus selected */
} emios_opwmcb_param_t;

/*!
* @brief OPWMT configuration parameters structure
*/
typedef struct
{
    emios_pwm_mode_config_t         mode;                     /*!< Sub-mode selected */
    emios_clock_internal_ps_t       internalPrescaler;        /*!< Internal prescaler, pre-scale channel clock by internalPrescaler +1 */
    bool                            internalPrescalerEn;      /*!< Internal prescaler Enable */
    emios_pulse_polarity_mode_t     outputActiveMode;         /*!< Output active value, Choose active low or high level */
    uint32_t                        leadingEdgePlacement;     /*!< Leading edge placement */
    uint32_t                        trailingEdgePlacement;    /*!< Trailing edge placement */
    emios_bus_select_t              timebase;                 /*!< Counter bus selected */
    uint32_t                        triggerEventPlacement;    /*!< Trigger Event placement */
} emios_opwmt_param_t;

/*******************************************************************************
 * Private API declaration
 ******************************************************************************/
/*!
 * brief Initialize Output Pulse Width and Frequency Modulation Buffered (OPWFMB) Mode
 *
 * param[in] emiosGroup The eMIOS group id
 * param[in] channel The channel in this eMIOS group
 * param[in] opwfmParam A pointer to the OPWFM configuration structure
 * return operation status
 *        - STATUS_SUCCESS  :  Operation was successful.
 *        - STATUS_ERROR    :  Operation failed, invalid input value.
 */
static status_t EMIOS_DRV_PWM_InitPeriodDutyCycleMode(uint8_t emiosGroup,
                                                      uint8_t channel,
                                                      emios_opwfm_param_t *opwfmParam);

/*!
 * brief Initialize Output Pulse Width Modulation Buffered (OPWMB) Mode
 *
 * param[in] emiosGroup The eMIOS group id
 * param[in] channel The channel in this eMIOS group
 * param[in] opwmbParam A pointer to the OPWMB configuration structure
 * return operation status
 *        - STATUS_SUCCESS  :  Operation was successful.
 *        - STATUS_ERROR    :  Operation failed, invalid input value.
 */
static status_t EMIOS_DRV_PWM_InitEdgePlacementMode(uint8_t emiosGroup,
                                                    uint8_t channel,
                                                    emios_opwmb_param_t *opwmbParam);

 /*!
 * brief Initialize Center Aligned Output Pulse Width Modulation with Dead Time Insertion Buffered (OPWMCB) Mode
 *
 * param[in] emiosGroup The eMIOS group id
 * param[in] channel The channel in this eMIOS group
 * param[in] opwmcbParam A pointer to the OPWMCB configuration structure.
 * return operation status
 *        - STATUS_SUCCESS  :  Operation was successful.
 *        - STATUS_ERROR    :  Operation failed, invalid input value.
 */
static status_t EMIOS_DRV_PWM_InitCenterAlignDeadTimeMode(uint8_t emiosGroup,
                                                          uint8_t channel,
                                                          emios_opwmcb_param_t *opwmcbParam);

 /*!
 * brief Initialize Output Pulse Width Modulation with Trigger (OPWMT) Mode
 *
 * param[in] emiosGroup The eMIOS group id
 * param[in] channel The channel in this eMIOS group
 * param[in] opwmtParam A pointer to the OPWMT configuration structure
 * return operation status
 *        - STATUS_SUCCESS  :  Operation was successful.
 *        - STATUS_ERROR    :  Operation failed, invalid input value.
 */
static status_t EMIOS_DRV_PWM_InitTriggerMode(uint8_t emiosGroup,
                                              uint8_t channel,
                                              emios_opwmt_param_t *opwmtParam);

/******************************************************************************
* API
******************************************************************************/

/*FUNCTION**********************************************************************
 *
 * Function Name : EMIOS_DRV_PWM_InitMode
 * Description   : Initialize PWM mode.
 * Select main mode
 *     - EMIOS OPWFMB
 *       Initialize Output Pulse Width and Frequency Modulation Buffered (OPWFMB) Mode.
 *       This mode provides waveforms with variable duty cycle and frequency. The internal channel counter
 *       is automatically selected as the time base when this mode is selected.
 *
 *     - EMIOS OPWMB
 *       Initialize Output Pulse Width Modulation Buffered (OPWMB) Mode.
 *       OPWMB mode is used to generate pulses with programmable leading and trailing edge placement.
 *       An external counter driven in MCB Up mode must be selected from one of the counter buses.
 *       opwmbParam defines the first edge and the second edge. The output signal polarity is defined
 *       by outputActiveMode in opwmbParam.
 *
 *     - EMIOS OPWMCB
 *       Initialize Center Aligned Output Pulse Width Modulation with Dead Time Insertion Buffered (OPWMCB) Mode.
 *       This operation mode generates a center aligned PWM with dead time insertion to the leading or trailing edge.
 *       Allow smooth output signal generation when changing duty cycle and deadtime values.
 *
 *     - EMIOS OPWMT
 *       Initialize Output Pulse Width Modulation with Trigger (OPWMT) Mode
 *       OPWMT mode is intended to support the generation of Pulse Width Modulation signals where
 *       the period is not modified while the signal is being output, but where the duty cycle will
 *       be varied and must not create glitches. The mode is intended to be used in conjunction with
 *       other channels executing in the same mode and sharing a common timebase. It will support each
 *       channel with a fixed PWM leading edge position with respect to the other channels and the
 *       ability to generate a trigger signal at any point in the period that can be output from
 *       the module to initiate activity in other parts of the SoC such as starting ADC conversions.
 * Implements    : EMIOS_DRV_PWM_InitMode_Activity
 *END**************************************************************************/
status_t EMIOS_DRV_PWM_InitMode(uint8_t emiosGroup,
                                uint8_t channel,
                                emios_pwm_param_t *pwmParam)
{
    status_t ret       = STATUS_ERROR;
    uint8_t busSelect  = (uint8_t)EMIOS_CNT_BUSA_DRIVEN;

    DEV_ASSERT(emiosGroup < EMIOS_NUMBER_GROUP_MAX);
    DEV_ASSERT(channel < EMIOS_NUMBER_CHANNEL_MAX);
    DEV_ASSERT(pwmParam != NULL);

    /* Check that device was initialized */
    DEV_ASSERT(eMIOS[emiosGroup]->UC[channel].C == 0UL);

    EMIOS_DRV_EnableAChOutputUpdate(emiosGroup, channel);

    if ((pwmParam->mode == EMIOS_MODE_OPWFMB_FLAGX1) || \
        (pwmParam->mode == EMIOS_MODE_OPWFMB_FLAGX2))
    {
        emios_opwfm_param_t opwfm_param;
        opwfm_param.mode                = pwmParam->mode;
        opwfm_param.dutyCycleCount      = pwmParam->dutyCycleCount;
        opwfm_param.internalPrescaler   = pwmParam->internalPrescaler;
        opwfm_param.internalPrescalerEn = pwmParam->internalPrescalerEn;
        opwfm_param.outputActiveMode    = pwmParam->outputActiveMode;
        opwfm_param.periodCount         = pwmParam->periodCount;

        ret = EMIOS_DRV_PWM_InitPeriodDutyCycleMode(emiosGroup, channel, &opwfm_param);
    }
    else if (((uint8_t)pwmParam->mode & EMIOS_FILTER_OPWMCB) == EMIOS_MASK_OPWMCB)
    {
        emios_opwmcb_param_t opwmcb_param;
        opwmcb_param.mode                = pwmParam->mode;
        opwmcb_param.deadTime            = pwmParam->deadTime;
        opwmcb_param.idealDutyCycle      = pwmParam->idealDutyCycle;
        opwmcb_param.internalPrescaler   = pwmParam->internalPrescaler;
        opwmcb_param.internalPrescalerEn = pwmParam->internalPrescalerEn;
        opwmcb_param.outputActiveMode    = pwmParam->outputActiveMode;
        opwmcb_param.timebase            = pwmParam->timebase;

        ret = EMIOS_DRV_PWM_InitCenterAlignDeadTimeMode(emiosGroup, channel, &opwmcb_param);
    } /* End of OPWMCB mode */
    else if ((pwmParam->mode == EMIOS_MODE_OPWMB_FLAGX1) || \
             (pwmParam->mode == EMIOS_MODE_OPWMB_FLAGX2))
    {
        emios_opwmb_param_t opwmb_param;

        opwmb_param.mode                  = pwmParam->mode;
        opwmb_param.internalPrescaler     = pwmParam->internalPrescaler;
        opwmb_param.internalPrescalerEn   = pwmParam->internalPrescalerEn;
        opwmb_param.leadingEdgePlacement  = 0UL;
        opwmb_param.trailingEdgePlacement = 0UL;

        if (pwmParam->timebase == EMIOS_BUS_SEL_A)
        {
            busSelect = (uint8_t)EMIOS_CNT_BUSA_DRIVEN;
        }
        else if (pwmParam->timebase == EMIOS_BUS_SEL_F)
        {
            busSelect = (uint8_t)EMIOS_CNT_BUSF_DRIVEN;
        }
        else if (pwmParam->timebase == EMIOS_BUS_SEL_BCDE)
        {
            busSelect = (uint8_t)(channel & 0xF8U);
        }
        else
        {
            /* unknown timebase value */
            DEV_ASSERT(false);
        }

        if (pwmParam->outputActiveMode == EMIOS_POSITIVE_PULSE)
        {
            opwmb_param.trailingEdgePlacement = pwmParam->dutyCycleCount;
        }
        else
        {
            if (EMIOS_DRV_MC_GetCounterPeriod(emiosGroup, busSelect) >= pwmParam->dutyCycleCount)
            {
                opwmb_param.trailingEdgePlacement = EMIOS_DRV_MC_GetCounterPeriod(emiosGroup, busSelect) - pwmParam->dutyCycleCount;
            }
            else
            {
                /* duty cycle count should not be greater than period of timebase  */
                DEV_ASSERT(false);
            }
        }

        /* If MCB counter mode, this timebase count from 1 */
        if (((uint8_t)EMIOS_GetUCRegCMode(emiosGroup, busSelect) & EMIOS_FILTER_MCB) == EMIOS_MASK_MCB_UP)
        {
            opwmb_param.leadingEdgePlacement = 1UL;
            opwmb_param.trailingEdgePlacement += 1UL;
        }

        opwmb_param.timebase              = pwmParam->timebase;
        opwmb_param.outputActiveMode      = pwmParam->outputActiveMode;

        ret = EMIOS_DRV_PWM_InitEdgePlacementMode(emiosGroup, channel, &opwmb_param);
    }
    else if (pwmParam->mode == EMIOS_MODE_OPWMT)
    {
        emios_opwmt_param_t opwmt_param;

        opwmt_param.mode                  = pwmParam->mode;
        opwmt_param.internalPrescaler     = pwmParam->internalPrescaler;
        opwmt_param.internalPrescalerEn   = pwmParam->internalPrescalerEn;
        opwmt_param.leadingEdgePlacement  = 0UL;
        opwmt_param.trailingEdgePlacement  = 0UL;

        if (pwmParam->timebase == EMIOS_BUS_SEL_A)
        {
            busSelect = (uint8_t)EMIOS_CNT_BUSA_DRIVEN;
        }
        else if (pwmParam->timebase == EMIOS_BUS_SEL_F)
        {
            busSelect = (uint8_t)EMIOS_CNT_BUSF_DRIVEN;
        }
        else if (pwmParam->timebase == EMIOS_BUS_SEL_BCDE)
        {
            busSelect = (uint8_t)(channel & 0xF8U);
        }
        else
        {
            /* unknown timebase value */
            DEV_ASSERT(false);
        }

        if (pwmParam->outputActiveMode == EMIOS_POSITIVE_PULSE)
        {
            opwmt_param.trailingEdgePlacement = pwmParam->dutyCycleCount;
        }
        else
        {
            if (EMIOS_DRV_MC_GetCounterPeriod(emiosGroup, busSelect) >= pwmParam->dutyCycleCount)
            {
                opwmt_param.trailingEdgePlacement = EMIOS_DRV_MC_GetCounterPeriod(emiosGroup, busSelect) - pwmParam->dutyCycleCount;
            }
            else
            {
                /* duty cycle count should not be greater than period of timebase  */
                DEV_ASSERT(false);
            }
        }

        /* If MCB counter mode, this timebase count from 1 */
        if (((uint8_t)EMIOS_GetUCRegCMode(emiosGroup, busSelect) & EMIOS_FILTER_MCB) == EMIOS_MASK_MCB_UP)
        {
            opwmt_param.leadingEdgePlacement = 1UL;
            opwmt_param.trailingEdgePlacement += 1UL;
        }

        opwmt_param.outputActiveMode      = pwmParam->outputActiveMode;
        opwmt_param.timebase              = pwmParam->timebase;
        opwmt_param.triggerEventPlacement = pwmParam->triggerEventPlacement;

        ret = EMIOS_DRV_PWM_InitTriggerMode(emiosGroup, channel, &opwmt_param);
    }
    else
    {
        /* Main mode selected is wrong */
        DEV_ASSERT(false);
    }
    return ret;
}

/*FUNCTION**********************************************************************
 *
 * Function Name : EMIOS_DRV_PWM_InitPeriodDutyCycleMode
 * Description   : Initial Output Pulse Width and Frequency Modulation Buffered (OPWFMB) Mode.
 * This mode provides waveforms with variable duty cycle and frequency. The internal channel counter
 * is automatically selected as the time base when this mode is selected.
 *END**************************************************************************/
static status_t EMIOS_DRV_PWM_InitPeriodDutyCycleMode(uint8_t emiosGroup,
                                                      uint8_t channel,
                                                      emios_opwfm_param_t *opwfmParam)
{
    DEV_ASSERT(emiosGroup < EMIOS_NUMBER_GROUP_MAX);
    DEV_ASSERT(channel < EMIOS_NUMBER_CHANNEL_MAX);
    DEV_ASSERT(opwfmParam != NULL);
    DEV_ASSERT((opwfmParam->mode == EMIOS_MODE_OPWFMB_FLAGX1) || \
               (opwfmParam->mode == EMIOS_MODE_OPWFMB_FLAGX2));

    /* Validate opwfm parametter */
    DEV_ASSERT((opwfmParam->periodCount <= EMIOS_OPWFMB_MAX_CNT_VAL) && \
               (opwfmParam->periodCount > EMIOS_OPWFMB_MIN_CNT_VAL));

    DEV_ASSERT(opwfmParam->dutyCycleCount <= opwfmParam->periodCount);

    /* Valid Opwfmb with channels supported */
    if (EMIOS_ValidateMode(emiosGroup, channel, (uint8_t)EMIOS_GMODE_OPWFMB) == false)
    {
         DEV_ASSERT(false);
    }

    /* Configure registers */
    eMIOS[emiosGroup]->UC[channel].C = 0UL; /* Disable channel pre-scaler (reset default) */
    if ((uint8_t)opwfmParam->outputActiveMode == (uint8_t)EMIOS_NEGATIVE_PULSE)
    {
        EMIOS_SetUCRegA(emiosGroup, channel, opwfmParam->dutyCycleCount);
    }
    else
    {
        EMIOS_SetUCRegA(emiosGroup, channel, opwfmParam->periodCount - opwfmParam->dutyCycleCount);
    }

    EMIOS_SetUCRegB(emiosGroup, channel, opwfmParam->periodCount);
    EMIOS_SetUCRegCEdpol(emiosGroup, channel, (uint32_t)opwfmParam->outputActiveMode);
    EMIOS_SetUCRegCMode(emiosGroup, channel, (uint32_t)opwfmParam->mode);
    EMIOS_SetUCRegC2UCEXTPRE(emiosGroup, channel, (uint32_t)opwfmParam->internalPrescaler);  /* Pre-scale channel clock by internalPrescaler +1 */
    EMIOS_SetUCRegC2UCPRECLK(emiosGroup, channel, 0UL);                                      /* Prescaler clock selected*/
    EMIOS_SetUCRegCUcpren(emiosGroup, channel, opwfmParam->internalPrescalerEn ? 1UL: 0UL);

    return STATUS_SUCCESS;
}

/*FUNCTION**********************************************************************
 *
 * Function Name : EMIOS_DRV_PWM_InitEdgePlacementMode
 * Description   : Initial Output Pulse Width Modulation Buffered (OPWMB) Mode.
 * OPWMB mode is used to generate pulses with programmable leading and trailing edge placement.
 * An external counter driven in MCB Up mode must be selected from one of the counter buses.
 * opwmbParam defines the first edge and the second edge. The output signal polarity is defined
 * by outputActiveMode in opwmbParam.
 *
 *END**************************************************************************/
static status_t EMIOS_DRV_PWM_InitEdgePlacementMode(uint8_t emiosGroup,
                                                    uint8_t channel,
                                                    emios_opwmb_param_t *opwmbParam)
{
    status_t ret = STATUS_ERROR;

    DEV_ASSERT(emiosGroup < EMIOS_NUMBER_GROUP_MAX);
    DEV_ASSERT(channel < EMIOS_NUMBER_CHANNEL_MAX);
    DEV_ASSERT(opwmbParam != NULL);
    DEV_ASSERT((opwmbParam->mode == EMIOS_MODE_OPWMB_FLAGX1) || \
               (opwmbParam->mode == EMIOS_MODE_OPWMB_FLAGX2));

    /* Validate timebase: must be external counter, MCB Up mode */
    if (opwmbParam->timebase == EMIOS_BUS_SEL_A)
    {
        if (((uint8_t)EMIOS_GetUCRegCMode(emiosGroup, (uint8_t)EMIOS_CNT_BUSA_DRIVEN) & EMIOS_FILTER_MCB) != EMIOS_MASK_MCB_UP)
        {
            DEV_ASSERT(false);
        }
    }
    else if (opwmbParam->timebase == EMIOS_BUS_SEL_F)
    {
        if (((uint8_t)EMIOS_GetUCRegCMode(emiosGroup, (uint8_t)EMIOS_CNT_BUSF_DRIVEN) & EMIOS_FILTER_MCB) != EMIOS_MASK_MCB_UP)
        {
            DEV_ASSERT(false);
        }
    }
    else if (opwmbParam->timebase == EMIOS_BUS_SEL_BCDE)
    {
        if (((uint8_t)EMIOS_GetUCRegCMode(emiosGroup, (uint8_t)(channel & 0xF8U)) & EMIOS_FILTER_MCB) != EMIOS_MASK_MCB_UP)
        {
            DEV_ASSERT(false);
        }
    }
    else
    {
        /* Wrong counter bus : choosing internal counter bus ? */
        ret = STATUS_ERROR;
    }

    /* Validate opwfm parametter */
    DEV_ASSERT(opwmbParam->leadingEdgePlacement <= opwmbParam->trailingEdgePlacement);
    DEV_ASSERT(opwmbParam->trailingEdgePlacement <= EMIOS_DATA_REG_MAX_VAL);

    /* Configure registers */
    eMIOS[emiosGroup]->UC[channel].C = 0UL;                                        /* Disable channel pre-scaler (reset default) */
    EMIOS_SetUCRegA(emiosGroup, channel, opwmbParam->leadingEdgePlacement);
    EMIOS_SetUCRegB(emiosGroup, channel, opwmbParam->trailingEdgePlacement);

    if (opwmbParam->timebase == EMIOS_BUS_SEL_INTERNAL)
    {
        /* Choose internal counter, this channel mode should be Type X or G */
        DEV_ASSERT(EMIOS_ValidateInternalCnt(emiosGroup, channel));
    }
    else
    {
        /* Choose external counter */
    }

    EMIOS_SetUCRegCBsl(emiosGroup, channel, (uint32_t)opwmbParam->timebase);
    EMIOS_SetUCRegCEdpol(emiosGroup, channel, (uint32_t)opwmbParam->outputActiveMode);
    EMIOS_SetUCRegCMode(emiosGroup, channel, (uint32_t)opwmbParam->mode);
    EMIOS_SetUCRegC2UCEXTPRE(emiosGroup, channel, (uint32_t)opwmbParam->internalPrescaler);  /* Pre-scale channel clock by internalPrescaler +1 */
    EMIOS_SetUCRegC2UCPRECLK(emiosGroup, channel, 0UL);                                      /* Prescaler clock selected */
    EMIOS_SetUCRegCUcpren(emiosGroup, channel, opwmbParam->internalPrescalerEn ? 1UL : 0UL);
    ret = STATUS_SUCCESS;

    return ret;
}

/*FUNCTION**********************************************************************
 *
 * Function Name : EMIOS_DRV_PWM_InitCenterAlignDeadTimeMode
 * Description   : Initial Center Aligned Output Pulse Width Modulation with Dead Time Insertion Buffered (OPWMCB) Mode.
 * This operation mode generates a center aligned PWM with dead time insertion to the leading or trailing edge.
 * Allow smooth output signal generation when changing duty cycle and deadtime values.
 *
 * The time base selected for a channel configured to OPWMCB mode should be a channel configured to MCB Up/Down mode.
 * It is recommended to start the MCB channel time base after the OPWMCB mode is entered
 * in order to avoid missing A matches at the very first duty cycle.
 *
 * The internal counter runs in the internal prescaler ratio, while the selected time base
 * may be running in a different prescaler ratio.
 *
 *END**************************************************************************/
static status_t EMIOS_DRV_PWM_InitCenterAlignDeadTimeMode(uint8_t emiosGroup,
                                                          uint8_t channel,
                                                          emios_opwmcb_param_t *opwmcbParam)
{
    DEV_ASSERT(emiosGroup < EMIOS_NUMBER_GROUP_MAX);
    DEV_ASSERT(channel < EMIOS_NUMBER_CHANNEL_MAX);
    DEV_ASSERT(opwmcbParam != NULL);
    DEV_ASSERT(((uint8_t)opwmcbParam->mode & EMIOS_FILTER_OPWMCB) == EMIOS_MASK_OPWMCB);

    /* Validate opwfm parametter */
    DEV_ASSERT((opwmcbParam->idealDutyCycle <= EMIOS_OPWMCB_MAX_CNT_VAL) && \
               (opwmcbParam->idealDutyCycle >= EMIOS_OPWMCB_MIN_CNT_VAL));
    DEV_ASSERT(opwmcbParam->deadTime <= EMIOS_OPWMCB_MAX_CNT_VAL);

    /* Validate timebase: must be external counter, MCB Up mode */
    if (opwmcbParam->timebase == EMIOS_BUS_SEL_A)
    {
        if (((uint8_t)EMIOS_GetUCRegCMode(emiosGroup, (uint8_t)EMIOS_CNT_BUSA_DRIVEN) & EMIOS_FILTER_MCB) != EMIOS_MASK_MCB_UPDOWN)
        {
            /* Wrong counter bus */
            DEV_ASSERT(false);
        }
    }
    else if (opwmcbParam->timebase == EMIOS_BUS_SEL_F)
    {
        if (((uint8_t)EMIOS_GetUCRegCMode(emiosGroup, (uint8_t)EMIOS_CNT_BUSF_DRIVEN) & EMIOS_FILTER_MCB) != EMIOS_MASK_MCB_UPDOWN)
        {
            /* Wrong counter bus */
            DEV_ASSERT(false);
        }
    }
    else if (opwmcbParam->timebase == EMIOS_BUS_SEL_BCDE)
    {
        if (((uint8_t)EMIOS_GetUCRegCMode(emiosGroup, (uint8_t)(channel & 0xF8U)) & EMIOS_FILTER_MCB) != EMIOS_MASK_MCB_UPDOWN)
        {
            /* Wrong counter bus */
            DEV_ASSERT(false);
        }
    }
    else
    {
        /* Wrong counter bus : choosing internal counter bus ? */
        DEV_ASSERT(false);
    }

    /* Valid Opwmcb with channels supported */
    if (EMIOS_ValidateMode(emiosGroup, channel, (uint8_t)EMIOS_GMODE_OPWMCB) == false)
    {
         DEV_ASSERT(false);
    }

    /* Configure registers */
    eMIOS[emiosGroup]->UC[channel].C = 0UL;                                        /* Disable channel pre-scaler (reset default) */
    EMIOS_SetUCRegA(emiosGroup, channel, opwmcbParam->idealDutyCycle);
    EMIOS_SetUCRegB(emiosGroup, channel, opwmcbParam->deadTime);
    EMIOS_SetUCRegCBsl(emiosGroup, channel, (uint32_t)opwmcbParam->timebase);
    EMIOS_SetUCRegCEdpol(emiosGroup, channel, (uint32_t)opwmcbParam->outputActiveMode);
    EMIOS_SetUCRegCMode(emiosGroup, channel, (uint32_t)opwmcbParam->mode);
    EMIOS_SetUCRegC2UCEXTPRE(emiosGroup, channel, (uint32_t)opwmcbParam->internalPrescaler); /* Pre-scale channel clock by internalPrescaler +1 */
    EMIOS_SetUCRegC2UCPRECLK(emiosGroup, channel, 0UL);                                      /* Prescaler clock selected */
    EMIOS_SetUCRegCUcpren(emiosGroup, channel, opwmcbParam->internalPrescalerEn ? 1UL: 0UL);

    return STATUS_SUCCESS;
}

/*FUNCTION**********************************************************************
 *
 * Function Name : EMIOS_DRV_PWM_InitTriggerMode
 * Description   : Initial Output Pulse Width Modulation with Trigger (OPWMT) Mode
 * OPWMT mode is intended to support the generation of Pulse Width Modulation signals where
 * the period is not modified while the signal is being output, but where the duty cycle will
 * be varied and must not create glitches. The mode is intended to be used in conjunction with
 * other channels executing in the same mode and sharing a common timebase. It will support each
 * channel with a fixed PWM leading edge position with respect to the other channels and the
 * ability to generate a trigger signal at any point in the period that can be output from
 * the module to initiate activity in other parts of the SoC such as starting ADC conversions.
 *
 * An external counter driven in either MC Up or MCB Up mode must be selected from one of the counter buses.
 *
 * The leading edge can be configured with any value within the range of the selected time base. Note that registers
 * loaded with 0x0 will not produce matches if the timebase is driven by a channel in MCB mode.
 *
 *END**************************************************************************/
 static status_t EMIOS_DRV_PWM_InitTriggerMode(uint8_t emiosGroup,
                                               uint8_t channel,
                                               emios_opwmt_param_t *opwmtParam)
{
    uint8_t selCounterBus = (uint8_t)EMIOS_CNT_BUSA_DRIVEN;
    uint8_t temp = 0U;

    DEV_ASSERT(emiosGroup < EMIOS_NUMBER_GROUP_MAX);
    DEV_ASSERT(channel < EMIOS_NUMBER_CHANNEL_MAX);
    DEV_ASSERT(opwmtParam != NULL);
    DEV_ASSERT(opwmtParam->mode == EMIOS_MODE_OPWMT);

    /* Validate timebase: must be external counter, MC or MCB Up mode */
    if (opwmtParam->timebase == EMIOS_BUS_SEL_A)
    {
        selCounterBus = (uint8_t)EMIOS_CNT_BUSA_DRIVEN;
    }
    else if (opwmtParam->timebase == EMIOS_BUS_SEL_F)
    {
        selCounterBus = (uint8_t)EMIOS_CNT_BUSF_DRIVEN;
    }
    else if (opwmtParam->timebase == EMIOS_BUS_SEL_BCDE)
    {
        selCounterBus = (uint8_t)(channel & 0xF8U);
    }
    else
    {
        /* Wrong counter bus : choosing internal counter bus ? */
        DEV_ASSERT(false);
    }

    temp = (uint8_t)EMIOS_GetUCRegCMode(emiosGroup, selCounterBus);

    /* Validate opwmt parametter */
    if (((temp & EMIOS_FILTER_MC) != EMIOS_MASK_MC_UP) && \
        ((temp & EMIOS_FILTER_MCB) != EMIOS_MASK_MCB_UP))
    {
        DEV_ASSERT(false);
    }
    else if ((temp & EMIOS_FILTER_MCB) == EMIOS_MASK_MCB_UP)
    {
        DEV_ASSERT(opwmtParam->leadingEdgePlacement >= EMIOS_MCB_MIN_CNT_VAL);
        DEV_ASSERT(opwmtParam->trailingEdgePlacement >= EMIOS_MCB_MIN_CNT_VAL);
    }
    else
    {
        /* Do nothing */
    }

    DEV_ASSERT((opwmtParam->trailingEdgePlacement >= opwmtParam->leadingEdgePlacement) && \
               (opwmtParam->trailingEdgePlacement <= (uint32_t)EMIOS_DATA_REG_MAX_VAL));

    DEV_ASSERT(opwmtParam->triggerEventPlacement <= EMIOS_DRV_MC_GetCounterPeriod(emiosGroup, selCounterBus));

    /* Configure registers */
    eMIOS[emiosGroup]->UC[channel].C = 0UL;                                         /* Disable channel pre-scaler (reset default) */
    EMIOS_SetUCRegA(emiosGroup, channel, opwmtParam->leadingEdgePlacement);
    EMIOS_SetUCRegB(emiosGroup, channel, opwmtParam->trailingEdgePlacement);
    EMIOS_SetUCRegALTA(emiosGroup, channel, opwmtParam->triggerEventPlacement);

    if (opwmtParam->timebase == EMIOS_BUS_SEL_INTERNAL)
    {
        /* Choose internal counter, this channel mode should be Type X or G */
        DEV_ASSERT(EMIOS_ValidateInternalCnt(emiosGroup, channel));
    }
    else
    {
        /* Choose external counter */
    }

    EMIOS_SetUCRegCBsl(emiosGroup, channel, (uint32_t)opwmtParam->timebase);
    EMIOS_SetUCRegCEdpol(emiosGroup, channel, (uint32_t)opwmtParam->outputActiveMode);
    EMIOS_SetUCRegCMode(emiosGroup, channel, (uint32_t)opwmtParam->mode);
    EMIOS_SetUCRegC2UCEXTPRE(emiosGroup, channel, (uint32_t)opwmtParam->internalPrescaler);   /* Pre-scale channel clock by internalPrescaler +1 */
    EMIOS_SetUCRegC2UCPRECLK(emiosGroup, channel, 0UL);                                       /* Prescaler clock selected */
    EMIOS_SetUCRegCUcpren(emiosGroup, channel, opwmtParam->internalPrescalerEn ? 1UL: 0UL);

    return STATUS_SUCCESS;
}

/*FUNCTION**********************************************************************
 *
 * Function Name : EMIOS_DRV_PWM_ForcePWMMatchLeadingEdge
 * Description   : Force the output flipflop to the level corresponding to a match on Leading edge
 *
 * In Center Aligned Output Pulse Width Modulation with Dead Time Insertion Buffered (OPWMCB) Mode
 *      FORCMA has different behaviors depending upon the selected dead time
 *      insertion mode, lead or trail. In lead dead time insertion FORCMA force a transition
 *      in the output flipflop to the opposite of EDPOL. In trail dead time insertion the
 *      output flip-flop is forced to the value of EDPOL bit.
 *      FORCMA bit set does not set the internal time-base to 0x1 as a regular A1 match.
 * Implements    : EMIOS_DRV_PWM_ForcePWMMatchLeadingEdge_Activity
 *END**************************************************************************/
 void EMIOS_DRV_PWM_ForcePWMMatchLeadingEdge(uint8_t emiosGroup,
                                             uint8_t channel)
 {
     uint32_t temp = 0UL;

    DEV_ASSERT(emiosGroup < EMIOS_NUMBER_GROUP_MAX);
    DEV_ASSERT(channel < EMIOS_NUMBER_CHANNEL_MAX);

    temp = EMIOS_GetUCRegCMode(emiosGroup, channel);

    DEV_ASSERT((temp == (uint32_t)EMIOS_MODE_OPWMT) || \
              ((temp & (uint32_t)EMIOS_FILTER_OPWMCB) == (uint32_t)EMIOS_MASK_OPWMCB) || \
               (temp == (uint32_t)EMIOS_MODE_OPWMB_FLAGX1) || \
               (temp == (uint32_t)EMIOS_MODE_OPWMB_FLAGX2) || \
               (temp == (uint32_t)EMIOS_MODE_OPWFMB_FLAGX1) || \
               (temp == (uint32_t)EMIOS_MODE_OPWFMB_FLAGX2));
    (void) temp;

    EMIOS_SetUCRegCForcma(emiosGroup, channel, 1UL);
 }

/*FUNCTION**********************************************************************
 *
 * Function Name : EMIOS_DRV_PWM_ForcePWMMatchTrailingEdge
 * Description   : Force the output flipflop to the level corresponding to a match on Trailing edge
 *
 * In Center Aligned Output Pulse Width Modulation with Dead Time Insertion Buffered (OPWMCB) Mode
 *       If FORCMB bit is set, the output flip-flop value depends upon the selected dead time
 *       insertion mode. In lead dead time insertion FORCMB forces the output flip-flop to transition to EDPOL
 *       bit value. In trail dead time insertion the output flip-flop is forced to the opposite of EDPOL bit value.
 * Implements    : EMIOS_DRV_PWM_ForcePWMMatchTrailingEdge_Activity
 *END**************************************************************************/
 void EMIOS_DRV_PWM_ForcePWMMatchTrailingEdge(uint8_t emiosGroup,
                                              uint8_t channel)
 {
    uint32_t temp = 0UL;

    DEV_ASSERT(emiosGroup < EMIOS_NUMBER_GROUP_MAX);
    DEV_ASSERT(channel < EMIOS_NUMBER_CHANNEL_MAX);

    temp = EMIOS_GetUCRegCMode(emiosGroup, channel);

    DEV_ASSERT((temp == (uint32_t)EMIOS_MODE_OPWMT) || \
              (((uint8_t)temp & EMIOS_FILTER_OPWMCB) == EMIOS_MASK_OPWMCB) || \
               (temp == (uint32_t)EMIOS_MODE_OPWMB_FLAGX1) || \
               (temp == (uint32_t)EMIOS_MODE_OPWMB_FLAGX2) || \
               (temp == (uint32_t)EMIOS_MODE_OPWFMB_FLAGX1) || \
               (temp == (uint32_t)EMIOS_MODE_OPWFMB_FLAGX2));
    (void) temp;

    EMIOS_SetUCRegCForcmb(emiosGroup, channel, 1UL);
 }

/*FUNCTION**********************************************************************
 *
 * Function Name : EMIOS_DRV_PWM_GetPeriod
 * Description   : Get period of waveforms in PWM mode.
 * Implements    : EMIOS_DRV_PWM_GetPeriod_Activity
 *END**************************************************************************/
 uint32_t EMIOS_DRV_PWM_GetPeriod(uint8_t emiosGroup,
                                  uint8_t channel)
 {
    uint32_t ret = 0UL;
    uint32_t temp = 0UL;

    DEV_ASSERT(emiosGroup < EMIOS_NUMBER_GROUP_MAX);
    DEV_ASSERT(channel < EMIOS_NUMBER_CHANNEL_MAX);

    temp = EMIOS_GetUCRegCMode(emiosGroup, channel);

    if ((temp == (uint32_t)EMIOS_MODE_OPWFMB_FLAGX1) || \
        (temp == (uint32_t)EMIOS_MODE_OPWFMB_FLAGX2))          /* OPWFMB mode */
    {
        ret = EMIOS_GetUCRegB(emiosGroup, channel);
    }
    else if ((((uint8_t)temp & EMIOS_FILTER_OPWMCB) == EMIOS_MASK_OPWMCB) || \
             (temp == (uint32_t)EMIOS_MODE_OPWMB_FLAGX1) || \
             (temp == (uint32_t)EMIOS_MODE_OPWMB_FLAGX2) || \
             (temp == (uint32_t)EMIOS_MODE_OPWMT)) /* OPWMCB mode or OPWMB or OPWMT mode */
    {
        if (EMIOS_GetUCRegCBsl(emiosGroup, channel) == (uint32_t)EMIOS_BUS_SEL_A)
        {
            ret = EMIOS_DRV_MC_GetCounterPeriod(emiosGroup, (uint8_t)EMIOS_CNT_BUSA_DRIVEN);
        }
        else if (EMIOS_GetUCRegCBsl(emiosGroup, channel) == (uint32_t)EMIOS_BUS_SEL_F)
        {
            ret = EMIOS_DRV_MC_GetCounterPeriod(emiosGroup, (uint8_t)EMIOS_CNT_BUSF_DRIVEN);
        }
        else if (EMIOS_GetUCRegCBsl(emiosGroup, channel) == (uint32_t)EMIOS_BUS_SEL_BCDE)
        {
            ret = EMIOS_DRV_MC_GetCounterPeriod(emiosGroup, (uint8_t)(channel & 0xF8U));
        }
        else
        {
            DEV_ASSERT(false);
        }
    }
    else
    {
        DEV_ASSERT(false);
    }

    return ret;
 }

/*FUNCTION**********************************************************************
 *
 * Function Name : EMIOS_DRV_PWM_SetPeriod
 * Description   : Setup period of waveforms in OPWFMB mode.
 * With PWM mode, the OPWFMB(Output Pulse Width and Frequency Modulation Buffered) can set
 * period. All other PWM modes can not, because OPWFMB mode using internal counter. All other
 * modes use external timebase and their period is timebase period.
 * Implements    : EMIOS_DRV_PWM_SetPeriod_Activity
 *END**************************************************************************/
 void EMIOS_DRV_PWM_SetPeriod(uint8_t emiosGroup,
                              uint8_t channel,
                              uint32_t newPeriod)
 {
     uint32_t temp = 0UL;

    DEV_ASSERT(emiosGroup < EMIOS_NUMBER_GROUP_MAX);
    DEV_ASSERT(channel < EMIOS_NUMBER_CHANNEL_MAX);

    temp = EMIOS_GetUCRegCMode(emiosGroup, channel);

    DEV_ASSERT((temp == (uint32_t)EMIOS_MODE_OPWFMB_FLAGX1) || \
               (temp == (uint32_t)EMIOS_MODE_OPWFMB_FLAGX2));
    (void) temp;

    DEV_ASSERT((newPeriod <= EMIOS_OPWFMB_MAX_CNT_VAL) && \
               (newPeriod > EMIOS_OPWFMB_MIN_CNT_VAL));

    EMIOS_SetUCRegB(emiosGroup, channel, newPeriod);
 }

/*FUNCTION**********************************************************************
 *
 * Function Name : EMIOS_DRV_PWM_GetDutyCycle
 * Description   : Get duty cycle of waveforms in PWM mode. Duty cycle is time ON in a period
 * Implements    : EMIOS_DRV_PWM_GetDutyCycle_Activity
 *END**************************************************************************/
 uint32_t EMIOS_DRV_PWM_GetDutyCycle(uint8_t emiosGroup,
                                     uint8_t channel)
 {
    uint32_t ret = 0UL;
    uint8_t busSelect = (uint8_t)EMIOS_CNT_BUSA_DRIVEN;
    uint32_t temp = 0UL;
    uint32_t ARegVal = 0UL;
    uint32_t BRegVal = 0UL;
    uint32_t cntPeriod = 0UL;

    DEV_ASSERT(emiosGroup < EMIOS_NUMBER_GROUP_MAX);
    DEV_ASSERT(channel < EMIOS_NUMBER_CHANNEL_MAX);

    temp = EMIOS_GetUCRegCMode(emiosGroup, channel);
    ARegVal = EMIOS_GetUCRegA(emiosGroup, channel);
    BRegVal = EMIOS_GetUCRegB(emiosGroup, channel);

    if (EMIOS_GetUCRegCBsl(emiosGroup, channel) == (uint32_t)EMIOS_BUS_SEL_A)
    {
        busSelect = (uint8_t)EMIOS_CNT_BUSA_DRIVEN;
    }
    else if (EMIOS_GetUCRegCBsl(emiosGroup, channel) == (uint32_t)EMIOS_BUS_SEL_F)
    {
        busSelect = (uint8_t)EMIOS_CNT_BUSF_DRIVEN;
    }
    else if (EMIOS_GetUCRegCBsl(emiosGroup, channel) == (uint32_t)EMIOS_BUS_SEL_BCDE)
    {
        busSelect = (uint8_t)(channel & 0xF8U);
    }
    else
    {
        if ((temp != (uint32_t)EMIOS_MODE_OPWFMB_FLAGX1) && \
            (temp != (uint32_t)EMIOS_MODE_OPWFMB_FLAGX2)) /* OPWFMB uses internal counter bus only */
        {
            DEV_ASSERT(false);
        }
    }

    if ((temp == (uint32_t)EMIOS_MODE_OPWFMB_FLAGX1) || \
        (temp == (uint32_t)EMIOS_MODE_OPWFMB_FLAGX2))
    {
        if (EMIOS_DRV_IsOutputUpdateDisabled(emiosGroup, channel) == true)
        {
            ret = 0UL; /* 0% duty cycle */
        }
        else if (ARegVal < BRegVal)
        {
            ret = ARegVal;
        }
        else
        {
            /* Duty cycle = period */
            ret = BRegVal; /* 100% duty cycle */
        }

        if (EMIOS_GetUCRegCEdpol(emiosGroup, channel) == 1UL)
        {
            ret = BRegVal - ret;
        }
    }
    else if (((uint8_t)temp & EMIOS_FILTER_OPWMCB) == EMIOS_MASK_OPWMCB)
    {
        if (EMIOS_DRV_IsOutputUpdateDisabled(emiosGroup, channel) == true)
        {
            /* 0% duty cycle, The output disable feature, if enabled, causes the output flip-flop to transition to the
               EDPOL inverted state, RM Page 1036/4083 MPC5748G_RM_Rev5_RC */
            ret = 0UL;
        }
        else if ((temp == (uint32_t)EMIOS_MODE_OPWMCB_LEAD_EDGE_DEADTIME_FLAGX1) || \
                 (temp == (uint32_t)EMIOS_MODE_OPWMCB_LEAD_EDGE_DEADTIME_FLAGX2))
        {
            /* Leading edge deadtime insertion */
            if (ARegVal < EMIOS_GetUCRegA(emiosGroup, busSelect))
            {
                if (((EMIOS_GetUCRegA(emiosGroup, busSelect) - ARegVal) << 1UL) <= BRegVal)
                {
                    ret = 0UL;
                }
                else
                {
                    ret = ((EMIOS_GetUCRegA(emiosGroup, busSelect) - ARegVal) << 1UL) - BRegVal;
                }
            }
            else if (ARegVal == EMIOS_GetUCRegA(emiosGroup, busSelect))
            {
                /* Special case Note RM Page 1035/4083 MPC5748G_RM_Rev5_RC */
                ret = EMIOS_DRV_MC_GetCounterPeriod(emiosGroup, busSelect);
            }
            else
            {
                ret = 0UL;
            }
        }
        else /* Trailing edge deadtime insertion */
        {
            if (ARegVal < EMIOS_GetUCRegA(emiosGroup, busSelect))
            {
                if (ARegVal <= (BRegVal + 1UL))
                {
                    /* 100% duty cycle signals can be generated if B1 occurs at or after the cycle boundary (external counter = 1) */
                    ret = EMIOS_DRV_MC_GetCounterPeriod(emiosGroup, busSelect);
                }
                else
                {
                    ret = ((EMIOS_GetUCRegA(emiosGroup, busSelect) - ARegVal) << 1UL) + BRegVal;

                    if (ret > EMIOS_DRV_MC_GetCounterPeriod(emiosGroup, busSelect))
                    {
                        ret = EMIOS_DRV_MC_GetCounterPeriod(emiosGroup, busSelect);
                    }
                }
            }
            else if (ARegVal == EMIOS_GetUCRegA(emiosGroup, busSelect))
            {
                /* Special case Note RM Page 1035/4083 MPC5748G_RM_Rev5_RC */
                ret = EMIOS_DRV_MC_GetCounterPeriod(emiosGroup, busSelect);
            }
            else
            {
                /* If A1 is greater than the maximum value of the selected counter bus period, then a 0% duty cycle is produced */
                ret = 0UL;
            }
        }

        if (EMIOS_GetUCRegCEdpol(emiosGroup, channel) == 0UL)
        {
            ret = EMIOS_DRV_MC_GetCounterPeriod(emiosGroup, busSelect) - ret;
        }
    } /* End of OPWMCB mode */
    else if ((temp == (uint32_t)EMIOS_MODE_OPWMB_FLAGX1) || \
             (temp == (uint32_t)EMIOS_MODE_OPWMB_FLAGX2))
    {
        cntPeriod = EMIOS_DRV_MC_GetCounterPeriod(emiosGroup, busSelect);

        if (EMIOS_DRV_IsOutputUpdateDisabled(emiosGroup, channel) == true)
        {
            ret = 0UL; /* 0% duty cycle */
        }
        else if ((ARegVal > cntPeriod) || ((ARegVal == cntPeriod) && (BRegVal == cntPeriod)))
        {
            /* Does not match any edge */
            ret = cntPeriod; /* 100% duty cycle */
        }
        else if ((ARegVal <= cntPeriod) && (BRegVal > cntPeriod))
        {
            /* Leading edge matches only */
            ret = 0UL;
        }
        else if (BRegVal == ARegVal)
        {
            /* Trailing edge matches have precedence over leading edge matches */
            ret = cntPeriod; /* 100% duty cycle */
        }
        else
        {
            ret = cntPeriod - (BRegVal - ARegVal);
        }

        if (EMIOS_GetUCRegCEdpol(emiosGroup, channel) == 1UL)
        {
            ret = cntPeriod - ret;
        }
    }
    else if (temp == (uint32_t)EMIOS_MODE_OPWMT)
    {
        /* Edpol = 1 */
        /* If the selected Output Disable input signal is asserted for the channel,
           the output pin will go to the inverse of the EDPOL */
        if (EMIOS_DRV_IsOutputUpdateDisabled(emiosGroup, channel) == true)
        {
            ret = 0UL; /* 0% duty cycle */
        }
        else if (BRegVal > EMIOS_DRV_MC_GetCounterPeriod(emiosGroup, busSelect))
        {
            ret = EMIOS_DRV_MC_GetCounterPeriod(emiosGroup, busSelect); /* 100% duty cycle */
        }
        else if (BRegVal == ARegVal)
        {
            ret = 0UL;
        }
        else
        {
            ret = BRegVal - ARegVal;
        }

        if (EMIOS_GetUCRegCEdpol(emiosGroup, channel) == 0UL)
        {
            ret = EMIOS_DRV_MC_GetCounterPeriod(emiosGroup, busSelect) - ret;
        }
    }
    else
    {
        DEV_ASSERT(false);
    }
    return ret;
 }

/*FUNCTION**********************************************************************
 *
 * Function Name : EMIOS_DRV_PWM_SetDutyCycle
 * Description   : Setup duty cycle of waveforms in OPWFMB mode.
 * Duty cycle should be not greater period value. When set duty cycle value greater period value
 * (and do not over 16 bits counter register) 100% duty cycle signal generated.
 * Implements    : EMIOS_DRV_PWM_SetDutyCycle_Activity
 *END**************************************************************************/
 status_t EMIOS_DRV_PWM_SetDutyCycle(uint8_t emiosGroup,
                                     uint8_t channel,
                                     uint32_t newDutyCycle)
 {
     status_t ret = STATUS_ERROR;
     uint32_t temp = 0UL;

     DEV_ASSERT(emiosGroup < EMIOS_NUMBER_GROUP_MAX);
     DEV_ASSERT(channel < EMIOS_NUMBER_CHANNEL_MAX);

     temp = EMIOS_GetUCRegCMode(emiosGroup, channel);

     DEV_ASSERT((temp == (uint32_t)EMIOS_MODE_OPWFMB_FLAGX1) || \
                (temp == (uint32_t)EMIOS_MODE_OPWFMB_FLAGX2));
    (void) temp;

     DEV_ASSERT(newDutyCycle <= EMIOS_DATA_REG_MAX_VAL);

     if (EMIOS_GetUCRegCEdpol(emiosGroup, channel) == 0UL)
     {
         if (EMIOS_DRV_IsOutputUpdateDisabled(emiosGroup, channel) == true)
         {
             if (newDutyCycle == 0UL)
             {
                 EMIOS_SetUCRegA(emiosGroup, channel, newDutyCycle);
                 ret = STATUS_SUCCESS; /* 0% duty cycle */
             }
             else
             {
                 ret = STATUS_ERROR;
             }
         }
         else if (newDutyCycle < EMIOS_GetUCRegB(emiosGroup, channel))
         {
             EMIOS_SetUCRegA(emiosGroup, channel, newDutyCycle);
             ret = STATUS_SUCCESS;
         }
         else
         {
             /* new duty cycle >= period */
             EMIOS_SetUCRegA(emiosGroup, channel, EMIOS_GetUCRegB(emiosGroup, channel)); /* 100% duty cycle */
             ret = STATUS_SUCCESS;
         }
     }
     else
     {
         if (EMIOS_DRV_IsOutputUpdateDisabled(emiosGroup, channel) == true)
         {
              if (newDutyCycle >= EMIOS_GetUCRegB(emiosGroup, channel))
             {
                 EMIOS_SetUCRegA(emiosGroup, channel,EMIOS_GetUCRegB(emiosGroup, channel));
                 ret = STATUS_SUCCESS; /* 100% duty cycle */
             }
             else
             {
                 ret = STATUS_ERROR;
             }
         }
         else if (newDutyCycle < EMIOS_GetUCRegB(emiosGroup, channel))
         {
             EMIOS_SetUCRegA(emiosGroup, channel, EMIOS_GetUCRegB(emiosGroup, channel) - newDutyCycle);
             ret = STATUS_SUCCESS;
         }
         else
         {
             /* new duty cycle >= period */
             EMIOS_SetUCRegA(emiosGroup, channel, 0UL); /* 100% duty cycle */
             ret = STATUS_SUCCESS;
         }
     }

     return ret;
 }

/*FUNCTION**********************************************************************
 *
 * Function Name : EMIOS_DRV_PWM_GetLeadingEdgePlacement
 * Description   : Get Leading edge position of waveforms in OPWMB & OPWMT mode.
 * Implements    : EMIOS_DRV_PWM_GetLeadingEdgePlacement_Activity
 *END**************************************************************************/
 uint32_t EMIOS_DRV_PWM_GetLeadingEdgePlacement(uint8_t emiosGroup,
                                                uint8_t channel)
 {
    uint32_t temp = 0UL;

    DEV_ASSERT(emiosGroup < EMIOS_NUMBER_GROUP_MAX);
    DEV_ASSERT(channel < EMIOS_NUMBER_CHANNEL_MAX);

    temp = EMIOS_GetUCRegCMode(emiosGroup, channel);

    DEV_ASSERT((temp == (uint32_t)EMIOS_MODE_OPWMB_FLAGX1) || \
               (temp == (uint32_t)EMIOS_MODE_OPWMB_FLAGX2) || \
               (temp == (uint32_t)EMIOS_MODE_OPWMT));
    (void) temp;

    return EMIOS_GetUCRegA(emiosGroup, channel);
 }

/*FUNCTION**********************************************************************
 *
 * Function Name : EMIOS_DRV_PWM_SetLeadingEdgePlacement
 * Description   : Set Leading edge position of waveforms in OPWMB & OPWMT mode.
 * With OPWMB mode when user write leading edge position is 0, then generating a 0 % duty cycle
 * signal (EDPOL = 0) or 100 % duty cycle (EDPOL = 1)
 * Implements    : EMIOS_DRV_PWM_SetLeadingEdgePlacement_Activity
 *END**************************************************************************/
 void EMIOS_DRV_PWM_SetLeadingEdgePlacement(uint8_t emiosGroup,
                                            uint8_t channel,
                                            uint32_t newLeadingEdgePlacement)
 {
    uint32_t temp = 0UL;

    DEV_ASSERT(emiosGroup < EMIOS_NUMBER_GROUP_MAX);
    DEV_ASSERT(channel < EMIOS_NUMBER_CHANNEL_MAX);

    temp = EMIOS_GetUCRegCMode(emiosGroup, channel);

    DEV_ASSERT((temp == (uint32_t)EMIOS_MODE_OPWMB_FLAGX1) || \
               (temp == (uint32_t)EMIOS_MODE_OPWMB_FLAGX2) || \
               (temp == (uint32_t)EMIOS_MODE_OPWMT));

    /* Validate opwmb parametter */
    DEV_ASSERT(newLeadingEdgePlacement  <= EMIOS_GetUCRegB(emiosGroup, channel));

    if (EMIOS_GetUCRegCBsl(emiosGroup, channel) == (uint32_t)EMIOS_BUS_SEL_A)
    {
        if (((uint8_t)EMIOS_GetUCRegCMode(emiosGroup, (uint8_t)EMIOS_CNT_BUSA_DRIVEN) & EMIOS_FILTER_MCB) == EMIOS_MASK_MCB_UP)
        {
            if (temp == (uint32_t)EMIOS_MODE_OPWMT)
            {
                DEV_ASSERT(newLeadingEdgePlacement >= EMIOS_MCB_MIN_CNT_VAL);
            }
        }
    }
    else if (EMIOS_GetUCRegCBsl(emiosGroup, channel) == (uint32_t)EMIOS_BUS_SEL_F)
    {
        if (((uint8_t)EMIOS_GetUCRegCMode(emiosGroup, (uint8_t)EMIOS_CNT_BUSF_DRIVEN) & EMIOS_FILTER_MCB) == EMIOS_MASK_MCB_UP)
        {
            if (temp == (uint32_t)EMIOS_MODE_OPWMT)
            {
                DEV_ASSERT(newLeadingEdgePlacement >= EMIOS_MCB_MIN_CNT_VAL);
            }
        }
    }
    else if (EMIOS_GetUCRegCBsl(emiosGroup, channel) == (uint32_t)EMIOS_BUS_SEL_BCDE)
    {
        if (((uint8_t)EMIOS_GetUCRegCMode(emiosGroup, (uint8_t)(channel & 0xF8U)) & EMIOS_FILTER_MCB) == EMIOS_MASK_MCB_UP)
        {
            if (temp == (uint32_t)EMIOS_MODE_OPWMT)
            {
                DEV_ASSERT(newLeadingEdgePlacement >= EMIOS_MCB_MIN_CNT_VAL);
            }
        }
    }
    else
    {
        DEV_ASSERT(false);
    }

    EMIOS_SetUCRegA(emiosGroup, channel, newLeadingEdgePlacement);
 }

/*FUNCTION**********************************************************************
 *
 * Function Name : EMIOS_DRV_PWM_GetTrailingEdgePlacement
 * Description   : Get Trailing edge position of waveforms in OPWMB & OPWMT mode.
 * Implements    : EMIOS_DRV_PWM_GetTrailingEdgePlacement_Activity
 *END**************************************************************************/
 uint32_t EMIOS_DRV_PWM_GetTrailingEdgePlacement(uint8_t emiosGroup,
                                                 uint8_t channel)
 {
    uint32_t temp = 0UL;

    DEV_ASSERT(emiosGroup < EMIOS_NUMBER_GROUP_MAX);
    DEV_ASSERT(channel < EMIOS_NUMBER_CHANNEL_MAX);

    temp = EMIOS_GetUCRegCMode(emiosGroup, channel);

    DEV_ASSERT((temp == (uint32_t)EMIOS_MODE_OPWMB_FLAGX1) || \
               (temp == (uint32_t)EMIOS_MODE_OPWMB_FLAGX2) || \
               (temp == (uint32_t)EMIOS_MODE_OPWMT));
    (void) temp;

    return EMIOS_GetUCRegB(emiosGroup, channel);
 }

/*FUNCTION**********************************************************************
 *
 * Function Name : EMIOS_DRV_PWM_SetTrailingEdgePlacement
 * Description   : Set Trailing edge position of waveforms in OPWMB & OPWMT mode.
 * When set Trailing edge to a value greater than maximum value of the selected time base,
 * a 100 % duty cycle(EDEPOL = 1) or 0 % duty cycle( EDEPOL = 0) signal generated.
 * Implements    : EMIOS_DRV_PWM_SetTrailingEdgePlacement_Activity
 *END**************************************************************************/
 void EMIOS_DRV_PWM_SetTrailingEdgePlacement(uint8_t emiosGroup,
                                             uint8_t channel,
                                             uint32_t newTrailingEdgePlacement)
 {
     uint32_t temp = 0UL;

    DEV_ASSERT(emiosGroup < EMIOS_NUMBER_GROUP_MAX);
    DEV_ASSERT(channel < EMIOS_NUMBER_CHANNEL_MAX);

    temp = EMIOS_GetUCRegCMode(emiosGroup, channel);

    DEV_ASSERT((temp == (uint32_t)EMIOS_MODE_OPWMB_FLAGX1) || \
               (temp == (uint32_t)EMIOS_MODE_OPWMB_FLAGX2) || \
               (temp == (uint32_t)EMIOS_MODE_OPWMT));
    (void) temp;

    /* Validate parameter */
    DEV_ASSERT(newTrailingEdgePlacement <= EMIOS_DATA_REG_MAX_VAL);
    DEV_ASSERT(newTrailingEdgePlacement >= EMIOS_GetUCRegA(emiosGroup, channel));

    EMIOS_SetUCRegB(emiosGroup, channel, newTrailingEdgePlacement);
 }

/*FUNCTION**********************************************************************
 *
 * Function Name : EMIOS_DRV_PWM_SetCenterAlignDeadTime
 * Description   : Set Dead time value of waveforms in OPWMCB mode. New dead time should
 * be no greater than 0xFFFF (16 bits).
 * Implements    : EMIOS_DRV_PWM_SetCenterAlignDeadTime_Activity
 *END**************************************************************************/
 void EMIOS_DRV_PWM_SetCenterAlignDeadTime(uint8_t emiosGroup,
                                           uint8_t channel,
                                           uint32_t newDeadTime)
 {
    DEV_ASSERT(emiosGroup < EMIOS_NUMBER_GROUP_MAX);
    DEV_ASSERT(channel < EMIOS_NUMBER_CHANNEL_MAX);
    DEV_ASSERT(((uint8_t)EMIOS_GetUCRegCMode(emiosGroup, channel) & EMIOS_FILTER_OPWMCB) == EMIOS_MASK_OPWMCB);

     /* Validate opwfm parameter */
    DEV_ASSERT(newDeadTime <= EMIOS_OPWMCB_MAX_CNT_VAL);

    EMIOS_SetUCRegB(emiosGroup, channel, newDeadTime);
 }

/*FUNCTION**********************************************************************
 *
 * Function Name : EMIOS_DRV_PWM_GetCenterAlignDeadTime
 * Description   : Get Dead time value of waveforms in OPWMCB mode.
 * Implements    : EMIOS_DRV_PWM_GetCenterAlignDeadTime_Activity
 *END**************************************************************************/
 uint32_t EMIOS_DRV_PWM_GetCenterAlignDeadTime(uint8_t emiosGroup,
                                               uint8_t channel)
 {
    DEV_ASSERT(emiosGroup < EMIOS_NUMBER_GROUP_MAX);
    DEV_ASSERT(channel < EMIOS_NUMBER_CHANNEL_MAX);
    DEV_ASSERT(((uint8_t)EMIOS_GetUCRegCMode(emiosGroup, channel) & EMIOS_FILTER_OPWMCB) == EMIOS_MASK_OPWMCB);

    return EMIOS_GetUCRegB(emiosGroup, channel);
 }

/*FUNCTION**********************************************************************
 *
 * Function Name : EMIOS_DRV_PWM_SetCenterAlignIdealDutyCycle
 * Description   : Set Ideal Duty Cycle value of waveforms in OPWMCB mode. New ideal duty cycle
 * should be no greater than 0xFFFF (16 bits) and greater than 0.
 * Implements    : EMIOS_DRV_PWM_SetCenterAlignIdealDutyCycle_Activity
 *END**************************************************************************/
 void EMIOS_DRV_PWM_SetCenterAlignIdealDutyCycle(uint8_t emiosGroup,
                                                 uint8_t channel,
                                                 uint32_t newIdealDutyCycle)
 {
    DEV_ASSERT(emiosGroup < EMIOS_NUMBER_GROUP_MAX);
    DEV_ASSERT(channel < EMIOS_NUMBER_CHANNEL_MAX);
    DEV_ASSERT(((uint8_t)EMIOS_GetUCRegCMode(emiosGroup, channel) & EMIOS_FILTER_OPWMCB) == EMIOS_MASK_OPWMCB);

     /* Validate opwfm parametter */
    DEV_ASSERT((newIdealDutyCycle <= EMIOS_OPWMCB_MAX_CNT_VAL) && \
               (newIdealDutyCycle >= EMIOS_OPWMCB_MIN_CNT_VAL));

    EMIOS_SetUCRegA(emiosGroup, channel, newIdealDutyCycle);
 }

/*FUNCTION**********************************************************************
 *
 * Function Name : EMIOS_DRV_PWM_GetCenterAlignIdealDutyCycle
 * Description   : Get Ideal Duty Cycle value of waveforms in OPWMCB mode.
 * Implements    : EMIOS_DRV_PWM_GetCenterAlignIdealDutyCycle_Activity
 *END**************************************************************************/
 uint32_t EMIOS_DRV_PWM_GetCenterAlignIdealDutyCycle(uint8_t emiosGroup,
                                                     uint8_t channel)
 {
    DEV_ASSERT(emiosGroup < EMIOS_NUMBER_GROUP_MAX);
    DEV_ASSERT(channel < EMIOS_NUMBER_CHANNEL_MAX);
    DEV_ASSERT(((uint8_t)EMIOS_GetUCRegCMode(emiosGroup, channel) & EMIOS_FILTER_OPWMCB) == EMIOS_MASK_OPWMCB);

    return EMIOS_GetUCRegA(emiosGroup, channel);
 }

/*FUNCTION**********************************************************************
 *
 * Function Name : EMIOS_DRV_PWM_SetTriggerPlacement
 * Description   : Set Trigger placement value in OPWMT mode. New trigger placement
 * should be no larger than 0xFFFFFF(24 bits).
 * Implements    : EMIOS_DRV_PWM_SetTriggerPlacement_Activity
 *END**************************************************************************/
 void EMIOS_DRV_PWM_SetTriggerPlacement(uint8_t emiosGroup,
                                        uint8_t channel,
                                        uint32_t newTriggerPlacement)
 {
    DEV_ASSERT(emiosGroup < EMIOS_NUMBER_GROUP_MAX);
    DEV_ASSERT(channel < EMIOS_NUMBER_CHANNEL_MAX);
    DEV_ASSERT(EMIOS_GetUCRegCMode(emiosGroup, channel) == (uint32_t)EMIOS_MODE_OPWMT);

    /* Validate opwmt parameter */
    DEV_ASSERT(newTriggerPlacement <= EMIOS_DATA_REG_MAX_VAL);
    EMIOS_SetUCRegALTA(emiosGroup, channel, newTriggerPlacement);
 }

/*FUNCTION**********************************************************************
 *
 * Function Name : EMIOS_DRV_PWM_GetTriggerPlacement
 * Description   : Get Trigger placement value in OPWMT mode
 * Implements    : EMIOS_DRV_PWM_GetTriggerPlacement_Activity
 *END**************************************************************************/
 uint32_t EMIOS_DRV_PWM_GetTriggerPlacement(uint8_t emiosGroup,
                                            uint8_t channel)
 {
    DEV_ASSERT(emiosGroup < EMIOS_NUMBER_GROUP_MAX);
    DEV_ASSERT(channel < EMIOS_NUMBER_CHANNEL_MAX);
    DEV_ASSERT(EMIOS_GetUCRegCMode(emiosGroup, channel) == (uint32_t)EMIOS_MODE_OPWMT);

    return EMIOS_GetUCRegALTA(emiosGroup, channel);
 }

/*******************************************************************************
* EOF
******************************************************************************/
