
#include "pin_mux.h"
#include "Cpu.h"
#include "FreeRTOS.h"
#include <stdint.h>
#include <stdbool.h>
#include "task.h"

#include "canCom1.h"
#include "Can.h"
#include "Can_Irq.h"
#include "CanIf.h"
#include "CanTp.h"
#include "Dcm.h"
#include "Com.h"
#include "EcuM.h"
#include "pit1.h"
#include "osif.h"

#define USR_TASK_INCLUDE
#include "usr_task_container.h"
#undef  USR_TASK_INCLUDE
//#include "os_task_container.h"
#include "project_cfg.h"

#define LED1_PORT   PTB
#define LED1        14          /* pin PB[14] - LED DS3  on LFGTWSEM core board */


/* CANID=0x3A2 发送数据  全局变量赋值的地方 */
uint8 varTest_S_Sig1;
uint8 varTest_S_Sig2;
uint8 varTest_S_Sig3;
uint8 varTest_S_Sig4;
uint8 varTest_S_Sig5;
uint8 varTest_S_Sig6;

//CANID=0X341 接收
uint8 varTest_R_Sig1;
uint8 varTest_R_Sig2;
//CANID=0X350 接收
uint8 varTest_R_Sig3;




void Led_DS3_MainFunction(void )
{

	//PINS_DRV_TogglePins(LED1_PORT, (1 << LED1));

}
/******************************************************************************/
/*
 * Brief               <BSW_Init>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
void BSW_Init(void)
{
	    FLEXCAN_DRV_Init(INST_CANCOM1, &canCom1_State, &canCom1_InitConfig0);
	    flexcan_data_info_t dataInfo =
	    {
	            .data_length = 8U,
	            .msg_id_type = FLEXCAN_MSG_ID_STD,
	            .enable_brs  = false,
	            .fd_enable   = false,
	            .fd_padding  = 0U
	    };

	    FLEXCAN_DRV_InstallEventCallback(INST_CANCOM1,FlexCanRxTxIntruptcallback,NULL);

		Os_InitCounterCB(0);
		Can_Init(NULL_PTR);
		CanIf_Init(NULL_PTR);
		Com_Init(&Com_Config);
		EcuM_Init();
		CanIf_SetControllerMode(CAN_CONTROLLER_C,CANIF_CS_STARTED);
		Com_IpduGroupStart(0,TRUE);
		Com_IpduGroupStart(1,TRUE);
		CanTp_Init();
		Dcm_Init();
}

/******************************************************************************/
/*
 * Brief               <Task_10ms>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
void APPTask_10ms(void)
{
    EcuM_MainFunction();
	Com_MainFunctionTx();
	Com_MainFunctionRx();
	Com_ReceiveMessage(0,&varTest_R_Sig1);
	Com_ReceiveMessage(2,&varTest_R_Sig3);
    Com_ReceiveMessage(1,&varTest_R_Sig2);
		//com 信号发送  10ms
		//CANID=0x3A2  循环发送数据
	varTest_S_Sig1=varTest_R_Sig2;
    Com_SendMessage(Test_S_Sig1,&varTest_S_Sig1);
    varTest_S_Sig2=0;
    Com_SendMessage(Test_S_Sig2,&varTest_S_Sig2);
    varTest_S_Sig3=varTest_R_Sig3;
    Com_SendMessage(Test_S_Sig3,&varTest_S_Sig3);
	varTest_S_Sig4=0;
	Com_SendMessage(Test_S_Sig4,&varTest_S_Sig4);
	varTest_S_Sig5=0;
    Com_SendMessage(Test_S_Sig5,&varTest_S_Sig5);
	varTest_S_Sig6=varTest_R_Sig1;
	Com_SendMessage(Test_S_Sig6,&varTest_S_Sig6);

}
/******************************************************************************/
/*
 * Brief               <Task_1ms>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
void APPTask_1ms(void)
{
	IncrementCounter(SystemTimer);
	CanTp_MainFunction();
	Dcm_MainFunction();
}
/******************************************************************************/
/*
 * Brief               <APPCANMail0_4Receive>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
void APPCANMail0_4Receive(void)
{
	   flexcan_msgbuff_t recvBuff;
       uint8 i;

	   for(i=0;i<4;i++)
	   {
		  FLEXCAN_DRV_Receive(INST_CANCOM1, i, &recvBuff);
	   }
}















