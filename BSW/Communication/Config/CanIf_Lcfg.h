/*============================================================================*/
/*  Copyright (C) 2009-2014
 *  
 *  All rights reserved. This software is  property. Duplication
 *  or disclosure without  written authorization is prohibited.
 *  
 *  @file       <CanIf_Lcfg.h>
 *  @brief      <>
 *  
 *  <MCU:MPC563x>
 *  
 *  @author     <>
 *  @date       <2014-06-10 09:41:04>
 */
/*============================================================================*/


#ifndef CANIF_LCFG_H
#define CANIF_LCFG_H

/*=======[V E R S I O N  I N F O R M A T I O N]===============================*/
#define CANIF_LCFG_H_AR_MAJOR_VERSION   3
#define CANIF_LCFG_H_AR_MINOR_VERSION   2
#define CANIF_LCFG_H_AR_PATCH_VERSION   0
#define CANIF_LCFG_H_SW_MAJOR_VERSION   1
#define CANIF_LCFG_H_SW_MINOR_VERSION   0
#define CANIF_LCFG_H_SW_PATCH_VERSION   0

/*=======[I N C L U D E S]====================================================*/

#include "CanIf_Types.h"

/*=======[M A C R O S]========================================================*/


#endif

/*=======[E N D   O F   F I L E]==============================================*/
