/************************************************************************
*				        ESEC UESTC
* 	 Copyright (C) 2005-2010 ESEC UESTC. All Rights Reserved.
***********************************************************************/

/**
 * Log:  
 * Revision 1.0 2010-5-13����10:52:40 stanley
 * description: create
 *
 */

/**
 * @file 	Dem.h
 * @brief
 *	<li>Function�� </li>
 *  <li>Design Points��</li>
 *  <p>
 *  
 *  </p>
 * @author 	stanley
 * @date 	2010-5-13
 * 
 */

#ifndef DEM_H
#define DEM_H


#define DEM_H_AR_MAJOR_VERSION  3
#define DEM_H_AR_MINOR_VERSION  1
/****************************** references *********************************/
#include "Dem_Types.h"
/**@req DEM-FUNR-300[Dem115]*/
#include "Dem_IntEvtId.h"
#include "Dem_IntErrId.h"

#ifdef __cplusplus
extern "C" {
#endif  /* __cplusplus */

/****************************** declarations *********************************/
/**Development errors*/
/** @req DEM-FUNR-301[Dem116] @req DEM-FUNR-302[Dem173]*/
#define DEM_E_PARAM_CONFIG    (uint8)0x10
#define DEM_E_PARAM_ADRRESS   (uint8)0x11
#define DEM_E_PARAM_DATA      (uint8)0x12
#define DEM_E_PARAM_LENGTH    (uint8)0x13
#define DEM_E_UNINIT          (uint8)0x20
#define DEM_E_NODATAAVAILABLE (uint8)0x30
#define DEM_E_WRONG_CONDITION (uint8)0x40

/** Service IDs*/
#define DEM_GETVERSIONINFO_ID                       0x00
#define DEM_PREINIT_ID                              0x01
#define DEM_INIT_ID                                 0x02
#define DEM_SHUTDOWN_ID                             0x03
#define DEM_REPORTERRORSTATUS_ID                    0x0f
#define DEM_SETEVENTSTATUS_ID                       0x04
#define DEM_RESETEVENTSTATUS_ID                     0x05
#define DEM_PRESTOREFREEZEFRAME_ID                  0x06
#define DEM_CLEARPRESTOREDFREEZEFRAME_ID            0x07
#define DEM_SETOPERATIONCYCLESTATE_ID               0x08
#define DEM_SETOPERATIONCYCLECNTVALUE_ID            0x09
#define DEM_SETAGINGCYCLESTATE_ID                   0x11
#define DEM_SETAGINGCYCLECOUNTERVALUE_ID            0x12
#define DEM_GETEVENTSTATUS_ID                       0x0a
#define DEM_GETEVENTFAILED_ID                       0x0b
#define DEM_GETEVENTTESTED_ID                       0x0c
#define DEM_GETDTCOFEVENT_ID                        0x0d
#define DEM_SETSTORAGECONDITION_ID                  0x38
#define DEM_SETENABLECONDITION_ID                   0x39
#define DEM_GETFAULTDETECTIONCOUNTER_ID             0x3e
#define DEM_GETINDICATORSTATUS_ID                   0x29
#define DEM_GETEVENTEXTENDEDDATARECORD_ID           0x30
#define DEM_GETEVENTFREEZEFRAMEDATA_ID              0x31
#define DEM_GETEVENTMEMORYOVERFLOW_ID               0x32
#define DEM_SETDTCFILTER_ID                         0x13
#define DEM_SETDTCFILTERFORRECORDS_ID               0x3f
#define DEM_GETSTATUSOFDTC_ID                       0x15
#define DEM_GETDTCSTATUSAVAILABILITYMASK_ID         0x16
#define DEM_GETNUMBEROFFILTEREDDTC_ID               0x17
#define DEM_GETNEXTFILTEREDDTC_ID                   0x18
#define DEM_GETDTCBYOCCURRENCETIME_ID               0x19
#define DEM_GETNEXTFILTEREDRECORD_ID                0x3a
#define DEM_GETNEXTFILTEREDDTCANDFDC_ID             0x3b
#define DEM_GETNEXTFILTEREDDTCANDSEVERITY_ID        0x3d
#define DEM_GETTRANSLATIONTYPE_ID                   0x3c
#define DEM_GETSEVERITYOFDTC_ID                     0x0e
#define DEM_DISABLEDTCRECORDUPDATE_ID               0x1a
#define DEM_ENABLEDTCRECORDUPDATE_ID                0x1b
#define DEM_GETDTCOFFREEZEFRAMERECORD_ID            0x1c
#define DEM_GETFREEZEFRAMEDATABYDTC_ID              0x1d
#define DEM_GETSIZEOFFREEZEFRAME_ID                 0x1f
#define DEM_GETEXTENDEDDATARECORDBYDTC_ID           0x20
#define DEM_GETSIZEOFEXTENDEDDATARECORDBYDTC_ID     0x21
#define DEM_CLEARDTC_ID                             0x22
#define DEM_DISABLEDTCSETTING_ID                    0x24
#define DEM_ENABLEDTCSETTING_ID                     0x25
#define DEM_DCMCANCELOPERATION_ID                   0x2a
#define DEM_MAINFUNCTION_ID                         0x55
#define DEM_GETFREEZEFRAMEDATAIDENTIFIERBYDTC_ID    0x1e
#define DEM_DISABLEDTCSTORAGE_ID                    0x24
#define DEM_ENABLEDTCSTORAGE_ID                     0x25
#define DEM_DISABLEEVENTSTATUSUPDATE_ID             0x26
#define DEM_ENABLEEVENTSTATUSUPDATE_ID              0x27
#define DEM_SETEVENTDISABLED_ID                     0x51
#define DEM_REPIUMPRFAULTDETECT_ID                  0x73
#define DEM_REPIUMPRDENLOCK_ID                      0x71
#define DEM_REPIUMPRDENRELEASE_ID                   0x72
#define DEM_GETINFOTYPEVALUE08_ID                   0x6b
#define DEM_GETINFOTYPEVALUE0B_ID                   0x6c
#define DEM_DCMGETPID01_ID                          0x61
#define DEM_DCMGETPID02_ID                          0x62
#define DEM_DCMGETPID1C_ID                          0x63
#define DEM_DCMGETPID21_ID                          0x64
#define DEM_DCMGETPID30_ID                          0x65
#define DEM_DCMGETPID31_ID                          0x66
#define DEM_DCMGETPID41_ID                          0x67
#define DEM_DCMGETPID4D_ID                          0x68
#define DEM_DCMGETPID4E_ID                          0x69
#define DEM_GETOBDFREEZEFRAMEDATA_ID                0x52
#define DEM_SETPTOSTATUS_ID                         0x79

/******************function declarations****************/
    /**
     * Report error status of BSWs.
     * @param eventId  ID of event
     * @param eventStatus status of event
     */
    /** @req DEM-APIR-005[Dem206]*/
    /**@req DEM-FUNR-208[Dem107]*/
    #define Dem_ReportErrorStatus(eventId, eventStatus)

/************ Interfaces with DCM **************/
/**
 * Set DTC filter for successor operations.
 * @param DTCStatusMask mask for DTC status
 * @param DTCKind kind of DTC
 * @param DTCOrigin origin of DTC
 * @param filterWithSeverity whether severity of DTC is used
 * @param DTCSeverityMask mask for DTC severity used for filtering
 * @param filterForFaultDetectionCounter
 * @return operation result
 */
/** @req DEM-APIR-025[Dem208]*/
/**@req DEM-FUNR-249[Dem057]*/
extern Dem_ReturnSetDTCFilterType Dem_SetDTCFilter(uint8 DTCStatusMask,
        Dem_DTCKindType DTCKind, Dem_DTCOriginType DTCOrigin,
        Dem_FilterWithSeverityType filterWithSeverity,
        Dem_DTCSeverityType DTCSeverityMask,
        Dem_FilterForFDCType filterForFaultDetectionCounter);

/**
 * Get DTC status mask supported.
 * @param DTCStatusMask DTC status mask supported
 * @return result of operation
 */
/** @req DEM-APIR-028[Dem213]*/
/**@req DEM-FUNR-248[Dem060]*/
extern Std_ReturnType Dem_GetDTCStatusAvailabilityMask(uint8* DTCStatusMask);

/**
 * Get number of DTCs which match current filter condition.
 * @param numberOfFilteredDTC number of matched DTC
 * @return result of operation
 */
/** @req DEM-APIR-029[Dem214]*/
/**@req DEM-FUNR-250[Dem061]*/
extern Dem_ReturnGetNumberOfFilteredDTCType Dem_GetNumberOfFilteredDTC(uint16* numberOfFilteredDTC);

/**
 * Get next DTC and its status which matches the current filter.
 * @param DTC code value of DTC
 * @param DTCStatus status of DTC
 * @return result of operation
 */
/** @req DEM-APIR-030[Dem215]*/
/**@req DEM-FUNR-251[Dem216]*/
extern Dem_ReturnGetNextFilteredDTCType Dem_GetNextFilteredDTC(uint32* DTC, uint8* DTCStatus);

/**
 * Clear specific DTC information.
 * @param DTC code value of DTC
 * @param DTCKind kind of DTC
 * @param DTCOrigin origin of DTC
 * @return result of operation
 */
/** @req DEM-APIR-044[Dem241]*/
/**@req DEM-FUNR-273[Dem009]*/
extern Dem_ReturnClearDTCType Dem_ClearDTC(uint32 DTC, Dem_DTCKindType DTCKind, Dem_DTCOriginType DTCOrigin);

/**
 * Disable handling for specific group of DTCs.
 * @param DTCGroup DTC code for DTC group
 * @param DTCKind kind of DTC
 * @return result of operation
 */
/** @req DEM-APIR-045[Dem242] */
/**@req DEM-FUNR-283[Dem035]*/
/**@req DEM-FUNR-284[Dem079]*/
extern Dem_ReturnControlDTCStorageType Dem_DisableDTCStorage(Dem_DTCGroupType DTCGroup, Dem_DTCKindType DTCKind);

/**
 * Enable handling for specific group of DTCs.
 * @param DTCGroup DTC code for DTC Group
 * @param DTCKind kind of DTC
 * @return result of operation
 */
/** @req DEM-APIR-046[Dem243]*/
/**@req DEM-FUNR-283[Dem035]*/
extern Dem_ReturnControlDTCStorageType Dem_EnableDTCStorage(Dem_DTCGroupType DTCGroup, Dem_DTCKindType DTCKind);
	
	/**
 * Get DTC format of the implementation.
 * @return DTC format type
 */
/** @req DEM-APIR-035[Dem230]*/
/**@req DEM-FUNR-245[Dem231]*/
extern uint8 Dem_GetTranslationType(void);
    
    
/*********************************************
Function: Gets the status of a DTC
Input Parameter: DTC: For this DTC its status is requested 
                 DTCKind:   This parameter defines the requested DTC, either only OBD-relevant DTCs or all DTCs 
                 DTCOrigin: this parameter is used to select the source memory the DTCs shall be read from.
Output Parameter:(*DTCStatus):This parameter receives the status information of the requested DTC
return value:  
/*********************************************/
extern Dem_ReturnGetStatusOfDTCType Dem_GetStatusOfDTC(uint32 DTC,
    											Dem_DTCKindType DTCKind,
    											Dem_DTCOriginType DTCOrigin,
   												uint8* DTCStatus);

/*********************************************
Function: Disables the DTC record update
Input Parameter: None
Output Parameter: None
return value:  
/*********************************************/
extern Std_ReturnType Dem_DisableDTCRecordUpdate(void);
   
   
extern Dem_ReturnGetFreezeFrameDataByDTCType Dem_GetFreezeFrameDataByDTC(
     								uint32  DTC,
     								Dem_DTCKindType  DTCKind,
     								Dem_DTCOriginType  DTCOrigin,
     								uint8  RecordNumber,
     								uint8*  DestBuffer,
     								uint8*  BufSize );
     								
extern Std_ReturnType Dem_EnableDTCRecordUpdate(void);

/*********************************************
Function: Returns the DTCExtendedDataRecord as defined in UDS Service 0x19 subfunction 0x06, 0x10
Input Parameter: DTC: This is the DTC the 'Extended Data Record' is assigned to. .
                 DTCKind:This parameter defines the requested DTC, either only OBD-relevant DTCs or all DTCs;
                 DTCOrigin:this parameter is used to select the source memory the DTCs shall be read from
                 ExtendedDataNumber:Identification of requested Extended data record
Output Parameter: DestBuffer:This parameter contains a byte pointer that points to the buffer to which the Extended Data data shall be written
input(output)parameter:BufSize:
				When the function is called this parameter contains the maximum number of data bytes that can be written 
				to the buffer.The function returns the actual number of written data 
bytes in this parameter.return value:  
/*********************************************/
extern Dem_ReturnGetExtendedDataRecordByDTCType Dem_GetExtendedDataRecordByDTC(uint32  DTC,
     																	Dem_DTCKindType  DTCKind,
     																	Dem_DTCOriginType  DTCOrigin,
     																	uint8  ExtendedDataNumber,
     																	uint8*  DestBuffer,
     																	uint8*  BufSize);


#ifdef __cplusplus
}
#endif  /* __cplusplus */

#endif /* DEM_H */
