/* ###################################################################
**     Filename    : usr_task_container.h
**     Project     : freertos_mpc5748g
**     Processor   : MPC5748G_324
**     Version     :
**     Compiler    : GNU C Compiler
**     Date/Time   :
**     Abstract    :
**         This head file contains all the user's periodic tasks.Task
**     period time various form 1ms,2ms to 100ms.
**     Settings    :
**     Contents    :
**
**
** ###################################################################*/

#if defined (USR_TASK_INCLUDE)


/*MCU driver head files*/
#include "clockMan1.h"
#include "pin_mux.h"

/*Application head files*/

/*app_demo.h is added as example.User can remove it.*/
#include "app_demo.h"

#include "CanIf.h"
#include "CanTp.h"
#include "Dcm.h"
#include "Com.h"
#include "EcuM.h"

#endif




/* ------------------------------------------------------------
Put all  the  MCU drivers' initialization functions here
--------------------------------------------------------------*/
#if defined(USR_MCU_INIT_TAG)

/* Initialize clocks */
CLOCK_SYS_Init(g_clockManConfigsArr,   CLOCK_MANAGER_CONFIG_CNT,
               g_clockManCallbacksArr, CLOCK_MANAGER_CALLBACK_CNT);
CLOCK_SYS_UpdateConfiguration(0U, CLOCK_MANAGER_POLICY_AGREEMENT);



/*Initialize ports */
PINS_DRV_Init(NUM_OF_CONFIGURED_PINS, g_pin_mux_InitConfigArr);

BSW_Init();


#endif



/* ------------------------------------------------------------
Put  runnables with  period time of 1 ms here.
--------------------------------------------------------------*/
#if defined(USR_TASK_1MS_TAG)


APPCANMail0_4Receive();
APPTask_1ms();
//Led_DS3_MainFunction();


#endif
/* ------------------------------------------------------------
Put  runnables with  period time of 2 ms here.
--------------------------------------------------------------*/
#if defined(USR_TASK_2MS_TAG)






#endif

/* ------------------------------------------------------------
Put  runnable with  period time of 5 ms here.
--------------------------------------------------------------*/
#if defined(USR_TASK_5MS_TAG)






#endif
/* ------------------------------------------------------------
Put  runnable with  period time of 10 ms here.
--------------------------------------------------------------*/
#if defined(USR_TASK_10MS_TAG)


APPTask_10ms();


#endif
/* ------------------------------------------------------------
Put  runnable with  period time of 20 ms here.
--------------------------------------------------------------*/
#if defined(USR_TASK_20MS_TAG)





#endif

/* ------------------------------------------------------------
Put  runnable with  period time of 50 ms here.
--------------------------------------------------------------*/
#if defined(USR_TASK_50MS_TAG)





#endif

/* ------------------------------------------------------------
Put  runnable with  period time of 100 ms here.
--------------------------------------------------------------*/
#if defined(USR_TASK_100MS_TAG)






#endif




