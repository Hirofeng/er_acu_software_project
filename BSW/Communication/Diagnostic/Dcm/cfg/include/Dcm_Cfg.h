/*============================================================================*/
/*  Copyright (C) 2009-2011,  INFRASTRUCTURE SOFTWARE CO.,LTD.
 *
 *  All rights reserved. This software is  property. Duplication
 *  or disclosure without  written authorization is prohibited.
 *
 *  @file        <Dcm_Cfg.h>
 *  @brief       <>
 *
 *  <Compiler: CodeWarrior    MCU:XXX>
 *
 *  @author     <chen maosen>
 *  @date       <2013-03-20>
 */
/*============================================================================*/

#ifndef DCMCFG_H
#define DCMCFG_H
/****************************** references *********************************/
#include "ComStack_Types.h"

/****************************************************************************************
*********************************DcmGeneral container***********************************
*****************************************************************************************/
#define  DCM_DEV_ERROR_DETECT               (STD_OFF)
#define  DCM_REQUEST_INDICATION_ENABLED     (STD_ON)
#define  DCM_RESPOND_ALL_REQUEST            (STD_ON)
#define  DCM_VERSION_INFO_API               (STD_ON)

/****************************************************************************************
********************************DcmPageBufferCfg container******************************
*****************************************************************************************/
#define  DCM_PAGEDBUFFER_ENABLED            (STD_OFF)

/****************************************************************************************
*********************************DcmDsl container***************************************
*****************************************************************************************/
#define  DCM_CHANNEL_NUM                    (2u)     /*number of configuration Channel */
#define  DCM_DSLPROTOCOLROW_NUM_MAX         (1u)     /*number of configuration protocol*/
#define  DCM_CHANNEL_LENGTH                 (256u)   /*the total length of channel*/

#define  DCM_DSLDIAGRESP_FORCERESPENDEN     (STD_ON)  /*Enable/Disable application directly trigger response Pending*/

/****************************************************************************************
*********************************DcmDsd container****************************************
*****************************************************************************************/
#define  DCM_SERVICE_TAB_NUM                (1u)     /*number of service table*/

/*******************************************/
/*UDS protocol*/
#define  DCM_UDS_FUNC_ENABLED               (STD_ON) /*Enable/Disable UDS Protocl*/
#if(STD_ON == DCM_UDS_FUNC_ENABLED)
    #define  DCM_UDS_SERVICE_NUM            (26U)    /*number of Uds protocol service,Note: ignored when writing scripts*/
    /*Diagnostic and communication management functional unit */
    #define  DCM_UDS_SERVICE0X10_ENABLED    (STD_ON) /*DiagnosticSessionControl*/
    #define  DCM_UDS_SERVICE0X11_ENABLED    (STD_ON) /*ECUReset*/
    #define  DCM_UDS_SERVICE0X27_ENABLED    (STD_ON) /*SecurityAccess*/
    #define  DCM_UDS_SERVICE0X28_ENABLED    (STD_ON) /*CommunicationControl*/
    #define  DCM_UDS_SERVICE0X3E_ENABLED    (STD_ON) /*TesterPresent*/
    #define  DCM_UDS_SERVICE0X83_ENABLED    (STD_OFF) /*AccessTimingParameter*/
    #define  DCM_UDS_SERVICE0X84_ENABLED    (STD_OFF) /*SecuredDataTransmission*/
    #define  DCM_UDS_SERVICE0X85_ENABLED    (STD_ON) /*ControlDTCSetting*/
    #define  DCM_UDS_SERVICE0X86_ENABLED    (STD_OFF) /*ResponseOnEvent*/
    #define  DCM_UDS_SERVICE0X87_ENABLED    (STD_OFF) /*LinkControl*/
    /*Data transmission functional unit */
    #define  DCM_UDS_SERVICE0X22_ENABLED    (STD_ON) /*ReadDataByIdentifier*/
    #define  DCM_UDS_SERVICE0X23_ENABLED    (STD_OFF) /*ReadMemoryByAddress*/
    #define  DCM_UDS_SERVICE0X24_ENABLED    (STD_OFF) /*ReadScalingDataByIdentifier*/
    #define  DCM_UDS_SERVICE0X2A_ENABLED    (STD_OFF) /*ReadDataByPeriodicIdentifier*/
    #define  DCM_UDS_SERVICE0X2C_ENABLED    (STD_OFF) /*DynamicallyDefineDataIdentifier*/
    #define  DCM_UDS_SERVICE0X2E_ENABLED    (STD_ON) /*WriteDataByIdentifier*/
    #define  DCM_UDS_SERVICE0X3D_ENABLED    (STD_OFF) /*WriteMemoryByAddress*/
    /* Stored data transmission functional unit*/
    #define  DCM_UDS_SERVICE0X14_ENABLED    (STD_ON) /*ClearDiagnosticInformation*/
    #define  DCM_UDS_SERVICE0X19_ENABLED    (STD_ON) /*ReadDTCInformation*/
    /*InputOutput control functional unit*/
    #define  DCM_UDS_SERVICE0X2F_ENABLED    (STD_ON) /*InputOutputControlByIdentifier*/
    /*Remote activation of routine functional unit*/
    #define  DCM_UDS_SERVICE0X31_ENABLED    (STD_OFF) /*RoutineControl*/
    /*Upload download functional unit*/
    #define  DCM_UDS_SERVICE0X34_ENABLED    (STD_OFF) /*RequestDownload*/
    #define  DCM_UDS_SERVICE0X35_ENABLED    (STD_OFF) /*RequestUpload*/
    #define  DCM_UDS_SERVICE0X36_ENABLED    (STD_OFF) /*TransferData*/
    #define  DCM_UDS_SERVICE0X37_ENABLED    (STD_OFF) /*RequestTransferExit*/
    
    #define  DCM_TEST_SERVICE0XB0_ENABLED   (STD_ON)
#endif

#if((STD_ON == DCM_UDS_FUNC_ENABLED) && (STD_ON == DCM_UDS_SERVICE0X19_ENABLED))
	#define	DCM_UDS_SERVICE0X19_01_ENABLED   (STD_OFF)	/*ReportNumberOfDTCByStatusMask*/		  
	#define	DCM_UDS_SERVICE0X19_02_ENABLED   (STD_ON)	/*ReportDTCByStatusMask*/
	#define	DCM_UDS_SERVICE0X19_04_ENABLED   (STD_OFF)	/*ReportDTCSnapshotRecordByDTCNumber*/
	#define	DCM_UDS_SERVICE0X19_06_ENABLED   (STD_OFF)	/*ReportDTCExtendedDataRecordsByDTCNumber*/
	#define	DCM_UDS_SERVICE0X19_0A_ENABLED   (STD_ON)	/*ReportSupportedDTC*/			  
#endif
/******************************************************************************
 ********************************DcmDsp container*****************************
 ******************************************************************************/
#define  DCM_DSP_DID_FUNC_ENABLED                (STD_ON)         	/*Enable/disable DID function*/

/**=======================================================**/
#define  DCM_DSP_ECU_RESET_FUNC_ENABLED           (STD_ON)       	/*Enable/disable EcuReset function,corresponding UDS protocol 0x11 Service*/

/**=======================================================**/
#define  DCM_DSP_ROUTINE_FUNC_ENABLED             (STD_OFF)       	/*Enable/disable RoutinControl function,corresponding UDS protocol 0x31 Service.*/
#if(STD_ON == DCM_DSP_ROUTINE_FUNC_ENABLED)
    #define  DCM_DSP_ROUTINE_MAX_NUM                  (3u)       	/*Number of RoutineID*/
    /*the maximum length of routineStatusRecord,*/
    #define  DCM_ROUTINE_CTRLOPTION_RECORD_MAXSIZE    (20u)
    #define  DCM_DSP_ROUTINE_ID1_REQUSET_RES_ENABLED  (STD_ON)  	/*Enable/disable DcmDspRoutineInfo1 contains DcmDspRoutineRequestRes,Note: ignored when writing scripts*/
    #define  DCM_DSP_ROUTINE_ID2_REQUSET_RES_ENABLED  (STD_ON)  	/*Enable/disable DcmDspRoutineInfo2 contains DcmDspRoutineRequestRes,Note: ignored when writing scripts*/
    #define  DCM_DSP_ROUTINE_ID3_REQUSET_RES_ENABLED  (STD_ON)  	/*Enable/disable DcmDspRoutineInfo3 contains DcmDspRoutineRequestRes,Note: ignored when writing scripts*/

    #define  DCM_DSP_ROUTINE_ID1_STOP_ENABLED         (STD_ON)  	/*Enable/disable DcmDspRoutineInfo1 contains DcmDspRoutineStop,Note: ignored when writing scripts*/
    #define  DCM_DSP_ROUTINE_ID2_STOP_ENABLED         (STD_ON)  	/*Enable/disable DcmDspRoutineInfo2 contains DcmDspRoutineStop,Note: ignored when writing scripts*/
    #define  DCM_DSP_ROUTINE_ID3_STOP_ENABLED         (STD_ON)  	/*Enable/disable DcmDspRoutineInfo3 contains DcmDspRoutineStop,Note: ignored when writing scripts*/

    #define  DCM_DSP_ROUTINE_ID1_USE_PORT             (STD_ON)  	/*Enable/disable DcmDspRoutineID1 to use RTE interface,Note: when writing scripts ignored*/
    #define  DCM_DSP_ROUTINE_ID2_USE_PORT             (STD_ON)  	/*Enable/disable DcmDspRoutineID1 to use RTE interface,Note: when writing scripts ignored*/
    #define  DCM_DSP_ROUTINE_ID3_USE_PORT             (STD_ON)  	/*Note: when writing scripts ignored*/
#endif

/**=======================================================**/
#define  DCM_SESSION_FUNC_ENABLED                    (STD_ON)   	/*Enable/disable SessionControl Function*/

/**=======================================================**/
#define  DCM_SECURITY_FUNC_ENABLED                   (STD_ON)   	/*Enable/disable SecurityAccess Function*/
#if(STD_ON == DCM_SECURITY_FUNC_ENABLED)
	#define  DCM_SECURITY_ADR_MAXSIZE       		 (8u)       	/*In all security levels,the maximum length of ADR*/
#endif

/**=======================================================**/
#define  DCM_DSP_PID_FUNC_ENABLED                 	(STD_OFF)		/*corresponding OBD protocol 0x01 Service*/
#if(STD_ON == DCM_DSP_PID_FUNC_ENABLED)
    #define  DCM_DSP_PID_ID1_USE_PORT             	(STD_OFF)   	/*Note: when writing scripts ignored.*/
    #define  DCM_DSP_PID_ID2_USE_PORT             	(STD_OFF)   	/*Note: when writing scripts ignored.*/
    #define  DCM_DSP_PID_ID3_USE_PORT             	(STD_OFF)   	/*Note: when writing scripts ignored.*/
#endif

/**=======================================================**/
#define  DCM_DSP_REQUESTCONTROL_FUNC_ENABLED      	(STD_OFF)   	/*corresponding OBD protocol 0x08 Service*/

/**=======================================================**/
#define  DCM_DSP_TEST_RESULT_BYOBDMID_FUNC_ENABLED	(STD_OFF)   	/*corresponding OBD protocol 0x06 Service*/

/**=======================================================**/
#define  DCM_DSP_VEHINFO_FUNC_ENABLED				(STD_OFF)   	/*corresponding OBD protocol 0x09 Service*/

/****************************************************************************************
********************************* OS ****************************************************
*****************************************************************************************/
#define  DCM_REFERENCE_OS_COUNTER_ID                 0x00	  		/*DCM reference OS Count ID*/

#endif /* DCMCFG_H_ */
