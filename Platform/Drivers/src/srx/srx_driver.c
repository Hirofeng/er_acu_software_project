/*
 * Copyright (c) 2013 - 2016, Freescale Semiconductor, Inc.
 * Copyright 2016-2018 NXP
 * All rights reserved.
 *
 * THIS SOFTWARE IS PROVIDED BY NXP "AS IS" AND ANY EXPRESSED OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL NXP OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

/*!
 * @page misra_violations MISRA-C:2012 violations
 *
 * @section [global]
 * Violates MISRA 2012 Required Rule 1.3, Taking address of near auto variable.
 * The code is not dynamically linked. An absolute stack address is obtained
 * when taking the address of the near auto variable. A source of error in
 * writing dynamic code is that the stack segment may be different from the data
 * segment.
 *
 * @section [global]
 * Violates MISRA 2012 Advisory Rule 8.7, External could be made static.
 * Function is defined for usage by application code.
 *
 * @section [global]
 * Violates MISRA 2012 Advisory Rule 8.9, Could define variable at block scope.
 * The variables are ROM-able and are shared between different functions in the module.
 *
 * @section [global]
 * Violates MISRA 2012 Advisory Rule 8.13, Could be declared as pointing to const.
 * Function prototype dependent on external function pointer typedef.
 *
 * @section [global]
 * Violates MISRA 2012 Advisory Rule 11.4, Conversion between a pointer and integer type.
 * The cast is required to initialize base pointers with unsigned integer values.
 *
 * @section [global]
 * Violates MISRA 2012 Advisory Rule 11.5, Conversion from pointer to void to pointer to other type.
 * The cast is required due to the way parameters are passed to the callback function.
 *
 * @section [global]
 * Violates MISRA 2012 Required Rule 11.6, Cast from unsigned int to pointer.
 * The cast is required to initialize a pointer with an unsigned long define,
 * representing an address.
 */

#include <stddef.h>
#include "device_registers.h"
#include "srx_driver.h"
#include "status.h"
#include "clock_manager.h"
#include "srx_hw_access.h"
#include "edma_driver.h"
#include "interrupt_manager.h"
#include "srx_irq.h"

/*! @cond DRIVER_INTERNAL_USE_ONLY */

/*******************************************************************************
 * Definitions
 ******************************************************************************/

/**
 * Defines 1s/x = SRX_ONE_MICROSECOND_CYCLES
 */
#define SRX_ONE_MICROSECOND_CYCLES (1000000u)

/*******************************************************************************
 * Types
 ******************************************************************************/

/**
 * @brief Interrupt mappings
 *
 * Interrupt mappings for the available peripherals
 *
 */
typedef struct
{
    IRQn_Type fastIrq;
    IRQn_Type slowIrq;
    IRQn_Type errIrq;
} srx_interrupt_mapping_t;

/*******************************************************************************
 * Private data
 ******************************************************************************/

/**
 * Array with clock instances to SRX peripherals
 */
static const clock_names_t srxClock[SRX_INSTANCE_COUNT] =
{
    SENT0_CLK
#if (SRX_INSTANCE_COUNT > 1u)
    , SENT1_CLK
#endif
#if (SRX_INSTANCE_COUNT > 2u)
    , SENT2_CLK
#endif
#if (SRX_INSTANCE_COUNT > 3u)
    , SENT3_CLK
#endif
#if (SRX_INSTANCE_COUNT > 4u)
    , SENT4_CLK
#endif
#if (SRX_INSTANCE_COUNT > 5u)
    , SENT5_CLK
#endif
#if (SRX_INSTANCE_COUNT > 6u)
    , SENT6_CLK
#endif
#if (SRX_INSTANCE_COUNT > 7u)
    , SENT7_CLK
#endif
};

/**
 * Base pointers for the peripheral registers.
 */
static SRX_Type * const srxBase[SRX_INSTANCE_COUNT] = SRX_BASE_PTRS;

/**
 * DMA multiplexer for the Fast channel.
 */
static const dma_request_source_t srxFastDMASrc[SRX_INSTANCE_COUNT] =
{
    EDMA_REQ_SENT_0_FAST_MSG
#if (SRX_INSTANCE_COUNT > 1u)
    , EDMA_REQ_SENT_1_FAST_MSG
#endif
#if (SRX_INSTANCE_COUNT > 2u)
    , EDMA_REQ_SENT_2_FAST_MSG
#endif
#if (SRX_INSTANCE_COUNT > 3u)
    , EDMA_REQ_SENT_3_FAST_MSG
#endif
#if (SRX_INSTANCE_COUNT > 4u)
    , EDMA_REQ_SENT_4_FAST_MSG
#endif
#if (SRX_INSTANCE_COUNT > 5u)
    , EDMA_REQ_SENT_5_FAST_MSG
#endif
#if (SRX_INSTANCE_COUNT > 6u)
    , EDMA_REQ_SENT_6_FAST_MSG
#endif
#if (SRX_INSTANCE_COUNT > 7u)
    , EDMA_REQ_SENT_7_FAST_MSG
#endif
};

/**
 * DMA multiplexer for the Slow channel.
 */
static const dma_request_source_t srxSlowDMASrc[SRX_INSTANCE_COUNT] =
{
    EDMA_REQ_SENT_0_SLOW_MSG
#if (SRX_INSTANCE_COUNT > 1u)
    , EDMA_REQ_SENT_1_SLOW_MSG
#endif
#if (SRX_INSTANCE_COUNT > 2u)
    , EDMA_REQ_SENT_2_SLOW_MSG
#endif
#if (SRX_INSTANCE_COUNT > 3u)
    , EDMA_REQ_SENT_3_SLOW_MSG
#endif
#if (SRX_INSTANCE_COUNT > 4u)
    , EDMA_REQ_SENT_4_SLOW_MSG
#endif
#if (SRX_INSTANCE_COUNT > 5u)
    , EDMA_REQ_SENT_5_SLOW_MSG
#endif
#if (SRX_INSTANCE_COUNT > 6u)
    , EDMA_REQ_SENT_6_SLOW_MSG
#endif
#if (SRX_INSTANCE_COUNT > 7u)
    , EDMA_REQ_SENT_7_SLOW_MSG
#endif
};

/**
 * Array containing interrupt mappings.
 */
static const srx_interrupt_mapping_t srxInterruptMappings[SRX_INSTANCE_COUNT][SRX_CHANNEL_COUNT] =
{
    {
        {SENT0_Fast_0_IRQn, SENT0_Slow_0_IRQn, SENT0_RxErr_0_IRQn}
    #if (SRX_CHANNEL_COUNT > 1u)
        , {SENT0_Fast_1_IRQn, SENT0_Slow_1_IRQn, SENT0_RxErr_1_IRQn}
    #endif
    #if (SRX_CHANNEL_COUNT > 2u)
        , {SENT0_Fast_2_IRQn, SENT0_Slow_2_IRQn, SENT0_RxErr_2_IRQn}
    #endif
    #if (SRX_CHANNEL_COUNT > 3u)
        , {SENT0_Fast_3_IRQn, SENT0_Slow_3_IRQn, SENT0_RxErr_3_IRQn}
    #endif
    #if (SRX_CHANNEL_COUNT > 4u)
        , {SENT0_Fast_4_IRQn, SENT0_Slow_4_IRQn, SENT0_RxErr_4_IRQn}
    #endif
    #if (SRX_CHANNEL_COUNT > 5u)
        , {SENT0_Fast_5_IRQn, SENT0_Slow_5_IRQn, SENT0_RxErr_5_IRQn}
    #endif
    #if (SRX_CHANNEL_COUNT > 6u)
        , {SENT0_Fast_6_IRQn, SENT0_Slow_6_IRQn, SENT0_RxErr_6_IRQn}
    #endif
    #if (SRX_CHANNEL_COUNT > 7u)
        , {SENT0_Fast_7_IRQn, SENT0_Slow_7_IRQn, SENT0_RxErr_7_IRQn}
    #endif
    }
#if (SRX_INSTANCE_COUNT > 1u)
    , {
            {SENT1_Fast_0_IRQn, SENT1_Slow_0_IRQn, SENT1_RxErr_0_IRQn}
        #if (SRX_CHANNEL_COUNT > 1u)
            , {SENT1_Fast_1_IRQn, SENT1_Slow_1_IRQn, SENT1_RxErr_1_IRQn}
        #endif
        #if (SRX_CHANNEL_COUNT > 2u)
            , {SENT1_Fast_2_IRQn, SENT1_Slow_2_IRQn, SENT1_RxErr_2_IRQn}
        #endif
        #if (SRX_CHANNEL_COUNT > 3u)
            , {SENT1_Fast_3_IRQn, SENT1_Slow_3_IRQn, SENT1_RxErr_3_IRQn}
        #endif
        #if (SRX_CHANNEL_COUNT > 4u)
            , {SENT1_Fast_4_IRQn, SENT1_Slow_4_IRQn, SENT1_RxErr_4_IRQn}
        #endif
        #if (SRX_CHANNEL_COUNT > 5u)
            , {SENT1_Fast_5_IRQn, SENT1_Slow_5_IRQn, SENT1_RxErr_5_IRQn}
        #endif
        #if (SRX_CHANNEL_COUNT > 6u)
            , {SENT1_Fast_6_IRQn, SENT1_Slow_6_IRQn, SENT1_RxErr_6_IRQn}
        #endif
        #if (SRX_CHANNEL_COUNT > 7u)
            , {SENT1_Fast_7_IRQn, SENT1_Slow_7_IRQn, SENT1_RxErr_7_IRQn}
        #endif
    }
#endif
#if (SRX_INSTANCE_COUNT > 2u)
    , {
            {SENT2_Fast_0_IRQn, SENT2_Slow_0_IRQn, SENT2_RxErr_0_IRQn}
        #if (SRX_CHANNEL_COUNT > 1u)
            , {SENT2_Fast_1_IRQn, SENT2_Slow_1_IRQn, SENT2_RxErr_1_IRQn}
        #endif
        #if (SRX_CHANNEL_COUNT > 2u)
            , {SENT2_Fast_2_IRQn, SENT2_Slow_2_IRQn, SENT2_RxErr_2_IRQn}
        #endif
        #if (SRX_CHANNEL_COUNT > 3u)
            , {SENT2_Fast_3_IRQn, SENT2_Slow_3_IRQn, SENT2_RxErr_3_IRQn}
        #endif
        #if (SRX_CHANNEL_COUNT > 4u)
            , {SENT2_Fast_4_IRQn, SENT2_Slow_4_IRQn, SENT2_RxErr_4_IRQn}
        #endif
        #if (SRX_CHANNEL_COUNT > 5u)
            , {SENT2_Fast_5_IRQn, SENT2_Slow_5_IRQn, SENT2_RxErr_5_IRQn}
        #endif
        #if (SRX_CHANNEL_COUNT > 6u)
            , {SENT2_Fast_6_IRQn, SENT2_Slow_6_IRQn, SENT2_RxErr_6_IRQn}
        #endif
        #if (SRX_CHANNEL_COUNT > 7u)
            , {SENT2_Fast_7_IRQn, SENT2_Slow_7_IRQn, SENT2_RxErr_7_IRQn}
        #endif
    }
#endif
#if (SRX_INSTANCE_COUNT > 3u)
    , {
            {SENT3_Fast_0_IRQn, SENT3_Slow_0_IRQn, SENT3_RxErr_0_IRQn}
        #if (SRX_CHANNEL_COUNT > 1u)
            , {SENT3_Fast_1_IRQn, SENT3_Slow_1_IRQn, SENT3_RxErr_1_IRQn}
        #endif
        #if (SRX_CHANNEL_COUNT > 2u)
            , {SENT3_Fast_2_IRQn, SENT3_Slow_2_IRQn, SENT3_RxErr_2_IRQn}
        #endif
        #if (SRX_CHANNEL_COUNT > 3u)
            , {SENT3_Fast_3_IRQn, SENT3_Slow_3_IRQn, SENT3_RxErr_3_IRQn}
        #endif
        #if (SRX_CHANNEL_COUNT > 4u)
            , {SENT3_Fast_4_IRQn, SENT3_Slow_4_IRQn, SENT3_RxErr_4_IRQn}
        #endif
        #if (SRX_CHANNEL_COUNT > 5u)
            , {SENT3_Fast_5_IRQn, SENT3_Slow_5_IRQn, SENT3_RxErr_5_IRQn}
        #endif
        #if (SRX_CHANNEL_COUNT > 6u)
            , {SENT3_Fast_6_IRQn, SENT3_Slow_6_IRQn, SENT3_RxErr_6_IRQn}
        #endif
        #if (SRX_CHANNEL_COUNT > 7u)
            , {SENT3_Fast_7_IRQn, SENT3_Slow_7_IRQn, SENT3_RxErr_7_IRQn}
        #endif
    }
#endif
#if (SRX_INSTANCE_COUNT > 4u)
    , {
            {SENT4_Fast_0_IRQn, SENT4_Slow_0_IRQn, SENT4_RxErr_0_IRQn}
        #if (SRX_CHANNEL_COUNT > 1u)
            , {SENT4_Fast_1_IRQn, SENT4_Slow_1_IRQn, SENT4_RxErr_1_IRQn}
        #endif
        #if (SRX_CHANNEL_COUNT > 2u)
            , {SENT4_Fast_2_IRQn, SENT4_Slow_2_IRQn, SENT4_RxErr_2_IRQn}
        #endif
        #if (SRX_CHANNEL_COUNT > 3u)
            , {SENT4_Fast_3_IRQn, SENT4_Slow_3_IRQn, SENT4_RxErr_3_IRQn}
        #endif
        #if (SRX_CHANNEL_COUNT > 4u)
            , {SENT4_Fast_4_IRQn, SENT4_Slow_4_IRQn, SENT4_RxErr_4_IRQn}
        #endif
        #if (SRX_CHANNEL_COUNT > 5u)
            , {SENT4_Fast_5_IRQn, SENT4_Slow_5_IRQn, SENT4_RxErr_5_IRQn}
        #endif
        #if (SRX_CHANNEL_COUNT > 6u)
            , {SENT4_Fast_6_IRQn, SENT4_Slow_6_IRQn, SENT4_RxErr_6_IRQn}
        #endif
        #if (SRX_CHANNEL_COUNT > 7u)
            , {SENT4_Fast_7_IRQn, SENT4_Slow_7_IRQn, SENT4_RxErr_7_IRQn}
        #endif
    }
#endif
#if (SRX_INSTANCE_COUNT > 5u)
    , {
            {SENT5_Fast_0_IRQn, SENT5_Slow_0_IRQn, SENT5_RxErr_0_IRQn}
        #if (SRX_CHANNEL_COUNT > 1u)
            , {SENT5_Fast_1_IRQn, SENT5_Slow_1_IRQn, SENT5_RxErr_1_IRQn}
        #endif
        #if (SRX_CHANNEL_COUNT > 2u)
            , {SENT5_Fast_2_IRQn, SENT5_Slow_2_IRQn, SENT5_RxErr_2_IRQn}
        #endif
        #if (SRX_CHANNEL_COUNT > 3u)
            , {SENT5_Fast_3_IRQn, SENT5_Slow_3_IRQn, SENT5_RxErr_3_IRQn}
        #endif
        #if (SRX_CHANNEL_COUNT > 4u)
            , {SENT5_Fast_4_IRQn, SENT5_Slow_4_IRQn, SENT5_RxErr_4_IRQn}
        #endif
        #if (SRX_CHANNEL_COUNT > 5u)
            , {SENT5_Fast_5_IRQn, SENT5_Slow_5_IRQn, SENT5_RxErr_5_IRQn}
        #endif
        #if (SRX_CHANNEL_COUNT > 6u)
            , {SENT5_Fast_6_IRQn, SENT5_Slow_6_IRQn, SENT5_RxErr_6_IRQn}
        #endif
        #if (SRX_CHANNEL_COUNT > 7u)
            , {SENT5_Fast_7_IRQn, SENT5_Slow_7_IRQn, SENT5_RxErr_7_IRQn}
        #endif
    }
#endif
#if (SRX_INSTANCE_COUNT > 6u)
    , {
            {SENT6_Fast_0_IRQn, SENT6_Slow_0_IRQn, SENT6_RxErr_0_IRQn}
        #if (SRX_CHANNEL_COUNT > 1u)
            , {SENT6_Fast_1_IRQn, SENT6_Slow_1_IRQn, SENT6_RxErr_1_IRQn}
        #endif
        #if (SRX_CHANNEL_COUNT > 2u)
            , {SENT6_Fast_2_IRQn, SENT6_Slow_2_IRQn, SENT6_RxErr_2_IRQn}
        #endif
        #if (SRX_CHANNEL_COUNT > 3u)
            , {SENT6_Fast_3_IRQn, SENT6_Slow_3_IRQn, SENT6_RxErr_3_IRQn}
        #endif
        #if (SRX_CHANNEL_COUNT > 4u)
            , {SENT6_Fast_4_IRQn, SENT6_Slow_4_IRQn, SENT6_RxErr_4_IRQn}
        #endif
        #if (SRX_CHANNEL_COUNT > 5u)
            , {SENT6_Fast_5_IRQn, SENT6_Slow_5_IRQn, SENT6_RxErr_5_IRQn}
        #endif
        #if (SRX_CHANNEL_COUNT > 6u)
            , {SENT6_Fast_6_IRQn, SENT6_Slow_6_IRQn, SENT6_RxErr_6_IRQn}
        #endif
        #if (SRX_CHANNEL_COUNT > 7u)
            , {SENT6_Fast_7_IRQn, SENT6_Slow_7_IRQn, SENT6_RxErr_7_IRQn}
        #endif
    }
#endif
#if (SRX_INSTANCE_COUNT > 7u)
    , {
            {SENT7_Fast_0_IRQn, SENT7_Slow_0_IRQn, SENT7_RxErr_0_IRQn}
        #if (SRX_CHANNEL_COUNT > 1u)
            , {SENT7_Fast_1_IRQn, SENT7_Slow_1_IRQn, SENT7_RxErr_1_IRQn}
        #endif
        #if (SRX_CHANNEL_COUNT > 2u)
            , {SENT7_Fast_2_IRQn, SENT7_Slow_2_IRQn, SENT7_RxErr_2_IRQn}
        #endif
        #if (SRX_CHANNEL_COUNT > 3u)
            , {SENT7_Fast_3_IRQn, SENT7_Slow_3_IRQn, SENT7_RxErr_3_IRQn}
        #endif
        #if (SRX_CHANNEL_COUNT > 4u)
            , {SENT7_Fast_4_IRQn, SENT7_Slow_4_IRQn, SENT7_RxErr_4_IRQn}
        #endif
        #if (SRX_CHANNEL_COUNT > 5u)
            , {SENT7_Fast_5_IRQn, SENT7_Slow_5_IRQn, SENT7_RxErr_5_IRQn}
        #endif
        #if (SRX_CHANNEL_COUNT > 6u)
            , {SENT7_Fast_6_IRQn, SENT7_Slow_6_IRQn, SENT7_RxErr_6_IRQn}
        #endif
        #if (SRX_CHANNEL_COUNT > 7u)
            , {SENT7_Fast_7_IRQn, SENT7_Slow_7_IRQn, SENT7_RxErr_7_IRQn}
        #endif
    }
#endif
};

/**
 * Pointers to the configuration structures
 */
static srx_state_t * srxStatePtr[SRX_INSTANCE_COUNT];

/**
 * Base image of the configuration structure used for DMA transfers.
 */
static const edma_transfer_config_t srxDmaBaseConfig =
{
    .srcAddr = 0u, /* Modified by function */
    .destAddr = 0u, /* Modified by function */
    .srcTransferSize = EDMA_TRANSFER_SIZE_4B,
    .destTransferSize = EDMA_TRANSFER_SIZE_4B,
    .srcOffset = 0, /* Modified by function */
    .destOffset = (int16_t)sizeof(uint32_t),
    .srcLastAddrAdjust = 0, /* Modified by function */
    .destLastAddrAdjust = 0, /* Modified by function */
    .srcModulo = EDMA_MODULO_OFF,
    .destModulo = EDMA_MODULO_OFF,
    .minorByteTransferCount = 0, /* Modified by function */
    .scatterGatherEnable = false,
    .scatterGatherNextDescAddr = 0u,
    .interruptEnable = true,
    .loopTransferConfig = &(edma_loop_transfer_config_t)
    {
        .majorLoopIterationCount = 1u,
        .srcOffsetEnable = true,
        .dstOffsetEnable = true,
        .minorLoopOffset = 0,
        .minorLoopChnLinkEnable = false,
        .minorLoopChnLinkNumber = 0u,
        .majorLoopChnLinkEnable = false,
        .majorLoopChnLinkNumber = 0u
    }
};

/*******************************************************************************
 * Private functions
 ******************************************************************************/

/*FUNCTION**********************************************************************
 *
 * Function Name : SRX_DRV_InitDiag
 * Description   : Initializes the diagnostics side of the driver.
 *
 *END**************************************************************************/
static void SRX_DRV_InitDiag(uint32_t instance, uint8_t channel, const srx_diag_config_t * config)
{
    /* Set bus IDLE count */
    SRX_DRV_HW_SetBusIdleCnt(srxBase[instance], channel, config->idleCount);

    /* Set calibration range */
    SRX_DRV_HW_SetCalRng(srxBase[instance], channel, config->calibVar);

    /* Set pause pulse diag check */
    SRX_DRV_HW_SetPpChkSel(srxBase[instance], channel, config->diagPulse);

    /* Set pause pulse enable status */
    SRX_DRV_HW_SetPausePulseEnable(srxBase[instance], channel, config->pausePulse);

    /* Successive calibration check */
    SRX_DRV_HW_SetSuccCalChk(srxBase[instance], channel, config->succesiveCal);
}

/*FUNCTION**********************************************************************
 *
 * Function Name : SRX_DRV_InitSlowMes
 * Description   : Initializes the Slow message reception side of the driver.
 *
 *END**************************************************************************/
static void SRX_DRV_InitSlowMsg(uint32_t instance, uint8_t channel, const srx_slow_msg_config_t * config)
{
    /* CRC related */
    SRX_DRV_HW_SetSlowCrcType(srxBase[instance], channel, config->crcType);
}

/*FUNCTION**********************************************************************
 *
 * Function Name : SRX_DRV_InitFastMes
 * Description   : Initializes the Fast message reception side of the driver.
 *
 *END**************************************************************************/
static void SRX_DRV_InitFastMsg(uint32_t instance, uint8_t channel, const srx_fast_msg_config_t * config)
{
    /* Number of nibbles */
    SRX_DRV_HW_SetFastNumNibbles(srxBase[instance], channel, config->numberOfNibbles);

    /* CRC related */
    SRX_DRV_HW_SetFastCrcType(srxBase[instance], channel, config->crcType);
    SRX_DRV_HW_SetFastCrcIncStatus(srxBase[instance], channel, config->crcIncludeStatus);
    SRX_DRV_HW_SetFastDisableCrc(srxBase[instance], channel, config->disableCrcCheck);
}

/*FUNCTION**********************************************************************
 *
 * Function Name : SRX_DRV_ComputeTimestampBase
 * Description   : Computes the message timestamp base prescaler.
 *
 *END**************************************************************************/
static uint8_t SRX_DRV_ComputeTimestampBase(uint32_t clock)
{
    uint32_t tsPre;

    /* 0 in worst case */
    tsPre = (uint32_t)(clock / SRX_ONE_MICROSECOND_CYCLES) - 1u;
    /* Saturate at upper interval */
    tsPre = (tsPre > 255u) ? 255u : tsPre;

    return (uint8_t)tsPre;
}

/*FUNCTION**********************************************************************
 *
 * Function Name : SRX_DRV_ComputeChannelPrescaler
 * Description   : Computes the channel reception clock prescaler.
 *
 *END**************************************************************************/
static uint32_t SRX_DRV_ComputeChannelPrescaler(uint8_t reqTick, uint32_t clock)
{
    uint32_t chPre;

    /* Compute and set the channel prescaler */
    chPre = clock / 1000u; /* Keep info on fractional frequencies */

    chPre = (reqTick * chPre) / 1000u;

    return chPre;
}

/*FUNCTION**********************************************************************
 *
 * Function Name : SRX_DRV_FastRxCompleteDma
 * Description   : Reception complete DMA notification
 * for the Fast channel.
 *
 *END**************************************************************************/
static void SRX_DRV_FastRxCompleteDma(void * parameter, edma_chn_status_t status)
{
    uint8_t instance = *(const uint8_t *)parameter;

    DEV_ASSERT(instance < SRX_INSTANCE_COUNT);

    if (status == EDMA_CHN_NORMAL)
    {
        DEV_ASSERT(srxStatePtr[instance] != NULL);

        /* Call the notification */
        if(srxStatePtr[instance]->callbackFunc.function != NULL)
        {
            srxStatePtr[instance]->callbackFunc.function(instance, 0u, /* Channel always passed as 0 in DMA mode */
                    SRX_CALLBACK_FAST_DMA_RX_COMPLETE, srxStatePtr[instance]->callbackFunc.param);
        }
    }
}

/*FUNCTION**********************************************************************
 *
 * Function Name : SRX_DRV_SlowRxCompleteDma
 * Description   : Reception complete DMA notification
 * for the Slow channel.
 *
 *END**************************************************************************/
static void SRX_DRV_SlowRxCompleteDma(void * parameter, edma_chn_status_t status)
{
    uint8_t instance = *(const uint8_t *)parameter;

    DEV_ASSERT(instance < SRX_INSTANCE_COUNT);

    if (status == EDMA_CHN_NORMAL)
    {
        DEV_ASSERT(srxStatePtr[instance] != NULL);

        /* Call the notification */
        if(srxStatePtr[instance]->callbackFunc.function != NULL)
        {
            srxStatePtr[instance]->callbackFunc.function(instance, 0u, /* Channel always passed as 0 in DMA mode */
                    SRX_CALLBACK_SLOW_DMA_RX_COMPLETE, srxStatePtr[instance]->callbackFunc.param);
        }
    }
}

/*FUNCTION**********************************************************************
 *
 * Function Name : SRX_DRV_ConfigureDmaTransfer
 * Description   : Configures a loop DMA transfer for the given channel.
 *
 *END**************************************************************************/
static void SRX_DRV_ConfigureDmaTransfer(uint8_t channel, uint32_t srcAddr, uint32_t destAddr, bool hasFifo, uint8_t lenFifo)
{
    /* Generate the structure. Passing it on the stack is OK
     * since the values are copied to peripheral registers */
    edma_transfer_config_t transferConfig = srxDmaBaseConfig;

    /* Modify source and destination */
    transferConfig.srcAddr = srcAddr;
    transferConfig.destAddr = destAddr;

    /* Depending on case, configure the parameters */
    if(hasFifo)
    {
        transferConfig.srcOffset = 0;
        transferConfig.minorByteTransferCount = lenFifo * sizeof(srx_raw_msg_t);
        transferConfig.destLastAddrAdjust = -((int32_t)lenFifo * (int32_t)sizeof(srx_raw_msg_t));
        transferConfig.srcLastAddrAdjust = 0;
    }
    else
    {
        transferConfig.srcOffset = (int16_t)(sizeof(uint32_t));
        transferConfig.minorByteTransferCount = sizeof(srx_raw_msg_t);
        transferConfig.destLastAddrAdjust = -((int32_t)sizeof(srx_raw_msg_t));
        transferConfig.srcLastAddrAdjust = -((int32_t)sizeof(srx_raw_msg_t));
    }

    /* Configure a loop transfer */
    (void)EDMA_DRV_ConfigLoopTransfer(channel, &transferConfig);
}

/*FUNCTION**********************************************************************
 *
 * Function Name : SRX_DRV_InstallCallbacks
 * Description   : Installs callbacks for the driver events.
 *
 *END**************************************************************************/
static void SRX_DRV_InstallCallbacks(uint32_t instance)
{
    uint32_t chInd;
    bool fastDmaSet = false;
    bool slowDmaSet = false;
    srx_state_t * lState;

    DEV_ASSERT(instance < SRX_INSTANCE_COUNT);
    DEV_ASSERT(srxStatePtr[instance] != NULL);

    lState = srxStatePtr[instance];

    /* Check for valid function first */
    if(lState->callbackFunc.function != NULL)
    {
        for(chInd = 0u; chInd < SRX_CHANNEL_COUNT; chInd++)
        {
            /* Only on active channel */
            if(lState->activeChannels[chInd])
            {
                /* Rx events only if no bits are set in mask */
                if((lState->channelEvents[chInd] & SRX_EV_ALL) != 0u)
                {
                    SRX_DRV_HW_SetEventConfig(srxBase[lState->instanceId], (uint8_t)chInd, lState->channelEvents[chInd]);
                    INT_SYS_EnableIRQ(srxInterruptMappings[lState->instanceId][chInd].errIrq);
                }
                else
                {
                    SRX_DRV_HW_SetEventConfig(srxBase[lState->instanceId], (uint8_t)chInd, SRX_EV_NONE);
                    INT_SYS_DisableIRQ(srxInterruptMappings[lState->instanceId][chInd].errIrq);
                }

                /* Fast events */
                if(lState->fastDmaEnabled[chInd])
                {
                    /* Mark to be set later */
                    fastDmaSet = true;

                    /* This means that there will be no more fast Channel Rx interrupts since DMA is enabled. */
                    SRX_DRV_HW_SetFastRxInterruptStatus(srxBase[lState->instanceId], (uint8_t)chInd, false);
                    INT_SYS_DisableIRQ(srxInterruptMappings[lState->instanceId][chInd].fastIrq);
                }
                else
                {
                    /* Enable channel interrupt, since DMA is not enabled */
                    SRX_DRV_HW_SetFastRxInterruptStatus(srxBase[lState->instanceId], (uint8_t)chInd, true);
                    INT_SYS_EnableIRQ(srxInterruptMappings[lState->instanceId][chInd].fastIrq);
                }

                /* Slow events */
                if(lState->slowDmaEnabled[chInd])
                {
                    /* Mark to be set later */
                    slowDmaSet = true;

                    /* This means that there will be no more fast Channel Rx interrupts since DMA is enabled. */
                    SRX_DRV_HW_SetSlowRxInterruptStatus(srxBase[lState->instanceId], (uint8_t)chInd, false);
                    INT_SYS_DisableIRQ(srxInterruptMappings[lState->instanceId][chInd].slowIrq);
                }
                else
                {
                    /* Enable channel interrupt, since DMA is not enabled */
                    SRX_DRV_HW_SetSlowRxInterruptStatus(srxBase[lState->instanceId], (uint8_t)chInd, true);
                    INT_SYS_EnableIRQ(srxInterruptMappings[lState->instanceId][chInd].slowIrq);
                }
            }
        }

        /* Fast DMA callbacks (global for all channels) */
        if(fastDmaSet)
        {
            (void)EDMA_DRV_InstallCallback(lState->fastDmaChannel,
                                           &SRX_DRV_FastRxCompleteDma,
                                           &lState->instanceId);
        }

        /* Slow DMA callbacks (global for all channels) */
        if(slowDmaSet)
        {
            (void)EDMA_DRV_InstallCallback(lState->slowDmaChannel,
                                           &SRX_DRV_SlowRxCompleteDma,
                                           &lState->instanceId);
        }
    }
    else /* if(lState->callbackFunc.function != NULL) Disable all callbacks in case the function was removed */
    {
        for(chInd = 0u; chInd < SRX_CHANNEL_COUNT; chInd++)
        {
            /* Only on active channel */
            if(lState->activeChannels[chInd])
            {
                /* Fast events */
                if(lState->fastDmaEnabled[chInd])
                {
                    /* Mark to be set later */
                    fastDmaSet = true;
                }

                /* Slow events */
                if(lState->slowDmaEnabled[chInd])
                {
                    /* Mark to be set later */
                    slowDmaSet = true;
                }

                /* Disable Diagnostics interrupts */
                SRX_DRV_HW_SetEventConfig(srxBase[lState->instanceId], (uint8_t)chInd, SRX_EV_NONE);
                INT_SYS_DisableIRQ(srxInterruptMappings[lState->instanceId][chInd].errIrq);

                /* Disable Fast interrupts */
                SRX_DRV_HW_SetFastRxInterruptStatus(srxBase[lState->instanceId], (uint8_t)chInd, false);
                INT_SYS_DisableIRQ(srxInterruptMappings[lState->instanceId][chInd].fastIrq);

                /* Disable slow interrupts */
                SRX_DRV_HW_SetSlowRxInterruptStatus(srxBase[lState->instanceId], (uint8_t)chInd, false);
                INT_SYS_DisableIRQ(srxInterruptMappings[lState->instanceId][chInd].slowIrq);
            }
        }

        /* Only if active */
        if(fastDmaSet)
        {
            (void)EDMA_DRV_InstallCallback(lState->fastDmaChannel,
                                           ((void *)0u),
                                           &lState->instanceId);
        }

        /* Only if active */
        if(slowDmaSet)
        {
            (void)EDMA_DRV_InstallCallback(lState->slowDmaChannel,
                                           ((void *)0u),
                                           &lState->instanceId);
        }
    }
}

/*******************************************************************************
 * Public functions
 ******************************************************************************/

/*FUNCTION**********************************************************************
 *
 * Function Name : SRX_DRV_IRQ_FastInstNChanNHandler
 * Description   : Gets called from the low level handler
 * with instance and channel as parameter.
 *
 *END**************************************************************************/
void SRX_DRV_IRQ_FastInstNChanNHandler(uint32_t instance, uint32_t channel)
{
    DEV_ASSERT(instance < SRX_INSTANCE_COUNT);
    DEV_ASSERT(channel < SRX_CHANNEL_COUNT);

    if(srxStatePtr[instance] != NULL)
    {
        /* Check if channel is valid first */
        if(srxStatePtr[instance]->activeChannels[channel])
        {

#if defined(ERRATA_E7425) /* Enable only when we have this active */
            if (SRX_DRV_HW_ErrataE7425Workaroud(srxBase[instance], instance, channel))
            {
#endif /* defined(ERRATA_E7425) */

            if(srxStatePtr[instance]->callbackFunc.function != NULL)
            {
                srxStatePtr[instance]->callbackFunc.function(instance, channel,
                        SRX_CALLBACK_FAST_RX_COMPLETE, srxStatePtr[instance]->callbackFunc.param);
            }

#if defined(ERRATA_E7425)
            }
#endif /* defined(ERRATA_E7425) */
        }
    }
}

/*FUNCTION**********************************************************************
 *
 * Function Name : SRX_DRV_IRQ_SlowInstNChanNHandler
 * Description   : Gets called from the low level handler
 * with instance and channel as parameter.
 *
 *END**************************************************************************/
void SRX_DRV_IRQ_SlowInstNChanNHandler(uint32_t instance, uint32_t channel)
{
    DEV_ASSERT(instance < SRX_INSTANCE_COUNT);
    DEV_ASSERT(channel < SRX_CHANNEL_COUNT);

    if(srxStatePtr[instance] != NULL)
    {
        /* Check if channel is valid first */
        if(srxStatePtr[instance]->activeChannels[channel])
        {
            if(srxStatePtr[instance]->callbackFunc.function != NULL)
            {
                srxStatePtr[instance]->callbackFunc.function(instance, channel,
                        SRX_CALLBACK_SLOW_RX_COMPLETE, srxStatePtr[instance]->callbackFunc.param);
            }
        }
    }
}

/*FUNCTION**********************************************************************
 *
 * Function Name : SRX_DRV_IRQ_RxErrInstNChanNHandler
 * Description   : Gets called from the low level handler
 * with instance and channel as parameter.
 *
 *END**************************************************************************/
void SRX_DRV_IRQ_RxErrInstNChanNHandler(uint32_t instance, uint32_t channel)
{
    DEV_ASSERT(instance < SRX_INSTANCE_COUNT);
    DEV_ASSERT(channel < SRX_CHANNEL_COUNT);

    if(srxStatePtr[instance] != NULL)
    {
        /* Check if channel is valid first */
        if(srxStatePtr[instance]->activeChannels[channel])
        {

#if defined(ERRATA_E7425) /* Enable only when we have this active */
            if (SRX_DRV_HW_ErrataE7425Workaroud(srxBase[instance], instance, channel))
            {
#endif /* defined(ERRATA_E7425) */

            if(srxStatePtr[instance]->callbackFunc.function != NULL)
            {
                srxStatePtr[instance]->callbackFunc.function(instance, channel,
                        SRX_CALLBACK_RX_ERROR, srxStatePtr[instance]->callbackFunc.param);
            }

#if defined(ERRATA_E7425)
            }
#endif /* defined(ERRATA_E7425) */
        }
    }
}

/*! @endcond */

/*******************************************************************************
 * API implementation
 ******************************************************************************/

/*FUNCTION**********************************************************************
 *
 * Function Name : SRX_DRV_Init
 * Description   : Initializes the driver for a given peripheral
 * according to the given configuration structure.
 *
 * Implements    : SRX_DRV_Init_Activity
 *END**************************************************************************/
status_t SRX_DRV_Init(uint32_t instance, const srx_driver_user_config_t * configPtr, srx_state_t * state)
{
    uint32_t inputClock;
    uint8_t chInd;
    bool fDmaEn = false;
    bool sDmaEn = false;
    status_t initErr;
    const srx_channel_config_t * chPtr;
    status_t retVal;

    /* Check for correct calling parameters */
    DEV_ASSERT(instance < SRX_INSTANCE_COUNT);
    DEV_ASSERT(configPtr != NULL);
    DEV_ASSERT(configPtr->numOfConfigs <= SRX_CHANNEL_COUNT);

    /* Only if not already initialized */
    if(srxStatePtr[instance] != NULL)
    {
        retVal = STATUS_ERROR;
    }
    else
    {
        /* Copy the references */
        srxStatePtr[instance] = state;

        /* Populate the configuration structure */
        state->fastMsgDmaPtr = configPtr->fastMsgDmaPtr;
        state->slowMsgDmaPtr = configPtr->slowMsgDmaPtr;
        state->fastDmaChannel = configPtr->fastDmaChannel;
        state->slowDmaChannel = configPtr->slowDmaChannel;
        state->callbackFunc = configPtr->callbackFunc;
        state->instanceId = (uint8_t)instance;

        /* Get the protocol clock frequency */
        initErr = CLOCK_SYS_GetFreq(srxClock[instance], &inputClock);
        DEV_ASSERT(initErr == STATUS_SUCCESS);
        DEV_ASSERT(inputClock >= SRX_ONE_MICROSECOND_CYCLES);
        (void) initErr;

        /* Set register time base */
        SRX_DRV_HW_SetTsPrescaler(srxBase[instance], SRX_DRV_ComputeTimestampBase(inputClock));

        /* Setup the channels */
        for(chInd = 0u; chInd < configPtr->numOfConfigs; chInd++)
        {
            /* Only if we have a valid channel */
            DEV_ASSERT(configPtr->channelConfig[chInd].channelId < SRX_CHANNEL_COUNT);

            /* Obtain a pointer to channel config */
            chPtr = &configPtr->channelConfig[chInd];

            /* Mark channel as active */
            state->activeChannels[chPtr->channelId] = true;

            /* Copy DMA status */
            state->fastDmaEnabled[chPtr->channelId] = chPtr->fastMsgConfig.dmaEnable;
            state->slowDmaEnabled[chPtr->channelId] = chPtr->slowMsgConfig.dmaEnable;

            /* Store active events */
            state->channelEvents[chPtr->channelId] = chPtr->diagConfig.diagEvents;

            /* pre-scaler and compensation */
            SRX_DRV_HW_SetChannelPrescaler(srxBase[instance], chPtr->channelId,
                    (uint16_t)SRX_DRV_ComputeChannelPrescaler(chPtr->tickDuration, inputClock));

            SRX_DRV_HW_SetChannelCompensationState(srxBase[instance], chPtr->channelId, true);

            /* DMA enablers */
            if(chPtr->fastMsgConfig.dmaEnable)
            {
                SRX_DRV_HW_SetFastDmaStatus(srxBase[instance], chPtr->channelId, true);
                fDmaEn = true;
            }

            if(chPtr->slowMsgConfig.dmaEnable)
            {
                SRX_DRV_HW_SetSlowDmaStatus(srxBase[instance], chPtr->channelId, true);
                sDmaEn = true;
            }

            /* Setup diagnostics */
            SRX_DRV_InitDiag(instance, chPtr->channelId, &chPtr->diagConfig);

            /* Setup fast messages */
            SRX_DRV_InitFastMsg(instance, chPtr->channelId, &chPtr->fastMsgConfig);

            /* Setup slow messages */
            SRX_DRV_InitSlowMsg(instance, chPtr->channelId, &chPtr->slowMsgConfig);
        }

        /* Enable DMA transfers */
        if(fDmaEn)
        {
            /* Assures correct designation, rather than configuring it from DMA */
            (void)EDMA_DRV_SetChannelRequest(state->fastDmaChannel, (uint8_t)srxFastDMASrc[instance]);

            /* Configure the FIFO */
            if(configPtr->fastDmaFIFOEnable)
            {
                SRX_DRV_HW_SetFifoState(srxBase[instance], true);
                SRX_DRV_HW_SetFifoWm(srxBase[instance], configPtr->fastDmaFIFOSize);
            }

            /* Configure DMA */
            SRX_DRV_ConfigureDmaTransfer(state->fastDmaChannel,
                                         (uint32_t)SRX_DRV_HW_GetFastDmaRegStartAddr(srxBase[instance]),
                                         (uint32_t)state->fastMsgDmaPtr,
                                         configPtr->fastDmaFIFOEnable,
                                         configPtr->fastDmaFIFOSize);

            /* Start channel */
            (void)EDMA_DRV_StartChannel(state->fastDmaChannel);
        }

        if(sDmaEn)
        {
            /* Configure DMA */
            (void)EDMA_DRV_SetChannelRequest(state->slowDmaChannel, (uint8_t)srxSlowDMASrc[instance]);

            /* Configure DMA */
            SRX_DRV_ConfigureDmaTransfer(state->slowDmaChannel,
                                         (uint32_t)SRX_DRV_HW_GetSlowDmaRegStartAddr(srxBase[instance]),
                                         (uint32_t)state->slowMsgDmaPtr,
                                         false, 0u);

            /* Start channel */
            (void)EDMA_DRV_StartChannel(state->slowDmaChannel);
        }

        /* Setup notifications */
        SRX_DRV_InstallCallbacks(instance);

        /* Enable channels */
        for(chInd = 0u; chInd < SRX_CHANNEL_COUNT; chInd++)
        {
            if(state->activeChannels[chInd])
            {
                /* Enable channel */
                SRX_DRV_HW_SetChannelStatus(srxBase[instance], chInd, true);
            }
        }

        /* Enable peripheral */
        SRX_DRV_HW_SetPeripheralStatus(srxBase[instance], true);

        /* Success */
        retVal = STATUS_SUCCESS;
    }

    return retVal;
}

/*FUNCTION**********************************************************************
 *
 * Function Name : SRX_DRV_GetEvents
 * Description   : Returns a list containing masks for the current active events.
 * Also clears the active events in the process.
 *
 * Implements    : SRX_DRV_GetEvents_Activity
 *END**************************************************************************/
status_t SRX_DRV_GetEvents(uint32_t instance, uint32_t channel, srx_event_t * events)
{
    status_t retVal;
    srx_event_t locEv;

    /* Invalid instance or channel */
    DEV_ASSERT(instance < SRX_INSTANCE_COUNT);
    DEV_ASSERT(channel < SRX_CHANNEL_COUNT);
    DEV_ASSERT(srxStatePtr[instance] != NULL);

    /* Only if channel is enabled */
    if(srxStatePtr[instance]->activeChannels[channel] != false)
    {
        /* Get, clear and return */
        locEv = SRX_DRV_HW_GetActiveEvents(srxBase[instance], (uint8_t)channel);
        SRX_DRV_HW_ClearActiveEvents(srxBase[instance], (uint8_t)channel, locEv);
        *events = locEv;
        retVal = STATUS_SUCCESS;
    }
    else
    {
        retVal = STATUS_ERROR;
    }

    return retVal;
}

/*FUNCTION**********************************************************************
 *
 * Function Name : SRX_DRV_GetFastMsg
 * Description   : Returns last received fast message and clears the
 * Rx complete flag.
 *
 * Implements    : SRX_DRV_GetFastMsg_Activity
 *END**************************************************************************/
status_t SRX_DRV_GetFastMsg(uint32_t instance, uint32_t channel, srx_fast_msg_t * message)
{
    srx_raw_msg_t locMsg;
    status_t retVal;

    /* Invalid instance or channel */
    DEV_ASSERT(instance < SRX_INSTANCE_COUNT);
    DEV_ASSERT(channel < SRX_CHANNEL_COUNT);
    DEV_ASSERT(srxStatePtr[instance] != NULL);

    /* Only if channel is enabled */
    if(srxStatePtr[instance]->activeChannels[channel] != false)
    {
        SRX_DRV_HW_GetFastRawMsg(srxBase[instance], (uint8_t)channel, &locMsg);
        SRX_DRV_HW_ConvertFastRaw(message, &locMsg);
        retVal = STATUS_SUCCESS;
    }
    else
    {
        retVal = STATUS_ERROR;
    }

    return retVal;
}

/*FUNCTION**********************************************************************
 *
 * Function Name : SRX_DRV_GetSlowMsg
 * Description   : Returns last received slow message and clears the
 * Rx complete flag.
 *
 * Implements    : SRX_DRV_GetSlowMsg_Activity
 *END**************************************************************************/
status_t SRX_DRV_GetSlowMsg(uint32_t instance, uint32_t channel, srx_slow_msg_t * message)
{
    srx_raw_msg_t locMsg;
    status_t retVal;

    /* Invalid instance or channel */
    DEV_ASSERT(channel < SRX_CHANNEL_COUNT);
    DEV_ASSERT(instance < SRX_INSTANCE_COUNT);
    DEV_ASSERT(srxStatePtr[instance] != NULL);


    /* Only if channel is enabled */
    if(srxStatePtr[instance]->activeChannels[channel] != false)
    {
        SRX_DRV_HW_GetSlowRawMsg(srxBase[instance], (uint8_t)channel, &locMsg);
        SRX_DRV_HW_ConvertSlowRaw(message, &locMsg);
        retVal = STATUS_SUCCESS;
    }
    else
    {
        retVal = STATUS_ERROR;
    }

    return retVal;
}

/*FUNCTION**********************************************************************
 *
 * Function Name : SRX_DRV_GetFastMsgFromRaw
 * Description   : Transforms a RAW fast message into a normal fast message.
 *
 * Implements    : SRX_DRV_GetFastMsgFromRaw_Activity
 *END**************************************************************************/
void SRX_DRV_GetFastMsgFromRaw(srx_fast_msg_t * msg, const srx_raw_msg_t * rawMsg)
{
    /* Just call the conversion function */
    SRX_DRV_HW_ConvertFastRaw(msg, rawMsg);
}

/*FUNCTION**********************************************************************
 *
 * Function Name : SRX_DRV_GetSlowMsgFromRaw
 * Description   : Transforms a RAW slow message into a normal slow message.
 *
 * Implements    : SRX_DRV_GetSlowMsgFromRaw_Activity
 *END**************************************************************************/
void SRX_DRV_GetSlowMsgFromRaw(srx_slow_msg_t * msg, const srx_raw_msg_t * rawMsg)
{
    /* Just call the conversion function */
    SRX_DRV_HW_ConvertSlowRaw(msg, rawMsg);
}

/*FUNCTION**********************************************************************
 *
 * Function Name : SRX_DRV_GetSlowRxStatus
 * Description   : Returns the buffer status for any incoming SLOW message.
 *
 * Implements    : SRX_DRV_GetSlowRxStatus_Activity
 *END**************************************************************************/
bool SRX_DRV_GetSlowRxStatus(uint32_t instance, uint32_t channel)
{
    bool retVal;

    /* Invalid instance or channel */
    DEV_ASSERT(instance < SRX_INSTANCE_COUNT);
    DEV_ASSERT(channel < SRX_CHANNEL_COUNT);
    DEV_ASSERT(srxStatePtr[instance] != NULL);

    /* Only if channel is enabled */
    if(srxStatePtr[instance]->activeChannels[channel] != false)
    {
        retVal = SRX_DRV_HW_GetSlowRxStatus(srxBase[instance], (uint8_t)channel);
    }
    else
    {
        retVal = false;
    }

    return retVal;
}

/*FUNCTION**********************************************************************
 *
 * Function Name : SRX_DRV_GetFastRxStatus
 * Description   : Returns the buffer status for any incoming FAST message.
 *
 * Implements    : SRX_DRV_GetFastRxStatus_Activity
 *END**************************************************************************/
bool SRX_DRV_GetFastRxStatus(uint32_t instance, uint32_t channel)
{
    bool retVal;

    /* Invalid instance or channel */
    DEV_ASSERT(instance < SRX_INSTANCE_COUNT);
    DEV_ASSERT(channel < SRX_CHANNEL_COUNT);
    DEV_ASSERT(srxStatePtr[instance] != NULL);

    /* Only if channel is enabled */
    if(srxStatePtr[instance]->activeChannels[channel] != false)
    {
        retVal = SRX_DRV_HW_GetFastRxStatus(srxBase[instance], (uint8_t)channel);
    }
    else
    {
        retVal = false;
    }

    return retVal;
}

/*FUNCTION**********************************************************************
 *
 * Function Name : SRX_DRV_SetFastMsgDmaBuffer
 * Description   : Sets (modifies) the buffer in which the DMA driven
 * reception for Fast messages is made. Length of the
 * buffer must be (fastDmaFIFOSize)
 * bytes in case fastDmaFIFOEnable is TRUE.
 *
 * Implements    : SRX_DRV_SetFastMsgDmaBuffer_Activity
 *END**************************************************************************/
status_t SRX_DRV_SetFastMsgDmaBuffer(uint32_t instance, srx_raw_msg_t * buffer)
{
    /* Invalid instance */
    DEV_ASSERT(instance < SRX_INSTANCE_COUNT);
    DEV_ASSERT(srxStatePtr[instance] != NULL);

    /* Store and update */
    srxStatePtr[instance]->fastMsgDmaPtr = buffer;
    EDMA_DRV_SetDestAddr(srxStatePtr[instance]->fastDmaChannel, (uint32_t)buffer);

    return STATUS_SUCCESS;
}

/*FUNCTION**********************************************************************
 *
 * Function Name : SRX_DRV_SetRxCallbackFunction
 * Description   : Sets (modifies) the callback function assigned to the
 * peripheral instance
 *
 * Implements    : SRX_DRV_SetRxCallbackFunction_Activity
 *END**************************************************************************/
status_t SRX_DRV_SetRxCallbackFunction(uint32_t instance, srx_callback_func_t function, void * param)
{
    /* Invalid instance */
    DEV_ASSERT(instance < SRX_INSTANCE_COUNT);
    DEV_ASSERT(srxStatePtr[instance] != NULL);

    /* Copy to state structure */
    srxStatePtr[instance]->callbackFunc.function = function;
    srxStatePtr[instance]->callbackFunc.param = param;

    /* Notification setup */
    SRX_DRV_InstallCallbacks(instance);

    return STATUS_SUCCESS;
}

/*FUNCTION**********************************************************************
 *
 * Function Name : SRX_DRV_SetSlowMsgDmaBuffer
 * Description   : Sets (modifies) the buffer in which the DMA driven
 * reception for slow messages is made.
 *
 * Implements    : SRX_DRV_SetSlowMsgDmaBuffer_Activity
 *END**************************************************************************/
status_t SRX_DRV_SetSlowMsgDmaBuffer(uint32_t instance, srx_raw_msg_t * buffer)
{
    /* Invalid instance or channel */
    DEV_ASSERT(instance < SRX_INSTANCE_COUNT);
    DEV_ASSERT(srxStatePtr[instance] != NULL);

    /* Store and update */
    srxStatePtr[instance]->slowMsgDmaPtr = buffer;
    EDMA_DRV_SetDestAddr(srxStatePtr[instance]->slowDmaChannel, (uint32_t)buffer);

    return STATUS_SUCCESS;
}

/*FUNCTION**********************************************************************
 *
 * Function Name : SRX_DRV_DeInit
 * Description   : De-Initializes the peripheral and brings it's registers into a reset state.
 *
 * Implements    : SRX_DRV_DeInit_Activity
 *END**************************************************************************/
status_t SRX_DRV_Deinit(uint32_t instance)
{
    uint8_t indChan;

    /* Invalid instance or channel */
    DEV_ASSERT(instance < SRX_INSTANCE_COUNT);
    DEV_ASSERT(srxStatePtr[instance] != NULL);

    for(indChan = 0u; indChan < SRX_CHANNEL_COUNT; indChan++)
    {
        /* Mask interrupts */
        INT_SYS_DisableIRQ(srxInterruptMappings[instance][indChan].fastIrq);
        INT_SYS_DisableIRQ(srxInterruptMappings[instance][indChan].slowIrq);
        INT_SYS_DisableIRQ(srxInterruptMappings[instance][indChan].errIrq);
    }

    /* Reset peripheral*/
    SRX_DRV_HW_ResetPeripheral(srxBase[instance]);

    /* Clear internal data */
    srxStatePtr[instance] = NULL;

    return STATUS_SUCCESS;
}

/*******************************************************************************
* EOF
*******************************************************************************/
