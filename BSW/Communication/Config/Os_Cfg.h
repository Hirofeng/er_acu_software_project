/*============================================================================*/
/*  Copyright (C) 2009-2014,  INFRASTRUCTURE SOFTWARE CO.,LTD.
 *  
 *  All rights reserved. This software is  property. Duplication
 *  or disclosure without  written authorization is prohibited.
 *  
 *  @file       <Os_Cfg.h>
 *  @brief      <>
 *  
 *  <MCU:MPC563x>
 *  
 *  @author     <>
 *  @date       <2014-06-24 15:12:56>
 */
/*============================================================================*/

#ifndef OS_CFG_H
#define OS_CFG_H

#include "Os_Types.h"


#define CFG_CC          				ECC2

#define CFG_STATUS      				STATUS_STANDARD

#define CFG_SCHEDULE    				SCHEDULE_MIXED

#define PRIORITY_GROUP					1

#define CFG_PRIORITY_MAX 				5
#define CFG_TASK_STACK_MAX  			6
#define CFG_TASK_MAX 					6
#define CFG_EXTENDED_TASK_MAX 			0

#define TASK_PRIORITY_MAX               CFG_PRIORITY_MAX

/*event definition*/
#define CFG_EVENT_MAX					1

#define CFG_COUNTER_MAX 				1
#define CFG_ALARM_MAX 					5
#define CFG_AUTO_ALARM_MAX 				0

#define CFG_ISR2_IPL_MAX 				41
#define INT_NEST_ENABLE					FALSE

#define CFG_RESOURCE_MAX 				3
#define CFG_STD_RESOURCE_MAX 			3
#define CFG_INTERNAL_RESOURCE_MAX 	    (CFG_RESOURCE_MAX - CFG_STD_RESOURCE_MAX)

/* task definition */
#define Task_init 	((Os_TaskType)0)
#define Task_10ms 	((Os_TaskType)1)
#define Task_100ms 	((Os_TaskType)2)
#define Task_1ms 	((Os_TaskType)3)
#define Task_1000ms 	((Os_TaskType)4)
#define Task_2ms_Flash_Main 	((Os_TaskType)5)

/* resource definition */
#define RES_SCHEDULER 	((Os_ResourceType) 0)
#define Res_ProtectVal_01 	((Os_ResourceType) 1)
#define Res_ProtectVal_02 	((Os_ResourceType) 2)

/* system clock config */
#define CPU_FREQUENCY 				80        /* cpu frequency in MHZ */
#define TICK_DURATION 				1000      /* the duration of ticks of the system in us */
#define DECR_V 						(TICK_DURATION * CPU_FREQUENCY)  /* the value should be set to DEC and DECAR */


/*counter definition*/
#define SystemTimer 		((Os_CounterType) 0)

#define OSMAXALLOWEDVALUE_SystemTimer   ((Os_TickType) 65535)
#define OSTICKSPERBASE_SystemTimer      ((Os_TickType) 1)
#define OSMINCYCLE_SystemTimer          ((Os_TickType) 1)

/*event definition*/
#define Evt_Trigger 		((EventMaskType) 0x1)

/* alarm definition */
#define Alarm_1000ms 		((Os_AlarmType) 0)
#define Alarm_100ms 		((Os_AlarmType) 1)
#define Alarm_10ms 		((Os_AlarmType) 2)
#define Alarm_1ms 		((Os_AlarmType) 3)
#define Alarm_2ms_Flash_Main 		((Os_AlarmType) 4)

/* appmode definition */
#define OsekOsAppMode ((Os_AppModeType) 		0x1)
#define CFG_ERRORHOOK 				TRUE
#define CFG_PRETASKHOOK 			FALSE
#define CFG_POSTTASKHOOK 			FALSE
#define CFG_STARTUPHOOK 			TRUE
#define CFG_SHUTDOWNHOOK 			FALSE

extern Os_StackType const Os_SystemStack;
#if (CFG_TASK_STACK_MAX > 0)
extern Os_StackType const Os_TaskStack[CFG_TASK_STACK_MAX];
#endif

#if ((CFG_CC == BCC1) || (CFG_CC == ECC1))
extern Os_TaskType Os_ReadyTable[CFG_PRIORITY_MAX];
#else
extern Os_TaskType* const Os_ReadyQueue[CFG_PRIORITY_MAX]; 
extern Os_SizeType const Os_ActivateQueueSize[CFG_PRIORITY_MAX];
extern Os_ReadyQueueType Os_ReadyQueueMark[CFG_PRIORITY_MAX];            
#endif

#if (CFG_TASK_MAX > 0)
extern const Os_TaskCfgType Os_TaskCfg[];
extern const Os_PriorityType Os_TaskPrioIndex[CFG_TASK_MAX]; 
extern const Os_PriorityType Os_TaskReadyPrioSet[CFG_TASK_MAX];
#endif

#if (CFG_RESOURCE_MAX > 0)
 
extern const Os_ResourceInfo Os_Resource[CFG_RESOURCE_MAX];

#if (CFG_INTERNAL_RESOURCE_MAX>0)
extern const Os_PriorityType Os_InternalResCeiling[CFG_TASK_MAX];
extern const Os_PriorityType Os_InternalResPrioIndex[CFG_TASK_MAX];
extern const Os_PriorityType Os_InterResReadyPrioSet[CFG_TASK_MAX];
extern boolean Os_OccupyInternalResource[CFG_TASK_MAX];
#endif

#if (CFG_STD_RESOURCE_MAX > 0) 
extern const Os_PriorityType Os_ResPrioIndex[CFG_STD_RESOURCE_MAX]; 
extern const Os_PriorityType Os_ResReadyPrioSet[CFG_STD_RESOURCE_MAX];
#endif

#endif

#if ( CFG_ALARM_MAX > 0)
extern const Os_AlarmCfgType Os_AlarmCfg[CFG_ALARM_MAX];
#endif

#if (CFG_COUNTER_MAX > 0)
extern const Os_CounterCfgType Os_CounterCfg[CFG_COUNTER_MAX];
#endif

#if (CFG_TASK_MAX > 0)
extern Os_TCBType Os_TCB[CFG_TASK_MAX];
#endif

#if (CFG_COUNTER_MAX > 0)
extern Os_CCBType Os_CCB[CFG_COUNTER_MAX];
#endif

#if (CFG_ALARM_MAX > 0)
extern Os_ACBType Os_ACB[CFG_ALARM_MAX];
#endif

#if (CFG_EXTENDED_TASK_MAX > 0)
extern Os_ECBType Os_ECB[CFG_EXTENDED_TASK_MAX];
#endif

#if (CFG_STD_RESOURCE_MAX > 0)
extern Os_RCBType Os_RCB[CFG_STD_RESOURCE_MAX];
#endif

DeclareTask(Task_init);
DeclareTask(Task_10ms);
DeclareTask(Task_100ms);
DeclareTask(Task_1ms);
DeclareTask(Task_1000ms);
DeclareTask(Task_2ms_Flash_Main);


#endif
