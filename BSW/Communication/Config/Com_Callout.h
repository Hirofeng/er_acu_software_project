/*============================================================================*/
/*  Copyright (C) 2009-2014,  INFRASTRUCTURE SOFTWARE CO.,LTD.
 *  
 *  All rights reserved. This software is  property. Duplication
 *  or disclosure without  written authorization is prohibited.
 *  
 *  @file       <Com_Callout.h>
 *  @brief      <>
 *  
 *  <MCU:MPC563x>
 *  
 *  @author     <>
 *  @date       <2014-06-10 14:01:41>
 */
/*============================================================================*/

   
#ifndef  COM_CALLOUT_H
#define  COM_CALLOUT_H

/*=======[V E R S I O N  I N F O R M A T I O N]===============================*/
#define COM_CALLOUT_H_AR_MAJOR_VERSION  3
#define COM_CALLOUT_H_AR_MINOR_VERSION  0
#define COM_CALLOUT_H_AR_PATCH_VERSION  3
#define COM_CALLOUT_H_SW_MAJOR_VERSION  1
#define COM_CALLOUT_H_SW_MINOR_VERSION  0
#define COM_CALLOUT_H_SW_PATCH_VERSION  0
 
 
/*=======[E X T E R N A L   F U N C T I O N   D E C L A R A T I O N S]========*/






#endif

/*=======[E N D   O F   F I L E]==============================================*/
