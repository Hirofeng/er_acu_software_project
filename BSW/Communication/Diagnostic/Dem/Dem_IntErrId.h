/************************************************************************
*				        ESEC UESTC
* 	 Copyright (C) 2005-2010 ESEC UESTC. All Rights Reserved.
***********************************************************************/

/**
 * Log:  
 * Revision 1.0 2010-5-13����11:04:49 stanley
 * description: create
 *
 */

/**
 * @file 	Dem_IntErrId.h
 * @brief
 *	<li>Function�� </li>
 *  <li>Design Points��</li>
 *  <p>
 *  
 *  </p>
 * @author 	stanley
 * @date 	2010-5-13
 * 
 */

#ifndef DEM_INTERRID_H
#define DEM_INTERRID_H

/****************************** references *********************************/

#ifdef __cplusplus
extern "C" {
#endif  /* __cplusplus */

/****************************** declarations *********************************/

/****************************** definitions *********************************/
#define CANTP_E_COM                      (uint16)0x01
#define CANTP_E_OPER_NOT_SUPPORTED       (uint16)0x02
#define NVM_E_INTEGRITY_FAILED           (uint16)0x03
#define NVM_E_REQ_FAILED                 (uint16)0x04
#define LINSM_E_CONFIRMATION_TIME_OUT    (uint16)0x05
#define LIN_E_TIMEOUT                    (uint16)0x06
#define LINIF_E_RESPONSE                 (uint16)0x07      
#define CAN_E_TIMEOUT                    (uint16)0x08
#define CANIF_E_FULL_TX_BUFFER           (uint16)0x09
#define PDUR_E_INIT_FAILED               (uint16)0x0A
#define PDUR_E_PDU_INSTANCE_LOST         (uint16)0x0B
#define	FLS_E_ERASE_FAILED               (uint16)0x0C
#define FLS_E_WRITE_FAILED               (uint16)0x0D
#define	FLS_E_READ_FAILED                (uint16)0x0E
#define	FLS_E_COMPARE_FAILED             (uint16)0x0F
#define	FLS_E_UNEXPECTED_FLASH_ID        (uint16)0x10
#define CANIF_E_INVALID_DLC              (uint16)0x11
#define CANIF_E_STOPPED                  (uint16)0x12
#define WDG_E_MODE_SWITCH_FALLED         (uint16)0x13
#define WDG_E_DISABLE_REJECTED           (uint16)0x14

#ifdef __cplusplus
}
#endif  /* __cplusplus */

#endif /* DEM_INTERRID_H */
