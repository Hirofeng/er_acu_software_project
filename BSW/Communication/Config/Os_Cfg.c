/*============================================================================*/
/*  Copyright (C) 2009-2014,  INFRASTRUCTURE SOFTWARE CO.,LTD.
 *  
 *  All rights reserved. This software is  property. Duplication
 *  or disclosure without  written authorization is prohibited.
 *  
 *  @file       <Os_Cfg.c>
 *  @brief      <>
 *  
 *  <MCU:MPC563x>
 *  
 *  @author     <>
 *  @date       <2014-06-18 10:32:56>
 */
/*============================================================================*/

#include "Os_Cfg.h"

#define Os_TopOfStack(stack)    ((Os_StackPtrType)((&(stack)[0]) + (sizeof(stack) / sizeof((stack)[0]))))
#define Os_BottomOfStack(stack)  ((Os_StackPtrType)(stack))

/*Ready Queue managment*/
#if ((CFG_CC == BCC1) || (CFG_CC == ECC1))
Os_TaskType Os_ReadyTable[CFG_PRIORITY_MAX];
#else


	
Os_ReadyQueueType Os_ReadyQueueMark[CFG_PRIORITY_MAX];
#endif

/*Stack management*/
#if 0
Os_StackDataType Os_SystemStack_0[1024];
Os_StackDataType Os_TaskStack_0[500];
Os_StackDataType Os_TaskStack_1[600];
Os_StackDataType Os_TaskStack_2[200];
Os_StackDataType Os_TaskStack_3[200];
Os_StackDataType Os_TaskStack_4[200];
Os_StackDataType Os_TaskStack_5[500];
#endif




/*Task management*/
#if (CFG_TASK_MAX>0)

/*end of os kernel refactoring*/
/*both of defination above can be inserted to the struct of TaskInfo*/
/*for os kernel refactoring end*/
#endif


/*Resource management*/
#if(CFG_RESOURCE_MAX>0)

#if (CFG_INTERNAL_RESOURCE_MAX>0)
const Os_PriorityType Os_InternalResCeiling[CFG_TASK_MAX] = 
{		
	0,		
	0,		
	0,		
	0,		
	0,		
	0
};
const Os_PriorityType Os_InternalResPrioIndex[CFG_TASK_MAX] = 
{		
	0,		
	0,		
	0,		
	0,		
	0,		
	0
};

const Os_PriorityType Os_InterResReadyPrioSet[CFG_TASK_MAX] = 
{		
	0x0001,		
	0x0001,		
	0x0001,		
	0x0001,		
	0x0001,		
	0x0001
};

boolean Os_OccupyInternalResource[CFG_TASK_MAX] =
{		
	0,		
	0,		
	0,		
	0,		
	0,		
	0
};
#endif

#if(CFG_STD_RESOURCE_MAX>0)

#endif

#endif
/*end of Resource management*/


/*Counter management*/
#if (CFG_COUNTER_MAX > 0)
const Os_CounterCfgType Os_CounterCfg[CFG_COUNTER_MAX] =
{
	{65535, 1, 1, COUNTER_SOFTWARE, 1000},
};
#endif


#if ( CFG_ALARM_MAX > 0)
	

	

	

	
/*
const Os_AlarmCfgType Os_AlarmCfg[CFG_ALARM_MAX] =
{	
	{
		SystemTimer,
		NULL,
		AlarmCallback_Alarm_1000ms
	},	
	{
		SystemTimer,
		NULL,
		AlarmCallback_Alarm_100ms
	},	
	{
		SystemTimer,
		NULL,
		AlarmCallback_Alarm_10ms
	},	
	{
		SystemTimer,
		NULL,
		AlarmCallback_Alarm_1ms
	},	
	{
		SystemTimer,
		NULL,
		AlarmCallback_Alarm_2ms_Flash_Main
	},		
};*/
#endif


#if (CFG_TASK_MAX > 0)
Os_TCBType Os_TCB[CFG_TASK_MAX];
#endif


#if (CFG_COUNTER_MAX > 0)
Os_CCBType Os_CCB[CFG_COUNTER_MAX];
#endif

#if (CFG_ALARM_MAX > 0)
Os_ACBType Os_ACB[CFG_ALARM_MAX];
#endif

#if (CFG_EXTENDED_TASK_MAX>0)
Os_ECBType Os_ECB[CFG_EXTENDED_TASK_MAX];
#endif

#if (CFG_STD_RESOURCE_MAX>0)
Os_RCBType Os_RCB[CFG_STD_RESOURCE_MAX];
#endif

