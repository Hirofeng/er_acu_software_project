/*
 * Dem.c
 *
 *  Created on: 2013-3-22
 *      Author: Administrator
 */
#include "Dem.h"
/******************function declarations****************/

/************ Interfaces with DCM **************/
    /**
     * Set DTC filter for successor operations.
     * @param DTCStatusMask mask for DTC status
     * @param DTCKind kind of DTC
     * @param DTCOrigin origin of DTC
     * @param filterWithSeverity whether severity of DTC is used
     * @param DTCSeverityMask mask for DTC severity used for filtering
     * @param filterForFaultDetectionCounter
     * @return operation result
     */
    /** @req DEM-APIR-025[Dem208]*/
    /**@req DEM-FUNR-249[Dem057]*/
Dem_ReturnSetDTCFilterType Dem_SetDTCFilter(uint8 DTCStatusMask,
            Dem_DTCKindType DTCKind, Dem_DTCOriginType DTCOrigin,
            Dem_FilterWithSeverityType filterWithSeverity,
            Dem_DTCSeverityType DTCSeverityMask,
            Dem_FilterForFDCType filterForFaultDetectionCounter)
{
    DTCStatusMask = DTCStatusMask;
	return DEM_FILTER_ACCEPTED;
}

/**
     * Get DTC status mask supported.
     * @param DTCStatusMask DTC status mask supported
     * @return result of operation
     */
    /** @req DEM-APIR-028[Dem213]*/
    /**@req DEM-FUNR-248[Dem060]*/
Std_ReturnType Dem_GetDTCStatusAvailabilityMask(uint8* DTCStatusMask)
{
	
	(*DTCStatusMask) = 0xFF;
	return E_OK;
}

    /**
     * Get number of DTCs which match current filter condition.
     * @param numberOfFilteredDTC number of matched DTC
     * @return result of operation
     */
    /** @req DEM-APIR-029[Dem214]*/
    /**@req DEM-FUNR-250[Dem061]*/
Dem_ReturnGetNumberOfFilteredDTCType Dem_GetNumberOfFilteredDTC(uint16* numberOfFilteredDTC)
{
    (*numberOfFilteredDTC) = 0u;
	return DEM_NUMBER_OK;
}
    /**
     * Get next DTC and its status which matches the current filter.
     * @param DTC code value of DTC
     * @param DTCStatus status of DTC
     * @return result of operation
     */
    /** @req DEM-APIR-030[Dem215]*/
    /**@req DEM-FUNR-251[Dem216]*/
Dem_ReturnGetNextFilteredDTCType Dem_GetNextFilteredDTC(uint32* DTC, uint8* DTCStatus)
{
   //(*DTC) = 0x55AA55;
   //(*DTCStatus) = 0x01;
   return DEM_FILTERED_OK;
}

/**
     * Clear specific DTC information.
     * @param DTC code value of DTC
     * @param DTCKind kind of DTC
     * @param DTCOrigin origin of DTC
     * @return result of operation
     */
    /** @req DEM-APIR-044[Dem241]*/
    /**@req DEM-FUNR-273[Dem009]*/
Dem_ReturnClearDTCType Dem_ClearDTC(uint32 DTC, Dem_DTCKindType DTCKind, Dem_DTCOriginType DTCOrigin)
{
  return DEM_CLEAR_OK;
}

/**
 * Disable handling for specific group of DTCs.
 * @param DTCGroup DTC code for DTC group
 * @param DTCKind kind of DTC
 * @return result of operation
 */
/** @req DEM-APIR-045[Dem242] */
/**@req DEM-FUNR-283[Dem035]*/
/**@req DEM-FUNR-284[Dem079]*/
Dem_ReturnControlDTCStorageType Dem_DisableDTCStorage(Dem_DTCGroupType DTCGroup, Dem_DTCKindType DTCKind)
{
	return DEM_CONTROL_DTC_STORAGE_OK;
}

/**
 * Enable handling for specific group of DTCs.
 * @param DTCGroup DTC code for DTC Group
 * @param DTCKind kind of DTC
 * @return result of operation
 */
/** @req DEM-APIR-046[Dem243]*/
/**@req DEM-FUNR-283[Dem035]*/
Dem_ReturnControlDTCStorageType Dem_EnableDTCStorage(Dem_DTCGroupType DTCGroup, Dem_DTCKindType DTCKind)
{
	return DEM_CONTROL_DTC_STORAGE_OK;
}
	
	/**
 * Get DTC format of the implementation.
 * @return DTC format type
 */
/** @req DEM-APIR-035[Dem230]*/
/**@req DEM-FUNR-245[Dem231]*/
uint8 Dem_GetTranslationType(void)
{
	return DEM_ISO15031_6;
}


/*********************************************
Function: Gets the status of a DTC
Input Parameter: DTC: For this DTC its status is requested 
                 DTCKind:   This parameter defines the requested DTC, either only OBD-relevant DTCs or all DTCs 
                 DTCOrigin: this parameter is used to select the source memory the DTCs shall be read from.
Output Parameter:(*DTCStatus):This parameter receives the status information of the requested DTC
return value:  
/*********************************************/
Dem_ReturnGetStatusOfDTCType Dem_GetStatusOfDTC(uint32 DTC,
    											Dem_DTCKindType DTCKind,
    											Dem_DTCOriginType DTCOrigin,
   												uint8* DTCStatus)
{

   return DEM_STATUS_OK;
}


/*********************************************
Function: Disables the DTC record update
Input Parameter: None
Output Parameter: None
return value:  
/*********************************************/
Std_ReturnType Dem_DisableDTCRecordUpdate(void)
{
	return E_OK;
}

/*********************************************
Function: 
Input Parameter: DTC: This is the DTC the FreezeFrame is assigned to.
                 DTCKind:This parameter defines the requested DTC, either only OBD-relevant DTCs or all DTCs;
                 DTCOrigin:this parameter is used to select the source memory the DTCs shall be read from
                 RecordNumber:identifier for a FreezeFrame record
Output Parameter: DestBuffer:This parameter contains a byte pointer that points to the buffer to which the FreezeFrame data shall be written
input(output)parameter:BufSize:
					When the function is called this parameter contains 
					the maximum number of data bytes that can be written to the buffer.
					The function returns the actual number of written data bytes in this parameter. 
return value:  
/*********************************************/
Dem_ReturnGetFreezeFrameDataByDTCType Dem_GetFreezeFrameDataByDTC(
     								uint32  DTC,
     								Dem_DTCKindType  DTCKind,
     								Dem_DTCOriginType  DTCOrigin,
     								uint8  RecordNumber,
     								uint8*  DestBuffer,
     								uint8*  BufSize )
{
    return DEM_GET_FFDATABYDTC_OK;	
}


Std_ReturnType Dem_EnableDTCRecordUpdate(void)
{
	return E_OK;
}


/*********************************************
Function: Returns the DTCExtendedDataRecord as defined in UDS Service 0x19 subfunction 0x06, 0x10
Input Parameter: DTC: This is the DTC the 'Extended Data Record' is assigned to. .
                 DTCKind:This parameter defines the requested DTC, either only OBD-relevant DTCs or all DTCs;
                 DTCOrigin:this parameter is used to select the source memory the DTCs shall be read from
                 ExtendedDataNumber:Identification of requested Extended data record
Output Parameter: DestBuffer:This parameter contains a byte pointer that points to the buffer to which the Extended Data data shall be written
input(output)parameter:BufSize:
				When the function is called this parameter contains the maximum number of data bytes that can be written 
				to the buffer.The function returns the actual number of written data 
bytes in this parameter.return value:  
/*********************************************/
Dem_ReturnGetExtendedDataRecordByDTCType Dem_GetExtendedDataRecordByDTC(uint32  DTC,
     																	Dem_DTCKindType  DTCKind,
     																	Dem_DTCOriginType  DTCOrigin,
     																	uint8  ExtendedDataNumber,
     																	uint8*  DestBuffer,
     																	uint8*  BufSize)
{
	return DEM_RECORD_OK;
}
