/*============================================================================*/
/*  Copyright (C) 2009-2014,  INFRASTRUCTURE SOFTWARE CO.,LTD.
 *  
 *  All rights reserved. This software is  property. Duplication
 *  or disclosure without  written authorization is prohibited.
 *  
 *  @file       <CanIf_Cfg.c>
 *  @brief      <>
 *  
 *  <MCU:MPC563x>
 *  
 *  @author     <>
 *  @date       <2014-06-18 10:32:55>
 */
/*============================================================================*/


/*=======[V E R S I O N  I N F O R M A T I O N]===============================*/

#define CANIF_CFG_C_AR_MAJOR_VERSION  3
#define CANIF_CFG_C_AR_MINOR_VERSION  2
#define CANIF_CFG_C_AR_PATCH_VERSION  0
#define CANIF_CFG_C_SW_MAJOR_VERSION  1
#define CANIF_CFG_C_SW_MINOR_VERSION  0
#define CANIF_CFG_C_SW_PATCH_VERSION  1

/*=======[I N C L U D E S]====================================================*/
#include "CanIf.h"
#include "Can.h"
#include "PduR_CanIf.h"
#include "CanTp_Cbk.h"
#include "EcuM_Cbk.h"
#include "Com_Cbk.h"
//#include "Ccp_Cbk.h"
/*=======[V E R S I O N  C H E C K]===========================================*/
#if(CANIF_CFG_C_AR_MAJOR_VERSION != CANIF_CFG_H_AR_MAJOR_VERSION)
    #error "CanIf.c:Mismatch in Specification Major Version"
#endif

#if(CANIF_CFG_C_AR_MINOR_VERSION != CANIF_CFG_H_AR_MINOR_VERSION)
    #error "CanIf.c:Mismatch in Specification Minor Version"
#endif

#if(CANIF_CFG_C_AR_PATCH_VERSION != CANIF_CFG_H_AR_PATCH_VERSION)
    #error "CanIf.c:Mismatch in Specification Patch Version"
#endif

#if(CANIF_CFG_C_SW_MAJOR_VERSION != CANIF_CFG_H_SW_MAJOR_VERSION)
    #error "CanIf.c:Mismatch in Specification Major Version"
#endif

#if(CANIF_CFG_C_SW_MINOR_VERSION != CANIF_CFG_H_SW_MINOR_VERSION)
    #error "CanIf.c:Mismatch in Specification Minor Version"
#endif

/*=======[E X T E R N A L   D A T A]==========================================*/

#define CANIF_START_SEC_VAR_UNSPECIFIED
#include "CanIf_MemMap.h"
P2VAR(CanIf_RxPduBufferType,AUTOMATIC,CANIF_VAR) CanIf_RxBuffers;
#define CANIF_STOP_SEC_VAR_UNSPECIFIED
#include "CanIf_MemMap.h"

#define CANIF_START_SEC_CONST_UNSPECIFIED
#include "CanIf_MemMap.h"
CONST(CanIfUserRxFct,CANIF_CONST) CanIf_UserRxIndication[CANIF_USER_MAX_COUNT] =
{
	NULL_PTR,/*&CanNm_RxIndication,Enable only when CanNm include*/
    CanTp_RxIndication,//&CanTp_RxIndication,/*&CanTp_RxIndication,Enable only when CanTp include*/
    &PduR_CanIfRxIndication, /*&PduR_CanIfRxIndication,Enable only when PduR include*/
    NULL_PTR,//&Ccp_CanIfRxIndication,  /**<&Ccp_CanIfRxIndication,CCP */
    NULL_PTR /**<&OsekNm_RxIndication,OskeNm*/

};
#define CANIF_STOP_SEC_CONST_UNSPECIFIED
#include "CanIf_MemMap.h"

#define CANIF_START_SEC_CONST_UNSPECIFIED
#include "CanIf_MemMap.h"
CONST(CanIfUserTxFct,CANIF_CONST) CanIf_UserTxConfirmation[CANIF_USER_MAX_COUNT] =
{
    NULL_PTR,/*&CanNm_TxConfirmation,Enable only when CanNm include*/
    CanTp_TxConfirmation,//&CanTp_TxConfirmation,/*&CanTp_TxConfirmation,Enable only when CanTp include*/
    &PduR_CanIfTxConfirmation, /*&PduR_CanIfTxConfirmation,Enable only when PduR include*/
    NULL_PTR,//&Ccp_CanIfTxConfirmation, /**<&Ccp_CanIfTxConfirmation,CCP */
    NULL_PTR /**<&OsekNm_TxConfirmation,OsekNm*/
};
#define CANIF_STOP_SEC_CONST_UNSPECIFIED
#include "CanIf_MemMap.h"

/* contains the references  to the configuration setup of each underlying CAN driver.*/
#define CANIF_START_SEC_CONST_UNSPECIFIED
#include "CanIf_MemMap.h"
CONST(CanIf_ControllerConfigType,CANIF_CONST)CanIf_ControllerConfiguration[CANIF_MAX_CONTROLLER] =
{
	{
	  /* Enables wakeup support and defines the source device of a wakeup event. */
	  CANIF_WAKEUP_SUPPORT_CONTROLLER,
	  
	  /* Logical handle of the underlying CAN controller  */
	  CAN_CONTROLLER_A,
	#if (STD_ON == CANIF_WAKEUP_VALIDATION)
	  /* CanIf WakeUp source,add by  */
	  ECUM_WKSOURCE_ALL_SOURCES1
	#endif
	  
	},
	{
	  /* Enables wakeup support and defines the source device of a wakeup event. */
	  CANIF_WAKEUP_SUPPORT_CONTROLLER,
	  
	  /* Logical handle of the underlying CAN controller  */
	  CAN_CONTROLLER_C,
	#if (STD_ON == CANIF_WAKEUP_VALIDATION)
	  /* CanIf WakeUp source,add by  */
	  ECUM_WKSOURCE_ALL_SOURCES2
	#endif
	  
	},
};
#define CANIF_STOP_SEC_CONST_UNSPECIFIED
#include "CanIf_MemMap.h"

/* Callout functions with respect to the upper layers */
#define CANIF_START_SEC_CONST_UNSPECIFIED
#include "CanIf_MemMap.h"
CONST(CanIf_DispatchConfigType,CANIF_CONST) CanIf_DispatchConfigData = 
{
    /* Name of target BusOff notification services  */
	NULL_PTR,/*&CanSM_ControllerBusOff,Enable only when CanSM include*/
	
    #if (STD_ON == CANIF_WAKEUP_VALIDATION)
	/*  Name of target wakeup notification services */
	&EcuM_SetWakeupEvent,/*&EcuM_SetWakeupEvent, Enable only when EcuM include */

	/* Name of target wakeup validation notification services */
	&EcuM_ValidateWakeupEvent, /*&EcuM_ValidateWakeupEvent, Enable only when EcuM include */
    #endif 
};
#define CANIF_STOP_SEC_CONST_UNSPECIFIED
#include "CanIf_MemMap.h"


/* Configuration parameters for all the underlying CAN drivers*/
#define CANIF_START_SEC_CONST_UNSPECIFIED
#include "CanIf_MemMap.h"
CONST(CanIf_DriverConfigType,CANIF_CONST) CanIf_DriverConfiguration =
{
    FALSE, /* CanIfBusoffNotification */
    TRUE, /* CanIfReceiveIndication */
    TRUE, /* CanIfTxConfirmation */
    TRUE  /* CanIfWakeupNotification */
};
#define CANIF_STOP_SEC_CONST_UNSPECIFIED
#include "CanIf_MemMap.h"


/* contains configuration parameters for each hardware receive object */
#define CANIF_START_SEC_CONST_UNSPECIFIED
#include "CanIf_MemMap.h"
CONST(CanIf_HrhConfigType,CANIF_CONST) CanIf_HrhConfigData[CANIF_NUMBER_OF_HRHUSED] =
{
	{
	    /* Define s the HRH type i.e, whether its a BasicCan or FullCan */
	    CANIF_FULL_CAN,

		/* perform software filtering */
		FALSE,
		
		/* Reference to controller Id to which the HRH belongs to */
		CAN_CONTROLLER_C
	},
	{
	    /* Define s the HRH type i.e, whether its a BasicCan or FullCan */
	    CANIF_FULL_CAN,

		/* perform software filtering */
		FALSE,
		
		/* Reference to controller Id to which the HRH belongs to */
		CAN_CONTROLLER_C
	},
	{
	    /* Define s the HRH type i.e, whether its a BasicCan or FullCan */
	    CANIF_FULL_CAN,

		/* perform software filtering */
		FALSE,
		
		/* Reference to controller Id to which the HRH belongs to */
		CAN_CONTROLLER_C
	},
	{
	    /* Define s the HRH type i.e, whether its a BasicCan or FullCan */
	    CANIF_FULL_CAN,

		/* perform software filtering */
		FALSE,
		
		/* Reference to controller Id to which the HRH belongs to */
		CAN_CONTROLLER_C
	},
	{
	    /* Define s the HRH type i.e, whether its a BasicCan or FullCan */
	    CANIF_FULL_CAN,

		/* perform software filtering */
		FALSE,
		
		/* Reference to controller Id to which the HRH belongs to */
		CAN_CONTROLLER_C
	},
	{
	    /* Define s the HRH type i.e, whether its a BasicCan or FullCan */
	    CANIF_FULL_CAN,

		/* perform software filtering */
		FALSE,
		
		/* Reference to controller Id to which the HRH belongs to */
		CAN_CONTROLLER_C
	},
	{
	    /* Define s the HRH type i.e, whether its a BasicCan or FullCan */
	    CANIF_FULL_CAN,

		/* perform software filtering */
		FALSE,
		
		/* Reference to controller Id to which the HRH belongs to */
		CAN_CONTROLLER_C
	},
	{
	    /* Define s the HRH type i.e, whether its a BasicCan or FullCan */
	    CANIF_BASIC_CAN,

		/* perform software filtering */
		TRUE,
		
		/* Reference to controller Id to which the HRH belongs to */
		CAN_CONTROLLER_C
	},
	{
	    /* Define s the HRH type i.e, whether its a BasicCan or FullCan */
	    CANIF_FULL_CAN,

		/* perform software filtering */
		FALSE,
		
		/* Reference to controller Id to which the HRH belongs to */
		CAN_CONTROLLER_C
	},
	{
	    /* Define s the HRH type i.e, whether its a BasicCan or FullCan */
	    CANIF_FULL_CAN,

		/* perform software filtering */
		FALSE,
		
		/* Reference to controller Id to which the HRH belongs to */
		CAN_CONTROLLER_C
	},
	{
	    /* Define s the HRH type i.e, whether its a BasicCan or FullCan */
	    CANIF_BASIC_CAN,

		/* perform software filtering */
		TRUE,
		
		/* Reference to controller Id to which the HRH belongs to */
		CAN_CONTROLLER_C
	},
	{
	    /* Define s the HRH type i.e, whether its a BasicCan or FullCan */
	    CANIF_FULL_CAN,

		/* perform software filtering */
		FALSE,
		
		/* Reference to controller Id to which the HRH belongs to */
		CAN_CONTROLLER_C
	},
	{
	    /* Define s the HRH type i.e, whether its a BasicCan or FullCan */
	    CANIF_FULL_CAN,

		/* perform software filtering */
		FALSE,
		
		/* Reference to controller Id to which the HRH belongs to */
		CAN_CONTROLLER_C
	},
	{
	    /* Define s the HRH type i.e, whether its a BasicCan or FullCan */
	    CANIF_FULL_CAN,

		/* perform software filtering */
		FALSE,
		
		/* Reference to controller Id to which the HRH belongs to */
		CAN_CONTROLLER_C
	},
	{
	    /* Define s the HRH type i.e, whether its a BasicCan or FullCan */
	    CANIF_FULL_CAN,

		/* perform software filtering */
		FALSE,
		
		/* Reference to controller Id to which the HRH belongs to */
		CAN_CONTROLLER_C
	},
	{
	    /* Define s the HRH type i.e, whether its a BasicCan or FullCan */
	    CANIF_FULL_CAN,

		/* perform software filtering */
		FALSE,
		
		/* Reference to controller Id to which the HRH belongs to */
		CAN_CONTROLLER_C
	},
	{
	    /* Define s the HRH type i.e, whether its a BasicCan or FullCan */
	    CANIF_FULL_CAN,

		/* perform software filtering */
		FALSE,
		
		/* Reference to controller Id to which the HRH belongs to */
		CAN_CONTROLLER_C
	},
	{
	    /* Define s the HRH type i.e, whether its a BasicCan or FullCan */
	    CANIF_FULL_CAN,

		/* perform software filtering */
		FALSE,
		
		/* Reference to controller Id to which the HRH belongs to */
		CAN_CONTROLLER_C
	},
};
#define CANIF_STOP_SEC_CONST_UNSPECIFIED
#include "CanIf_MemMap.h"


/* contains parameters related to each HTH */
#define CANIF_START_SEC_CONST_UNSPECIFIED
#include "CanIf_MemMap.h"
CONST(CanIf_HthConfigType,CANIF_CONST) CanIf_HthConfigData[CANIF_NUMBER_OF_HTHUSED] =
{
	{ //0
	    /*Transmission method of the corresponding HTH */
	    CANIF_BASIC_CAN,

		/* Reference to controller Id to which the HTH belongs to */
		CAN_CONTROLLER_C,
		
		/* refers to a particular HTH object in the CAN Driver Module configuration*/
		35
	},
	{ //1
	    /*Transmission method of the corresponding HTH */
	    CANIF_BASIC_CAN,

		/* Reference to controller Id to which the HTH belongs to */
		CAN_CONTROLLER_C,
		
		/* refers to a particular HTH object in the CAN Driver Module configuration*/
		26
	},
	{ //2
	    /*Transmission method of the corresponding HTH */
	    CANIF_FULL_CAN,

		/* Reference to controller Id to which the HTH belongs to */
		CAN_CONTROLLER_C,
		
		/* refers to a particular HTH object in the CAN Driver Module configuration*/
		27
	},
	{ //3
	    /*Transmission method of the corresponding HTH */
	    CANIF_FULL_CAN,

		/* Reference to controller Id to which the HTH belongs to */
		CAN_CONTROLLER_C,
		
		/* refers to a particular HTH object in the CAN Driver Module configuration*/
		28
	},
	{ //4
	    /*Transmission method of the corresponding HTH */
	    CANIF_FULL_CAN,

		/* Reference to controller Id to which the HTH belongs to */
		CAN_CONTROLLER_C,
		
		/* refers to a particular HTH object in the CAN Driver Module configuration*/
		29-15
	},
	{ //5
	    /*Transmission method of the corresponding HTH */
	    CANIF_FULL_CAN,

		/* Reference to controller Id to which the HTH belongs to */
		CAN_CONTROLLER_C,
		
		/* refers to a particular HTH object in the CAN Driver Module configuration*/
		30-15
	},
	{ //6
	    /*Transmission method of the corresponding HTH */
	    CANIF_FULL_CAN,

		/* Reference to controller Id to which the HTH belongs to */
		CAN_CONTROLLER_C,
		
		/* refers to a particular HTH object in the CAN Driver Module configuration*/
		31-15
	},
	{ //7
	    /*Transmission method of the corresponding HTH */
	    CANIF_FULL_CAN,

		/* Reference to controller Id to which the HTH belongs to */
		CAN_CONTROLLER_C,
		
		/* refers to a particular HTH object in the CAN Driver Module configuration*/
		32-15
	},
	{ //8
	    /*Transmission method of the corresponding HTH */
	    CANIF_FULL_CAN,

		/* Reference to controller Id to which the HTH belongs to */
		CAN_CONTROLLER_C,
		
		/* refers to a particular HTH object in the CAN Driver Module configuration*/
		33
	},
	{ //9 
	    /*Transmission method of the corresponding HTH */
	    CANIF_FULL_CAN,

		/* Reference to controller Id to which the HTH belongs to */
		CAN_CONTROLLER_C,
		
		/* refers to a particular HTH object in the CAN Driver Module configuration*/
		34
	},
};
#define CANIF_STOP_SEC_CONST_UNSPECIFIED
#include "CanIf_MemMap.h"


#define CANIF_START_SEC_VAR_UNSPECIFIED
#include "CanIf_MemMap.h"
VAR(CanIf_NotifStatusType, CANIF_VAR) CanIf_TxNotifStatus[CANIF_MAX_NUMBER_OF_CANTXPDUIDS];
#define CANIF_STOP_SEC_VAR_UNSPECIFIED
#include "CanIf_MemMap.h"

#define CANIF_START_SEC_VAR_UNSPECIFIED
#include "CanIf_MemMap.h"
VAR(CanIf_NotifStatusType, CANIF_VAR) CanIf_RxNotifStatus[CANIF_MAX_NUMBER_OF_CANRXPDUIDS];
#define CANIF_STOP_SEC_VAR_UNSPECIFIED
#include "CanIf_MemMap.h"

/* define dynamic tx pdu Canid */
#define CANIF_START_SEC_VAR_UNSPECIFIED
#include "CanIf_MemMap.h"
P2VAR(Can_IdType,AUTOMATIC,CANIF_VAR) CanIf_DynamicTxPduCanIds;
#define CANIF_STOP_SEC_VAR_UNSPECIFIED
#include "CanIf_MemMap.h"


/* references to the configuration setup of each underlying CAN Driver */
#define CANIF_START_CONST_PBCFG
#include "CanIf_MemMap.h"
CONST(CanIf_InitHohConfigType, CANIF_CONST_PBCFG) CanIf_InitHohConfig =
{  
    /* Selects the CAN Interface specific configuration setup */
    &Can_ControllerConfigData[0u]
};
#define CANIF_STOP_CONST_PBCFG
#include "CanIf_MemMap.h"


/* contains the configuration (parameters) of each transmit CAN L-PDU */
#define CANIF_START_CONST_PBCFG
#include "CanIf_MemMap.h"
CONST(CanIf_TxPduConfigType, CANIF_CONST_PBCFG) CanIf_TxPduConfigData[CANIF_MAX_NUMBER_OF_CANTXPDUIDS] =
{
	{
		0x732,      /* CanIfCanTxPduIdCanId*/
		8,          /* CanIfCanTxPduIdDlc*/
		0,			/* CanIfCanTxPduIndex*/
		CANIF_PDU_TYPE_STATIC,  /* CanIfCanTxPduType*/
		0xFFFFu,    /* CanIfDynamicTxPduCanIdIndex*/
		FALSE,       /* CanIfReadTxPduNotifyStatus*/
        0xFF,       /* CanIfTxNotifyIndex*/
        CANIF_STANDARD_CAN,     /* CanIfTxPduIdCanIdType */
		CANIF_USER_TYPE_XCP_CCP, /* CanIfTxUserType*/
		0        /* CanIfCanTxPduHthRef*/
    },
	{
		0x733,      /* CanIfCanTxPduIdCanId*/
		8,          /* CanIfCanTxPduIdDlc*/
		1,			/* CanIfCanTxPduIndex*/
		CANIF_PDU_TYPE_STATIC,  /* CanIfCanTxPduType*/
		0xFFFFu,    /* CanIfDynamicTxPduCanIdIndex*/
		FALSE,       /* CanIfReadTxPduNotifyStatus*/
        0xFF,       /* CanIfTxNotifyIndex*/
        CANIF_STANDARD_CAN,     /* CanIfTxPduIdCanIdType */
		CANIF_USER_TYPE_XCP_CCP, /* CanIfTxUserType*/
		0        /* CanIfCanTxPduHthRef*/
    },
	{
		0x734,      /* CanIfCanTxPduIdCanId*/
		8,          /* CanIfCanTxPduIdDlc*/
		2,			/* CanIfCanTxPduIndex*/
		CANIF_PDU_TYPE_STATIC,  /* CanIfCanTxPduType*/
		0xFFFFu,    /* CanIfDynamicTxPduCanIdIndex*/
		FALSE,       /* CanIfReadTxPduNotifyStatus*/
        0xFF,       /* CanIfTxNotifyIndex*/
        CANIF_STANDARD_CAN,     /* CanIfTxPduIdCanIdType */
		CANIF_USER_TYPE_XCP_CCP, /* CanIfTxUserType*/
		0        /* CanIfCanTxPduHthRef*/
    },
	{
		0x735,      /* CanIfCanTxPduIdCanId*/
		8,          /* CanIfCanTxPduIdDlc*/
		3,			/* CanIfCanTxPduIndex*/
		CANIF_PDU_TYPE_STATIC,  /* CanIfCanTxPduType*/
		0xFFFFu,    /* CanIfDynamicTxPduCanIdIndex*/
		FALSE,       /* CanIfReadTxPduNotifyStatus*/
        0xFF,       /* CanIfTxNotifyIndex*/
        CANIF_STANDARD_CAN,     /* CanIfTxPduIdCanIdType */
		CANIF_USER_TYPE_XCP_CCP, /* CanIfTxUserType*/
		0        /* CanIfCanTxPduHthRef*/
    },
	{
		0x115,      /* CanIfCanTxPduIdCanId*/
		8,          /* CanIfCanTxPduIdDlc*/
		4,				/* CanIfCanTxPduIndex*/
		CANIF_PDU_TYPE_STATIC,  /* CanIfCanTxPduType*/
		0xFFFFu,    /* CanIfDynamicTxPduCanIdIndex*/
		FALSE,       /* CanIfReadTxPduNotifyStatus*/
        0xFF,       /* CanIfTxNotifyIndex*/
        CANIF_STANDARD_CAN,     /* CanIfTxPduIdCanIdType */
		CANIF_USER_TYPE_CAN_PDUR, /* CanIfTxUserType*/
		1        /* CanIfCanTxPduHthRef*/
    },
	{
		0x2a1,      /* CanIfCanTxPduIdCanId*/
		8,          /* CanIfCanTxPduIdDlc*/
		5,				/* CanIfCanTxPduIndex*/
		CANIF_PDU_TYPE_STATIC,  /* CanIfCanTxPduType*/
		0xFFFFu,    /* CanIfDynamicTxPduCanIdIndex*/
		FALSE,       /* CanIfReadTxPduNotifyStatus*/
        0xFF,       /* CanIfTxNotifyIndex*/
        CANIF_STANDARD_CAN,     /* CanIfTxPduIdCanIdType */
		CANIF_USER_TYPE_CAN_PDUR, /* CanIfTxUserType*/
		2        /* CanIfCanTxPduHthRef*/
    },
	{
		0x2a8,      /* CanIfCanTxPduIdCanId*/
		8,          /* CanIfCanTxPduIdDlc*/
		6,				/* CanIfCanTxPduIndex*/
		CANIF_PDU_TYPE_STATIC,  /* CanIfCanTxPduType*/
		0xFFFFu,    /* CanIfDynamicTxPduCanIdIndex*/
		FALSE,       /* CanIfReadTxPduNotifyStatus*/
        0xFF,       /* CanIfTxNotifyIndex*/
        CANIF_STANDARD_CAN,     /* CanIfTxPduIdCanIdType */
		CANIF_USER_TYPE_CAN_PDUR, /* CanIfTxUserType*/
		3        /* CanIfCanTxPduHthRef*/
    },
	{
		0x78D,      /* CanIfCanTxPduIdCanId*/
		8,          /* CanIfCanTxPduIdDlc*/
		0,				/* CanIfCanTxPduIndex*/ /*发送确认ID号,该数值为CanTp_TxNSdus[]或CanTp_RxNSdus[]的下标,用于获取发送时的静态配置*/
		CANIF_PDU_TYPE_STATIC,  /* CanIfCanTxPduType*/
		0xFFFFu,    /* CanIfDynamicTxPduCanIdIndex*/
		FALSE,       /* CanIfReadTxPduNotifyStatus*/
        0xFF,       /* CanIfTxNotifyIndex*/
        CANIF_STANDARD_CAN,     /* CanIfTxPduIdCanIdType */
		CANIF_USER_TYPE_CAN_TP, /* CanIfTxUserType*/
		4        /* CanIfCanTxPduHthRef*/
    },
	{
	    0x3A2,//0x308,      /* CanIfCanTxPduIdCanId*/
		8,          /* CanIfCanTxPduIdDlc*/
		7,				/* CanIfCanTxPduIndex*/
		CANIF_PDU_TYPE_STATIC,  /* CanIfCanTxPduType*/
		0xFFFFu,    /* CanIfDynamicTxPduCanIdIndex*/
		FALSE,       /* CanIfReadTxPduNotifyStatus*/
        0xFF,       /* CanIfTxNotifyIndex*/
        CANIF_STANDARD_CAN,     /* CanIfTxPduIdCanIdType */
		CANIF_USER_TYPE_CAN_PDUR, /* CanIfTxUserType*/
		5        /* CanIfCanTxPduHthRef*/
    },
	{
		0x1ca,      /* CanIfCanTxPduIdCanId*/
		8,          /* CanIfCanTxPduIdDlc*/
		22,				/* CanIfCanTxPduIndex*/
		CANIF_PDU_TYPE_STATIC,  /* CanIfCanTxPduType*/
		0xFFFFu,    /* CanIfDynamicTxPduCanIdIndex*/
		FALSE,       /* CanIfReadTxPduNotifyStatus*/
        0xFF,       /* CanIfTxNotifyIndex*/
        CANIF_STANDARD_CAN,     /* CanIfTxPduIdCanIdType */
		CANIF_USER_TYPE_CAN_PDUR, /* CanIfTxUserType*/
		6        /* CanIfCanTxPduHthRef*/
    },
	{
		0x122,      /* CanIfCanTxPduIdCanId*/
		8,          /* CanIfCanTxPduIdDlc*/
		23,				/* CanIfCanTxPduIndex*/
		CANIF_PDU_TYPE_STATIC,  /* CanIfCanTxPduType*/
		0xFFFFu,    /* CanIfDynamicTxPduCanIdIndex*/
		FALSE,       /* CanIfReadTxPduNotifyStatus*/
        0xFF,       /* CanIfTxNotifyIndex*/
        CANIF_STANDARD_CAN,     /* CanIfTxPduIdCanIdType */
		CANIF_USER_TYPE_CAN_PDUR, /* CanIfTxUserType*/
		7        /* CanIfCanTxPduHthRef*/
    },
	{
		0x1b6,      /* CanIfCanTxPduIdCanId*/
		8,          /* CanIfCanTxPduIdDlc*/
		24,				/* CanIfCanTxPduIndex*/
		CANIF_PDU_TYPE_STATIC,  /* CanIfCanTxPduType*/
		0xFFFFu,    /* CanIfDynamicTxPduCanIdIndex*/
		FALSE,       /* CanIfReadTxPduNotifyStatus*/
        0xFF,       /* CanIfTxNotifyIndex*/
        CANIF_STANDARD_CAN,     /* CanIfTxPduIdCanIdType */
		CANIF_USER_TYPE_CAN_PDUR, /* CanIfTxUserType*/
		8        /* CanIfCanTxPduHthRef*/
    },
	{
		0x364,      /* CanIfCanTxPduIdCanId*/
		8,          /* CanIfCanTxPduIdDlc*/
		25,				/* CanIfCanTxPduIndex*/
		CANIF_PDU_TYPE_STATIC,  /* CanIfCanTxPduType*/
		0xFFFFu,    /* CanIfDynamicTxPduCanIdIndex*/
		FALSE,       /* CanIfReadTxPduNotifyStatus*/
        0xFF,       /* CanIfTxNotifyIndex*/
        CANIF_STANDARD_CAN,     /* CanIfTxPduIdCanIdType */
		CANIF_USER_TYPE_CAN_PDUR, /* CanIfTxUserType*/
		9        /* CanIfCanTxPduHthRef*/
    },
};
#define CANIF_STOP_CONST_PBCFG
#include "CanIf_MemMap.h"


/* contains the configuration (parameters) of each receive CAN L-PDU */
#define CANIF_START_CONST_PBCFG
#include "CanIf_MemMap.h"
CONST(CanIf_RxPduConfigType, CANIF_CONST_PBCFG)CanIf_RxPduConfigData[CANIF_MAX_NUMBER_OF_CANRXPDUIDS] =
{
	{
		0x341,      /* CanIfCanRxPduCanId*/
		8,         /* CanIfCanRxPduDlc*/
		13,         /* CanIfCanRxPduId*/
		FALSE,          /* CanIfReadRxPduData*/
        0xFF,             /* CanIfRxBufferIndex */
		FALSE,         /* CanIfReadRxPduNotifyStatus*/	
		0xFF,             /* CanIfRxNotifyIndex */
		CANIF_STANDARD_CAN,      /* CanIfRxPduIdCanIdType*/
		CANIF_USER_TYPE_CAN_PDUR   /* CanIfRxUserType*/
	
    },
	{
		0x350,      /* CanIfCanRxPduCanId*/
		8,         /* CanIfCanRxPduDlc*/
		14,         /* CanIfCanRxPduId*/
		FALSE,          /* CanIfReadRxPduData*/
        0xFF,             /* CanIfRxBufferIndex */
		FALSE,         /* CanIfReadRxPduNotifyStatus*/	
		0xFF,             /* CanIfRxNotifyIndex */
		CANIF_STANDARD_CAN,      /* CanIfRxPduIdCanIdType*/
		CANIF_USER_TYPE_CAN_PDUR   /* CanIfRxUserType*/
	
    },
	{
		0x3be,      /* CanIfCanRxPduCanId*/
		8,         /* CanIfCanRxPduDlc*/
		15,         /* CanIfCanRxPduId*/
		FALSE,          /* CanIfReadRxPduData*/
        0xFF,             /* CanIfRxBufferIndex */
		FALSE,         /* CanIfReadRxPduNotifyStatus*/	
		0xFF,             /* CanIfRxNotifyIndex */
		CANIF_STANDARD_CAN,      /* CanIfRxPduIdCanIdType*/
		CANIF_USER_TYPE_CAN_PDUR   /* CanIfRxUserType*/
	
    },
	{
		0x377,      /* CanIfCanRxPduCanId*/
		8,         /* CanIfCanRxPduDlc*/
		16,         /* CanIfCanRxPduId*/
		FALSE,          /* CanIfReadRxPduData*/
        0xFF,             /* CanIfRxBufferIndex */
		FALSE,         /* CanIfReadRxPduNotifyStatus*/	
		0xFF,             /* CanIfRxNotifyIndex */
		CANIF_STANDARD_CAN,      /* CanIfRxPduIdCanIdType*/
		CANIF_USER_TYPE_CAN_PDUR   /* CanIfRxUserType*/
	
    },
	{
		0x36f,      /* CanIfCanRxPduCanId*/
		8,         /* CanIfCanRxPduDlc*/
		17,         /* CanIfCanRxPduId*/
		FALSE,          /* CanIfReadRxPduData*/
        0xFF,             /* CanIfRxBufferIndex */
		FALSE,         /* CanIfReadRxPduNotifyStatus*/	
		0xFF,             /* CanIfRxNotifyIndex */
		CANIF_STANDARD_CAN,      /* CanIfRxPduIdCanIdType*/
		CANIF_USER_TYPE_CAN_PDUR   /* CanIfRxUserType*/
	
    },
	{
		0x36c,      /* CanIfCanRxPduCanId*/
		8,         /* CanIfCanRxPduDlc*/
		18,         /* CanIfCanRxPduId*/
		FALSE,          /* CanIfReadRxPduData*/
        0xFF,             /* CanIfRxBufferIndex */
		FALSE,         /* CanIfReadRxPduNotifyStatus*/	
		0xFF,             /* CanIfRxNotifyIndex */
		CANIF_STANDARD_CAN,      /* CanIfRxPduIdCanIdType*/
		CANIF_USER_TYPE_CAN_PDUR   /* CanIfRxUserType*/
	
    },
	{
		0x2a6,      /* CanIfCanRxPduCanId*/
		8,         /* CanIfCanRxPduDlc*/
		19,         /* CanIfCanRxPduId*/
		FALSE,          /* CanIfReadRxPduData*/
        0xFF,             /* CanIfRxBufferIndex */
		FALSE,         /* CanIfReadRxPduNotifyStatus*/	
		0xFF,             /* CanIfRxNotifyIndex */
		CANIF_STANDARD_CAN,      /* CanIfRxPduIdCanIdType*/
		CANIF_USER_TYPE_CAN_PDUR   /* CanIfRxUserType*/
	
    },
	{
		0x612,      /* CanIfCanRxPduCanId*/
		8,         /* CanIfCanRxPduDlc*/
		20,         /* CanIfCanRxPduId*/
		FALSE,          /* CanIfReadRxPduData*/
        0xFF,             /* CanIfRxBufferIndex */
		FALSE,         /* CanIfReadRxPduNotifyStatus*/	
		0xFF,             /* CanIfRxNotifyIndex */
		CANIF_STANDARD_CAN,      /* CanIfRxPduIdCanIdType*/
		CANIF_USER_TYPE_CAN_PDUR   /* CanIfRxUserType*/
	
    },
	{
		0x611,      /* CanIfCanRxPduCanId*/
		8,         /* CanIfCanRxPduDlc*/
		21,         /* CanIfCanRxPduId*/
		FALSE,          /* CanIfReadRxPduData*/
        0xFF,             /* CanIfRxBufferIndex */
		FALSE,         /* CanIfReadRxPduNotifyStatus*/	
		0xFF,             /* CanIfRxNotifyIndex */
		CANIF_STANDARD_CAN,      /* CanIfRxPduIdCanIdType*/
		CANIF_USER_TYPE_CAN_PDUR   /* CanIfRxUserType*/
	
    },
	{
		0x613,      /* CanIfCanRxPduCanId*/
		8,         /* CanIfCanRxPduDlc*/
		22,         /* CanIfCanRxPduId*/
		FALSE,          /* CanIfReadRxPduData*/
        0xFF,             /* CanIfRxBufferIndex */
		FALSE,         /* CanIfReadRxPduNotifyStatus*/	
		0xFF,             /* CanIfRxNotifyIndex */
		CANIF_STANDARD_CAN,      /* CanIfRxPduIdCanIdType*/
		CANIF_USER_TYPE_CAN_PDUR   /* CanIfRxUserType*/
	
    },
	{
		0x379,      /* CanIfCanRxPduCanId*/
		8,         /* CanIfCanRxPduDlc*/
		23,         /* CanIfCanRxPduId*/
		FALSE,          /* CanIfReadRxPduData*/
        0xFF,             /* CanIfRxBufferIndex */
		FALSE,         /* CanIfReadRxPduNotifyStatus*/	
		0xFF,             /* CanIfRxNotifyIndex */
		CANIF_STANDARD_CAN,      /* CanIfRxPduIdCanIdType*/
		CANIF_USER_TYPE_CAN_PDUR   /* CanIfRxUserType*/
	
    },
	{
		0x368,      /* CanIfCanRxPduCanId*/
		8,         /* CanIfCanRxPduDlc*/
		24,         /* CanIfCanRxPduId*/
		FALSE,          /* CanIfReadRxPduData*/
        0xFF,             /* CanIfRxBufferIndex */
		FALSE,         /* CanIfReadRxPduNotifyStatus*/	
		0xFF,             /* CanIfRxNotifyIndex */
		CANIF_STANDARD_CAN,      /* CanIfRxPduIdCanIdType*/
		CANIF_USER_TYPE_CAN_PDUR   /* CanIfRxUserType*/
	
    },
	{
		0x55c,      /* CanIfCanRxPduCanId*/
		8,         /* CanIfCanRxPduDlc*/
		25,         /* CanIfCanRxPduId*/
		FALSE,          /* CanIfReadRxPduData*/
        0xFF,             /* CanIfRxBufferIndex */
		FALSE,         /* CanIfReadRxPduNotifyStatus*/	
		0xFF,             /* CanIfRxNotifyIndex */
		CANIF_STANDARD_CAN,      /* CanIfRxPduIdCanIdType*/
		CANIF_USER_TYPE_CAN_PDUR   /* CanIfRxUserType*/
	
    },
	{
		0x580,      /* CanIfCanRxPduCanId*/
		8,         /* CanIfCanRxPduDlc*/
		26,         /* CanIfCanRxPduId*/
		FALSE,          /* CanIfReadRxPduData*/
        0xFF,             /* CanIfRxBufferIndex */
		FALSE,         /* CanIfReadRxPduNotifyStatus*/	
		0xFF,             /* CanIfRxNotifyIndex */
		CANIF_STANDARD_CAN,      /* CanIfRxPduIdCanIdType*/
		CANIF_USER_TYPE_CAN_PDUR   /* CanIfRxUserType*/
	
    },
	{
		0x171,      /* CanIfCanRxPduCanId*/
		8,         /* CanIfCanRxPduDlc*/
		27,         /* CanIfCanRxPduId*/
		FALSE,          /* CanIfReadRxPduData*/
        0xFF,             /* CanIfRxBufferIndex */
		FALSE,         /* CanIfReadRxPduNotifyStatus*/	
		0xFF,             /* CanIfRxNotifyIndex */
		CANIF_STANDARD_CAN,      /* CanIfRxPduIdCanIdType*/
		CANIF_USER_TYPE_CAN_PDUR   /* CanIfRxUserType*/
	
    },
	{
		0x398,      /* CanIfCanRxPduCanId*/
		8,         /* CanIfCanRxPduDlc*/
		28,         /* CanIfCanRxPduId*/
		FALSE,          /* CanIfReadRxPduData*/
        0xFF,             /* CanIfRxBufferIndex */
		FALSE,         /* CanIfReadRxPduNotifyStatus*/	
		0xFF,             /* CanIfRxNotifyIndex */
		CANIF_STANDARD_CAN,      /* CanIfRxPduIdCanIdType*/
		CANIF_USER_TYPE_CAN_PDUR   /* CanIfRxUserType*/
	
    },
	{
		0x181,      /* CanIfCanRxPduCanId*/
		8,         /* CanIfCanRxPduDlc*/
		29,         /* CanIfCanRxPduId*/
		FALSE,          /* CanIfReadRxPduData*/
        0xFF,             /* CanIfRxBufferIndex */
		FALSE,         /* CanIfReadRxPduNotifyStatus*/	
		0xFF,             /* CanIfRxNotifyIndex */
		CANIF_STANDARD_CAN,      /* CanIfRxPduIdCanIdType*/
		CANIF_USER_TYPE_CAN_PDUR   /* CanIfRxUserType*/
	
    },
	{
		0x179,      /* CanIfCanRxPduCanId*/
		8,         /* CanIfCanRxPduDlc*/
		30,         /* CanIfCanRxPduId*/
		FALSE,          /* CanIfReadRxPduData*/
        0xFF,             /* CanIfRxBufferIndex */
		FALSE,         /* CanIfReadRxPduNotifyStatus*/	
		0xFF,             /* CanIfRxNotifyIndex */
		CANIF_STANDARD_CAN,      /* CanIfRxPduIdCanIdType*/
		CANIF_USER_TYPE_CAN_PDUR   /* CanIfRxUserType*/
	
    },
	{
		0x722,      /* CanIfCanRxPduCanId*/
		8,         /* CanIfCanRxPduDlc*/
		18,         /* CanIfCanRxPduId*/
		FALSE,          /* CanIfReadRxPduData*/
        0xFF,             /* CanIfRxBufferIndex */
		FALSE,         /* CanIfReadRxPduNotifyStatus*/	
		0xFF,             /* CanIfRxNotifyIndex */
		CANIF_STANDARD_CAN,      /* CanIfRxPduIdCanIdType*/
		CANIF_USER_TYPE_XCP_CCP   /* CanIfRxUserType*/
	
    },
	{
		0x7df,      /* CanIfCanRxPduCanId*/
		8,         /* CanIfCanRxPduDlc*/
		44,         /* CanIfCanRxPduId*/
		FALSE,          /* CanIfReadRxPduData*/
        0xFF,             /* CanIfRxBufferIndex */
		FALSE,         /* CanIfReadRxPduNotifyStatus*/	
		0xFF,             /* CanIfRxNotifyIndex */
		CANIF_STANDARD_CAN,      /* CanIfRxPduIdCanIdType*/
		CANIF_USER_TYPE_CAN_TP   /* CanIfRxUserType*/
	
    },
	{
		0x70D,      /* CanIfCanRxPduCanId*/ //LIUYUN
		8,         /* CanIfCanRxPduDlc*/
		45,         /* CanIfCanRxPduId*/
		FALSE,          /* CanIfReadRxPduData*/
        0xFF,             /* CanIfRxBufferIndex */
		FALSE,         /* CanIfReadRxPduNotifyStatus*/	
		0xFF,             /* CanIfRxNotifyIndex */
		CANIF_STANDARD_CAN,      /* CanIfRxPduIdCanIdType*/
		CANIF_USER_TYPE_CAN_TP   /* CanIfRxUserType*/
	
    },
};
#define CANIF_STOP_CONST_PBCFG
#include "CanIf_MemMap.h"

/* define hth index ,every Hth used as BASIC CAN have a index cfg 
 * The Max size determined at link time, equal to CanIf_HthConfigData size.
 */
#define CANIF_START_CONST_PBCFG
#include "CanIf_MemMap.h"
CONST(CanIf_HohIndexType, CANIF_CONST_PBCFG) CanIf_HthIndexCfg[CANIF_NUMBER_OF_HTHUSED] = 
{
    {
        0,/* start index */
        3 /* stop index */
    },
    {
        4,/* start index */
        4 /* stop index */
    },
    {
        5,/* start index */
        5 /* stop index */
    },
    {
        6,/* start index */
        6 /* stop index */
    },
    {
        7,/* start index */
        7 /* stop index */
    },
    {
        8,/* start index */
        8 /* stop index */
    },
    {
        9,/* start index */
        9 /* stop index */
    },
    {
        10,/* start index */
        10 /* stop index */
    },
    {
        11,/* start index */
        11 /* stop index */
    },
    {
        12,/* start index */
        12 /* stop index */
    },
};
#define CANIF_STOP_CONST_PBCFG
#include "CanIf_MemMap.h"


/* every Hrh used as BASIC CAN and 'CanIfSoftwareFilterHrh' Configured TRUE have a RangeMask */
#define CANIF_START_CONST_PBCFG
#include "CanIf_MemMap.h"
CONST(CanIf_HrhRangeMaskType, CANIF_CONST_PBCFG) CanIf_HrhRangeMaskConfig[CANIF_NUMBER_OF_HRHUSED] =
{
    {
        0x7ff, 
        0x326      
    },
    {
        0x7ff, 
        0x230      
    },
    {
        0x7ff, 
        0x3be      
    },
    {
        0x7ff, 
        0x377      
    },
    {
        0x7ff, 
        0x36f      
    },
    {
        0x7ff, 
        0x36c      
    },
    {
        0x7ff, 
        0x2a6      
    },
    {
        0x7f0, 
        0x610      
    },
    {
        0x7ff, 
        0x379      
    },
    {
        0x7ff, 
        0x368      
    },
    {
        0x700, 
        0x500      
    },
    {
        0x7ff, 
        0x171      
    },
    {
        0x7ff, 
        0x398      
    },
    {
        0x7ff, 
        0x181      
    },
    {
        0x7ff, 
        0x179      
    },
    {
        0x7ff, 
        0x722      
    },
    {
        0x7ff, 
        0x7df      
    },
    {
        0x7ff, 
        0x7b0      
    },  
};
#define CANIF_STOP_CONST_PBCFG
#include "CanIf_MemMap.h"


#define CANIF_START_CONST_PBCFG
#include "CanIf_MemMap.h"
CONST(CanIf_HohIndexType, CANIF_CONST_PBCFG) CanIf_HrhPduIndex[CANIF_NUMBER_OF_HRHUSED] = 
{
    {
        0,/* start index */
        0 /* stop index */
    },
    {
        1,/* start index */
        1 /* stop index */
    },
    {
        2,/* start index */
        2 /* stop index */
    },
    {
        3,/* start index */
        3 /* stop index */
    },
    {
        4,/* start index */
        4 /* stop index */
    },
    {
        5,/* start index */
        5 /* stop index */
    },
    {
        6,/* start index */
        6 /* stop index */
    },
    {
        7,/* start index */
        9 /* stop index */
    },
    {
        10,/* start index */
        10 /* stop index */
    },
    {
        11,/* start index */
        11 /* stop index */
    },
    {
        12,/* start index */
        13 /* stop index */
    },
    {
        14,/* start index */
        14 /* stop index */
    },
    {
        15,/* start index */
        15 /* stop index */
    },
    {
        16,/* start index */
        16 /* stop index */
    },
    {
        17,/* start index */
        17 /* stop index */
    },
    {
        18,/* start index */
        18 /* stop index */
    },
    {
        19,/* start index */
        19 /* stop index */
    },
    {
        20,/* start index */
        20 /* stop index */
    },
};
#define CANIF_STOP_CONST_PBCFG
#include "CanIf_MemMap.h"


/* each CanIfHrhConfig have a corresponding HrhIdConfig which contains the HRH */
#define CANIF_START_CONST_PBCFG
#include "CanIf_MemMap.h"
CONST(uint8, CANIF_CONST_PBCFG) CanIf_HrhIdConfig[CANIF_NUMBER_OF_HRHUSED] =
{
    10, 
    11, 
    12, 
    13, 
    14, 
    15, 
    16, 
    17, 
    18, 
    19, 
    20, 
    22, 
    21, 
    23, 
    24, 
    25, 
    8, 
    9, 
};
#define CANIF_STOP_CONST_PBCFG
#include "CanIf_MemMap.h"

#define CANIF_START_CONST_PBCFG
#include "CanIf_MemMap.h"
CONST(CanIf_HrhFilterConfigType, CANIF_CONST_PBCFG) CanIf_HrhFilterRefCfg =
{
    &CanIf_HrhIdConfig[0u],
    &CanIf_HrhRangeMaskConfig[0u],
    &CanIf_HrhPduIndex[0u]
};
#define CANIF_STOP_CONST_PBCFG
#include "CanIf_MemMap.h"

/*=======[E N D   O F   F I L E]==============================================*/
