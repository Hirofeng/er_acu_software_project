/*============================================================================*/
/*  Copyright (C) 2009-2011,  INFRASTRUCTURE SOFTWARE CO.,LTD.
 *
 *  All rights reserved. This software is  property. Duplication
 *  or disclosure without  written authorization is prohibited.
 *
 *  @file        <Dcm_Cfg.c>
 *  @brief       <>
 *
 *  <Compiler: CodeWarrior    MCU:XXX>
 *
 *  @author     <chen maosen>
 *  @date       <2013-03-20>
 */
/*============================================================================*/
/******************************* references ************************************/
#include "Dcm_Types.h"
#include "Dcm_CfgType.h"
#include "Rte_Dcm.h"
#include "Dcm_Cfg.h"

#include "UDS.h"


#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "MemMap.h"
/**********************************************************************
 ***********************DcmGeneral Container***************************
 **********************************************************************/
CONST(Dcm_GeneralCfgType,DCM_CONST)Dcm_GeneralCfg =
{
        DCM_DEV_ERROR_DETECT,
        DCM_REQUEST_INDICATION_ENABLED,
        DCM_RESPOND_ALL_REQUEST,
        DCM_VERSION_INFO_API,
        500u
};

/*********************************************************************
 ***********************DcmPage Container*****************************
 *********************************************************************/
#if(STD_ON == DCM_PAGEDBUFFER_ENABLED)
CONST(Dcm_PageBufferCfgType,DCM_CONST)Dcm_PageBufferCfg =
{
        DCM_PAGEDBUFFER_ENABLED,
        50u
};
#endif

/**********************************************************************
 ***********************DSP Container**********************************
 *********************************************************************/
/************************************************
 ****DcmDspSecurityRow container(Multiplicity=0..31)****
 ************************************************/
#if(STD_ON == DCM_SECURITY_FUNC_ENABLED)
STATIC  CONST(Dcm_DspSecurityRowType,DCM_CONST)Dcm_DspSecurityRow[4] =
{
        {
         0u,      	/*DcmDspSecurityLevel*/
         0u,      	/*DcmDspSecuritySeedSize*/
         0u,      	/*DcmDspSecurityKeySize*/
         0u,      	/*DcmDspSecurityADRSize*/
         3u,      	/*DcmDspSecurityNumAttDelay*/
         0u,     	/*DcmDspSecurityNumAttLock*/
         10000u,    	/*DcmDspSecurityDelayTime*/
         0u,   		/*DcmDspSecurityDelayTimeOnBoot*/
         {NULL_PTR,NULL_PTR}
       },
       {
         1u,        /*level1(1,2)*/
         4u,
         4u,
         0u,
         3u,
         0u,
         10000u,    /*10s*/  
         0u,
         {Rte_CompareKey1,Rte_GetSeed1}
       },
       {
         2u,       /*level3(3,4)*/
         4u,
         4u,
         0u,
         3u,
         0u,
         10000u,   /*10s*/
         0u,
         {Rte_CompareKey3,Rte_GetSeed3}
       },
       {
         9u,      /*level11(0x11,0x12)*/
         4u,
         4u,
         0u,
         3u,
         0u,
         10000u,  /*10s*/
         0u,
         {Rte_CompareKey11,Rte_GetSeed11}
       },
};
#endif

/************************************************
 ****DcmDspSecurity container(Multiplicity=1)****
 ************************************************/
STATIC  CONST(Dcm_DspSecurityType,DCM_CONST)Dcm_DspSecurity =
{
        &Dcm_DspSecurityRow[0],
        4u,
};

/************************************************
 ****DcmDspSessionRow container(Multiplicity=0..31)
 ************************************************/
#if(STD_ON == DCM_SESSION_FUNC_ENABLED)
STATIC  CONST(Dcm_DspSessionRowType,DCM_CONST)Dcm_DspSessionRow[3] =
{
        {1u,50u,2000u},/*DcmDspSessionLevel,DcmDspSessionP2ServerMax,DcmDspSessionP2StarServerMax*/
        {2u,50u,2000u},
        {3u,50u,2000u},
};
#endif

/************************************************
 *******Dcm_DspSession container(Multiplicity=1)*
 ************************************************/
STATIC  CONST(Dcm_DspSessionType,DCM_CONST)Dcm_DspSession =
{
        &Dcm_DspSessionRow[0],
        3u,
};

/************************************************
 ******DcmDspDid container(Multiplicity=0..*)****
 ***********************************************/

/*****************************************************
 *Dcm_DspDidControlRecordSizes container configration
 *****************************************************/
/*DID=0002*/
STATIC  CONST( Dcm_DspDidControlRecordSizesType,DCM_CONST)Dcm_DspDid1_ControlRecordSizesCfg[2]=
{
		{1,0,0},	/*DcmDspDidControlEnableMaskRecordSize,DcmDspDidControlOptionRecordSize,DcmDspDidControlStatusRecordSize*/
		{1,1,0},    
};
/*DID=0003*/    
STATIC  CONST(Dcm_DspDidControlRecordSizesType,DCM_CONST)Dcm_DspDid2_ControlRecordSizesCfg[1]=
{
		{1,1,0},
};
/*********************************************
 *DcmDspDidControl configration
 ********************************************/
STATIC  CONST(uint8,DCM_CONST)Dcm_DspDid1_Control_SecRefCfg[1]= {1};	/*DID=0002*/
STATIC  CONST(uint8,DCM_CONST)Dcm_DspDid1_Control_SesRefCfg[1]= {3};
/**DcmDspDidControl configration**/
STATIC  CONST(Dcm_DspDidControlType,DCM_CONST)Dcm_DspDid1_ControlContainerCfg =
{
        1u,
        &Dcm_DspDid1_Control_SecRefCfg[0],
        1u,
        &Dcm_DspDid1_Control_SesRefCfg[0],
        NULL_PTR,   							 /*02,DcmDspDidFreezeCurrentState reference to DcmDspDidControlRecordSizes*/
        NULL_PTR,   							 /*01,DcmDspDidResetToDefault reference to DcmDspDidControlRecordSizes*/
        &Dcm_DspDid1_ControlRecordSizesCfg[0],   /*00,DcmDspDidReturnControlToEcu reference to DcmDspDidControlRecordSizes*/
        &Dcm_DspDid1_ControlRecordSizesCfg[1],   /*03,DcmDspDidShortTermAdjustement reference to DcmDspDidControlRecordSizes*/
};
/*=========*/
STATIC  CONST(uint8,DCM_CONST)Dcm_DspDid2_Control_SecRefCfg[1]= {1};	/*DID=0003*/	
STATIC  CONST(uint8,DCM_CONST)Dcm_DspDid2_Control_SesRefCfg[1]= {3};
STATIC  CONST(Dcm_DspDidControlType,DCM_CONST)Dcm_DspDid2_ControlContainerCfg =
{
        1u,
        &Dcm_DspDid2_Control_SecRefCfg[0],
        1u,
        &Dcm_DspDid2_Control_SesRefCfg[0],
        NULL_PTR,   /*DcmDspDidFreezeCurrentState reference to DcmDspDidControlRecordSizes*/
        NULL_PTR,   /*DcmDspDidResetToDefault reference to DcmDspDidControlRecordSizes*/
        NULL_PTR,   /*DcmDspDidReturnControlToEcu reference to DcmDspDidControlRecordSizes*/
        &Dcm_DspDid2_ControlRecordSizesCfg[0],   /*DcmDspDidShortTermAdjustement reference to DcmDspDidControlRecordSizes*/
};
/****************************************
*DcmDspDidRead container configration
*****************************************/
//STATIC  CONST(uint8,DCM_CONST)Dcm_DspDid1_Read_SecRefCfg[1] = {1};		/*DID=0x0091,Read*/	
STATIC  CONST(uint8,DCM_CONST)Dcm_DspDid1_Read_SecRefCfg[4] = {0,1,2,9};		/*DID=0x0091,Read*/	
STATIC  CONST(uint8,DCM_CONST)Dcm_DspDid1_Read_SesRefCfg[2] = {1,3};
STATIC  CONST(Dcm_DspDidReadType,DCM_CONST)Dcm_DspDid1_ReadContainerCfg =
{
      //  1u,
		4u,
        &Dcm_DspDid1_Read_SecRefCfg[0],
        2u,
        &Dcm_DspDid1_Read_SesRefCfg[0],
};

STATIC  CONST(uint8,DCM_CONST)Dcm_DspDid2_Read_SecRefCfg[4] = {0,1,2,9};		/*DID=0x0002,Read*/	
STATIC  CONST(uint8,DCM_CONST)Dcm_DspDid2_Read_SesRefCfg[2] = {1,3};
STATIC  CONST(Dcm_DspDidReadType,DCM_CONST)Dcm_DspDid2_ReadContainerCfg =
{
        4u,
        &Dcm_DspDid2_Read_SecRefCfg[0],
        2u,
        &Dcm_DspDid2_Read_SesRefCfg[0],
};

STATIC  CONST(uint8,DCM_CONST)Dcm_DspDid3_Read_SecRefCfg[4] = {0,1,2,9};		/*DID=0x0003,Read*/	
STATIC  CONST(uint8,DCM_CONST)Dcm_DspDid3_Read_SesRefCfg[2] = {1,3};
STATIC  CONST(Dcm_DspDidReadType,DCM_CONST)Dcm_DspDid3_ReadContainerCfg =
{
        4u,
        &Dcm_DspDid3_Read_SecRefCfg[0],
        2u,
        &Dcm_DspDid3_Read_SesRefCfg[0],
};

STATIC  CONST(uint8,DCM_CONST)Dcm_DspDid4_Read_SecRefCfg[4] = {0,1,2,9};		/*DID=0x0003,Read*/	
STATIC  CONST(uint8,DCM_CONST)Dcm_DspDid4_Read_SesRefCfg[2] = {1,3};
STATIC  CONST(Dcm_DspDidReadType,DCM_CONST)Dcm_DspDid4_ReadContainerCfg =
{
        4u,
        &Dcm_DspDid4_Read_SecRefCfg[0],
        2u,
        &Dcm_DspDid4_Read_SesRefCfg[0],
};

STATIC  CONST(uint8,DCM_CONST)Dcm_DspDid5_Read_SecRefCfg[4] = {0,1,2,9};		/*DID=0x0003,Read*/	
STATIC  CONST(uint8,DCM_CONST)Dcm_DspDid5_Read_SesRefCfg[2] = {1,3};
STATIC  CONST(Dcm_DspDidReadType,DCM_CONST)Dcm_DspDid5_ReadContainerCfg =
{
        4u,
        &Dcm_DspDid5_Read_SecRefCfg[0],
        2u,
        &Dcm_DspDid5_Read_SesRefCfg[0],
};

STATIC  CONST(uint8,DCM_CONST)Dcm_DspDid6_Read_SecRefCfg[4] = {0,1,2,9};		/*DID=0x0003,Read*/	
STATIC  CONST(uint8,DCM_CONST)Dcm_DspDid6_Read_SesRefCfg[2] = {1,3};
STATIC  CONST(Dcm_DspDidReadType,DCM_CONST)Dcm_DspDid6_ReadContainerCfg =
{
        4u,
        &Dcm_DspDid6_Read_SecRefCfg[0],
        2u,
        &Dcm_DspDid6_Read_SesRefCfg[0],
};

STATIC  CONST(uint8,DCM_CONST)Dcm_DspDid7_Read_SecRefCfg[4] = {0,1,2,9};		/*DID=0x0003,Read*/	
STATIC  CONST(uint8,DCM_CONST)Dcm_DspDid7_Read_SesRefCfg[2] = {1,3};
STATIC  CONST(Dcm_DspDidReadType,DCM_CONST)Dcm_DspDid7_ReadContainerCfg =
{
        4u,
        &Dcm_DspDid7_Read_SecRefCfg[0],
        2u,
        &Dcm_DspDid7_Read_SesRefCfg[0],
};

STATIC  CONST(uint8,DCM_CONST)Dcm_DspDid8_Read_SecRefCfg[4] = {0,1,2,9};		/*DID=0x0003,Read*/	
STATIC  CONST(uint8,DCM_CONST)Dcm_DspDid8_Read_SesRefCfg[2] = {1,3};
STATIC  CONST(Dcm_DspDidReadType,DCM_CONST)Dcm_DspDid8_ReadContainerCfg =
{
        4u,
        &Dcm_DspDid8_Read_SecRefCfg[0],
        2u,
        &Dcm_DspDid8_Read_SesRefCfg[0],
};

STATIC  CONST(uint8,DCM_CONST)Dcm_DspDid9_Read_SecRefCfg[4] = {0,1,2,9};		/*DID=0x0003,Read*/	
STATIC  CONST(uint8,DCM_CONST)Dcm_DspDid9_Read_SesRefCfg[2] = {1,3};
STATIC  CONST(Dcm_DspDidReadType,DCM_CONST)Dcm_DspDid9_ReadContainerCfg =
{
        4u,
        &Dcm_DspDid9_Read_SecRefCfg[0],
        2u,
        &Dcm_DspDid9_Read_SesRefCfg[0],
};

STATIC  CONST(uint8,DCM_CONST)Dcm_DspDid10_Read_SecRefCfg[4] = {0,1,2,9};		/*DID=0x0003,Read*/	
STATIC  CONST(uint8,DCM_CONST)Dcm_DspDid10_Read_SesRefCfg[2] = {1,3};
STATIC  CONST(Dcm_DspDidReadType,DCM_CONST)Dcm_DspDid10_ReadContainerCfg =
{
        4u,
        &Dcm_DspDid10_Read_SecRefCfg[0],
        2u,
        &Dcm_DspDid10_Read_SesRefCfg[0],
};

STATIC  CONST(uint8,DCM_CONST)Dcm_DspDid11_Read_SecRefCfg[4] = {0,1,2,9};		/*DID=0x0003,Read*/	
STATIC  CONST(uint8,DCM_CONST)Dcm_DspDid11_Read_SesRefCfg[2] = {1,3};
STATIC  CONST(Dcm_DspDidReadType,DCM_CONST)Dcm_DspDid11_ReadContainerCfg =
{
        4u,
        &Dcm_DspDid11_Read_SecRefCfg[0],
        2u,
        &Dcm_DspDid1_Read_SesRefCfg[0],
};

STATIC  CONST(uint8,DCM_CONST)Dcm_DspDid12_Read_SecRefCfg[4] = {0,1,2,9};		/*DID=0x0003,Read*/	
STATIC  CONST(uint8,DCM_CONST)Dcm_DspDid12_Read_SesRefCfg[2] = {1,3};
STATIC  CONST(Dcm_DspDidReadType,DCM_CONST)Dcm_DspDid12_ReadContainerCfg =
{
        4u,
        &Dcm_DspDid12_Read_SecRefCfg[0],
        2u,
        &Dcm_DspDid12_Read_SesRefCfg[0],
};


/*******************************************
 *DcmDspDidWrite container configuration,which is in the DcmDspDidInfo container
 ******************************************/
STATIC  CONST(uint8,DCM_CONST)Dcm_DspDid1_Write_SecRefCfg[1] = { 1 };  /*DID=0x0091,Write*/
STATIC  CONST(uint8,DCM_CONST)Dcm_DspDid1_Write_SesRefCfg[1] = { 3 };
STATIC  CONST(Dcm_DspDidWriteType,DCM_CONST)Dcm_DspDid1_WriteContainerCfg=
{
        1u,
        &Dcm_DspDid1_Write_SecRefCfg[0],
        1u,
        &Dcm_DspDid1_Write_SesRefCfg[0],
};

STATIC  CONST(uint8,DCM_CONST)Dcm_DspDid2_Write_SecRefCfg[1] = { 1 };  /*DID=0x0091,Write*/
STATIC  CONST(uint8,DCM_CONST)Dcm_DspDid2_Write_SesRefCfg[1] = { 3 };
STATIC  CONST(Dcm_DspDidWriteType,DCM_CONST)Dcm_DspDid2_WriteContainerCfg=
{
        1u,
        &Dcm_DspDid2_Write_SecRefCfg[0],
        1u,
        &Dcm_DspDid2_Write_SesRefCfg[0],
};
STATIC  CONST(uint8,DCM_CONST)Dcm_DspDid3_Write_SecRefCfg[1] = { 1 };  /*DID=0x0091,Write*/
STATIC  CONST(uint8,DCM_CONST)Dcm_DspDid3_Write_SesRefCfg[1] = { 3 };
STATIC  CONST(Dcm_DspDidWriteType,DCM_CONST)Dcm_DspDid3_WriteContainerCfg=
{
        1u,
        &Dcm_DspDid3_Write_SecRefCfg[0],
        1u,
        &Dcm_DspDid3_Write_SesRefCfg[0],
};
/*******************************************
 *DcmDspDidWrite container configuration,which is in the DcmDspDidInfo container
 ******************************************/
STATIC  CONST(uint8,DCM_CONST)Dcm_DspDid4_Write_SecRefCfg[1] = { 1 };  /*DID=0x0091,Write*/
STATIC  CONST(uint8,DCM_CONST)Dcm_DspDid4_Write_SesRefCfg[1] = { 3 };
STATIC  CONST(Dcm_DspDidWriteType,DCM_CONST)Dcm_DspDid4_WriteContainerCfg=
{
        1u,
        &Dcm_DspDid4_Write_SecRefCfg[0],
        1u,
        &Dcm_DspDid4_Write_SesRefCfg[0],
};

/*******************************************
 *DcmDspDidWrite container configuration,which is in the DcmDspDidInfo container
 ******************************************/
STATIC  CONST(uint8,DCM_CONST)Dcm_DspDid5_Write_SecRefCfg[1] = { 1 };  /*DID=0x0091,Write*/
STATIC  CONST(uint8,DCM_CONST)Dcm_DspDid5_Write_SesRefCfg[1] = { 3 };
STATIC  CONST(Dcm_DspDidWriteType,DCM_CONST)Dcm_DspDid5_WriteContainerCfg=
{
        1u,
        &Dcm_DspDid5_Write_SecRefCfg[0],
        1u,
        &Dcm_DspDid5_Write_SesRefCfg[0],
};

/*******************************************
 *DcmDspDidWrite container configuration,which is in the DcmDspDidInfo container
 ******************************************/
STATIC  CONST(uint8,DCM_CONST)Dcm_DspDid6_Write_SecRefCfg[1] = { 1 };  /*DID=0x0091,Write*/
STATIC  CONST(uint8,DCM_CONST)Dcm_DspDid6_Write_SesRefCfg[1] = { 3 };
STATIC  CONST(Dcm_DspDidWriteType,DCM_CONST)Dcm_DspDid6_WriteContainerCfg=
{
        1u,
        &Dcm_DspDid6_Write_SecRefCfg[0],
        1u,
        &Dcm_DspDid6_Write_SesRefCfg[0],
};

/*******************************************
 *DcmDspDidWrite container configuration,which is in the DcmDspDidInfo container
 ******************************************/
STATIC  CONST(uint8,DCM_CONST)Dcm_DspDid7_Write_SecRefCfg[1] = { 1 };  /*DID=0x0091,Write*/
STATIC  CONST(uint8,DCM_CONST)Dcm_DspDid7_Write_SesRefCfg[1] = { 3 };
STATIC  CONST(Dcm_DspDidWriteType,DCM_CONST)Dcm_DspDid7_WriteContainerCfg=
{
        1u,
        &Dcm_DspDid7_Write_SecRefCfg[0],
        1u,
        &Dcm_DspDid7_Write_SesRefCfg[0],
};

/*******************************************
 *DcmDspDidWrite container configuration,which is in the DcmDspDidInfo container
 ******************************************/
STATIC  CONST(uint8,DCM_CONST)Dcm_DspDid8_Write_SecRefCfg[1] = { 1 };  /*DID=0x0091,Write*/
STATIC  CONST(uint8,DCM_CONST)Dcm_DspDid8_Write_SesRefCfg[1] = { 3 };
STATIC  CONST(Dcm_DspDidWriteType,DCM_CONST)Dcm_DspDid8_WriteContainerCfg=
{
        1u,
        &Dcm_DspDid8_Write_SecRefCfg[0],
        1u,
        &Dcm_DspDid8_Write_SesRefCfg[0],
};

/*******************************************
 *DcmDspDidWrite container configuration,which is in the DcmDspDidInfo container
 ******************************************/
STATIC  CONST(uint8,DCM_CONST)Dcm_DspDid9_Write_SecRefCfg[1] = { 1 };  /*DID=0x0091,Write*/
STATIC  CONST(uint8,DCM_CONST)Dcm_DspDid9_Write_SesRefCfg[1] = { 3 };
STATIC  CONST(Dcm_DspDidWriteType,DCM_CONST)Dcm_DspDid9_WriteContainerCfg=
{
        1u,
        &Dcm_DspDid9_Write_SecRefCfg[0],
        1u,
        &Dcm_DspDid9_Write_SesRefCfg[0],
};

/*******************************************
 *DcmDspDidWrite container configuration,which is in the DcmDspDidInfo container
 ******************************************/
STATIC  CONST(uint8,DCM_CONST)Dcm_DspDid10_Write_SecRefCfg[1] = { 1 };  /*DID=0x0091,Write*/
STATIC  CONST(uint8,DCM_CONST)Dcm_DspDid10_Write_SesRefCfg[1] = { 3 };
STATIC  CONST(Dcm_DspDidWriteType,DCM_CONST)Dcm_DspDid10_WriteContainerCfg=
{
        1u,
        &Dcm_DspDid10_Write_SecRefCfg[0],
        1u,
        &Dcm_DspDid10_Write_SesRefCfg[0],
};

/*******************************************
 *DcmDspDidWrite container configuration,which is in the DcmDspDidInfo container
 ******************************************/
STATIC  CONST(uint8,DCM_CONST)Dcm_DspDid11_Write_SecRefCfg[1] = { 1 };  /*DID=0x0091,Write*/
STATIC  CONST(uint8,DCM_CONST)Dcm_DspDid11_Write_SesRefCfg[1] = { 3 };
STATIC  CONST(Dcm_DspDidWriteType,DCM_CONST)Dcm_DspDid11_WriteContainerCfg=
{
        1u,
        &Dcm_DspDid11_Write_SecRefCfg[0],
        1u,
        &Dcm_DspDid11_Write_SesRefCfg[0],
};

/*******************************************
 *DcmDspDidWrite container configuration,which is in the DcmDspDidInfo container
 ******************************************/
STATIC  CONST(uint8,DCM_CONST)Dcm_DspDid12_Write_SecRefCfg[1] = { 1 };  /*DID=0x0091,Write*/
STATIC  CONST(uint8,DCM_CONST)Dcm_DspDid12_Write_SesRefCfg[1] = { 3 };
STATIC  CONST(Dcm_DspDidWriteType,DCM_CONST)Dcm_DspDid12_WriteContainerCfg=
{
        1u,
        &Dcm_DspDid12_Write_SecRefCfg[0],
        1u,
        &Dcm_DspDid12_Write_SesRefCfg[0],
};
/*******************************************
 **DcmDspDidAccess container configration**
 ******************************************/
STATIC  CONST(Dcm_DspDidAccessType,DCM_CONST)Dcm_DspDid1_AccessCfg =   	/*DID=0x0091*/
{
       NULL_PTR,
       &Dcm_DspDid1_ReadContainerCfg,
       &Dcm_DspDid1_WriteContainerCfg,
};
    
    
STATIC  CONST(Dcm_DspDidAccessType,DCM_CONST)Dcm_DspDid2_AccessCfg =   	/*DID=0x0002*/
{
	       NULL_PTR,
	       &Dcm_DspDid2_ReadContainerCfg,
	       &Dcm_DspDid2_WriteContainerCfg,
};
STATIC  CONST(Dcm_DspDidAccessType,DCM_CONST)Dcm_DspDid3_AccessCfg =	/*DID=0x0003*/
{
	       NULL_PTR,
	       &Dcm_DspDid3_ReadContainerCfg,
	       &Dcm_DspDid3_WriteContainerCfg,
};

/*******************************************
 **DcmDspDidAccess container configration**
 ******************************************/
STATIC  CONST(Dcm_DspDidAccessType,DCM_CONST)Dcm_DspDid4_AccessCfg =   	/*DID=0x0091*/
{
	       NULL_PTR,
	       &Dcm_DspDid4_ReadContainerCfg,
	       &Dcm_DspDid4_WriteContainerCfg,
};
    
    
STATIC  CONST(Dcm_DspDidAccessType,DCM_CONST)Dcm_DspDid5_AccessCfg =   	/*DID=0x0002*/
{
	       NULL_PTR,
	       &Dcm_DspDid5_ReadContainerCfg,
	       &Dcm_DspDid5_WriteContainerCfg,
};
STATIC  CONST(Dcm_DspDidAccessType,DCM_CONST)Dcm_DspDid6_AccessCfg =	/*DID=0x0003*/
{
	       NULL_PTR,
	       &Dcm_DspDid6_ReadContainerCfg,
	       &Dcm_DspDid6_WriteContainerCfg,
};

/*******************************************
 **DcmDspDidAccess container configration**
 ******************************************/
STATIC  CONST(Dcm_DspDidAccessType,DCM_CONST)Dcm_DspDid7_AccessCfg =   	/*DID=0x0091*/
{
	       NULL_PTR,
	       &Dcm_DspDid7_ReadContainerCfg,
	       &Dcm_DspDid7_WriteContainerCfg,
};
    
    
STATIC  CONST(Dcm_DspDidAccessType,DCM_CONST)Dcm_DspDid8_AccessCfg =   	/*DID=0x0002*/
{
	       NULL_PTR,
	       &Dcm_DspDid8_ReadContainerCfg,
	       &Dcm_DspDid8_WriteContainerCfg,
};
STATIC  CONST(Dcm_DspDidAccessType,DCM_CONST)Dcm_DspDid9_AccessCfg =	/*DID=0x0003*/
{
	       NULL_PTR,
	       &Dcm_DspDid9_ReadContainerCfg,
	       &Dcm_DspDid9_WriteContainerCfg,
};

/*******************************************
 **DcmDspDidAccess container configration**
 ******************************************/
STATIC  CONST(Dcm_DspDidAccessType,DCM_CONST)Dcm_DspDid10_AccessCfg =   	/*DID=0x0091*/
{
	       NULL_PTR,
	       &Dcm_DspDid10_ReadContainerCfg,
	       &Dcm_DspDid10_WriteContainerCfg,
};
    
    
STATIC  CONST(Dcm_DspDidAccessType,DCM_CONST)Dcm_DspDid11_AccessCfg =   	/*DID=0x0002*/
{
	       NULL_PTR,
	       &Dcm_DspDid11_ReadContainerCfg,
	       &Dcm_DspDid11_WriteContainerCfg,
};
STATIC  CONST(Dcm_DspDidAccessType,DCM_CONST)Dcm_DspDid12_AccessCfg =	/*DID=0x0003*/
{
	       NULL_PTR,
	       &Dcm_DspDid12_ReadContainerCfg,
	       &Dcm_DspDid12_WriteContainerCfg,
};
/******************************************
 *DcmDspDidInfo container configration*****
 ******************************************/
STATIC  CONST(Dcm_DspDidInfoType,DCM_CONST)Dcm_DspDidInfoCfg[12] =
{
        {
           FALSE,	/*true = DID can be dynamically defined, false = DID can not bedynamically defined*/
           TRUE,    /*true = datalength of the DID is fixed, false = datalength of the DID is variable*/
           0u,      /*If Scaling information service is available for this DID, it provides the size of the scaling information.*/
           &Dcm_DspDid1_AccessCfg
        },
        {
           FALSE,
           TRUE,
           0,
           &Dcm_DspDid2_AccessCfg
        },
        
        {
           FALSE,	/*true = DID can be dynamically defined, false = DID can not bedynamically defined*/
           TRUE,    /*true = datalength of the DID is fixed, false = datalength of the DID is variable*/
           0u,      /*If Scaling information service is available for this DID, it provides the size of the scaling information.*/
           &Dcm_DspDid3_AccessCfg
        },
        
        {
           FALSE,	/*true = DID can be dynamically defined, false = DID can not bedynamically defined*/
           TRUE,    /*true = datalength of the DID is fixed, false = datalength of the DID is variable*/
           0u,      /*If Scaling information service is available for this DID, it provides the size of the scaling information.*/
           &Dcm_DspDid4_AccessCfg
        },
        
        {
           FALSE,	/*true = DID can be dynamically defined, false = DID can not bedynamically defined*/
           TRUE,    /*true = datalength of the DID is fixed, false = datalength of the DID is variable*/
           0u,      /*If Scaling information service is available for this DID, it provides the size of the scaling information.*/
           &Dcm_DspDid5_AccessCfg
        },
        
        {
           FALSE,	/*true = DID can be dynamically defined, false = DID can not bedynamically defined*/
           TRUE,    /*true = datalength of the DID is fixed, false = datalength of the DID is variable*/
           0u,      /*If Scaling information service is available for this DID, it provides the size of the scaling information.*/
           &Dcm_DspDid6_AccessCfg
        },
        
        {
           FALSE,	/*true = DID can be dynamically defined, false = DID can not bedynamically defined*/
           TRUE,    /*true = datalength of the DID is fixed, false = datalength of the DID is variable*/
           0u,      /*If Scaling information service is available for this DID, it provides the size of the scaling information.*/
           &Dcm_DspDid7_AccessCfg
        },
        
        {
           FALSE,	/*true = DID can be dynamically defined, false = DID can not bedynamically defined*/
           TRUE,    /*true = datalength of the DID is fixed, false = datalength of the DID is variable*/
           0u,      /*If Scaling information service is available for this DID, it provides the size of the scaling information.*/
           &Dcm_DspDid8_AccessCfg
        },
        
        {
           FALSE,	/*true = DID can be dynamically defined, false = DID can not bedynamically defined*/
           TRUE,    /*true = datalength of the DID is fixed, false = datalength of the DID is variable*/
           0u,      /*If Scaling information service is available for this DID, it provides the size of the scaling information.*/
           &Dcm_DspDid9_AccessCfg
        },
        
        {
           FALSE,	/*true = DID can be dynamically defined, false = DID can not bedynamically defined*/
           TRUE,    /*true = datalength of the DID is fixed, false = datalength of the DID is variable*/
           0u,      /*If Scaling information service is available for this DID, it provides the size of the scaling information.*/
           &Dcm_DspDid10_AccessCfg
        },
        
        {
           FALSE,	/*true = DID can be dynamically defined, false = DID can not bedynamically defined*/
           TRUE,    /*true = datalength of the DID is fixed, false = datalength of the DID is variable*/
           0u,      /*If Scaling information service is available for this DID, it provides the size of the scaling information.*/
           &Dcm_DspDid11_AccessCfg
        },
       
        {
           FALSE,
           TRUE,
           0,
           &Dcm_DspDid12_AccessCfg
        }
};
/**********************************************
 *DcmDspDid container configration*************
 **********************************************/
STATIC  CONST(Dcm_DspDidType,DCM_CONST)Dcm_DspDidCfg[12] =
{
        {
            0xF189,        /*Did1 Indentifier F089 */
            21u,            /*Did1 Size*/
            Rte_DidConditionCheckRead_F189,
            Rte_DidConditionCheckWrite_F189,
            NULL_PTR,
            NULL_PTR,
            NULL_PTR,
            Rte_DidReadData_F189,
            NULL_PTR,
            NULL_PTR,
            NULL_PTR,
            Rte_DidWriteData_F189,

            0u,           /*DcmDspDidInfo array subscript*/
            
            0u,    		  /*DcmDspRefDidNum*/
            NULL_PTR,     /*pDcmDspRefDidIdArray*/

            0u,			  /*DcmDspDidControlRecordSizes*/
            NULL_PTR	  /*pDcmDspDidControlRecordSizes*/
        },
        {
            0xF089,        /*Did1 Indentifier F089 */
            8u,            /*Did1 Size*/
            Rte_DidConditionCheckRead_F089,
            Rte_DidConditionCheckWrite_F089,
            NULL_PTR,
            NULL_PTR,
            NULL_PTR,
            Rte_DidReadData_F089,
            NULL_PTR,
            NULL_PTR,
            NULL_PTR,
            Rte_DidWriteData_F089,

            0u,           /*DcmDspDidInfo array subscript*/
            
            0u,    		  /*DcmDspRefDidNum*/
            NULL_PTR,     /*pDcmDspRefDidIdArray*/

            0u,			  /*DcmDspDidControlRecordSizes*/
            NULL_PTR	  /*pDcmDspDidControlRecordSizes*/
        },
        {
		    0xF187,        /*Did1 Indentifier F187 */
		    12u,            /*Did1 Size*/
		    Rte_DidConditionCheckRead_F187,
		    Rte_DidConditionCheckWrite_F187,
		    NULL_PTR,
		    NULL_PTR,
		    NULL_PTR,
		    Rte_DidReadData_F187,
		    NULL_PTR,
		    NULL_PTR,
		    NULL_PTR,
		    Rte_DidWriteData_F187,

		    0u,           /*DcmDspDidInfo array subscript*/
		  
		    0u,    		  /*DcmDspRefDidNum*/
		    NULL_PTR,     /*pDcmDspRefDidIdArray*/

		    0u,			  /*DcmDspDidControlRecordSizes*/
		    NULL_PTR	  /*pDcmDspDidControlRecordSizes*/
         },
        {
            0xF18A,        /*Did1 Indentifier F18A */
            10u,            /*Did1 Size*/
            Rte_DidConditionCheckRead_F18A,
            Rte_DidConditionCheckWrite_F18A,
            NULL_PTR,
            NULL_PTR,
            NULL_PTR,
            Rte_DidReadData_F18A,
            NULL_PTR,
            NULL_PTR,
            NULL_PTR,
            Rte_DidWriteData_F18A,

            0u,           /*DcmDspDidInfo array subscript*/
            
            0u,    		  /*DcmDspRefDidNum*/
            NULL_PTR,     /*pDcmDspRefDidIdArray*/

            0u,			  /*DcmDspDidControlRecordSizes*/
            NULL_PTR	  /*pDcmDspDidControlRecordSizes*/
        },
        {
            0xF197,        /*Did1 Indentifier F197 */
            10u,            /*Did1 Size*/
            Rte_DidConditionCheckRead_F197,
            Rte_DidConditionCheckWrite_F197,
            NULL_PTR,
            NULL_PTR,
            NULL_PTR,
            Rte_DidReadData_F197,
            NULL_PTR,
            NULL_PTR,
            NULL_PTR,
            Rte_DidWriteData_F197,

            0u,           /*DcmDspDidInfo array subscript*/
            
            0u,    		  /*DcmDspRefDidNum*/
            NULL_PTR,     /*pDcmDspRefDidIdArray*/

            0u,			  /*DcmDspDidControlRecordSizes*/
            NULL_PTR	  /*pDcmDspDidControlRecordSizes*/
        },
        {
            0xF193,        /*Did1 Indentifier F193 */
            10u,            /*Did1 Size*/
            Rte_DidConditionCheckRead_F193,
            Rte_DidConditionCheckWrite_F193,
            NULL_PTR,
            NULL_PTR,
            NULL_PTR,
            Rte_DidReadData_F193,
            NULL_PTR,
            NULL_PTR,
            NULL_PTR,
            Rte_DidWriteData_F193,

            0u,           /*DcmDspDidInfo array subscript*/
            
            0u,    		  /*DcmDspRefDidNum*/
            NULL_PTR,     /*pDcmDspRefDidIdArray*/

            0u,			  /*DcmDspDidControlRecordSizes*/
            NULL_PTR	  /*pDcmDspDidControlRecordSizes*/
        },
        {
            0xF195,        /*Did1 Indentifier F195 */
            10u,            /*Did1 Size*/
            Rte_DidConditionCheckRead_F195,
            Rte_DidConditionCheckWrite_F195,
            NULL_PTR,
            NULL_PTR,
            NULL_PTR,
            Rte_DidReadData_F195,
            NULL_PTR,
            NULL_PTR,
            NULL_PTR,
            Rte_DidWriteData_F195,

            0u,           /*DcmDspDidInfo array subscript*/
            
            0u,    		  /*DcmDspRefDidNum*/
            NULL_PTR,     /*pDcmDspRefDidIdArray*/

            0u,			  /*DcmDspDidControlRecordSizes*/
            NULL_PTR	  /*pDcmDspDidControlRecordSizes*/
        },
        {
            0xF18C,        /*Did1 Indentifier F18C */
            8u,            /*Did1 Size*/
            Rte_DidConditionCheckRead_F18C,
            Rte_DidConditionCheckWrite_F18C,
            NULL_PTR,
            NULL_PTR,
            NULL_PTR,
            Rte_DidReadData_F18C,
            NULL_PTR,
            NULL_PTR,
            NULL_PTR,
            Rte_DidWriteData_F18C,

            0u,           /*DcmDspDidInfo array subscript*/
            
            0u,    		  /*DcmDspRefDidNum*/
            NULL_PTR,     /*pDcmDspRefDidIdArray*/

            0u,			  /*DcmDspDidControlRecordSizes*/
            NULL_PTR	  /*pDcmDspDidControlRecordSizes*/
        },
        {
            0xF010,        /*Did1 Indentifier F010 */
            8u,            /*Did1 Size*/
            Rte_DidConditionCheckRead_F010,
            Rte_DidConditionCheckWrite_F010,
            NULL_PTR,
            NULL_PTR,
            NULL_PTR,
            Rte_DidReadData_F010,
            NULL_PTR,
            NULL_PTR,
            NULL_PTR,
            Rte_DidWriteData_F010,

            0u,           /*DcmDspDidInfo array subscript*/
            
            0u,    		  /*DcmDspRefDidNum*/
            NULL_PTR,     /*pDcmDspRefDidIdArray*/

            0u,			  /*DcmDspDidControlRecordSizes*/
            NULL_PTR	  /*pDcmDspDidControlRecordSizes*/
        },
        {
            0xF190,        /*Did1 Indentifier F190 */
            17u,            /*Did1 Size*/
            Rte_DidConditionCheckRead_F190,
            Rte_DidConditionCheckWrite_F190,
            NULL_PTR,
            NULL_PTR,
            NULL_PTR,
            Rte_DidReadData_F190,
            NULL_PTR,
            NULL_PTR,
            NULL_PTR,
            Rte_DidWriteData_F190,

            0u,           /*DcmDspDidInfo array subscript*/
            
            0u,    		  /*DcmDspRefDidNum*/
            NULL_PTR,     /*pDcmDspRefDidIdArray*/

            0u,			  /*DcmDspDidControlRecordSizes*/
            NULL_PTR	  /*pDcmDspDidControlRecordSizes*/
        },
        {
            0xF184,        /*Did1 Indentifier F184 */
            10u,            /*Did1 Size*/
            Rte_DidConditionCheckRead_F184,
            Rte_DidConditionCheckWrite_F184,
            NULL_PTR,
            NULL_PTR,
            NULL_PTR,
            Rte_DidReadData_F184,
            NULL_PTR,
            NULL_PTR,
            NULL_PTR,
            Rte_DidWriteData_F184,

            0u,           /*DcmDspDidInfo array subscript*/
            
            0u,    		  /*DcmDspRefDidNum*/
            NULL_PTR,     /*pDcmDspRefDidIdArray*/

            0u,			  /*DcmDspDidControlRecordSizes*/
            NULL_PTR	  /*pDcmDspDidControlRecordSizes*/
        },  
        {
            0x0003,  /*Did3 Indentifier*/
            8u,
            NULL_PTR,
            NULL_PTR,
            NULL_PTR,
            NULL_PTR,
            NULL_PTR,
            Rte_DidReadData_0003,
            NULL_PTR,
            NULL_PTR,
            Rte_DidShortTermAdjustment_0003,
            NULL_PTR,

            2u,                       /*DcmDspDidInfo array subscript*/
            0u,
            NULL_PTR,

            1u,
            &Dcm_DspDid2_ControlRecordSizesCfg[0]
        },
};

/***********************************************
 ***DcmDspEcuReset container configration*******
 ***********************************************/
STATIC  CONST(Dcm_EcuResetType,DCM_CONST)Dcm_EcuResetPort[1]=
{
    Rte_DcmEcuReset1,
};

STATIC  CONST(uint8,DCM_CONST)Dcm_DspEcuReset_RefSecCfg[4] = {0,1,2,9};
STATIC  CONST(uint8,DCM_CONST)Dcm_DspEcuReset_RefSesCfg[1] = {3};
STATIC  CONST(Dcm_DspEcuResetType,DCM_CONST)Dcm_DspEcuResetCfg[1]=
{
        {
            1u,    /*ResetType=HardReset*/
            4u,    /*DcmDspEcuResetSecurityLevelRefNum*/
            &Dcm_DspEcuReset_RefSecCfg[0],
            1u,    /*DcmDspEcuResetSessionRefNum*/
            &Dcm_DspEcuReset_RefSesCfg[0],
        }
};


/*************************************************
 ************DcmDspReadDTC container configration
 *************************************************/
CONST(uint8,DCM_CONST)Dcm_DspReadDTC_SubFunction0x02_RefSecCfg[4] =  {0,1,2,9};
CONST(uint8,DCM_CONST)Dcm_DspReadDTC_SubFunction0x0A_RefSecCfg[4] =  {0,1,2,9};

CONST(Dcm_DspReadDTCRowType,DCM_CONST)Dcm_DspReadDTCRowCfg[2] =
{
        /************************/
        {
            0x02,  /*subfunction: reportDTCByStatusMask */
            TRUE,  /*DcmDspDTCInfoSubFuncSupp*/
            4u,    /*DcmDspDTCInfoSecLevelRefNum*/
            &Dcm_DspReadDTC_SubFunction0x02_RefSecCfg[0],
        },
        {
            0x0A, /*subfunction: reportSupportedDTC */
            TRUE, /*DcmDspDTCInfoSubFuncSupp*/
            4u,   /*DcmDspDTCInfoSecLevelRefNum*/
            &Dcm_DspReadDTC_SubFunction0x0A_RefSecCfg[0],
        },
};

CONST(Dcm_DspReadDTCType,DCM_CONST)Dcm_DspReadDTCCfg =
{
        2u,
        &Dcm_DspReadDTCRowCfg[0],
};


/*************************************************
 *****DcmDspRoutine container configration********
 *************************************************/
#if(STD_ON == DCM_DSP_ROUTINE_FUNC_ENABLED)
    /*************************************
     *DcmDspRoutineAuthorization container
     ************************************/
    #if(STD_ON == DCM_SECURITY_FUNC_ENABLED)       /*Routine1=0xDA00*/
        STATIC  CONST(uint8,DCM_CONST)Dcm_DspRoutine1_Authorization_RefSecCfg[2] = {0,1};
    #endif
    #if(STD_ON == DCM_SESSION_FUNC_ENABLED)
        STATIC  CONST(uint8,DCM_CONST)Dcm_DspRoutine1_Authorization_RefSesCfg[3] = {1,3,40};
    #endif
    STATIC  CONST(Dcm_DspRoutineAuthorizationType,DCM_CONST)Dcm_DspRoutine1_AuthorizationCfg =
    {
        #if(STD_ON == DCM_SECURITY_FUNC_ENABLED)
            2u,
            &Dcm_DspRoutine1_Authorization_RefSecCfg[0],
        #else
            0u,
            NULL_PTR,
        #endif
        #if(STD_ON == DCM_SESSION_FUNC_ENABLED)
            3u,
            &Dcm_DspRoutine1_Authorization_RefSesCfg[0],
        #else
            0u,
            NULL_PTR,
        #endif
    };
    /*==============*/
    #if(STD_ON == DCM_SECURITY_FUNC_ENABLED)
        STATIC  CONST(uint8,DCM_CONST)Dcm_DspRoutine2_Authorization_RefSecCfg[2] = {1,3};     /*Routine2=0xDA01*/
    #endif
    #if(STD_ON == DCM_SESSION_FUNC_ENABLED)
        STATIC  CONST(uint8,DCM_CONST)Dcm_DspRoutine2_Authorization_RefSesCfg[3] = {1,2,3};
    #endif
    STATIC  CONST(Dcm_DspRoutineAuthorizationType,DCM_CONST)Dcm_DspRoutine2_AuthorizationCfg =
    {
        #if(STD_ON == DCM_SECURITY_FUNC_ENABLED)
            2u,
            &Dcm_DspRoutine2_Authorization_RefSecCfg[0],
        #else
            0u,
            NULL_PTR,
        #endif
        #if(STD_ON == DCM_SESSION_FUNC_ENABLED)
            3u,
            &Dcm_DspRoutine2_Authorization_RefSesCfg[0],
        #else
            0u,
            NULL_PTR,
        #endif
    };
    /*==============*/
    #if(STD_ON == DCM_SECURITY_FUNC_ENABLED)
        STATIC  CONST(uint8,DCM_CONST)Dcm_DspRoutine3_Authorization_RefSecCfg[2] = {2,3};     /*Routine3=0xDA02*/
    #endif
    #if(STD_ON == DCM_SESSION_FUNC_ENABLED)
        STATIC  CONST(uint8,DCM_CONST)Dcm_DspRoutine3_Authorization_RefSesCfg[3] = {1,2,3};
    #endif
    STATIC  CONST(Dcm_DspRoutineAuthorizationType,DCM_CONST)Dcm_DspRoutine3_AuthorizationCfg =
    {
        #if(STD_ON == DCM_SECURITY_FUNC_ENABLED)
            2u,
            &Dcm_DspRoutine3_Authorization_RefSecCfg[0],
        #else
            0u,
            NULL_PTR,
        #endif
        #if(STD_ON == DCM_SESSION_FUNC_ENABLED)
            3u,
            &Dcm_DspRoutine3_Authorization_RefSesCfg[0],
        #else
            0u,
            NULL_PTR,
        #endif
    };
    /***********************************
     *DcmDspRoutineStart container
     **********************************/
    STATIC CONST(Dcm_DspStartRoutineType,DCM_CONST)Dcm_DspRoutine1_StartCfg =   /*Routine1=0xDA00*/
    {
         1u,  /*Size of optional record in the Routine Start request*/
         1u   /*Size of optional record in the Routine Start response*/
    };

    STATIC CONST(Dcm_DspStartRoutineType,DCM_CONST)Dcm_DspRoutine2_StartCfg =    /*Routine2=0xDA01*/
    {
         0u,
         0u
    };

    STATIC CONST(Dcm_DspStartRoutineType,DCM_CONST)Dcm_DspRoutine3_StartCfg =    /*Routine3=0xDA02*/
    {
         0u,
         0u
    };
    /***********************************
     *DcmDspRoutineStop container
     **********************************/
    #if(STD_ON == DCM_DSP_ROUTINE_ID1_STOP_ENABLED)
        STATIC CONST(Dcm_DspRoutineStopType,DCM_CONST)Dcm_DspRoutine1_StopCfg =    /*Routine1=0xDA00*/
        {
            1u, /*Size of optional record in the stop request*/
            1u, /*Size of optional record in the stop response*/
        };
    #endif

    #if(STD_ON == DCM_DSP_ROUTINE_ID2_STOP_ENABLED)
        STATIC CONST(Dcm_DspRoutineStopType,DCM_CONST)Dcm_DspRoutine2_StopCfg =    /*Routine2=0xDA01*/
        {
            0u,
            0u,
        };
    #endif

    #if(STD_ON == DCM_DSP_ROUTINE_ID3_STOP_ENABLED)
        STATIC CONST(Dcm_DspRoutineStopType,DCM_CONST)Dcm_DspRoutine3_StopCfg =   /*Routine3=0xDA02*/
        {
            0u,
            0u,
        };
    #endif
    /***********************************
     *DcmDspRoutineRequestRes container configration
     **********************************/
    #if(STD_ON == DCM_DSP_ROUTINE_ID1_REQUSET_RES_ENABLED)
        /*In the the response of RequestResult,the size of the number of bytes for Optional record  */
        STATIC CONST(Dcm_DspRoutineRequestResType,DCM_CONST)Dcm_DspRoutine1_RequestResCfg = {4};
    #endif
    #if(STD_ON == DCM_DSP_ROUTINE_ID2_REQUSET_RES_ENABLED)
        STATIC CONST(Dcm_DspRoutineRequestResType,DCM_CONST)Dcm_DspRoutine2_RequestResCfg = {3};
    #endif
    #if(STD_ON == DCM_DSP_ROUTINE_ID3_REQUSET_RES_ENABLED)
        STATIC CONST(Dcm_DspRoutineRequestResType,DCM_CONST)Dcm_DspRoutine3_RequestResCfg = {2};
    #endif

    /***********************************
     *DcmDspRoutineInfo container configration
     **********************************/
    STATIC  CONST(Dcm_DspRoutineInfoType,DCM_CONST)Dcm_DspRoutineInfoCfg[3] =
    {
            {
               &Dcm_DspRoutine1_AuthorizationCfg,
               &Dcm_DspRoutine1_StartCfg,
            #if(STD_ON == DCM_DSP_ROUTINE_ID1_STOP_ENABLED)
               &Dcm_DspRoutine1_StopCfg,
            #else
               NULL_PTR,
            #endif
            #if(STD_ON == DCM_DSP_ROUTINE_ID1_REQUSET_RES_ENABLED)
               &Dcm_DspRoutine1_RequestResCfg,
            #else
               NULL_PTR,
            #endif
            },
            /*****/
            {
               &Dcm_DspRoutine2_AuthorizationCfg,
               &Dcm_DspRoutine2_StartCfg,
            #if(STD_ON == DCM_DSP_ROUTINE_ID2_STOP_ENABLED)
               &Dcm_DspRoutine2_StopCfg,
            #else
               NULL_PTR,
            #endif
            #if(STD_ON == DCM_DSP_ROUTINE_ID2_REQUSET_RES_ENABLED)
               &Dcm_DspRoutine2_RequestResCfg,
            #else
               NULL_PTR,
            #endif
            },
            /*****/
            {
               &Dcm_DspRoutine3_AuthorizationCfg,
               &Dcm_DspRoutine3_StartCfg,
            #if(STD_ON == DCM_DSP_ROUTINE_ID3_STOP_ENABLED)
               &Dcm_DspRoutine3_StopCfg,
            #else
               NULL_PTR,
            #endif
            #if(STD_ON == DCM_DSP_ROUTINE_ID3_REQUSET_RES_ENABLED)
               &Dcm_DspRoutine3_RequestResCfg,
            #else
               NULL_PTR,
            #endif
            },
    };
    /***********************************
     *DcmDspRoutine container configration
     **********************************/
    STATIC  CONST(Dcm_DspRoutineType,DCM_CONST)Dcm_DspRoutineCfg[3] =
    {
            {
                0xDA00,  /*Routine ID*/
            #if(STD_ON == DCM_DSP_ROUTINE_ID1_USE_PORT)
                Rte_DcmRoutineStartDA00,   /*RTE Callback interface*/
                Rte_DcmRoutineStopDA00,
                Rte_DcmRoutineResultDA00,
            #else
                NULL_PTR,                 /*Configration interface*/
                NULL_PTR,
                NULL_PTR,
            #endif
                0u,  					 /*RoutineInfo array subscript*/
            },

            {
                0xDA01,  /*Routine ID*/
            #if(STD_ON == DCM_DSP_ROUTINE_ID2_USE_PORT)
                NULL_PTR,				 /*Configration interface*/
                NULL_PTR,
                NULL_PTR,
            #else
                NULL_PTR,				 /*Configration interface*/
                NULL_PTR,
                NULL_PTR,
            #endif
                1u,  					 /*RoutineInfo array subscript*/
            },

            {
                0xDA02,  /*Routine ID*/
            #if(STD_ON == DCM_DSP_ROUTINE_ID3_USE_PORT)
                NULL_PTR,				 /*Configration interface*/
                NULL_PTR,
                NULL_PTR,
            #else
                NULL_PTR,
                NULL_PTR,
                NULL_PTR,
            #endif
                2u,                   /*RoutineInfo array subscript*/
            },

    };
#endif
/************************************************
 *******DcmDsp container configration(Multiplicity=1)**
 ************************************************/
CONST(Dcm_DspCfgType,DCM_CONST)Dcm_DspCfg =
{
   1u,               /*Indicates the maximum allowed DIDs in a single "ReadDataByIdentifier" request. If set to 0, then no limitation is applied. */
   
   /*DcmDspDid and DcmDspDidInfo configeration(Multiplicity=0..*)*/
   12u,
   &Dcm_DspDidCfg[0],
   12u,
   &Dcm_DspDidInfoCfg[0],
   
   /*EcuReset container configration(Multiplicity=0..*)*/
   1u,
   &Dcm_DspEcuResetCfg[0],
   1u,
   &Dcm_EcuResetPort[0],
       
   /*DcmDspDid container configration(Multiplicity=0..*)*/
   #if(STD_ON == DCM_DSP_PID_FUNC_ENABLED)
       0u,
       NULL_PTR,
   #endif
   /*DcmDspReadDTC container configration(Multiplicity=1)*/
   &Dcm_DspReadDTCCfg,
   
   /*DcmDspRequestControl container configration(Multiplicity=0..*)*/
   #if(STD_ON == DCM_DSP_REQUESTCONTROL_FUNC_ENABLED)
       0u,
       NULL_PTR,
   #endif
   /*DcmDspRoutine and DcmDspRoutineInfo(Multiplicity=0..*)*/
   #if(STD_ON == DCM_DSP_ROUTINE_FUNC_ENABLED)
       3u,
       &Dcm_DspRoutineCfg[0],
       3u,
       &Dcm_DspRoutineInfoCfg[0],
   #endif
   /*DcmDspSecurity(Multiplicity=1)*/
   &Dcm_DspSecurity,
   /*DcmDspSession(Multiplicity=1)*/
   &Dcm_DspSession,
   /*DcmDspTestResultByObdmid(Multiplicity=0..*)*/
   #if(STD_ON == DCM_DSP_TEST_RESULT_BYOBDMID_FUNC_ENABLED)
       0u,
       NULL_PTR,
   #endif
   /*DcmDspVehInfo(Multiplicity=0..*)*/
   #if(STD_ON == DCM_DSP_VEHINFO_FUNC_ENABLED)
      0u,
      NULL_PTR,
   #endif
};
/*****************************************************************************************
 ********************************* DSD container configration*****************************
 *****************************************************************************************/
/**********************************************************************/
/******UDS Service session and security configration******/
    STATIC  CONST(uint8,DCM_CONST)Dcm_UDS_Service10_SecRef[4] = {0,1,2,9};
    STATIC  CONST(uint8,DCM_CONST)Dcm_UDS_Service10_SesRef[2] = {1,3};

    STATIC  CONST(uint8,DCM_CONST)Dcm_UDS_Service11_SecRef[4] = {0,1,2,9};
    STATIC  CONST(uint8,DCM_CONST)Dcm_UDS_Service11_SesRef[1] = {3};

    STATIC  CONST(uint8,DCM_CONST)Dcm_UDS_Service14_SecRef[4] = {0,1,2,9};
    STATIC  CONST(uint8,DCM_CONST)Dcm_UDS_Service14_SesRef[2] = {1,3};

    STATIC  CONST(uint8,DCM_CONST)Dcm_UDS_Service19_SecRef[4] = {0,1,2,9};
    STATIC  CONST(uint8,DCM_CONST)Dcm_UDS_Service19_SesRef[2] = {1,3};

    STATIC  CONST(uint8,DCM_CONST)Dcm_UDS_Service22_SecRef[4] = {0,1,2,9};
    STATIC  CONST(uint8,DCM_CONST)Dcm_UDS_Service22_SesRef[2] = {1,3};


    STATIC  CONST(uint8,DCM_CONST)Dcm_UDS_Service27_SecRef[4] = {0,1,2,9};
    STATIC  CONST(uint8,DCM_CONST)Dcm_UDS_Service27_SesRef[1] = {3};


    STATIC  CONST(uint8,DCM_CONST)Dcm_UDS_Service2E_SecRef[2]= {1,9};
    STATIC  CONST(uint8,DCM_CONST)Dcm_UDS_Service2E_SesRef[1]= {3};

    STATIC  CONST(uint8,DCM_CONST)Dcm_UDS_Service2F_SecRef[1]= {1};
    STATIC  CONST(uint8,DCM_CONST)Dcm_UDS_Service2F_SesRef[1]= {3};

    STATIC  CONST(uint8,DCM_CONST)Dcm_UDS_Service3E_SecRef[4]= {0,1,2,9};
    STATIC  CONST(uint8,DCM_CONST)Dcm_UDS_Service3E_SesRef[2]= {1,3};

    STATIC  CONST(uint8,DCM_CONST)Dcm_UDS_Service85_SecRef[4]= {0,1,2,9};
    STATIC  CONST(uint8,DCM_CONST)Dcm_UDS_Service85_SesRef[1]= {3};

    STATIC  CONST(uint8,DCM_CONST)Dcm_UDS_Service28_SecRef[4]= {0,1,2,9};
    STATIC  CONST(uint8,DCM_CONST)Dcm_UDS_Service28_SesRef[1]= {3};  
    
    STATIC  CONST(uint8,DCM_CONST)Dcm_UDS_ServiceB0_SecRef[4]= {0,1,2,9};
    STATIC  CONST(uint8,DCM_CONST)Dcm_UDS_ServiceB0_SesRef[2]= {1,3};  
     
/**********************************************************************/
/*UDS Service table configration(Multiplicity=1..*)*/
STATIC CONST(Dcm_DsdServiceCfgType,DCM_CONST)Dcm_Dsd_UDS_ServiceTable_Service[12] =
{
    {
     0x10U,
     TRUE,
     4u,
     &Dcm_UDS_Service10_SecRef[0],
     2U,
     &Dcm_UDS_Service10_SesRef[0],
     DCM_ADDRESSING_PHYANDFUNC,
     DspInternal_UDS0x10,
    },
    
    {
     0x11U,
     TRUE,
     4U,
     &Dcm_UDS_Service11_SecRef[0],
     1U,
     &Dcm_UDS_Service11_SesRef[0],
     DCM_ADDRESSING_PHYANDFUNC,
     DspInternal_UDS0x11,
    },
    
    {
     0x14U,
     FALSE,
     4U,
     &Dcm_UDS_Service14_SecRef[0],
     2U,
     &Dcm_UDS_Service14_SesRef[0],
     DCM_ADDRESSING_PHYANDFUNC,
     DspInternal_UDS0x14,
    },

    {
     0x19U,
     TRUE,
     4U,
     &Dcm_UDS_Service19_SecRef[0],
     2U,
     &Dcm_UDS_Service19_SesRef[0],
     DCM_ADDRESSING_PHYANDFUNC,
     DspInternal_UDS0x19,
    },
    
    {
     0x22U,
     FALSE,
     4U,
     &Dcm_UDS_Service22_SecRef[0],
     2U,
     &Dcm_UDS_Service22_SesRef[0],
     DCM_ADDRESSING_PHYANDFUNC,
     DspInternal_UDS0x22,
    },

    {
     0x27U,
     TRUE,
     4U,
     &Dcm_UDS_Service27_SecRef[0],
     1U,
     &Dcm_UDS_Service27_SesRef[0],
     DCM_ADDRESSING_PHYSICAL,
     DspInternal_UDS0x27,
    },

    {
     0x2EU,
     FALSE,
     2U,
     &Dcm_UDS_Service2E_SecRef[0],
     1U,
     &Dcm_UDS_Service2E_SesRef[0],
     DCM_ADDRESSING_PHYSICAL,
     DspInternal_UDS0x2E,
    },

    {
     0x2FU,
     FALSE,
     1U,
     &Dcm_UDS_Service2F_SecRef[0],
     1U,
     &Dcm_UDS_Service2F_SesRef[0],
     DCM_ADDRESSING_PHYSICAL,
     DspInternal_UDS0x2F,
    },
    
    {
     0x3EU,
     TRUE,
     4U,
     &Dcm_UDS_Service3E_SecRef[0],
     2U,
     &Dcm_UDS_Service3E_SesRef[0],
     DCM_ADDRESSING_PHYANDFUNC,
     DspInternal_UDS0x3E,
    },
    
    {
     0x85U,
     TRUE,
     4U,
     &Dcm_UDS_Service85_SecRef[0],
     1U,
     &Dcm_UDS_Service85_SesRef[0],
     DCM_ADDRESSING_PHYANDFUNC,
     DspInternal_UDS0x85,
    },

    {
     0x28U,
     TRUE,
     4U,
     &Dcm_UDS_Service28_SecRef[0],
     1U,
     &Dcm_UDS_Service28_SesRef[0],
     DCM_ADDRESSING_PHYANDFUNC,
     DspInternal_UDS0x28,
    },

    {
     0xB0U,   /*test*/
     TRUE,
     4U,
     &Dcm_UDS_ServiceB0_SecRef[0],
     2U,
     &Dcm_UDS_ServiceB0_SesRef[0],
     DCM_ADDRESSING_PHYSICAL,
     DspInternal_UDS0xB0,
    },
};

/**********************************************************************/
/*DCM Support Service Table(Multiplicity=1..256)*/
STATIC  CONST(Dcm_DsdServiceTableCfgType,DCM_CONST)Dcm_DsdServiceTable[DCM_SERVICE_TAB_NUM]=
{
        {0x01,&Dcm_Dsd_UDS_ServiceTable_Service[0],12},
};
/**********************************************************************/
/*Dsd container(Multiplicity=1)*/
CONST(Dcm_DsdCfgType,DCM_CONST)Dcm_DsdCfg ={
											&Dcm_DsdServiceTable[0],
											DCM_SERVICE_TAB_NUM
										   };

/*****************************************************************************************
 ********************************* DSL container configration*****************************
 *****************************************************************************************/
/*DcmDslBuffer container(Multiplicity=1..256)*/
STATIC  CONST(Dcm_DslBufferType,DCM_CONST)Dcm_DslBufferCfg[DCM_CHANNEL_NUM] =
{
        {0x55u, 128u, 0u  },/*Dcm_DslBufferId,Dcm_DslBufferSize,offset*/
        {0xAAu, 128u, 128u},
};

/***********************************/
/*DcmDslDiagResp container(Multiplicity=1)*/
STATIC  CONST(Dcm_DslDiagRespType,DCM_CONST)Dcm_DslDiagRespCfg ={DCM_DSLDIAGRESP_FORCERESPENDEN,
															     5u};

/*****************************************************
 *DcmDslCallbackDCMRequestService port configration(Multiplicity=1..*)
 *****************************************************/
STATIC  CONST(Dcm_DslCallbackDCMRequestServiceType,DCM_CONST)Dcm_DslCallbackDCMRequestServiceCfg[1] =
{
        {Rte_StartProtocol,Rte_StopProtocol}
};

/*****************************************************
 *DcmDslSessionControl port configration(Multiplicity=1..*)*******
 ****************************************************/
STATIC  CONST(Dcm_DslSessionControlType,DCM_CONST)Dcm_DslSessionControlCfg[1]=
{
        {Rte_DcmSessChgIndication,Rte_DcmGetSessChgPermission},
};

/*****************************************************
 *DcmDslServiceRequestIndication port configration(Multiplicity=0..*)*
 ****************************************************/
#if(STD_ON==DCM_REQUEST_INDICATION_ENABLED)
STATIC  CONST(Dcm_ServiceRequestIndicationType,DCM_CONST)Dcm_ServiceRequestIndicationCfg[1] =
{
        {Rte_DcmServiceReqIndication},
 };
#endif


/*****************************************************
 ****DcmDslProtocolTiming container(Multiplicity=1)***********
 ****************************************************/
STATIC  CONST(Dcm_DslProtocolTimingType,DCM_CONST)Dcm_DslProtocolTimingCfg =
{
       NULL_PTR,
       0U,
};

/******************************************************
 *************DcmDslConnection container***************
 *****************************************************/
/********************UDS protocal Connection configration*******************/
#if(STD_ON == DCM_UDS_FUNC_ENABLED)
/*Connection1,Mainconnection,ProtocolRx configration(Multiplicity=1..*)*/
STATIC  CONST(Dcm_DslProtocolRxType,DCM_CONST)Dcm_DslProtocol2_MainConnection1_RxCfg[2]=
{
        {
           DCM_FUNCTIONAL,      /*DcmDslProtocolRxAddrType*/
           0xA2A2               /*DcmDslProtocolRxPduId*/
        },
        {
           DCM_PHYSICAL,      
           0xA3A3              
        }
};
/*Connection1 Mainconnection configration*/
STATIC  CONST(Dcm_DslMainConnectionType,DCM_CONST)Dcm_DslProtocol2_MainConnection1Cfg =
{
        NULL_PTR,             /*pDcmDslPeriodicTranmissionConRef*/
        NULL_PTR,             /*pDcmDslROEConnectionRef*/
        &Dcm_DslProtocol2_MainConnection1_RxCfg[0], /*pDcmDslProtocolRx*/
        2u,                   /*DcmDslProtocolRx_Num*/
        0x55AAu               /*DcmDslProtocolTxPduId*/
};

/*Connection1 configration*/
STATIC  CONST(Dcm_DslConnectionType,DCM_CONST)Dcm_Dsl_Protocol2_ConnectionCfg[1]=
{
        {
            &Dcm_DslProtocol2_MainConnection1Cfg,  /*pDcmDslMainConnection*/
            NULL_PTR,         /*pDcmDslPeriodicTransmission*/
            NULL_PTR          /*pDcmDslResponseOnEvent*/
        }
};
#endif


/*****************************************************
 ****Dcm_DslProtocolRow container configration(Multiplicity=1..*)*******
 ****************************************************/
STATIC  CONST(Dcm_DslProtocolRowType,DCM_CONST)Dcm_DslProtocolRowCfg[DCM_DSLPROTOCOLROW_NUM_MAX] =
{
/*******UDS Protocal Configration*********/
#if(STD_ON == DCM_UDS_FUNC_ENABLED)
        {
                DCM_UDS_ON_CAN,			/*DcmDslProtocolID*/
                FALSE,					/*DcmDslProtocolIsParallelExecutab*/
                0U,						/*DcmDslProtocolPreemptTimes*/
                1U,						/*DcmDslProtocolPriority*/
                DCM_PROTOCOL_TRAN_TYPE1,/*DcmDslProtocolTransType*/
                0x55,					/*DcmDslProtocolRxBufferID*/
                0xAA,					/*DcmDslProtocolTxBufferID*/
                0x01, 					/*DcmDslServiceTableID*/
                NULL_PTR,               /*pDcmDslProtocolTimeLimit*/
                &Dcm_Dsl_Protocol2_ConnectionCfg[0],/*DcmDslConnection*/
                1U,
        },
#endif
};

/*****************************************************
 *DcmDslProtocol container configration(Multiplicity=1)
 ****************************************************/
STATIC  CONST(Dcm_DslProtocolType,DCM_CONST)Dcm_DslProtocol =
{
        &Dcm_DslProtocolRowCfg[0],
        DCM_DSLPROTOCOLROW_NUM_MAX,
};

/*****************************************************
 ****************DcmDsl container configration*****
 ****************************************************/
CONST(Dcm_DslCfgType,DCM_CONST)Dcm_DslCfg =
{
       DCM_CHANNEL_NUM,				/*Number of Channel configration*/
       &Dcm_DslBufferCfg[0],
       
       1U,							/*Number of DslCallbackDCMRequestService port*/
       &Dcm_DslCallbackDCMRequestServiceCfg[0],
       
#if(STD_ON==DCM_REQUEST_INDICATION_ENABLED)
       1U,							/*Number of ServiceRequestIndication port*/
       &Dcm_ServiceRequestIndicationCfg[0],
#else
       0U,
       NULL_PTR,
#endif
       1U,							 /*Number of SessionControl port*/
       &Dcm_DslSessionControlCfg[0], /*reference to SessionControl port configration*/
       &Dcm_DslDiagRespCfg,          /*reference to DcmDslDiagResp configration*/
       &Dcm_DslProtocol,             /*reference to DcmDslProtocol configration*/
       &Dcm_DslProtocolTimingCfg
};

#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "MemMap.h"




