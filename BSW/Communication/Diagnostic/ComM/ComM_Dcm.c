#include "Dcm_Cbk.h"   
#include "ComM_Dcm.h"

/**************************************
 * 函数原型:  ComM_DCM_ActiveDiagnostic
 * 功能描述:  通知ComM模块进入“FULL COMMUNICATION”状态
 * 服务ID:    0x1f
 * 同步/异步: 同步
 * 可重入性:  可重入
 * 输入:      无
 * 输出:      无
 * 返回值:    无
 ************************************/
void ComM_DCM_ActiveDiagnostic(void)
{
    Dcm_ComM_FullComModeEntered();
    return;
}

/**************************************
 * 函数原型:  ComM_DCM_InactiveDiagnostic
 * 功能描述:  通知ComM模块退出“FULL COMMUNICATION”状态
 * 服务ID:    0x20
 * 同步/异步: 同步
 * 可重入性:  可重入
 * 输入:      无
 * 输出:      无
 * 返回值:    无
 ************************************/
void ComM_DCM_InactiveDiagnostic(void)
{
    Dcm_ComM_NoComModeEntered();
    return;
}
