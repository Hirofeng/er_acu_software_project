/*============================================================================*/
/*  Copyright (C) 2009-2011,  INFRASTRUCTURE SOFTWARE CO.,LTD.
 *
 *  All rights reserved. This software is  property. Duplication
 *  or disclosure without  written authorization is prohibited.
 *
 *
 *  @file       <Os_Types.h>
 *  @brief      <>
 *
 *  <Compiler: CodeWarrior    MCU:freescale xep 100>
 *
 *  @author     <yaoxuan.zhang>
 *  @date        <15-04-2013>
 */
/*============================================================================*/

#ifndef OS_TYPES_H
#define OS_TYPES_H

/*=======[R E V I S I O N   H I S T O R Y]====================================*/
/*  <VERSION>    <DATE>    <AUTHOR>    <REVISION LOG>
 *  V0.1.0     20130415  yaoxuan.zhang       Initial version
 *  V0.2.0     20130619  huaming             modified the format of data type and macro definition,
 *                                           and added the information about MISRAC rule
 *  V0.3.0     20130621  huaming             To be consistent with the OSEK OS protocol,added the
 *                                           definitions of Os_ResourceInfo,ResourceCB,TaskStateType,
 *                                           TaskStateRefType and so on
 *  V0.4.0     20130702  yaoxuan.zhang      1.To be consistent with the OSEK OS protocol,added the
 *								              definitions of EventType,EventMaskType,EventMaskRefType,
 *								              CounterType,TickType,TickRefType,AlarmType,AlarmRefType,
 *								              AlarmBaseRefType;
 *								            2.Delete the type define of Os_HookCfg struct.
 *  V0.5.0     20130711  huaming            modified the format of data type and macro definition
 *								            base on rule of i-soft
 *  V0.6.0     20130722  yaoxuan.zhang       Add OS_SrvParamType struct.
 *  V0.7.0     20130723  yaoxuan.zhang       Modified the code to adapt MISRA C specification.
 *  V0.8.0     20130730  huaming             add the defination of AppModeType
 */
/*============================================================================*/

/*=======[M I S R A C  R U L E  V I O L A T I O N]============================*/
/*  <MESSAGE ID>    <CODE LINE>    <REASON>
 */
/*============================================================================*/

/*=======[V E R S I O N  I N F O R M A T I O N]===============================*/
#define OSTYPES_H_VENDOR_ID  				0
#define OSTYPES_H_MODULE_ID  				0
#define OSTYPES_H_AR_MAJOR_VERSION  		1
#define OSTYPES_H_AR_MINOR_VERSION  		0
#define OSTYPES_H_AR_PATCH_VERSION  		0
#define OSTYPES_H_SW_MAJOR_VERSION  		1
#define OSTYPES_H_SW_MINOR_VERSION  		0
#define OSTYPES_H_SW_PATCH_VERSION  		0
#define OSTYPES_H_VENDOR_API_INFIX  		0

/*=======[I N C L U D E S]====================================================*/
#include "Platform_Types.h"
#include "Error.h"

/*=======[M A C R O S]========================================================*/

/* Define the status of kernel */
#define STATUS_STANDARD 	                0x00u
#define STATUS_EXTENDED                 	0x01u

/* Define the scheduling strategy of kernel*/
#define SCHEDULE_NON 		                0
#define SCHEDULE_FULL 		                1
#define SCHEDULE_MIXED 	                    2

/* Defines for all conformance classes */
#define BCC1                                1
#define BCC2                                2
#define ECC1                                3
#define ECC2                                4

/* Define the MACROS that used to extending*/
#define TASK(name)                          void OSEK_TASK_##name(void)
#define ISR(name)                           void name(void)
#define ALARMCALLBACK(name)                 void name(void)
#define DeclareAlarmCallback(name)          extern void name(void)
#define DeclareTask(TaskId)                 extern void OSEK_TASK_##TaskId(void)
#define DeclareISR(name)	                extern void name(void)
#define DeclareAlarm(AlarmId)
#define DeclareEvent(EventId)
#define DeclareResource(ResourceId)
#define DeclareCounter(CounterId)

/* Define the invalid parameter */
#define IVALID_ID 				            0xFFu 						/* Ivalid ID */
#define INVALID_TASK 			            ((Os_TaskType)0xFFu)		/* Ivalid task ID */
#define INVALID_ALARM_ID 		            ((Os_AlarmType)0xFFu)		/* Ivalid alarm ID */
#define INVALID_PRIORITY 		            ((Os_PriorityType)0xFFu )	/* Ivalid priority*/
#define STACK_FILL_PATTERN 	            	0xCC
#define NULL_APPMODE 		            	((Os_AppModeType)0x00u)

/*=======[T Y P E   D E F I N I T I O N S]====================================*/

/*
 * General struct type definations
 */


/* Defination of Os level of call API */
typedef enum
{
	OS_LEVEL_TASK 		        = 0,
	OS_LEVEL_ISR 		        = 1,
	OS_LEVEL_ERRORHOOK 	        = 2,
	OS_LEVEL_PRETASKHOOK 	    = 3,
	OS_LEVEL_POSTTASKHOOK 	    = 4,
	OS_LEVEL_STARTUPHOOK 	    = 5,
	OS_LEVEL_SHUTDOWNHOOK 	    = 6,
	OS_LEVEL_STANDARD_RESOURCE 	= 7,
	OS_LEVEL_INTERNAL_RESOURCE 	= 8
}Os_CallLevelType;

/* Stack type defination*/
typedef uint32 	Os_StackDataType;
typedef uint8* 	Os_StackPtrType;
typedef uint16 	Os_SizeType;

typedef struct
{
	Os_StackPtrType	    	stackTop;
	Os_StackPtrType 		stackBottom;
} Os_StackType;

/* Task type defination*/
typedef uint16 				Os_TaskType;
typedef Os_TaskType* 		Os_TaskRefType;
typedef Os_TaskType 		TaskType;
typedef Os_TaskType* 		TaskRefType;

//typedef enum
//{
//	TASK_STATE_WAITING		= 0,
//	TASK_STATE_READY		= 1,
//	TASK_STATE_SUSPENDED	= 2,
//	TASK_STATE_RUNNING		= 3,
//	TASK_STATE_START		= 4,/*for distinguish the autostarted task,the first activatedtask and preempted task */
//}Os_TaskStateType;

/* Add for comformance test*/
#define  SUSPENDED          TASK_STATE_SUSPENDED
#define  READY              TASK_STATE_READY
#define  RUNNING            TASK_STATE_RUNNING
#define  WAITING            TASK_STATE_WAITING

//typedef Os_TaskStateType    TaskStateType;
///typedef Os_TaskStateType*   TaskStateRefType;
//typedef Os_TaskStateType*   Os_TaskStateRefType;
typedef uint8 				Os_TaskScheduleType;


/* Resource ref type defination*/
typedef uint8 			    Os_ResourceType;
typedef uint8 			    ResourceType;

typedef enum
{
	RES_PRO_INTERNAL        = 0,
	RES_PRO_LINKED	        = 1,
	RES_PRO_STANDARD        = 2
}Os_ResourcePropertyType;

/* Isr ref type defination*/
typedef enum
{
	OS_ISR_CATEGORY1        = 0,
	OS_ISR_CATEGORY2        = 1
}Os_IsrCategoryType;

/* Event ref type defination*/
typedef uint32 				Os_EventType;
typedef uint32 				Os_EventMaskType;
typedef Os_EventMaskType* 	Os_EventMaskRefType;
typedef Os_EventType 		EventType;
typedef Os_EventMaskType 	EventMaskType;
typedef EventMaskType		*EventMaskRefType;


/* Counter ref type defination*/
typedef uint16 				Os_CounterType;
typedef uint32 				Os_TickType;
typedef Os_TickType			*Os_TickRefType;
typedef Os_CounterType 		CounterType;
typedef Os_TickType 		TickType;
typedef TickType			*TickRefType;

typedef enum
{
	COUNTER_SOFTWARE        = 0,
	COUNTER_HARDWARE        = 1
}Os_CounterRefType;

/* Alarm ref type defination*/
typedef uint16 				Os_AlarmType;
typedef Os_AlarmType* 		Os_AlarmRefType;
typedef Os_AlarmType 		AlarmType;
typedef Os_AlarmType* 		AlarmRefType;

typedef void				(*Os_AlarmCallbackType)(void);

/* To unify with conformance testcases */
typedef struct
{
	Os_TickType             maxallowedvalue;
	Os_TickType             ticksperbase;
	Os_TickType             mincycle;
} Os_AlarmBaseType;

typedef Os_AlarmBaseType     AlarmBaseType; /*to unify with conformance testcases */
typedef Os_AlarmBaseType	*Os_AlarmBaseRefType;
typedef Os_AlarmBaseType	*AlarmBaseRefType;

typedef enum
{
	ALARM_AUTOSTART_ABSOLUTE = 0,
	ALARM_AUTOSTART_RELATIVE = 1
}Os_AlarmAutostartTypeType;


typedef enum
{
	PRO_IGNORE					= 0,
	PRO_TERMINATETASKISR		= 1,
	PRO_TERMINATEAPPL			= 2,
	PRO_TERMINATEAPPL_RESTART	= 3,
	PRO_SHUTDOWN				= 4
} Os_ProtectionReturnType;


typedef uint16 	                Os_PriorityType;
typedef uint8 	                Os_IPLType;
typedef uint8 					Os_LockerType;
typedef uint8					Os_AppModeType;
typedef Os_AppModeType 			AppModeType;

typedef enum
{
	SCALABILITY_SC1       		= 0,
	SCALABILITY_SC2       		= 1,
	SCALABILITY_SC3       		= 2,
	SCALABILITY_SC4       		= 3
}Os_ScalabilityType;

typedef union
{
	Os_TaskType	 				taskId;
	Os_TaskRefType				taskRef;
	//Os_TaskStateRefType			state;
	Os_ResourceType				resourceId;
	Os_EventMaskType			eventMask;
	Os_EventMaskRefType			eventRef;
	Os_AlarmType				alarmId;
	Os_AlarmBaseRefType			baseRef;
	Os_TickRefType				tickRef;
	Os_TickType					tick;
	Os_AppModeType				mode;
	Os_CounterType				counterId;
}Os_SrvParamType;

typedef struct
{
   	Os_TaskType	 				taskId;
    Os_TaskRefType				taskRef;
	//Os_TaskStateRefType			taskState;
    Os_AlarmType				alarmId;
    Os_AlarmBaseRefType			alarmBaseRef;
    Os_TickRefType              alarmTickRef;
    Os_TickType					alarmTime;
    Os_TickType					alarmCycle;
    Os_ResourceType				resourceId;
    Os_EventMaskType			eventMask;
	Os_EventMaskRefType			eventRef;
}Os_RunTimeSrvPrarmType;



/* Os configration struct type definations*/

typedef void (* TaskEntry)(void);


/* OsResource type define*/

typedef enum
{
	OCCUPIED_BY_TASK				= 0,                    /*occupied by task*/
	OCCUPIED_BY_INTERRUPT			= 1,                    /*occupied by interrupt*/
	OCCUPIED_BY_TASK_OR_INTERRUPT	= 2                     /*occupied by task or interrupt*/
}Os_ResourceOccupyType;

typedef uint32 Os_ResourceOccupyMaskType; /*Judge whether task or interrupt has the right to occupy a resource*/
typedef uint32 Os_ResourceIdMaskType;

typedef struct
{
	Os_PriorityType                 ceiling;
	Os_ResourceOccupyType           resourceOccupyType;
	Os_ResourceIdMaskType           osResourceIdMask;
	Os_ResourceOccupyMaskType       osResourceOccupyMask;
}Os_ResourceInfo;

/* For manage the resource module*/
/* Task  type define*/
typedef struct
{
	Os_AppModeType	        osTaskAppMode;/*Reference to application modes in which that task is activated on startup of the OS  */
}Os_TaskAutoStartCfgType;

typedef struct
{
	TaskEntry 				osTaskEntry;			/*Task entry*/
	uint8 					osTaskStackId;			/*Task stack id*/
	uint16					osTaskActivation;		/* The maximum number of queued activation requests for the task*/
	uint16					osTaskPriority;			/* The priority of a task*/
	Os_TaskScheduleType		osTaskSchedule;		    /*defines the preemptability of the task*/
	Os_EventMaskType 	    osEventMask;            /*This reference defines the list of events the extended task may react on*/
    Os_ResourceIdMaskType   osResourceIdMask;       /*define the resource id mask base the rule of 32 resources as a group */
    Os_ResourceOccupyMaskType osResourceOccupyMask; /*Judge whether task or interrupt has the right
													  to occupy a resource*/
	Os_AppModeType	        osTaskAutoStartMode;	/*This container determines whether the task is activated during
													  the system start-up procedure or not for some specific application modes*/
}Os_TaskCfgType;


/* Counter  type define*/
typedef struct
{
	Os_TickType 			osCounterMaxAllowedValue;
	Os_TickType 			osCounterMinCycle;
	Os_TickType			    osCounterTicksPerBase;
	Os_CounterRefType		osCounterType;
	Os_TickType			    osSecondsPerTick;
} Os_CounterCfgType;


typedef struct
{
	Os_TickType 			osAlarmStartTime;
	Os_AlarmAutostartTypeType 	osAlarmAutostartType;
	Os_TickType 			osAlarmCycleTime;
	Os_AppModeType 			osAlarmAppMode;
} Os_AlarmAutostartCfgType;


typedef struct
{
	Os_CounterType 			osAlarmCounter;
	const Os_AlarmAutostartCfgType	*osAlarmAutostartRef;
	Os_AlarmCallbackType 	osAlarmCallback;
}Os_AlarmCfgType;



/* Os runtime struct type definations*/

typedef struct
{
	uint8 	    queueHead;
	uint8	 	queueTail;
} Os_ReadyQueueType;

/* Task control block type definations*/
typedef struct
{
	Os_StackPtrType		    taskTop;
	Os_StackPtrType 		taskStackBottom;
	uint16 				    taskRunPrio;
	uint8					taskActCount;
//	Os_TaskStateType 		taskState;
	boolean				    taskRunning;
	Os_ResourceType 		taskResCount;
}Os_TCBType;

/* Event control block type definations*/
typedef struct
{
	Os_TaskType			    eventTaskID;
	Os_EventMaskType 		eventSetEvent;
	Os_EventMaskType		eventWaitEvent;
}Os_ECBType;

/* Counter control block type definations*/
typedef struct
{
	Os_TickType 			counterCurVal;
	Os_TickType 			counterLastVal;
	Os_AlarmType 			counterAlmQue;
}Os_CCBType;

/* Alarm control block type definations*/
typedef struct
{
	Os_TickType 		    alarmStart;
	Os_TickType 		    alarmCycle;
	Os_AlarmType 		    alarmPre;
	Os_AlarmType 		    alarmNext;
} Os_ACBType;

/* Resource control block type definations*/
typedef struct
{
	uint8                   saveCount;
	Os_PriorityType         savePrio;
}Os_RCBType;

/* System control block type definations*/
typedef struct
{
    Os_TCBType			    *sysRunningTCB;
    Os_LockerType  		    sysDispatchLocker;
    Os_PriorityType  		sysHighPrio;
    Os_TaskType   			sysHighTaskID;
    Os_TaskType			    sysRunningTaskID;
    Os_AppModeType 		    sysActiveAppMode;
    boolean				    sysInSystemStack;
    Os_CallLevelType 		sysOsLevel;
    Os_ServiceIdType 		sysOsServiceId;
    Os_RunTimeSrvPrarmType  sysRunTimeSrvPrarm;
}Os_SCBType;


/*=======[E X T E R N A L   D A T A]==========================================*/


/*=======[E X T E R N A L   F U N C T I O N   D E C L A R A T I O N S]========*/


/*=======[I N T E R N A L   D A T A]==========================================*/


/*=======[I N T E R N A L   F U N C T I O N   D E C L A R A T I O N S]========*/


#endif  /* end of OS_TYPES_H */
/*=======[E N D   O F   F I L E]==============================================*/
