/*============================================================================*/
/*  Copyright (C) 2009-2013,  INFRASTRUCTURE SOFTWARE CO.,LTD.
 *  
 *  All rights reserved. This software is  property. Duplication
 *  or disclosure without  written authorization is prohibited.
 *  
 *  
 *  @file      
 *  @brief     
 *  
 *  
 *  @author     chenmaosen
 *  @date       2013-4-7
 */
/*============================================================================*/

#ifndef RTE_DCM_H
#define RTE_DCM_H

#include "Dcm_Types.h"
#ifdef __cplusplus
extern "C" {
#endif  /* __cplusplus */

/*=======[M A C R O S]========================================================*/
/** The physical memory location of boot request flag. LOCAL address*/
/* @type:uint32 range:0x00000000~0xFFFFFFFF note:NONE */
#define FL_BOOT_MODE  0x40017000uL
/** The physical memory location of application software update flag. LOCAL address*/
/* @type:uint32 range:0x00000000~0xFFFFFFFF note:NONE */
#define FL_APPL_UPDATE 0x40017001uL

/****************************** declarations *********************************/
/*******function for Protocol Start and stop***********/
extern  Std_ReturnType  Rte_StartProtocol(Dcm_ProtocolType  ProtocolID);
extern  Std_ReturnType  Rte_StopProtocol(Dcm_ProtocolType  ProtocolID);

/*******function for security access***********/
extern  Std_ReturnType  Rte_CompareKey1(uint8 *key);
extern  Std_ReturnType  Rte_GetSeed1(uint8 *SecurityAccessRecord,uint8 *Seed,Dcm_NegativeResponseCodeType *ErrorCode);

extern  Std_ReturnType  Rte_CompareKey3(uint8 *key);
extern  Std_ReturnType  Rte_GetSeed3(uint8 *SecurityAccessRecord,uint8 *Seed,Dcm_NegativeResponseCodeType *ErrorCode);

extern  Std_ReturnType  Rte_CompareKey11(uint8 *key);
extern  Std_ReturnType  Rte_GetSeed11(uint8 *SecurityAccessRecord,uint8 *Seed,Dcm_NegativeResponseCodeType *ErrorCode);

extern  Std_ReturnType  Rte_StoreFalseCounter(uint8  falseCounter);
extern  Std_ReturnType  Rte_ReadFalseCounter(uint8  *pfalseCounter);

/*************functions for session control service*****************/
extern  Std_ReturnType  Rte_DcmSessChgIndication(Dcm_SesType  SesCtrlTypeOld,   Dcm_SesType  SesCtrlTypeNew);

extern  Std_ReturnType  Rte_DcmGetSessChgPermission(Dcm_SesType  SesCtrlTypeActive,Dcm_SesType  SesCtrlTypeNew);

/***********call back functions called by DCM when a service request received******/
extern  Std_ReturnType  Rte_DcmServiceReqIndication(uint8 SID,uint8 *RequestData,uint16 DataSize);

/*************callback functions for DID services*************/
extern  Std_ReturnType Rte_DidConditionCheckRead_F189(Dcm_NegativeResponseCodeType* ErrorCode);
extern  Std_ReturnType Rte_DidConditionCheckWrite_F189(Dcm_NegativeResponseCodeType* ErrorCode);
extern  Std_ReturnType Rte_DidReadData_F189(uint8* data);
extern  Std_ReturnType Rte_DidWriteData_F189(	uint8* data,
            							uint16 dataLength,
            							Dcm_NegativeResponseCodeType* ErrorCode);

extern  Std_ReturnType Rte_DidConditionCheckRead_F089(Dcm_NegativeResponseCodeType* ErrorCode);
extern  Std_ReturnType Rte_DidConditionCheckWrite_F089(Dcm_NegativeResponseCodeType* ErrorCode);
extern  Std_ReturnType Rte_DidReadData_F089(uint8* data);
extern  Std_ReturnType Rte_DidWriteData_F089(	uint8* data,
            							uint16 dataLength,
            							Dcm_NegativeResponseCodeType* ErrorCode);

extern  Std_ReturnType Rte_DidConditionCheckRead_F187(Dcm_NegativeResponseCodeType* ErrorCode);
extern  Std_ReturnType Rte_DidConditionCheckWrite_F187(Dcm_NegativeResponseCodeType* ErrorCode);
extern  Std_ReturnType Rte_DidReadData_F187(uint8* data);
extern  Std_ReturnType Rte_DidWriteData_F187(	uint8* data,
            							uint16 dataLength,
            							Dcm_NegativeResponseCodeType* ErrorCode);

extern  Std_ReturnType Rte_DidConditionCheckRead_F18A(Dcm_NegativeResponseCodeType* ErrorCode);
extern  Std_ReturnType Rte_DidConditionCheckWrite_F18A(Dcm_NegativeResponseCodeType* ErrorCode);
extern  Std_ReturnType Rte_DidReadData_F18A(uint8* data);
extern  Std_ReturnType Rte_DidWriteData_F18A(	uint8* data,
            							uint16 dataLength,
            							Dcm_NegativeResponseCodeType* ErrorCode);


extern  Std_ReturnType Rte_DidConditionCheckRead_F197(Dcm_NegativeResponseCodeType* ErrorCode);
extern  Std_ReturnType Rte_DidConditionCheckWrite_F197(Dcm_NegativeResponseCodeType* ErrorCode);
extern  Std_ReturnType Rte_DidReadData_F197(uint8* data);
extern  Std_ReturnType Rte_DidWriteData_F197(	uint8* data,
            							uint16 dataLength,
            							Dcm_NegativeResponseCodeType* ErrorCode);

extern  Std_ReturnType Rte_DidConditionCheckRead_F193(Dcm_NegativeResponseCodeType* ErrorCode);
extern  Std_ReturnType Rte_DidConditionCheckWrite_F193(Dcm_NegativeResponseCodeType* ErrorCode);
extern  Std_ReturnType Rte_DidReadData_F193(uint8* data);
extern  Std_ReturnType Rte_DidWriteData_F193(	uint8* data,
            							uint16 dataLength,
            							Dcm_NegativeResponseCodeType* ErrorCode);

 
extern  Std_ReturnType Rte_DidConditionCheckRead_F195(Dcm_NegativeResponseCodeType* ErrorCode);
extern  Std_ReturnType Rte_DidConditionCheckWrite_F195(Dcm_NegativeResponseCodeType* ErrorCode);
extern  Std_ReturnType Rte_DidReadData_F195(uint8* data);
extern  Std_ReturnType Rte_DidWriteData_F195(	uint8* data,
            							uint16 dataLength,
            							Dcm_NegativeResponseCodeType* ErrorCode);


extern  Std_ReturnType Rte_DidConditionCheckRead_F18C(Dcm_NegativeResponseCodeType* ErrorCode);
extern  Std_ReturnType Rte_DidConditionCheckWrite_F18C(Dcm_NegativeResponseCodeType* ErrorCode);
extern  Std_ReturnType Rte_DidReadData_F18C(uint8* data);
extern  Std_ReturnType Rte_DidWriteData_F18C(	uint8* data,
            							uint16 dataLength,
            							Dcm_NegativeResponseCodeType* ErrorCode);


extern  Std_ReturnType Rte_DidConditionCheckRead_F010(Dcm_NegativeResponseCodeType* ErrorCode);
extern  Std_ReturnType Rte_DidConditionCheckWrite_F010(Dcm_NegativeResponseCodeType* ErrorCode);
extern  Std_ReturnType Rte_DidReadData_F010(uint8* data);
extern  Std_ReturnType Rte_DidWriteData_F010(	uint8* data,
            							uint16 dataLength,
            							Dcm_NegativeResponseCodeType* ErrorCode);



extern  Std_ReturnType Rte_DidConditionCheckRead_F190(Dcm_NegativeResponseCodeType* ErrorCode);
extern  Std_ReturnType Rte_DidConditionCheckWrite_F190(Dcm_NegativeResponseCodeType* ErrorCode);
extern  Std_ReturnType Rte_DidReadData_F190(uint8* data);
extern  Std_ReturnType Rte_DidWriteData_F190(	uint8* data,
            							uint16 dataLength,
            							Dcm_NegativeResponseCodeType* ErrorCode);


extern  Std_ReturnType Rte_DidConditionCheckRead_F184(Dcm_NegativeResponseCodeType* ErrorCode);
extern  Std_ReturnType Rte_DidConditionCheckWrite_F184(Dcm_NegativeResponseCodeType* ErrorCode);
extern  Std_ReturnType Rte_DidReadData_F184(uint8* data);
extern  Std_ReturnType Rte_DidWriteData_F184(	uint8* data,
            							uint16 dataLength,
            							Dcm_NegativeResponseCodeType* ErrorCode);

/*DID = 0x0003*/
extern  Std_ReturnType Rte_DidShortTermAdjustment_0003(	uint8 *ControlOptionRecord,
                                       			uint8 *ControlEnableMaskRecord,
                                       			uint8 *ControlStatusRecord,
                                       			Dcm_NegativeResponseCodeType  *ErrorCode);
                                       			
extern  Std_ReturnType Rte_DidReadData_0003(uint8* data);
/**************************************************************************
 ********************* callback function for ECU Reset********************
 *************************************************************************/
extern  Std_ReturnType Rte_DcmEcuReset1(uint8 ResetType,Dcm_NegativeResponseCodeType* ErrorCode);

/**************************************************************************
 *********************callback functions for Routine control
 *************************************************************************/
/*DA00*/
extern  Std_ReturnType Rte_DcmRoutineStartDA00(	uint8* InBuffer,
												uint8* OutBuffer,
												Dcm_NegativeResponseCodeType* ErrorCode);

extern  Std_ReturnType Rte_DcmRoutineStopDA00(	uint8* InBuffer,
            									uint8* OutBuffer,
												Dcm_NegativeResponseCodeType* ErrorCode);

extern  Std_ReturnType Rte_DcmRoutineResultDA00(uint8* OutBuffer,
												Dcm_NegativeResponseCodeType* ErrorCode);
												
/*********callback function for Communication Control 0x28 Service*********/
extern Std_ReturnType  Rte_CommunicaitonControl(uint8  controlType,uint8 communicationType);


/*********callback function for Communication Control 0xB0 Service*********/
extern Std_ReturnType Rte_TestService(	uint8  *pInData,
										uint16 InDataLength,
                               			uint8  *pOutDta,
                               			uint16 *pOutDataLength,
                                		Dcm_NegativeResponseCodeType *Error);
                                		
/***************************************************************************
     会话切换从非默认会话到默认会话，则恢复DTC的记录功能
 ***************************************************************************/
extern void Rte_EnableAllDtcsRecord(void);



#ifdef __cplusplus
}
#endif  /* __cplusplus */

#endif /* RTE_DCM_H */
