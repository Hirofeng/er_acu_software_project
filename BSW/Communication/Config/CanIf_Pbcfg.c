/*============================================================================*/
/*  Copyright (C) 2009-2014,
 *  
 *  All rights reserved. This software is  property. Duplication
 *  or disclosure without  written authorization is prohibited.
 *  
 *  @file       <CanIf_Pbcfg.c>
 *  @brief      <>
 *  
 *  <MCU:MPC563x>
 *  
 *  @author     <>
 *  @date       <2014-06-10 09:41:05>
 */
/*============================================================================*/


/*=======[V E R S I O N  I N F O R M A T I O N]===============================*/

#define CANIF_PBCFG_C_AR_MAJOR_VERSION  3
#define CANIF_PBCFG_C_AR_MINOR_VERSION  2
#define CANIF_PBCFG_C_AR_PATCH_VERSION  0
#define CANIF_PBCFG_C_SW_MAJOR_VERSION  1
#define CANIF_PBCFG_C_SW_MINOR_VERSION  0
#define CANIF_PBCFG_C_SW_PATCH_VERSION  0

/*=======[E N D   O F   F I L E]==============================================*/
