/*========================================================================*/
/*  Copyright (C) 2009-2011,  INFRASTRUCTURE SOFTWARE CO.,LTD.
 *
 *  All rights reserved. This software is  property. Duplication
 *  or disclosure without  written authorization is prohibited.
 *
 *
 *  @file       <Error.h>
 *  @brief      <>
 *
 *  <Compiler: CodeWarrior    MCU:freescale xep 100>
 *
 *  @author     <yaoxuan.zhang>
 *  @date        <15-04-2013>
 */
/*========================================================================*/

#ifndef ERROR_H
#define ERROR_H

/*=======[R E V I S I O N   H I S T O R Y]================================*/
/*  <VERSION>    <DATE>    <AUTHOR>    <REVISION LOG>
 *  V0.1.0       20130415  yaoxuan.zhang       Initial version
 *  V0.2.0       20130619  huaming             modified the format of macro definition,and
 *                                             added the information about MISRAC rule
 *  V0.3.0       20130722  yaoxuan.zhang       Add error hook reference macro definition.
 *  V0.4.0       20130723  yaoxuan.zhang       Modified the code to adapt MISRA C specification.
 */
/*========================================================================*/

/*=======[M I S R A C  R U L E  V I O L A T I O N]========================*/
/*  <MESSAGE ID>    <CODE LINE>    <REASON>
 */
/*========================================================================*/


/*=======[V E R S I O N  I N F O R M A T I O N]===========================*/
#define ERROR_H_VENDOR_ID           0
#define ERROR_H_MODULE_ID           0
#define ERROR_H_AR_MAJOR_VERSION    1
#define ERROR_H_AR_MINOR_VERSION    0
#define ERROR_H_AR_PATCH_VERSION    0
#define ERROR_H_SW_MAJOR_VERSION    1
#define ERROR_H_SW_MINOR_VERSION    0
#define ERROR_H_SW_PATCH_VERSION    0
#define ERROR_H_VENDOR_API_INFIX    0


/*=======[I N C L U D E S]=================================================*/
#include "Std_Types.h"

/*=======[M A C R O S]=====================================================*/
#define E_OS_ACCESS					1	/* STD OSEK */
#define	E_OS_CALLEVEL 				2	/* STD OSEK */
#define	E_OS_ID						3	/* STD OSEK */
#define	E_OS_LIMIT 					4	/* STD OSEK */
#define	E_OS_NOFUNC 				5	/* STD OSEK */
#define	E_OS_RESOURCE				6	/* STD OSEK */
#define	E_OS_STATE 					7	/* STD OSEK */
#define	E_OS_VALUE 					8	/* STD OSEK */

#define	E_OS_SERVICEID 				9	/* AUTOSAR, see 7.10 */
#define	E_OS_RATE 					10	/* AUTOSAR, see 7.10 */
#define	E_OS_ILLEGAL_ADDRESS 		11	/* AUTOSAR, see 7.10 */
#define	E_OS_MISSINGEND				12	/* AUTOSAR, see 7.10 */
#define	E_OS_DISABLEDINT 			13	/* AUTOSAR, see 7.10 */
#define	E_OS_STACKFAULT				14	/* AUTOSAR, see 7.10 */
#define	E_OS_PROTECTION_MEMORY 		15	/* AUTOSAR, see 7.10 */
#define	E_OS_PROTECTION_TIME		16	/* AUTOSAR, see 7.10 */
#define	E_OS_PROTECTION_LOCKED 		17	/* AUTOSAR, see 7.10 */
#define	E_OS_PROTECTION_EXCEPTION	18  /* AUTOSAR, see 7.10 */
#define	E_OS_PROTECTION_RATE 		19	/* AUTOSAR, see 7.10 */


/*=======[T Y P E   D E F I N I T I O N S]==================================*/

typedef enum
{
	OSServiceId_ActivateTask		= 0,
	OSServiceId_TerminateTask		= 1,
	OSServiceId_ChainTask			= 2,
	OSServiceId_Schedule			= 3,
	OSServiceId_GetTaskID			= 4,
	OSServiceId_GetResource			= 5,
	OSServiceId_ReleaseResource		= 6,
	OSServiceId_SetEvent			= 7,
	OSServiceId_ClearEvent			= 8,
	OSServiceId_GetEvent			= 9,
	OSServiceId_WaitEvent			= 10,
	OSServiceId_GetAlarmBase		= 11,
	OSServiceId_GetAlarm			= 12,
	OSServiceId_SetRelAlarm			= 13,
	OSServiceId_SetAbsAlarm			= 14,
	OSServiceId_CancelAlarm			= 15,
	OSServiceId_StartOS				= 16,
	OSServiceId_ShutdownOS			= 17,
	OSServiceId_ErrorHook			= 18,
	OSServiceId_PreTaskHook			= 19,
	OSServiceId_PostTaskHook		= 20,
	OSServiceId_StartupHook			= 21,
	OSServiceId_ShutdownHook		= 22,
	OSServiceId_GetTaskState		= 23
} Os_ServiceIdType;

/*=======[E X T E R N A L   D A T A]========================================*/

/*=======[E X T E R N A L   F U N C T I O N   D E C L A R A T I O N S]======*/

#define OSErrorGetServiceId()			 	(System_SCB.sysOsServiceId)

#define OSError_ActivateTask_TaskID()		(osSrvParam1.taskId)
#define OSError_ChainTask_TaskID()			(osSrvParam1.taskId)
#define OSError_GetTaskID_TaskID()			(osSrvParam1.taskRef)
#define OSError_GetTaskState_TaskID()		(osSrvParam1.taskId)
#define OSError_GetTaskState_State()		(osSrvParam2.stateRef)
#define OSError_GetResource_ResID()			(osSrvParam1.resourceId)
#define OSError_ReleaseResource_ResID()		(osSrvParam1.resourceId)
#define OSError_SetEvent_TaskID()			(osSrvParam1.taskId)
#define OSError_SetEvent_Mask()				(osSrvParam2.eventMask)
#define OSError_ClearEvent_Mask()			(osSrvParam1.eventMask)
#define OSError_GetEvent_TaskID()			(osSrvParam1.taskId)
#define OSError_GetEvent_Mask()				(osSrvParam2.eventRef)
#define OSError_WaitEvent_Mask()			(osSrvParam1.eventMask)
#define OSError_GetAlarmBase_AlarmID()		(osSrvParam1.alarmId)
#define OSError_GetAlarmBase_Info()			(osSrvParam2.baseRef)
#define OSError_GetAlarm_AlarmID()			(osSrvParam1.alarmId)
#define OSError_GetAlarm_Tick()				(osSrvParam2.tickRef)
#define OSError_SetRelAlarm_AlarmID()		(osSrvParam1.alarmId)
#define OSError_SetRelAlarm_tickement()		(osSrvParam2.tick)
#define OSError_SetRelAlarm_tick()			(osSrvParam3.tick)
#define OSError_SetAbsAlarm_AlarmID()		(osSrvParam1.alarmId)
#define OSError_SetAbsAlarm_start()			(osSrvParam2.start)
#define OSError_SetAbsAlarm_tick()			(osSrvParam3.tick)
#define OSError_CancelAlarm_AlarmID()		(osSrvParam1.alarmId)


#define OSError_Save_ActivateTask(param1)	\
	{										\
		osSrvParam1.taskId=(param1);		\
	}

#define OSError_Save_TerminateTask()

#define OSError_Save_ChainTask(param1)		\
	{										\
		osSrvParam1.taskId=(param1);		\
	}

#define OSError_Save_Schedule()

#define OSError_Save_GetTaskID(param1)		\
	{										\
		osSrvParam1.taskRef=(param1);		\
	}

#define OSError_Save_GetTaskState(param1,param2)	\
	{												\
		osSrvParam1.taskId=(param1); 				\
		osSrvParam2.stateRef=(param2);				\
	}

#define OSError_Save_GetResource(param1)		\
	{											\
		osSrvParam1.resourceId=(param1);		\
	}

#define OSError_Save_ReleaseResource(param1)	\
	{											\
		osSrvParam1.resourceId=(param1);		\
	}

#define OSError_Save_SetEvent(param1, param2)	\
	{											\
		osSrvParam1.taskId=(param1);			\
		osSrvParam2.eventMask=(param2);			\
	}

#define OSError_Save_ClearEvent(param1)			\
	{											\
		osSrvParam1.eventMask=(param1);			\
	}

#define OSError_Save_GetEvent(param1,param2)	\
	{											\
		osSrvParam1.taskId=(param1);			\
		osSrvParam2.eventRef=(param2);			\
	}

#define OSError_Save_WaitEvent(param1)			\
	{											\
		osSrvParam1.eventMask=(param1);			\
	}

#define OSError_Save_GetAlarm(param1,param2)	\
	{											\
		osSrvParam1.alarmId=(param1);			\
		osSrvParam2.tickRef=(param2);			\
	}

#define OSError_Save_GetAlarmBase(param1,param2)	\
	{												\
		osSrvParam1.alarmId=(param1);				\
		osSrvParam2.baseRef=(param2);				\
	}

#define OSError_Save_CancelAlarm(param1)	\
	{										\
		osSrvParam1.alarmId=(param1);		\
	}

#define OSError_Save_SetRelAlarm(param1,param2,param3)	\
	{													\
		osSrvParam1.alarmId=(param1);					\
		osSrvParam2.tick=(param2);						\
		osSrvParam3.tick=(param3);						\
	}

#define OSError_Save_SetAbsAlarm(param1,param2,param3)	\
	{														\
		osSrvParam1.alarmId=(param1);						\
		osSrvParam2.tick=(param2);							\
		osSrvParam3.tick=(param3);							\
	}

#define OSError_Save_IncrementCounter(param1)	\
	{											\
		osSrvParam1.counterId=(param1);			\
	}

#define OSError_Save_GetCounterValue(param1,param2)	\
	{													\
		osSrvParam1.counterId=(param1);					\
		osSrvParam2.tickRef=(param2);					\
	}

#define OSError_Save_GetElapsedCounterValue(param1,param2,param3)	\
	{																\
		osSrvParam1.counterId=(param1);								\
		osSrvParam2.tickRef=(param2);								\
		osSrvParam3.tickRef=(param3);								\
	}

#define error_lock()
#define error_unlock()

/*=======[I N T E R N A L   D A T A]========================================*/

/*=======[I N T E R N A L   F U N C T I O N   D E C L A R A T I O N S]======*/

#endif  /* end ERROR_H */

/*=======[E N D   O F   F I L E]============================================*/
