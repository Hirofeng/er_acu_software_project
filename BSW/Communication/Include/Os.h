/*============================================================================*/
/*  Copyright (C) 2009-2011,  INFRASTRUCTURE SOFTWARE CO.,LTD.
 *  
 *  All rights reserved. This software is  property. Duplication
 *  or disclosure without  written authorization is prohibited.
 *  
 *  
 *  @file       <Os.h>
 *  @brief      <>
 *  
 *  <Compiler: CodeWarrior    MCU:freescale xep 100>
 *  
 *  @author     <yaoxuan.zhang>
 *  @date        <15-04-2013>
 */
/*============================================================================*/

#ifndef OS_H
#define OS_H

/*=======[R E V I S I O N   H I S T O R Y]====================================*/
/*  <VERSION>    <DATE>    <AUTHOR>    <REVISION LOG>
 *  V0.1.0       20130415  yaoxuan.zhang       Initial version
 *  V0.2.0       20130619  huaming             modified the format of macro definition,and 
 *                                             added the information about MISRAC rule             
 *  V0.3.0       20130702  yaoxuan.zhang       Add the declare of error hook function
 *  V0.4.0       20130723  yaoxuan.zhang       Modified the code to adapt MISRA C specification. 
 */
/*============================================================================*/

/*=======[M I S R A C  R U L E  V I O L A T I O N]============================*/
/*  <MESSAGE ID>    <CODE LINE>    <REASON>    
 */
/*============================================================================*/


/*=======[V E R S I O N  I N F O R M A T I O N]===============================*/


/*=======[I N C L U D E S]====================================================*/

#include "Os_Types.h"

#include "Os_Cfg.h"
/*=======[M A C R O S]========================================================*/

#define OS_H_VENDOR_ID              0
#define OS_H_MODULE_ID              0
#define OS_H_AR_MAJOR_VERSION       1
#define OS_H_AR_MINOR_VERSION       0
#define OS_H_AR_PATCH_VERSION       0
#define OS_H_SW_MAJOR_VERSION       1
#define OS_H_SW_MINOR_VERSION       0
#define OS_H_SW_PATCH_VERSION       0
#define OS_H_VENDOR_API_INFIX       0

/*=======[T Y P E   D E F I N I T I O N S]====================================*/


/*=======[E X T E R N A L   D A T A]==========================================*/
extern Os_SCBType volatile System_SCB;
extern Os_SrvParamType volatile osSrvParam1;
extern Os_SrvParamType volatile osSrvParam2;
extern Os_SrvParamType volatile osSrvParam3;

/*=======[E X T E R N A L   F U N C T I O N   D E C L A R A T I O N S]========*/
StatusType ActivateTask(TaskType TaskID);
StatusType TerminateTask(void);
StatusType ChainTask(TaskType TaskID);
StatusType Schedule(void);
StatusType GetTaskID(TaskRefType TaskID);
//StatusType GetTaskState(TaskType TaskID, TaskStateRefType State);


StatusType GetResource(ResourceType ResID);
StatusType ReleaseResource(ResourceType ResID);


StatusType SetEvent(TaskType TaskID, EventMaskType Mask);
StatusType ClearEvent (EventMaskType Mask);
StatusType GetEvent(TaskType TaskID, EventMaskRefType Event);
StatusType WaitEvent(EventMaskType Mask);


StatusType GetAlarm(AlarmType AlarmID, TickRefType Tick);  
StatusType GetAlarmBase(AlarmType AlarmID, AlarmBaseRefType Info);
StatusType CancelAlarm(AlarmType AlarmID);
StatusType SetRelAlarm(AlarmType AlarmID, TickType increment,TickType cycle);   
StatusType SetAbsAlarm(AlarmType AlarmID, TickType start, TickType cycle);  


StatusType IncrementCounter(CounterType CounterID);
StatusType GetCounterValue(CounterType CounterID, TickRefType Value);
StatusType GetElapsedCounterValue(CounterType CounterID, TickRefType Value, TickRefType ElapsedValue);

AppModeType GetActiveApplicationMode(void);

void DisableAllInterrupts(void);
void EnableAllInterrupts(void);
void ResumeAllInterrupts(void);
void SuspendAllInterrupts(void);
void ResumeOSInterrupts(void);
void SuspendOSInterrupts(void);

#if (CFG_ERRORHOOK == TRUE)
void ErrorHook(StatusType Error);
#endif

#if (CFG_PRETASKHOOK == TRUE)
void PreTaskHook(void);
#endif

#if (CFG_POSTTASKHOOK == TRUE)
void PostTaskHook(void);
#endif

#if (CFG_STARTUPHOOK == TRUE)
void StartupHook(void);
#endif

#if (CFG_SHUTDOWNHOOK == TRUE)
void ShutdownHook(StatusType Error);
#endif


void StartOS(Os_AppModeType Mode);
void ShutdownOS(StatusType Error);

/*=======[I N T E R N A L   D A T A]==========================================*/


/*=======[I N T E R N A L   F U N C T I O N   D E C L A R A T I O N S]========*/

#endif  /* end of OS_H */
/*=======[E N D   O F   F I L E]==============================================*/
