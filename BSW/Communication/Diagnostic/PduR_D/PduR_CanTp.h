/*============================================================================*/
/*  Copyright (C) 2009-2013,  INFRASTRUCTURE SOFTWARE CO.,LTD.
 *  
 *  All rights reserved. This software is  property. Duplication
 *  or disclosure without  written authorization is prohibited.
 *  
 *  
 *  @file       PduR_CanTp.h
 *  @brief      header file for CanTp module from PDUR
 *  
 *  
 *  @author     stanley
 *  @date       2013-4-7
 */
/*============================================================================*/

#ifndef PDUR_CANTP_H
#define PDUR_CANTP_H

/****************************** references *********************************/
#include "Dcm_Cbk.h"

#ifdef __cplusplus
extern "C" {
#endif  /* __cplusplus */

#define PDUR_CANTP_H_AR_MAJOR_VERSION  2
#define PDUR_CANTP_H_AR_MINOR_VERSION  3
/****************************** declarations *********************************/
/**
 * @brief
 *   Handling request from CanTp for getting receiving buffer
 * @param canTpRxPduId identifier of this N-SDU
 * @param tpSduLength length of N-SDU data
 * @param PduInfoPtr pointer to pointer of PduInfoType
 * @return BUFREQ_OK  buffer request accomplished successful
 *         BUFREQ_E_BUSY currently no buffer available
 *         BUFREQ_E_OVFL receiver is not able to receive number of
 *                       tpSduLength bytes data; no buffer provided
 *         BUFREQ_E_NOT_OK buffer request no successful, no buffer provided
 */

#define PduR_CanTpProvideRxBuffer Dcm_ProvideRxBuffer

/**
 * @brief
 *   Handling Rx indication for CanTp
 * @param canTpRxPduId identifier of this N-SDU
 * @param result result of TP reception
 */
#define PduR_CanTpRxIndication Dcm_RxIndication

#define PduR_CanTpProvideTxBuffer Dcm_ProvideTxBuffer

#define PduR_CanTpTxConfirmation Dcm_TxConfirmation

/****************************** definitions *********************************/

#ifdef __cplusplus
}
#endif  /* __cplusplus */

#endif /* PDUR_CANTP_H */
