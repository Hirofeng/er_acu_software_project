/*============================================================================*/
/*  Copyright (C) 2009-2011,  INFRASTRUCTURE SOFTWARE CO.,LTD.
 *
 *  All rights reserved. This software is  property. Duplication
 *  or disclosure without  written authorization is prohibited.
 *
 *  @file       <SchM_Com.h>
 *  @brief      <Briefly describe file(one line)>
 *
 *  <Compiler: Cygwin C Compiler    MCU:--->
 *
 *  @author     <zheng fang>
 *  @date       <2013-5-10>
 */
/*============================================================================*/

/*=======[R E V I S I O N   H I S T O R Y]====================================*/
/*  <VERSION>    <DATE>         <AUTHOR>                    <REVISION LOG>
 *  V1.0.0      20130510        zheng fang                 Initial version
 */
/*============================================================================*/

/*=======[V E R S I O N  I N F O R M A T I O N]===============================*/
#define SCHM_COM_H_AR_MAJOR_VERSION   3U
#define SCHM_COM_H_AR_MINOR_VERSION   0U
#define SCHM_COM_H_AR_PATCH_VERSION   3U
#define SCHM_COM_H_SW_MAJOR_VERSION   1U
#define SCHM_COM_H_SW_MINOR_VERSION   0U
#define SCHM_COM_H_SW_PATCH_VERSION   0U

#ifndef SCHM_COM_H_
#define SCHM_COM_H_

#define WRITE_PROTECTION_AREA 1U

/*进入临界区宏函数定义*/
#define SchM_Enter_Com(Exclusive_Area)
/*进入临界区宏函数定义*/
#define SchM_Exit_Com(Exclusive_Area)

#endif /* SCHM_COM_H_ */
