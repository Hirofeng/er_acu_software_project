/*============================================================================*/
/*  Copyright (C) 2009-2014,  INFRASTRUCTURE SOFTWARE CO.,LTD.
 *  
 *  All rights reserved. This software is  property. Duplication
 *  or disclosure without  written authorization is prohibited.
 *  
 *  @file       <Can_Cfg.c>
 *  @brief      <>
 *  
 *  <MCU:MPC563x>
 *  
 *  @author     <>
 *  @date       <2014-06-23 17:29:03>
 */
/*============================================================================*/

	
/*=======[I N C L U D E S]====================================================*/
#include "Can.h"

/*=======[V E R S I O N   I N F O R M A T I O N]===============================*/
#define CAN_CFG_C_AR_MAJOR_VERSION     2
#define CAN_CFG_C_AR_MINOR_VERSION     4
#define CAN_CFG_C_AR_PATCH_VERSION     0
#define CAN_CFG_C_SW_MAJOR_VERSION     1
#define CAN_CFG_C_SW_MINOR_VERSION     0
#define CAN_CFG_C_SW_PATCH_VERSION     0

/*=======[V E R S I O N  C H E C K]===========================================*/
#if (CAN_CFG_C_AR_MAJOR_VERSION != CAN_CFG_H_AR_MAJOR_VERSION)
    #error "Can_Cfg.c:Mismatch in Specification Major Version"
#endif 
#if (CAN_CFG_C_AR_MINOR_VERSION != CAN_CFG_H_AR_MINOR_VERSION)
    #error "Can_Cfg.c:Mismatch in Specification Minor Version"
#endif
#if (CAN_CFG_C_AR_PATCH_VERSION != CAN_CFG_H_AR_PATCH_VERSION)
    #error "Can_Cfg.c:Mismatch in Specification Patch Version"
#endif
#if (CAN_CFG_C_SW_MAJOR_VERSION != CAN_CFG_H_SW_MAJOR_VERSION)
    #error "Can_Cfg.c:Mismatch in Specification Major Version"
#endif
#if (CAN_CFG_C_SW_MINOR_VERSION != CAN_CFG_H_SW_MINOR_VERSION)
    #error "Can_Cfg.c:Mismatch in Specification Minor Version"
#endif

/*=======[E X T E R N A L   D A T A]==========================================*/

#define CAN_START_CONST_PBCFG
#include "Can_MemMap.h"
/* HRHs first,HTH next*/
/* Controller ID must be grouped together */
/* Can ID to ensure top priority IDs are first */
CONST(Can_HardwareObjectType, CAN_CONST_PBCFG) Can_HardwareObjectConfigData[CAN_MAX_HARDWAREOBJECTS] = 
{
    {
        CAN_HANDLE_TYPE_BASIC,      /* CanHandleType */
        CAN_ID_TYPE_STANDARD,       /* CanIdType*/
        0x300,                      /* CanIdValue */ 
        0,                          /* CanObjectId */
        CAN_OBJECT_TYPE_RECEIVE,    /* CanObjectType */  
        CAN_CONTROLLER_A,           /* CanControllerRef */
        0xFFFFFF00U                 /* CanFilterMask */
    },
    {
        CAN_HANDLE_TYPE_BASIC,      /* CanHandleType */
        CAN_ID_TYPE_STANDARD,       /* CanIdType*/
        0x400,                      /* CanIdValue */ 
        1,                          /* CanObjectId */
        CAN_OBJECT_TYPE_RECEIVE,    /* CanObjectType */  
        CAN_CONTROLLER_A,           /* CanControllerRef */
        0xFFFFFF00U                 /* CanFilterMask */
    },
    {
        CAN_HANDLE_TYPE_BASIC,      /* CanHandleType */
        CAN_ID_TYPE_STANDARD,       /* CanIdType*/
        0x7A0,                      /* CanIdValue */ 
        2,                          /* CanObjectId */
        CAN_OBJECT_TYPE_RECEIVE,    /* CanObjectType */  
        CAN_CONTROLLER_A,           /* CanControllerRef */
        0xFFFFFC0U                 /* CanFilterMask */
    },
    {
        CAN_HANDLE_TYPE_BASIC,      /* CanHandleType */
        CAN_ID_TYPE_STANDARD,       /* CanIdType*/
        0x7DF,                      /* CanIdValue */ 
        3,                          /* CanObjectId */
        CAN_OBJECT_TYPE_RECEIVE,    /* CanObjectType */  
        CAN_CONTROLLER_A,           /* CanControllerRef */
        0xFFFFFC0U                 /* CanFilterMask */
    },
    {
        CAN_HANDLE_TYPE_BASIC,      /* CanHandleType */
        CAN_ID_TYPE_STANDARD,       /* CanIdType*/
        0x500,                      /* CanIdValue */ 
        4,                          /* CanObjectId */
        CAN_OBJECT_TYPE_TRANSMIT,    /* CanObjectType */  
        CAN_CONTROLLER_A,           /* CanControllerRef */
        0xFFFFFFFFU                 /* CanFilterMask */
    },
    {
        CAN_HANDLE_TYPE_BASIC,      /* CanHandleType */
        CAN_ID_TYPE_STANDARD,       /* CanIdType*/
        0x600,                      /* CanIdValue */ 
        5,                          /* CanObjectId */
        CAN_OBJECT_TYPE_TRANSMIT,    /* CanObjectType */  
        CAN_CONTROLLER_A,           /* CanControllerRef */
        0xFFFFFFFFU                 /* CanFilterMask */
    },
    {
        CAN_HANDLE_TYPE_BASIC,      /* CanHandleType */
        CAN_ID_TYPE_STANDARD,       /* CanIdType*/
        0x7B0,                      /* CanIdValue */ 
        6,                          /* CanObjectId */
        CAN_OBJECT_TYPE_TRANSMIT,    /* CanObjectType */  
        CAN_CONTROLLER_A,           /* CanControllerRef */
        0xFFFFFFFFU                 /* CanFilterMask */
    },
    {
        CAN_HANDLE_TYPE_BASIC,      /* CanHandleType */
        CAN_ID_TYPE_STANDARD,       /* CanIdType*/
        0x7EE,                      /* CanIdValue */ 
        7,                          /* CanObjectId */
        CAN_OBJECT_TYPE_TRANSMIT,    /* CanObjectType */  
        CAN_CONTROLLER_A,           /* CanControllerRef */
        0xFFFFFFFFU                 /* CanFilterMask */
    },
    {
        CAN_HANDLE_TYPE_FULL,       /* CanHandleType */
        CAN_ID_TYPE_STANDARD,       /* CanIdType*/
        0x7DF,                      /* CanIdValue */ 
        8,                          /* CanObjectId */
        CAN_OBJECT_TYPE_RECEIVE,    /* CanObjectType */  
        CAN_CONTROLLER_C,           /* CanControllerRef */
        0xFFFFFFFFU                 /* CanFilterMask */
    },
    {
        CAN_HANDLE_TYPE_FULL,      /* CanHandleType */
        CAN_ID_TYPE_STANDARD,       /* CanIdType*/
        0x70D,                      /* CanIdValue */ 
        9,                          /* CanObjectId */
        CAN_OBJECT_TYPE_RECEIVE,    /* CanObjectType */  
        CAN_CONTROLLER_C,           /* CanControllerRef */
        0xFFFFFFFFU                 /* CanFilterMask */
    },
    {
        CAN_HANDLE_TYPE_FULL,      /* CanHandleType */
        CAN_ID_TYPE_STANDARD,       /* CanIdType*/
        0x341,                      /* CanIdValue */ 
        10,                          /* CanObjectId */
        CAN_OBJECT_TYPE_RECEIVE,    /* CanObjectType */  
        CAN_CONTROLLER_C,           /* CanControllerRef */
        0xFFFFFFFFU                 /* CanFilterMask */
    },
    {
        CAN_HANDLE_TYPE_FULL,      /* CanHandleType */
        CAN_ID_TYPE_STANDARD,       /* CanIdType*/
        0x350,                      /* CanIdValue */ 
        11,                          /* CanObjectId */
        CAN_OBJECT_TYPE_RECEIVE,    /* CanObjectType */  
        CAN_CONTROLLER_C,           /* CanControllerRef */
        0xFFFFFFFFU                 /* CanFilterMask */
    },
    {
        CAN_HANDLE_TYPE_FULL,      /* CanHandleType */
        CAN_ID_TYPE_STANDARD,       /* CanIdType*/
        0x3BE,                      /* CanIdValue */ 
        12,                          /* CanObjectId */
        CAN_OBJECT_TYPE_RECEIVE,    /* CanObjectType */  
        CAN_CONTROLLER_C,           /* CanControllerRef */
        0xFFFFFFFFU                 /* CanFilterMask */
    },
    {
        CAN_HANDLE_TYPE_FULL,      /* CanHandleType */
        CAN_ID_TYPE_STANDARD,       /* CanIdType*/
        0x377,                      /* CanIdValue */ 
        13,                          /* CanObjectId */
        CAN_OBJECT_TYPE_RECEIVE,    /* CanObjectType */  
        CAN_CONTROLLER_C,           /* CanControllerRef */
        0xFFFFFFFFU                 /* CanFilterMask */
    },
    {
        CAN_HANDLE_TYPE_FULL,      /* CanHandleType */
        CAN_ID_TYPE_STANDARD,       /* CanIdType*/
        0x78D,                      /* CanIdValue */    //发送函数
        14,                          /* CanObjectId */
        CAN_OBJECT_TYPE_TRANSMIT,    /* CanObjectType */  
        CAN_CONTROLLER_C,           /* CanControllerRef */
        0xFFFFFFFFU                 /* CanFilterMask */
    },
    {
        CAN_HANDLE_TYPE_FULL,      /* CanHandleType */
        CAN_ID_TYPE_STANDARD,       /* CanIdType*/
        0x3A2,//0x308,                      /* CanIdValue */ 
        15,                          /* CanObjectId */
        CAN_OBJECT_TYPE_TRANSMIT,    /* CanObjectType */  
        CAN_CONTROLLER_C,           /* CanControllerRef */
        0xFFFFFFFFU                 /* CanFilterMask */
    },
};

CONST(uint16, CAN_CONST_PBCFG) Can_HohConfigData[CAN_MAX_HOHS] 
= {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35};

CONST(Can_ControllerConfigType, CAN_CONST_PBCFG) Can_ControllerConfigData[CAN_MAX_CONTROLLERS] =
{
    {
        0x09dc0085,                             /* CanCtrlValue--> BaudRate:500 Kbps */
        0,                                      /* CanRxHwObjFirst */
        4,                                      /* CanRxHwObjCount */
        4,                                      /* CanTxHwObjFirst */
        4,                                      /* CanTxHwObjCount */
    },
    {
        0x09dc0085,                             /* CanCtrlValue--> BaudRate:500 Kbps */
        8,                                      /* CanRxHwObjFirst */
        6,//18                                   /* CanRxHwObjCount */    //接收邮箱数量  LIUYUN
        14,//26,                                 /* CanTxHwObjFirst */
        2,//10,                                  /* CanTxHwObjCount */
    },
};
#define CAN_STOP_CONST_PBCFG
#include "Can_MemMap.h"

#define CAN_START_SEC_CONST_UNSPECIFIED
#include "Can_MemMap.h"
CONST(Can_ControllerPCConfigType, CAN_CONST) Can_ControllerPCConfigData[CAN_MAX_CONTROLLERS] = 
{
    {
        0xE800,//0xFFFC0000U,                         /* CanControllerBaseAddr:CAN_A:0xFFFC0000U or CAN_B: 0xFFFC8000U*/
        CAN_CONTROLLER_A,                    /* CanControllerId */
        TRUE,                                /* CanControllerActivation */   
        CAN_PROCESS_TYPE_INTERRUPT,          /* CanBusOffProcessing */
        CAN_PROCESS_TYPE_INTERRUPT,            /* CanRxProcessing */
        CAN_PROCESS_TYPE_INTERRUPT,            /* CanTxProcessing */
        #if(STD_ON == CAN_WAKEUP_SUPPORT)
        CAN_PROCESS_TYPE_INTERRUPT,          /* CanWakeupProcessing */
        ECUM_WKSOURCE_ALL_SOURCES1,                 /* CanWakeupSourceRef */
        #endif /* #if(STD_ON == CAN_WAKEUP_SUPPORT) */
        80000000U,                            /* CanCpuClock */
		CAN_CLOCKSRC_TYPE_BUSCLOCK,               /* CanClockSource */
		#if(STD_ON == CAN_WAKEUP_SUPPORT)
		0x00001000U,
		#endif
		16
    },
    {
        0xE800,//0xFFFC8000U,                         /* CanControllerBaseAddr:CAN_A:0xFFFC0000U or CAN_B: 0xFFFC8000U*/
        CAN_CONTROLLER_C,                    /* CanControllerId */
        TRUE,                                /* CanControllerActivation */   
        CAN_PROCESS_TYPE_INTERRUPT,          /* CanBusOffProcessing */
        CAN_PROCESS_TYPE_INTERRUPT,            /* CanRxProcessing */
        CAN_PROCESS_TYPE_INTERRUPT,            /* CanTxProcessing */
        #if(STD_ON == CAN_WAKEUP_SUPPORT)
        CAN_PROCESS_TYPE_INTERRUPT,          /* CanWakeupProcessing */
        ECUM_WKSOURCE_ALL_SOURCES2,                 /* CanWakeupSourceRef */
        #endif /* #if(STD_ON == CAN_WAKEUP_SUPPORT) */
        80000000U,                            /* CanCpuClock */
		CAN_CLOCKSRC_TYPE_BUSCLOCK,               /* CanClockSource */
		#if(STD_ON == CAN_WAKEUP_SUPPORT)
		0x00004000U,
		#endif
		16//32
    },
};
#define CAN_STOP_SEC_CONST_UNSPECIFIED
#include "Can_MemMap.h"

/*=======[E N D   O F   F I L E]==============================================*/
