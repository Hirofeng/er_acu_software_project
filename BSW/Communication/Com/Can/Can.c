/*============================================================================*/
/*  Copyright (C) 2009-2011,  INFRASTRUCTURE SOFTWARE CO.,LTD.
 *
 *  All rights reserved. This software is  property. Duplication
 *  or disclosure without  written authorization is prohibited.
 *
 *  @file       <Can.c>
 *  @brief      <Can driver Module source file>
 *
 * <Compiler: CodeWarrior V2.7    MCU:MPC55XX>
 *
 *  @author     <bo.zeng>
 *  @date       <10-07-2013>
 */
/*============================================================================*/

/*=======[R E V I S I O N   H I S T O R Y]====================================*/
/*  <VERSION>    <DATE>    <AUTHOR>    <REVISION LOG>
 *  V1.0.0       20130710  bo.zeng      Initial version
 *                                      these features not support in this version:
 *                                      1. multiple Can Drivers.
 *                                      2. Can transceiver.
 *                                      3. mixed type HRH only according to CANID 
 *                                      configured now.
 *  V1.0.1       20131101  jianan.liu   1. Clear buffer when controller of CAN goes
 *                                         to STOP;
 *                                      2. Make the code more united in the same
 *                                         series of chips
 *  V1.0.2       20140429  bo.zeng      1. Add configure item CanHwMbNum
 *                                      2. init other all mailboxs not used
 *                                      3. Debug Standard frame filter(filter << 18)
 *  V1.0.3       20140523  bo.zeng      change automatic busoff recovery to manual 
 *                                      busoff recovery
 */
/*============================================================================*/

/*=======[V E R S I O N  I N F O R M A T I O N]===============================*/
#define CAN_C_AR_MAJOR_VERSION      2
#define CAN_C_AR_MINOR_VERSION      4
#define CAN_C_AR_PATCH_VERSION      0

#define CAN_C_SW_MAJOR_VERSION      1
#define CAN_C_SW_MINOR_VERSION      0
#define CAN_C_SW_PATCH_VERSION      3


#include "Cpu.h"
#include "clockMan1.h"
#include "canCom1.h"
#include "dmaController1.h"
#include "pin_mux.h"
#if CPU_INIT_CONFIG
  #include "Init_Config.h"
#endif

#include <stdint.h>
#include <stdbool.h>



/*=======[I N C L U D E S]====================================================*/
#include "Can.h"
#if (STD_ON == CAN_DEV_ERROR_DETECT)
#include "Det.h"
#endif /* STD_ON == CAN_DEV_ERROR_DETECT */
#if (STD_ON == CAN_DEM_ERROR_DETECT)
#include "Dem.h"
#endif /* STD_ON == CAN_DEM_ERROR_DETECT */
#include "CanIf_Cbk.h"
#include "SchM_Can.h"
#include "Can_Regs.h"
#include "Can_Irq.h"
//#include "FlexCAN.h"
/*=======[V E R S I O N  C H E C K]===========================================*/
#if (CAN_C_AR_MAJOR_VERSION != CAN_H_AR_MAJOR_VERSION)
    #error "Can.c:Mismatch in Specification Major Version"
#endif /* CAN_C_AR_MAJOR_VERSION != CAN_H_AR_MAJOR_VERSION */
#if (CAN_C_AR_MINOR_VERSION != CAN_H_AR_MINOR_VERSION)
    #error "Can.c:Mismatch in Specification Minor Version"
#endif /* CAN_C_AR_MINOR_VERSION != CAN_H_AR_MINOR_VERSION */
#if (CAN_C_AR_PATCH_VERSION != CAN_H_AR_PATCH_VERSION)
    #error "Can.c:Mismatch in Specification Patch Version"
#endif /* CAN_C_AR_PATCH_VERSION != CAN_H_AR_PATCH_VERSION */
#if (CAN_C_SW_MAJOR_VERSION != CAN_H_SW_MAJOR_VERSION)
    #error "Can.c:Mismatch in Specification Major Version"
#endif /* CAN_C_SW_MAJOR_VERSION != CAN_H_SW_MAJOR_VERSION */
#if (CAN_C_SW_MINOR_VERSION != CAN_H_SW_MINOR_VERSION)
    #error "Can.c:Mismatch in Specification Minor Version"
#endif /* CAN_C_SW_MINOR_VERSION != CAN_H_SW_MINOR_VERSION */


#if (STD_ON == CAN_DEV_ERROR_DETECT)
#if (2 != DET_H_AR_MAJOR_VERSION)
    #error "Can.c:Mismatch in Specification Major Version"
#endif
#if (2 != DET_H_AR_MINOR_VERSION)
    #error "Can.c:Mismatch in Specification Minor Version"
#endif
#endif /* STD_ON == CAN_DEV_ERROR_DETECT */

#if (STD_ON == CAN_DEM_ERROR_DETECT)
#if (3 != DEM_H_AR_MAJOR_VERSION)
    #error "Can.c:Mismatch in Specification Major Version"
#endif
#if (1 != DEM_H_AR_MINOR_VERSION)
    #error "Can.c:Mismatch in Specification Minor Version"
#endif
#endif /* STD_ON == CAN_DEM_ERROR_DETECT */

#if (3 != CANIF_CBK_H_AR_MAJOR_VERSION)
    #error "Can.c:Mismatch in Specification Major Version"
#endif
#if (2 != CANIF_CBK_H_AR_MINOR_VERSION)
    #error "Can.c:Mismatch in Specification Minor Version"
#endif

#if (1 != SCHM_CAN_H_AR_MAJOR_VERSION)
    #error "Can.c:Mismatch in Specification Major Version"
#endif
#if (1 != SCHM_CAN_H_AR_MINOR_VERSION)
    #error "Can.c:Mismatch in Specification Minor Version"
#endif

#if (2 != CAN_REG_H_AR_MAJOR_VERSION)
    #error "Can.c:Mismatch in Specification Major Version"
#endif
#if (4 != CAN_REG_H_AR_MINOR_VERSION)
    #error "Can.c:Mismatch in Specification Minor Version"
#endif

#if (2 != CAN_IRQ_H_AR_MAJOR_VERSION)
    #error "Can.c:Mismatch in Specification Major Version"
#endif
#if (4 != CAN_IRQ_H_AR_MINOR_VERSION)
    #error "Can.c:Mismatch in Specification Minor Version"
#endif

/*=======[M A C R O S]========================================================*/
/* Can Hardware Number of Mailboxs */
#define CAN_HW_MAX_MAILBOXES         64U

/* DEM Report function empty */
#if (STD_OFF == CAN_DEM_ERROR_DETECT)
#define Dem_ReportErrorStatus(eventId, eventStatus)   do{}while(0)
#endif

/* MISRA RULE 19.7:3453 VIOLATION: the marco like a function */
#if(CAN_VARIANT_PB == CAN_VARIANT_CFG)
/* MISRA RULE 17.4:491 VIOLATION: Array subscripting applied to an object pointer */
#define CAN_CNTRL_CFG(controller)         (Can_GlobalConfigPtr->CanController[controller])
#define CAN_HWOBJ_CFG(mbId)               (Can_GlobalConfigPtr->CanHardwareObject[mbId])
#define CAN_HWOBJ_ID(hth)                 (Can_GlobalConfigPtr->CanHoh[hth])
#define CAN_HWOBJ_NUM                     (Can_GlobalConfigPtr->CanHardwareObjectNum)
#define CAN_HOH_NUM                       (Can_GlobalConfigPtr->CanHohNum)

#else /* CAN_VARIANT_PB == CAN_VARIANT_CFG */
#define CAN_CNTRL_CFG(controller)         (Can_ControllerConfigData[controller])
#define CAN_HWOBJ_CFG(mbId)               (Can_HardwareObjectConfigData[mbId])
#define CAN_HWOBJ_ID(hth)                 (Can_HohConfigData[hth])
#define CAN_HWOBJ_NUM                     CAN_MAX_HARDWAREOBJECTS
#define CAN_HOH_NUM                       CAN_MAX_HOHS
#endif /* CAN_VARIANT_PB == CAN_VARIANT_CFG */

#define CAN_CNTRL_HOH_NUM(controller)     (uint16)(CAN_CNTRL_CFG(controller).CanRxHwObjCount + CAN_CNTRL_CFG(controller).CanTxHwObjCount)

/* MISRA RULE 11.3:303 VIOLATION: Hardware register address operation */
/* Contorller Register */
#define CAN_CNTRL_REG(controller)  \
    ((volatile Can_FlexCanRegType *)Can_ControllerPCConfigData[controller].CanControllerBaseAddr)

/* controller PC config */
#define CAN_CNTRL_PCCFG(controller)  (Can_ControllerPCConfigData[controller])

/* hardware object ID to mailbox ID */
#define CAN_MB_ID(hwObjId)  \
    ((uint16)((hwObjId) - CAN_CNTRL_CFG(CAN_HWOBJ_CFG(hwObjId).CanControllerRef).CanRxHwObjFirst))

/* mailbox ID to hardware object ID */
#define CAN_MBID_TO_HWOBJ(Controller, mbId) \
    ((uint16)((mbId) + CAN_CNTRL_CFG(Controller).CanRxHwObjFirst))





/*=======[T Y P E   D E F I N I T I O N S]====================================*/
#if(STD_ON == CAN_DEV_ERROR_DETECT)
/* Can Driver State Machine */
typedef enum
{
    CAN_UNINIT = 0,
    CAN_READY
} Can_DriverStatusType;
#endif /* STD_ON == CAN_DEV_ERROR_DETECT */

typedef enum
{
    CAN_CS_UNINT = 0U,
    CAN_CS_STOPPED,
    CAN_CS_STARTED,
    CAN_CS_SLEEP
} Can_ControllerModeType;

/* Controller Runtime Structure */
typedef struct
{
    Can_ControllerModeType  CntrlMode;      /* controller mode */

    /* Interrupt masks software backup */
    uint32                  RxMask[2];
    uint32                  TxMask[2];
    uint8                   SwIntFlag;      /* seftware interrupt flag */   

    uint32                  IntLockCount;  
    /* Transmit PDU handles for TxConfirmation callbacks to CANIF */
    PduIdType               TxPduHandles[CAN_HW_MAX_MAILBOXES];  
} Can_ControllerStatusType;

/*=======[I N T E R N A L   D A T A]==========================================*/
#if(STD_ON == CAN_DEV_ERROR_DETECT)
/* @req <CAN103> */
/* can module status:(CAN_UNINIT, CAN_READY) */
#define CAN_START_SEC_VAR_POWER_ON_INIT_UNSPECIFIED
#include "Can_MemMap.h"
STATIC VAR(Can_DriverStatusType, CAN_VAR_POWER_ON_INIT) Can_DriverStatus = CAN_UNINIT;
#define CAN_STOP_SEC_VAR_POWER_ON_INIT_UNSPECIFIED
#include "Can_MemMap.h"
#endif /* STD_ON == CAN_DEV_ERROR_DETECT */

/* Global Config Pointer */
#define CAN_START_SEC_CONST_UNSPECIFIED
#include "Can_MemMap.h"
STATIC P2CONST(Can_ConfigType, CAN_CONST, CAN_CONST_PBCFG) Can_GlobalConfigPtr;
#define CAN_STOP_SEC_CONST_UNSPECIFIED
#include "Can_MemMap.h"

/* Controller Runtime structure */
#define CAN_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Can_MemMap.h"
STATIC VAR(Can_ControllerStatusType, CAN_VAR) Can_Cntrl[CAN_MAX_CONTROLLERS];
#define CAN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Can_MemMap.h"

/*=======[I N T E R N A L   F U N C T I O N   D E C L A R A T I O N S]========*/
#define CAN_START_SEC_CODE
#include "Can_MemMap.h"

/* Mode Control */
STATIC FUNC(Can_ReturnType, CAN_CODE) Can_StartMode(uint8 Controller);
STATIC FUNC(Can_ReturnType, CAN_CODE) Can_StopMode(uint8 Controller);
STATIC FUNC(Can_ReturnType, CAN_CODE) Can_SleepMode(uint8 Controller);
STATIC FUNC(Can_ReturnType, CAN_CODE) Can_WakeupMode(uint8 Controller);

STATIC FUNC(void, CAN_CODE) Can_InitHwCntrl
(
    uint8 Controller, 
    P2CONST(Can_ControllerConfigType, AUTOMATIC, CAN_CONST_PBCFG) Config
);
STATIC FUNC(void,    CAN_CODE)   Can_InitMB(uint8 Controller);
#if (STD_ON == CAN_TX_POLLING)
STATIC FUNC(void,    CAN_CODE)   Can_TxProcess(uint8 Controller);
#endif /* STD_ON == CAN_TX_POLLING */
#if (STD_ON == CAN_RX_POLLING)
STATIC FUNC(void,    CAN_CODE)   Can_RxProcess(uint8 Controller);
#endif /* STD_ON == CAN_RX_POLLING */
#if(STD_ON == CAN_HW_TRANSMIT_CANCELLATION)
STATIC FUNC(void,    CAN_CODE)   Can_TxCancel(uint8 Controller, uint32 mbId);
#endif /* STD_ON == CAN_HW_TRANSMIT_CANCELLATION */
STATIC FUNC(void,    CAN_CODE)   Can_BusOff_Handler(uint8 Controller);
#if(STD_ON == CAN_MULTIPLEXED_TRANSMISSION)
STATIC FUNC(uint16,  CAN_CODE)   Can_FindLowPriorityMb(uint8 Hth);
#endif /* STD_ON == CAN_MULTIPLEXED_TRANSMISSION */
STATIC FUNC(boolean, CAN_CODE)   Can_IsTxMbFree(uint16 HwObjId);
STATIC FUNC(void, CAN_CODE)      Can_WriteMb
(
    uint16 HwObjId, 
    P2CONST(Can_PduType, AUTOMATIC, CAN_APPL_CONST) PduInfo
);
STATIC FUNC(uint32,  CAN_CODE)   Can_GetMBCanId(uint16 HwObjId);
STATIC FUNC(void,    CAN_CODE)   Can_GetMBInfo
(
    uint16 HwObjId, 
    P2VAR(Can_PduType, AUTOMATIC, AUTOMATIC)pdu 
);
STATIC FUNC(void,    CAN_CODE)   Can_ApplySWF(uint8 Controller, uint8 SwIntFlag);
#if ((STD_ON==CAN_HW_TRANSMIT_CANCELLATION)&&(STD_ON==CAN_MULTIPLEXED_TRANSMISSION))
STATIC FUNC(void, CAN_CODE) Can_CheckAbortMb(uint8 Controller, uint8 Hth);
#endif /* (STD_ON==CAN_HW_TRANSMIT_CANCELLATION)&&(STD_ON==CAN_MULTIPLEXED_TRANSMISSION) */

#if ((STD_ON==CAN_HW_TRANSMIT_CANCELLATION)||(STD_ON==CAN_MULTIPLEXED_TRANSMISSION))
STATIC FUNC(boolean, CAN_CODE) Can_PriorityHigher(Can_IdType destId, Can_IdType srcId);
#endif /* (STD_ON==CAN_HW_TRANSMIT_CANCELLATION)||(STD_ON==CAN_MULTIPLEXED_TRANSMISSION) */
 
/*=======[F U N C T I O N   I M P L E M E N T A T I O N S]====================*/

/******************************************************************************/
/*
 * Brief               <This function initializes the CAN driver>
 * ServiceId           <0x00>
 * Sync/Async          <Synchronous>
 * Reentrancy          <Non Reentrant>
 * Param-Name[in]      <Config:Pointer to driver configuration>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
FUNC(void, CAN_CODE) Can_Init
(
    P2CONST(Can_ConfigType, AUTOMATIC, CAN_CONST_PBCFG) Config
)
{
    uint8 Controller = 0;
 
    #if(STD_ON == CAN_DEV_ERROR_DETECT)
    if(CAN_UNINIT != Can_DriverStatus) /* @req <CAN174> @req <CAN247> */
    {
        Det_ReportError(CAN_MODULE_ID, CAN_INSTANCE, CAN_INIT_ID, CAN_E_TRANSITION);
    }
    #if(CAN_VARIANT_PB == CAN_VARIANT_CFG)   
    else if(NULL_PTR == Config)        /* @req <CAN175> */
    {
        Det_ReportError(CAN_MODULE_ID, CAN_INSTANCE, CAN_INIT_ID, CAN_E_PARAM_POINTER);
    }
    #endif /* CAN_VARIANT_PB == CAN_VARIANT_CFG */
    else
    #endif /* STD_ON == CAN_DEV_ERROR_DETECT */
    {
        /* backup config pointer */
        #if(CAN_VARIANT_PB == CAN_VARIANT_CFG)   
        Can_GlobalConfigPtr = Config;
        #endif /* #if(CAN_VARIANT_PB == CAN_VARIANT_CFG) */

        /*@req <CAN245> init each controller */
        for(Controller = 0; Controller < CAN_MAX_CONTROLLERS; Controller++)
        {
            Can_Cntrl[Controller].SwIntFlag       = 0U;
            Can_Cntrl[Controller].IntLockCount    = 0U;
            Can_Cntrl[Controller].RxMask[0]       = 0U;
            Can_Cntrl[Controller].RxMask[1]       = 0U;
            Can_Cntrl[Controller].TxMask[0]       = 0U;
            Can_Cntrl[Controller].TxMask[1]       = 0U;
            Can_Cntrl[Controller].CntrlMode       = CAN_CS_STOPPED; /* @req <CAN259> */
        }

        /* @req <CAN246> */
        #if (STD_ON == CAN_DEV_ERROR_DETECT)
        Can_DriverStatus = CAN_READY;  
        #endif /* STD_ON == CAN_DEV_ERROR_DETECT */
    }
    
    return;
}

/******************************************************************************/
/*
 * Brief               <This function initializing only Can controller specific settings>
 * ServiceId           <0x02>
 * Sync/Async          <Synchronous>
 * Reentrancy          <Non Reentrant>
 * Param-Name[in]      <Controller-CAN controller to be initialized>
 * Param-Name[out]     <Config-pointer to controller configuration>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
FUNC(void, CAN_CODE) Can_InitController
(
    uint8 Controller,
    P2CONST(Can_ControllerConfigType, AUTOMATIC, CAN_CONST_PBCFG) Config
)
{
    #if(STD_ON == CAN_DEV_ERROR_DETECT)
    if(CAN_READY != Can_DriverStatus)  /* @req <CAN187> */
    {
        Det_ReportError(CAN_MODULE_ID, CAN_INSTANCE, CAN_INITCONTROLLER_ID, CAN_E_UNINIT);
    }
    else if(NULL_PTR == Config)        /* @req <CAN188> */
    {
        Det_ReportError(CAN_MODULE_ID, CAN_INSTANCE, CAN_INITCONTROLLER_ID, CAN_E_PARAM_POINTER);
    }
    else if(Controller >= CAN_MAX_CONTROLLERS) /* @req <CAN189> */
    {
        Det_ReportError(CAN_MODULE_ID, CAN_INSTANCE, CAN_INITCONTROLLER_ID, 
                CAN_E_PARAM_CONTROLLER);
    }
    else if(CAN_CS_STOPPED != Can_Cntrl[Controller].CntrlMode) /* @req <CAN190> */
    {
        Det_ReportError(CAN_MODULE_ID, CAN_INSTANCE, CAN_INITCONTROLLER_ID, CAN_E_TRANSITION);
    }
    else
    #endif /* STD_ON == CAN_DEV_ERROR_DETECT */   
    {
        Can_InitHwCntrl(Controller, Config);

        Can_Cntrl[Controller].CntrlMode = CAN_CS_STOPPED;  /* @req <CAN256> */
    }
    
    return;
}

/******************************************************************************/
/*
 * Brief               <This function performs software triggered state transitions 
 *                         of the CAN controller State machine.>
 * ServiceId           <0x03>
 * Sync/Async          <Asynchronous>
 * Reentrancy          <Non Reentrant>
 * Param-Name[in]      <Controller-CAN controller for which the status shall be changed>
 * Param-Name[in]      <Transition-Possible transitions>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <Can_ReturnType-CAN_OK or CAN_NOT_OK>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
/*@req <CAN230> @req <CAN017> */
FUNC(Can_ReturnType, CAN_CODE) Can_SetControllerMode
(
    uint8 Controller,
    Can_StateTransitionType Transition
)
{
    Can_ReturnType ret = CAN_NOT_OK;

    #if(STD_ON == CAN_DEV_ERROR_DETECT)
    if(CAN_READY != Can_DriverStatus)          /* @req <CAN0198> */
    {
        Det_ReportError(CAN_MODULE_ID, CAN_INSTANCE, CAN_SETCONTROLLERMODE_ID, CAN_E_UNINIT);
        ret = CAN_NOT_OK;
    }
    else if(Controller >= CAN_MAX_CONTROLLERS) /* @req <CAN0199> */
    {
        Det_ReportError(CAN_MODULE_ID, CAN_INSTANCE, 
                CAN_SETCONTROLLERMODE_ID, CAN_E_PARAM_CONTROLLER);
        ret = CAN_NOT_OK;
    }    
    /* @req <CAN0200> */
    else if(Transition > CAN_T_CNT)
    {
        Det_ReportError(CAN_MODULE_ID, CAN_INSTANCE, CAN_SETCONTROLLERMODE_ID, CAN_E_TRANSITION);
        ret = CAN_NOT_OK;
    }
    else
    #endif /* STD_ON == CAN_DEV_ERROR_DETECT */
    {
        switch (Transition)
        {
            case CAN_T_START:
                ret = Can_StartMode(Controller);
                break;
            case CAN_T_STOP:
                ret = Can_StopMode(Controller);
                break;
            case CAN_T_SLEEP:
                ret = Can_SleepMode(Controller);
                break;
            case CAN_T_WAKEUP:
                ret = Can_WakeupMode(Controller);
                break;
            default:
                ret = CAN_NOT_OK;
                break;
        }
    }

    return ret;
}

/******************************************************************************/
/*
 * Brief               <This function disable all interrupt for this controller. >
 * ServiceId           <0x04>
 * Sync/Async          <Synchronous>
 * Reentrancy          <Reentrant>
 * Param-Name[in]      <Controller- CAN controller for which interrupts shall be disabled>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
/*@req <CAN0231>*/
FUNC(void, CAN_CODE) Can_DisableControllerInterrupts(uint8 Controller)
{
    P2VAR(volatile Can_FlexCanRegType, AUTOMATIC, AUTOMATIC) CanRegs;

    /*@req <CAN0205>*/
    /*@req <CAN0206>*/
    #if(STD_ON == CAN_DEV_ERROR_DETECT)
    if(CAN_READY != Can_DriverStatus)
    {
        Det_ReportError(CAN_MODULE_ID, CAN_INSTANCE, 
                CAN_DISABLECONTROLLERINTERRUPTS_ID, CAN_E_UNINIT);
    }
    else if(Controller >= CAN_MAX_CONTROLLERS)
    {
        Det_ReportError(CAN_MODULE_ID, CAN_INSTANCE, 
                CAN_DISABLECONTROLLERINTERRUPTS_ID, CAN_E_PARAM_CONTROLLER);
    }
    else
    #endif /* STD_ON == CAN_DEV_ERROR_DETECT */
    {
        SchM_Enter_Can(CAN_INSTANCE, INTERRUPT_PROTECTION_AREA);

    //    CanRegs = CAN_CNTRL_REG(Controller);
        if(0U == Can_Cntrl[Controller].IntLockCount)
        {
            /* Turn off Tx/Rx/Busoff/Err/Tx warning/Rx warning/Wakeup interrupts */
      //      CanRegs->imask[CAN_IMASK1_INDEX] = 0;
            if(CAN_CNTRL_HOH_NUM(Controller) > CAN_LOW_MB_NUMBER)
            {
     //           CanRegs->imask[CAN_IMASK2_INDEX] = 0;
            }  
    //        CanRegs->ctrl &= ~CAN_CTRL_BOFFMSK;
     //       CanRegs->ctrl &= ~CAN_CTRL_ERRMSK;
     //       CanRegs->ctrl &= ~CAN_CTRL_TWRNMSK;
     //       CanRegs->ctrl &= ~CAN_CTRL_RWRNMSK;
     //       CanRegs->mcr  &= ~CAN_MCR_WAK_MSK;
        }
        Can_Cntrl[Controller].IntLockCount++;

        SchM_Exit_Can(CAN_INSTANCE, INTERRUPT_PROTECTION_AREA);             
    }

    return;
}

/******************************************************************************/
/*
 * Brief               <This function enable all allowed interrupts. >
 * ServiceId           <0x05>
 * Sync/Async          <Synchronous>
 * Reentrancy          <Reentrant>
 * Param-Name[in]      <Controller- CAN controller for which interrupts shall be disabled>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
/* @req <CAN0232> @req <CAN050> */
FUNC(void, CAN_CODE)  Can_EnableControllerInterrupts(uint8 Controller)
{
    /*@req <CAN0209>*/
    /*@req <CAN0210>*/
    #if(STD_ON == CAN_DEV_ERROR_DETECT)
    if(CAN_READY != Can_DriverStatus)
    {
        Det_ReportError(CAN_MODULE_ID, CAN_INSTANCE, 
                CAN_ENABLECONTROLLERINTERRUPTS_ID, CAN_E_UNINIT);
    }
    else if(Controller >= CAN_MAX_CONTROLLERS)
    {
        Det_ReportError(CAN_MODULE_ID, CAN_INSTANCE, 
                CAN_ENABLECONTROLLERINTERRUPTS_ID, CAN_E_PARAM_CONTROLLER);
    }
    else
    #endif /* STD_ON == CAN_DEV_ERROR_DETECT */   
    {
        SchM_Enter_Can(CAN_INSTANCE, INTERRUPT_PROTECTION_AREA);

        /* @req <CAN0209> enable interrupt call before disable interrupt no action */
        if(Can_Cntrl[Controller].IntLockCount > 0U)
        {
            Can_Cntrl[Controller].IntLockCount--;

            if(0U == Can_Cntrl[Controller].IntLockCount)
            {
                /*
                 * recovery software flag to hardware
                 */
                Can_ApplySWF(Controller, Can_Cntrl[Controller].SwIntFlag);
            }
        }

        SchM_Exit_Can(CAN_INSTANCE, INTERRUPT_PROTECTION_AREA);             
    }

    return;
}

/******************************************************************************/
/*
 * Brief               <This function perform HW-Transmit handle transmit. >
 * ServiceId           <0x06>
 * Sync/Async          <Synchronous>
 * Reentrancy          <Reentrant>
 * Param-Name[in]      <Hth-information which HW-transmit handle shall be used for transmit.>
 * Param-Name[in]      <PduInfo-Pointer to SDU user memory,DLC and Identifier>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <Can_ReturnType-Returns CAN_OK,CAN_NOT_OK or CAN_BUSY>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
/* @req <CAN276> @req <CAN0233> @req <CAN0212> @req <CAN0275> */
FUNC(Can_ReturnType, CAN_CODE) Can_Write
(
    uint8 Hth,
    P2CONST(Can_PduType, AUTOMATIC, CAN_APPL_DATA) PduInfo
)
{
    Can_ReturnType ret = CAN_NOT_OK;
    uint8   Controller;
    uint16  HwObjId = 0;
    
    #if(STD_ON == CAN_DEV_ERROR_DETECT)
    boolean errFlag = FALSE;

    if(CAN_READY != Can_DriverStatus)
    {
        Det_ReportError(CAN_MODULE_ID, CAN_INSTANCE, CAN_WRITE_ID, CAN_E_UNINIT);
        errFlag = TRUE;
        ret = CAN_NOT_OK;
    }
    /* check hth is vaild */
    else if((Hth >= CAN_HOH_NUM) 
            || (CAN_OBJECT_TYPE_TRANSMIT != CAN_HWOBJ_CFG(CAN_HWOBJ_ID(Hth)).CanObjectType)) 
    {
        Det_ReportError(CAN_MODULE_ID, CAN_INSTANCE, CAN_WRITE_ID, CAN_E_PARAM_HANDLE);
        errFlag = TRUE;
        ret = CAN_NOT_OK;
    }
    else if((NULL_PTR == PduInfo) || (NULL_PTR == PduInfo->sdu))
    {
        Det_ReportError(CAN_MODULE_ID, CAN_INSTANCE, CAN_WRITE_ID, CAN_E_PARAM_POINTER);
        errFlag = TRUE;
        ret = CAN_NOT_OK;
    }
    else if(CAN_DATA_LENGTH < PduInfo->length)
    {
        Det_ReportError(CAN_MODULE_ID, CAN_INSTANCE, CAN_WRITE_ID, CAN_E_PARAM_DLC);
        errFlag = TRUE;
        ret = CAN_NOT_OK;
    }
    else
    {
        /* do nothing */
    }
    if (FALSE == errFlag)
    #endif /* STD_ON == CAN_DEV_ERROR_DETECT */                    
    {
        HwObjId = CAN_HWOBJ_ID(Hth);
        Controller = CAN_HWOBJ_CFG(HwObjId).CanControllerRef;

        if (CAN_CS_STARTED == Can_Cntrl[Controller].CntrlMode)
        {
            #if(STD_ON == CAN_MULTIPLEXED_TRANSMISSION)
            HwObjId = Can_FindLowPriorityMb(Hth);
            #endif /* STD_ON == CAN_MULTIPLEXED_TRANSMISSION */

            if (TRUE == Can_IsTxMbFree(HwObjId))
            {
                Can_WriteMb(HwObjId, PduInfo);
                ret = CAN_OK;
            }
            else 
            {
                #if(STD_ON == CAN_HW_TRANSMIT_CANCELLATION)
                if (TRUE == Can_PriorityHigher(PduInfo->id, Can_GetMBCanId(HwObjId)))
                {
                    Can_TxCancel(Controller, CAN_MB_ID(HwObjId));
                }
                #endif /* STD_ON == CAN_HW_TRANSMIT_CANCELLATION */

                ret = CAN_BUSY;
            }
        }
    }

    
    return ret;    
}

/******************************************************************************/
/*
 * Brief               <This function checks if a wakeup has occurred for the given controller. >
 * ServiceId           <0x0b>
 * Sync/Async          <Synchronous>
 * Reentrancy          <Non Reentrant>
 * Param-Name[in]      <Controller- Controller to be checked for a wakeup>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <Std_ReturnType- Returns-E_OK or E_NOT_OK>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
FUNC(Std_ReturnType, CAN_CODE) Can_Cbk_CheckWakeup(uint8 Controller)
{
    #if(STD_ON == CAN_WAKEUP_SUPPORT)

    Std_ReturnType ret = E_NOT_OK;
    P2VAR(volatile Can_FlexCanRegType, AUTOMATIC, AUTOMATIC) CanRegs;

     /*@req <CAN362>*/
    #if(STD_ON == CAN_DEV_ERROR_DETECT)
    if(CAN_READY != Can_DriverStatus)
    {
        Det_ReportError(CAN_MODULE_ID, CAN_INSTANCE, CAN_CBK_CHECKWAKEUP_ID, CAN_E_UNINIT);
        ret = E_NOT_OK;
    }
    /*@req <CAN363>*/
    else if(Controller >= CAN_MAX_CONTROLLERS)
    {
        Det_ReportError(CAN_MODULE_ID, CAN_INSTANCE, 
                CAN_CBK_CHECKWAKEUP_ID, CAN_E_PARAM_CONTROLLER);
        ret = E_NOT_OK;
    } 
    else
    #endif /* STD_ON == CAN_DEV_ERROR_DETECT */
    {
        CanRegs = CAN_CNTRL_REG(Controller);

        /* Check Controller Wakeup flag */
        if(CAN_ESR_WAK_INT == (CAN_ESR_WAK_INT & CanRegs->esr))
        {
            ret = E_OK;
        }
    }
    
    return ret;
    #else
    return E_OK;
    #endif /* STD_ON == CAN_WAKEUP_SUPPORT */
}

/******************************************************************************/
/*
 * Brief               <This function performs the polling of TX confirmation and TX cancellation 
 *                          confirmation when.CAN_TX_PROCESSING is set to POLLING. >
 * ServiceId           <0x01>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/

/* @req <CAN0225> @req <CAN031> */
FUNC(void, CAN_CODE) Can_MainFunction_Write(void)
{
    #if (STD_ON == CAN_TX_POLLING)
    uint8 Controller;

    /*@req <CAN179>*/
    #if(STD_ON == CAN_DEV_ERROR_DETECT)
    /*@req <CAN187>*/
    if(CAN_READY != Can_DriverStatus)
    {
        Det_ReportError(CAN_MODULE_ID, CAN_INSTANCE, CAN_MAINFUCTION_WRITE_ID, CAN_E_UNINIT);
    }
    else
    #endif /* STD_ON == CAN_DEV_ERROR_DETECT */
    {
        /* loop each Controller TX comfirmation and TX cancel comfirmation */
        for (Controller = 0; Controller < CAN_MAX_CONTROLLERS; Controller++)
        {
            /*@req <CAN178>*/
            if((CAN_PROCESS_TYPE_POLLING == CAN_CNTRL_PCCFG(Controller).CanTxProcessing)
                    && (CAN_CS_STARTED == Can_Cntrl[Controller].CntrlMode))
            {
                Can_TxProcess(Controller);
            }
        }
    }
    #endif /* STD_ON == CAN_TX_POLLING */

    return;
}

/******************************************************************************/
/*
 * Brief               <This function performs the polling of RX indications when 
 *                          CAN_RX_PROCESSING is set to POLLING.> 
 * ServiceId           <0x08>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
 /*@req <CAN012>
[heguarantee that neither the ISRs 
 nor the function Can_MainFunction_Read can be interrupted by itself. ]
*/
/* @req <CAN226> @req <CAN108> @req <CAN180> */
FUNC(void, CAN_CODE) Can_MainFunction_Read(void)
{
    #if (STD_ON == CAN_RX_POLLING)
    uint8 Controller;

    /*@req <CAN181>*/
    #if(STD_ON == CAN_DEV_ERROR_DETECT)
    /*@req <CAN187>*/
    if(CAN_READY != Can_DriverStatus)
    {
        Det_ReportError(CAN_MODULE_ID, CAN_INSTANCE, CAN_MAINFUNCTION_READ_ID, CAN_E_UNINIT);
    }
    else
    #endif /* STD_ON == CAN_DEV_ERROR_DETECT */    
    {
        /* scan each Controller */
        for (Controller = 0; Controller < CAN_MAX_CONTROLLERS; Controller++)
        {
            if((CAN_PROCESS_TYPE_POLLING == CAN_CNTRL_PCCFG(Controller).CanRxProcessing)
                    && (CAN_CS_STARTED == Can_Cntrl[Controller].CntrlMode))
            {
                Can_RxProcess(Controller);
            }
        }        
    }
    #endif /* STD_ON == CAN_RX_POLLING */

    return;
}

/******************************************************************************/
/*
 * Brief               <This function performs the polling of bus-off events that are configured
 *                          statically as "to be polled".> 
 * ServiceId           <0x09>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
FUNC(void, CAN_CODE) Can_MainFunction_BusOff(void)
{
    #if (STD_ON == CAN_BUSOFF_POLLING)
    P2VAR(volatile Can_FlexCanRegType, AUTOMATIC, AUTOMATIC) CanRegs;
    uint8 Controller;

    #if(STD_ON == CAN_DEV_ERROR_DETECT)
    if(CAN_READY != Can_DriverStatus)  /* @req <CAN184> */
    {
        Det_ReportError(CAN_MODULE_ID, CAN_INSTANCE, CAN_MAINFUNCTION_BUSOFF_ID, CAN_E_UNINIT);
    }
    else
    #endif /* STD_ON == CAN_DEV_ERROR_DETECT */  
    {
        for (Controller = 0; Controller < CAN_MAX_CONTROLLERS; Controller++)
        {
            if((CAN_PROCESS_TYPE_POLLING == CAN_CNTRL_PCCFG(Controller).CanBusOffProcessing)
                    && (CAN_CS_STARTED == Can_Cntrl[Controller].CntrlMode))
            {
                CanRegs = CAN_CNTRL_REG(Controller);
                if(CAN_ESR_BOFFINT == (CAN_ESR_BOFFINT & CanRegs->esr))
                {
                    Can_BusOff_Handler(Controller);
                }
            }
        }       
    }
    #endif /* STD_ON == CAN_BUSOFF_POLLING */

    return;
}

/******************************************************************************/
/*
 * Brief               <This function performs the polling of wake-up events that are configured
 *                          statically as "to be polled".> 
 * ServiceId           <0x0a>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
FUNC(void, CAN_CODE) Can_MainFunction_Wakeup(void)
{
    #if((STD_ON == CAN_WAKEUP_POLLING) && (STD_ON == CAN_WAKEUP_SUPPORT))
    P2VAR(volatile Can_FlexCanRegType, AUTOMATIC, AUTOMATIC) CanRegs;
    uint8 Controller;

    #if(STD_ON == CAN_DEV_ERROR_DETECT)
    /*@req <CAN184>*/
    if(CAN_READY != Can_DriverStatus)
    {
        Det_ReportError(CAN_MODULE_ID, CAN_INSTANCE, CAN_MAINFUNCTION_WAKEUP_ID, CAN_E_UNINIT);
    }
    else
    #endif /* STD_ON == CAN_DEV_ERROR_DETECT */    
    {
        for (Controller = 0; Controller < CAN_MAX_CONTROLLERS; Controller++)
        {
            if((CAN_PROCESS_TYPE_POLLING == CAN_CNTRL_PCCFG(Controller).CanWakeupProcessing)
                    && (CAN_CS_SLEEP == Can_Cntrl[Controller].CntrlMode))
            {
                CanRegs = CAN_CNTRL_REG(Controller);
                if(CAN_ESR_WAK_INT == (CAN_ESR_WAK_INT & CanRegs->esr))
                {
                    Can_Wakeup_Handler(Controller);
                }
            }
        }        
    }
    #endif /* (STD_ON == CAN_WAKEUP_POLLING) && (STD_ON == CAN_WAKEUP_SUPPORT) */

    return;
}
void FlexCanRxTxIntruptcallback(uint8_t instance,
                flexcan_event_type_t eventType,
                uint32_t buffIdx,
                struct FlexCANState *driverState)
{
	//state->callback(instance, FLEXCAN_EVENT_RX_COMPLETE, mb_idx, state);
	//state->callback(instance, FLEXCAN_EVENT_TX_COMPLETE, mb_idx, state);
	uint8 Controller;

	Controller=instance;


	if(eventType==FLEXCAN_EVENT_RX_COMPLETE)
	{
        uint8  tempData[8],i;
        Can_PduType pdu;
       // driverState->mbs[mb_idx].mb_message
        /* Get pdu */
        Controller=instance+1;
        for(i=0;i<8;i++)
        {
        	tempData[i]=driverState->mbs[buffIdx].mb_message->data[i];
        }
        pdu.length=8;
        pdu.id=driverState->mbs[buffIdx].mb_message->msgId;
        pdu.sdu = tempData;///* mailbox ID to hardware object ID */
      //  Can_GetMBInfo(CAN_MBID_TO_HWOBJ(Controller, buffIdx), &pdu);

        CanIf_RxIndication(CAN_HWOBJ_CFG(CAN_MBID_TO_HWOBJ(Controller,buffIdx)).CanObjectId,
        		pdu.id, pdu.length, pdu.sdu);
	}

	if(eventType==FLEXCAN_EVENT_TX_COMPLETE)
	{
		 CanIf_TxConfirmation(Can_Cntrl[Controller].TxPduHandles[buffIdx]);
	}

}
/*=======[I N T E R N A L  F U N C T I O N   I M P L E M E N T A T I O N S]====================*/
/******************************************************************************/
/*
 * Brief               <This function performs Tx/Rx Interrupt Handle>
 * Param-Name[in]      <Controller - CAN controller to be error process>
 * Param-Name[in]      <startMbId  - start mailbox id>
 * Param-Name[in]      <endMbId    - end mailbox id>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/

FUNC(void, CAN_CODE) Can_RxTxInt_Handler(uint8 Controller, uint8 startMbId, uint8 endMbId,Can_PduType ppdu)
{
    P2VAR(volatile Can_FlexCanRegType, AUTOMATIC, AUTOMATIC) CanRegs;
    uint16 mbId = 0;

    CanRegs = CAN_CNTRL_REG(Controller);

    for (mbId = startMbId; mbId <= endMbId; mbId++)
    {
    
     //   if((CanRegs->iflag[1- (mbId >> 5)] & ((uint32)0x1 << (mbId & 0x1F))) != 0)
        {
     //       CanRegs->iflag[1-(mbId >> 5)] = ((uint32)0x1 << (mbId & 0x1F));

            #if(STD_ON == CAN_HW_TRANSMIT_CANCELLATION)
            /* Abort Acknowledge */
            if((CAN_MBCS_CODETX|CAN_MBCS_CODETXABORT) == (CanRegs->mb[mbId].cs&CAN_MBCS_CODE))
            {
                uint8  tempData[8];
                Can_PduType pdu;

                pdu.sdu = tempData;
                Can_GetMBInfo(CAN_MBID_TO_HWOBJ(Controller, mbId), &pdu);
                pdu.swPduHandle = Can_Cntrl[Controller].TxPduHandles[mbId];
                CanIf_CancelTxConfirmation(&pdu);

                /* clear abort flag */
                CanRegs->mb[mbId].cs &= ~CAN_MBCS_CODETXABORT;
            }
            else 
            #endif /* STD_ON == CAN_HW_TRANSMIT_CANCELLATION */
            {
                /* Transmit Acknowledge */
                if(CAN_MBCS_CODETX == (CanRegs->mb[mbId].cs & CAN_MBCS_CODE))
                {
                    CanIf_TxConfirmation(Can_Cntrl[Controller].TxPduHandles[mbId]);

                    #if ((STD_ON==CAN_HW_TRANSMIT_CANCELLATION)&&(STD_ON==CAN_MULTIPLEXED_TRANSMISSION))
                    Can_CheckAbortMb(Controller, CAN_HWOBJ_CFG(CAN_MBID_TO_HWOBJ(Controller,mbId)).CanObjectId);
                    #endif /* (STD_ON==CAN_HW_TRANSMIT_CANCELLATION)&&(STD_ON==CAN_MULTIPLEXED_TRANSMISSION) */
                }
                else  /* Receive Acknowledge */
                {
                    uint8  tempData[8];
                    Can_PduType pdu;

                    /* Get pdu */
                    pdu.sdu = tempData;///* mailbox ID to hardware object ID */
                    Can_GetMBInfo(CAN_MBID_TO_HWOBJ(Controller, mbId), &pdu);

                    CanIf_RxIndication(CAN_HWOBJ_CFG(CAN_MBID_TO_HWOBJ(Controller,mbId)).CanObjectId, 
                    		pdu.id, pdu.length, pdu.sdu);
                }
            }
        }
    }    
    return;
}

/******************************************************************************/
/*
 * Brief               <This function performs bus-off  process>
 * Param-Name[in]      <Controller- CAN controller to be Tw/Rw/bus-off process>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
FUNC(void, CAN_CODE) Can_TwRwBusOff_Handler(uint8 Controller)
{
    P2VAR(volatile Can_FlexCanRegType, AUTOMATIC, AUTOMATIC) CanRegs;
    Can_ReturnType ret_val = CAN_NOT_OK;

  //  CanRegs = CAN_CNTRL_REG(Controller);

    /* busoff */
    if (CAN_ESR_BOFFINT == (CanRegs->esr & CAN_ESR_BOFFINT))
    {
        Can_BusOff_Handler(Controller);
    }
    /* Tx warning */
    else if (CAN_ESR_TWRNINT == (CanRegs->esr & CAN_ESR_TWRNINT))
    {
   //     CanRegs->esr = CAN_ESR_TWRNINT;
    }
    /* Rx warning */
    else if (CAN_ESR_RWRNINT == (CanRegs->esr & CAN_ESR_RWRNINT))
    {
  //      CanRegs->esr = CAN_ESR_RWRNINT;
    }
    else
    {
        /* do nothing */
    }

    return;
}

/******************************************************************************/
/*
 * Brief               <This function performs wake up  process>
 * Param-Name[in]      <Controller- CAN controller to be wake up process>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
FUNC(void, CAN_CODE) Can_Wakeup_Handler(uint8 Controller)
{
    #if(STD_ON == CAN_WAKEUP_SUPPORT)
    P2VAR(volatile Can_FlexCanRegType, AUTOMATIC, AUTOMATIC) CanRegs;
    Can_ReturnType ret_val = CAN_NOT_OK;

    /* Transition the controller to freeze mode */
    ret_val = Can_WakeupMode(Controller); 
    if(CAN_OK == ret_val)
    {
    //    CanRegs = CAN_CNTRL_REG(Controller);

        /* @req CAN271 */
        EcuM_CheckWakeup(CAN_CNTRL_PCCFG(Controller).CanWakeupSourceRef); 

    //    CanRegs->esr = CAN_ESR_WAK_INT;    /* Clear the Wakeup flag */
    }
    #endif /* #if(STD_ON == CAN_WAKEUP_SUPPORT) */

    return;
}

/******************************************************************************/
/*
 * Brief               <This function performs error  process>
 * Param-Name[in]      <Controller- CAN controller to be error process>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
FUNC(void, CAN_CODE) Can_Error_Handler(uint8 Controller)
{
    P2VAR(volatile Can_FlexCanRegType, AUTOMATIC, AUTOMATIC) CanRegs;

 //   CanRegs = CAN_CNTRL_REG(Controller);

    /* Clear the Error flag */
 //   CanRegs->esr = CAN_ESR_ERRINT;
//
    /* user add ... */

    return;
}

/******************************************************************************/
/*
 * Brief               <This function performs bus-off  process>
 * Param-Name[in]      <Controller- CAN controller to be Tw/Rw/bus-off process>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
STATIC FUNC(void, CAN_CODE) Can_BusOff_Handler(uint8 Controller)
{
    P2VAR(volatile Can_FlexCanRegType, AUTOMATIC, AUTOMATIC) CanRegs;

    CanRegs = CAN_CNTRL_REG(Controller);

    CanRegs->esr = CAN_ESR_BOFFINT;  

    if(CAN_OK == Can_StopMode(Controller))
    {
        /* request busoff recovery */
      //  CanRegs->ctrl &= ~CAN_CTRL_BOFFMSK;
        /* Disable automatic recovery */
    //    CanRegs->ctrl |= CAN_CTRL_BOFFMSK;

        CanIf_ControllerBusOff(Controller);
    }

    return;
}

#if (STD_ON == CAN_TX_POLLING)
/******************************************************************************/
/*
 * Brief               <This function performs Tx confirmation and Tx cancellation process>
 * Param-Name[in]      <Controller- CAN controller to be Tx process>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
STATIC FUNC(void, CAN_CODE) Can_TxProcess(uint8 Controller)
{
    P2VAR(volatile Can_FlexCanRegType, AUTOMATIC, AUTOMATIC) CanRegs;
    uint16 HwObjId    = 0;
    uint16 EndHwObjId = 0;
    uint16 mbId = 0;

    CanRegs = CAN_CNTRL_REG(Controller);
 
    EndHwObjId = (uint16)(CAN_CNTRL_CFG(Controller).CanTxHwObjFirst
                          + CAN_CNTRL_CFG(Controller).CanTxHwObjCount);
    for (HwObjId = CAN_CNTRL_CFG(Controller).CanTxHwObjFirst; HwObjId < EndHwObjId; HwObjId++)
    {
        mbId = CAN_MB_ID(HwObjId);
        if((CanRegs->iflag[1-(mbId>>5)] & ((uint32)0x1<<(mbId&0x1F))) != 0)
        {
            CanRegs->iflag[1-(mbId>>5)] = ((uint32)0x1<<(mbId&0x1F));

            #if(STD_ON == CAN_HW_TRANSMIT_CANCELLATION)
            /* Abort Acknowledge */
            if((CAN_MBCS_CODETX|CAN_MBCS_CODETXABORT) == (CanRegs->mb[mbId].cs&CAN_MBCS_CODE))
            {
                uint8  RxData[8];
                Can_PduType pdu;

                pdu.sdu = RxData;
                Can_GetMBInfo(HwObjId, &pdu);
                pdu.swPduHandle = Can_Cntrl[Controller].TxPduHandles[mbId];

                /* clear abort flag */
                CanRegs->mb[mbId].cs &= ~CAN_MBCS_CODETXABORT;

                CanIf_CancelTxConfirmation(&pdu);
            }
            else 
            #endif /* STD_ON == CAN_HW_TRANSMIT_CANCELLATION */
            {
                /* Transmit Acknowledge */
                if(CAN_MBCS_CODETX == (CanRegs->mb[mbId].cs & CAN_MBCS_CODE))
                {
                    CanIf_TxConfirmation(Can_Cntrl[Controller].TxPduHandles[mbId]);
                }

                #if ((STD_ON==CAN_HW_TRANSMIT_CANCELLATION)&&(STD_ON==CAN_MULTIPLEXED_TRANSMISSION))
                Can_CheckAbortMb(Controller, CAN_HWOBJ_CFG(HwObjId).CanObjectId);
                #endif /* (STD_ON==CAN_HW_TRANSMIT_CANCELLATION)&&(STD_ON==CAN_MULTIPLEXED_TRANSMISSION) */
            }
        }
    }

    return;
}
#endif /* STD_ON == CAN_TX_POLLING */

#if (STD_ON == CAN_RX_POLLING)
/******************************************************************************/
/*
 * Brief               <This function performs Rx indications  process>
 * Param-Name[in]      <Controller- CAN controller to be Rx process>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
STATIC FUNC(void, CAN_CODE) Can_RxProcess(uint8 Controller)
{
    P2VAR(volatile Can_FlexCanRegType, AUTOMATIC, AUTOMATIC) CanRegs;
    uint16 HwObjId    = 0;
    uint16 EndHwObjId = 0;
    uint16 mbId = 0;
    Can_PduType pdu;
    uint8  RxData[8];

    CanRegs = CAN_CNTRL_REG(Controller);

    EndHwObjId = (uint16)(CAN_CNTRL_CFG(Controller).CanRxHwObjFirst
                          + CAN_CNTRL_CFG(Controller).CanRxHwObjCount);
    for (HwObjId = CAN_CNTRL_CFG(Controller).CanRxHwObjFirst; HwObjId < EndHwObjId; HwObjId++) 
    {
        mbId = CAN_MB_ID(HwObjId);

        /* Check flag the curent mb */
        if((CanRegs->iflag[1-(mbId>>5)] & ((uint32)0x01 << (mbId&0x1F))) != 0)
        {
            /* clear IFALG */
            CanRegs->iflag[1-(mbId>>5)] = ((uint32)1 << (mbId&0x1F));

            /* Get pdu */
            pdu.sdu = RxData;
            Can_GetMBInfo(HwObjId, &pdu);

            CanIf_RxIndication(CAN_HWOBJ_CFG(HwObjId).CanObjectId, pdu.id, pdu.length, pdu.sdu);
        }/*End of if*/
    }

    return;
}
#endif /* STD_ON == CAN_RX_POLLING */

#if(STD_ON == CAN_HW_TRANSMIT_CANCELLATION)
/******************************************************************************/
/*
 * Brief               <This function performs Tx cancellation process>
 * Param-Name[in]      <Controller- CAN controller to be Tx process>
 * Param-Name[in]      <mbId- CAN controller message buffer index>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
STATIC FUNC(void, CAN_CODE) Can_TxCancel(uint8 Controller, uint32 mbId)
{
    P2VAR(volatile Can_FlexCanRegType, AUTOMATIC, AUTOMATIC) CanRegs;

    CanRegs = CAN_CNTRL_REG(Controller);

    if((CAN_MBCS_CODETX|CAN_MBCS_CODETXEN) == (CanRegs->mb[mbId].cs & CAN_MBCS_CODE))
    {
        CanRegs->mb[mbId].cs = (CanRegs->mb[mbId].cs & ~CAN_MBCS_CODE) \
                                   | CAN_MBCS_CODETX | CAN_MBCS_CODETXABORT;
    }

    return;
}
#endif /* STD_ON == CAN_HW_TRANSMIT_CANCELLATION */

/******************************************************************************/
/*
 * Brief               <This function performs controller start Mode  process>
 * Param-Name[in]      <Controller- CAN controller to be Mode process>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
/* @req <CAN261> */
STATIC FUNC(Can_ReturnType, CAN_CODE) Can_StartMode(uint8 Controller)
{
    P2VAR(volatile Can_FlexCanRegType, AUTOMATIC, AUTOMATIC) CanRegs;
    Can_ReturnType Ret = CAN_NOT_OK;
    uint16 Timeout  = 0;

    if (CAN_CS_STARTED == Can_Cntrl[Controller].CntrlMode)
    {
        Ret = CAN_OK;
    }
    else if(CAN_CS_STOPPED == Can_Cntrl[Controller].CntrlMode)  
    {

        if(10 == Timeout)
        {
            Dem_ReportErrorStatus(CAN_E_TIMEOUT, DEM_EVENT_STATUS_FAILED);
            Ret = CAN_NOT_OK;
        }                
        else
        {
            /* 
             * turn off Rx/Tx/Busoff/Err/Tx Warning/Rx Warning interrupt 
             */
            if (CAN_PROCESS_TYPE_INTERRUPT == CAN_CNTRL_PCCFG(Controller).CanRxProcessing)
            {
                /* Turn on the interrupt mailboxes */
                Can_Cntrl[Controller].SwIntFlag |= SWF_RX_MASK;
            }
            if (CAN_PROCESS_TYPE_INTERRUPT == CAN_CNTRL_PCCFG(Controller).CanTxProcessing)
            {
                /* Turn on the interrupt mailboxes */
                Can_Cntrl[Controller].SwIntFlag |= SWF_TX_MASK;
            }
            if (CAN_PROCESS_TYPE_INTERRUPT == CAN_CNTRL_PCCFG(Controller).CanBusOffProcessing)
            {
                /* BusOff here represents all errors and warnings*/
                Can_Cntrl[Controller].SwIntFlag |= SWF_BOFF_MASK;
            }
            #if(STD_ON == CAN_WAKEUP_SUPPORT)
            if (CAN_PROCESS_TYPE_INTERRUPT == CAN_CNTRL_PCCFG(Controller).CanWakeupProcessing)
            {
                /* Enable wake up interrupt */
                Can_Cntrl[Controller].SwIntFlag |= SWF_WAK_MASK;
            }
            #endif /* #if(STD_ON == CAN_WAKEUP_SUPPORT) */

            /* apply software int flag to hardware */
            if (0 == Can_Cntrl[Controller].IntLockCount)
            {
                Can_ApplySWF(Controller, Can_Cntrl[Controller].SwIntFlag);
            }
            
            Can_Cntrl[Controller].CntrlMode = CAN_CS_STARTED;
            
            Ret = CAN_OK;
        }

    }
    else
    {
        #if(STD_ON == CAN_DEV_ERROR_DETECT)
        Det_ReportError(CAN_MODULE_ID, CAN_INSTANCE, CAN_SETCONTROLLERMODE_ID, CAN_E_TRANSITION);
        #endif
        Ret = CAN_NOT_OK;
    }

    return Ret;
}

/******************************************************************************/
/*
 * Brief               <This function performs controller stop Mode  process>
 * Param-Name[in]      <Controller- CAN controller to be Mode process>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
/* @req <CAN283> */
STATIC FUNC(Can_ReturnType, CAN_CODE) Can_StopMode(uint8 Controller)
{
    P2VAR(volatile Can_FlexCanRegType, AUTOMATIC, AUTOMATIC) CanRegs;
    Can_ReturnType Ret = CAN_NOT_OK;
    uint16 Timeout  = 0;
    uint16 HwObjId    = 0;
    uint16 EndHwObjId = 0;
    uint16 mbId = 0;

    if (CAN_CS_STOPPED == Can_Cntrl[Controller].CntrlMode)
    {
        Ret = CAN_OK;
    }
    else if(CAN_CS_STARTED == Can_Cntrl[Controller].CntrlMode)  
    {
 //       CanRegs = CAN_CNTRL_REG(Controller);

        /* goto freeze mode */
        Timeout = CAN_TIMEOUT_DURATION;
 //       CanRegs->mcr |= CAN_MCR_FRZ|CAN_MCR_HALT;

 //       while((Timeout > 0) && (CAN_MCR_FRZACK != (CanRegs->mcr & CAN_MCR_FRZACK)))
 //       {
 //           Timeout--;
 //       }
        if(0 == Timeout)
        {
            /* report product error  */
            Dem_ReportErrorStatus(CAN_E_TIMEOUT, DEM_EVENT_STATUS_FAILED);
            Ret = CAN_NOT_OK;
        } 
        else
        {
            /* 
             * turn off Rx/Tx/Busoff/Err/Tx Warning/Rx Warning/wakeup interrupt 
             */
            /* change software flag */
            Can_Cntrl[Controller].SwIntFlag &= ~SWF_RX_MASK;
            Can_Cntrl[Controller].SwIntFlag &= ~SWF_TX_MASK;
            Can_Cntrl[Controller].SwIntFlag &= ~SWF_BOFF_MASK;
            Can_Cntrl[Controller].SwIntFlag &= ~SWF_ERR_MASK;
            Can_Cntrl[Controller].SwIntFlag &= ~SWF_TWRN_MASK;
            Can_Cntrl[Controller].SwIntFlag &= ~SWF_RWRN_MASK;
            Can_Cntrl[Controller].SwIntFlag &= ~SWF_WAK_MASK;

            /* apply software flag to hardware */
            if (0 == Can_Cntrl[Controller].IntLockCount)
            {
                CanRegs->imask[CAN_IMASK1_INDEX] = 0;
                if(CAN_CNTRL_HOH_NUM(Controller) > CAN_LOW_MB_NUMBER)
                {
                    CanRegs->imask[CAN_IMASK2_INDEX] = 0;
                }  
         //       CanRegs->ctrl &= ~CAN_CTRL_BOFFMSK;
         //       CanRegs->ctrl &= ~CAN_CTRL_ERRMSK;
         //       CanRegs->ctrl &= ~CAN_CTRL_TWRNMSK;
         //       CanRegs->ctrl &= ~CAN_CTRL_RWRNMSK;
        //        CanRegs->mcr  &= ~CAN_MCR_WAK_MSK;
            }
            /* Clear Tx buffers */
            EndHwObjId = (uint16)(CAN_CNTRL_CFG(Controller).CanTxHwObjFirst
                          + CAN_CNTRL_CFG(Controller).CanTxHwObjCount);
            for (HwObjId = CAN_CNTRL_CFG(Controller).CanTxHwObjFirst; HwObjId < EndHwObjId; HwObjId++)
            {
                mbId = CAN_MB_ID(HwObjId);
            //    CanRegs->mb[mbId].cs &= CAN_MBCS_CODETXINACTIVE;
            }
            
            /* Clear Rx buffers */
            EndHwObjId = (uint16)(CAN_CNTRL_CFG(Controller).CanRxHwObjFirst
                          + CAN_CNTRL_CFG(Controller).CanRxHwObjCount);
            for (HwObjId = CAN_CNTRL_CFG(Controller).CanRxHwObjFirst; HwObjId < EndHwObjId; HwObjId++)
            {
                mbId = CAN_MB_ID(HwObjId);
            //    CanRegs->mb[mbId].cs &= CAN_MBCS_CODERXINACTIVE;
            //    CanRegs->mb[mbId].cs |= CAN_MBCS_CODERXEMPTY | CAN_MBCS_SRR;
            }

            /* clear iflag of all buffers */
            CanRegs->iflag[CAN_IMASK1_INDEX] = 0xFFFFFFFFU;
            if(CAN_CNTRL_HOH_NUM(Controller) > CAN_LOW_MB_NUMBER)
            {
               // CanRegs->iflag[CAN_IMASK2_INDEX] = 0xFFFFFFFFU;
            }  

            Can_Cntrl[Controller].CntrlMode = CAN_CS_STOPPED;
            Ret = CAN_OK;
        }
    }
    else
    {
        #if(STD_ON == CAN_DEV_ERROR_DETECT)
        Det_ReportError(CAN_MODULE_ID, CAN_INSTANCE, CAN_SETCONTROLLERMODE_ID, CAN_E_TRANSITION);
        #endif
        Ret = CAN_NOT_OK;
    } 

    return Ret;
}

/******************************************************************************/
/*
 * Brief               <This function performs controller sleep Mode  process>
 * Param-Name[in]      <Controller- CAN controller to be Mode process>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
/* @req <CAN265> */
STATIC FUNC(Can_ReturnType, CAN_CODE) Can_SleepMode(uint8 Controller)
{
    Can_ReturnType Ret = CAN_NOT_OK;

    /* Don't Support sleep & wakeup in MPC5602D */
    if (CAN_CS_SLEEP == Can_Cntrl[Controller].CntrlMode)
    {
        Ret = CAN_OK;
    }
    else if(CAN_CS_STOPPED == Can_Cntrl[Controller].CntrlMode)  
    {
        #if (CAN_WAKEUP_SUPPORT == STD_ON)
        P2VAR(volatile Can_FlexCanRegType, AUTOMATIC, AUTOMATIC) CanRegs;
        uint32 HaltValue = 0;
        uint16 Timeout  = 0;

        CanRegs = CAN_CNTRL_REG(Controller);
        HaltValue = CAN_CNTRL_PCCFG(Controller).SiuHltStp;

        /* Enable Wakeup Interrupt */
        if (CAN_PROCESS_TYPE_INTERRUPT == CAN_CNTRL_PCCFG(Controller).CanWakeupProcessing)
        {
            /* Enable wake up interrupt */
            Can_Cntrl[Controller].SwIntFlag |= SWF_WAK_MASK;
        }

        /* apply software int flag to hardware */
        if (0 == Can_Cntrl[Controller].IntLockCount)
        {
            Can_ApplySWF(Controller, Can_Cntrl[Controller].SwIntFlag);
        }

        Timeout = CAN_TIMEOUT_DURATION;
        SIU_HLT = HaltValue;
        while((Timeout > 0) && (1 == (SIU_HLTACK & HaltValue)))
        {
            Timeout--;
        }
        if(Timeout == 0)
        {
            /* Disable Wakeup Interrupt */
            Can_Cntrl[Controller].SwIntFlag &= ~SWF_WAK_MASK;
            if (0 == Can_Cntrl[Controller].IntLockCount)
            {
                CanRegs->mcr &= ~CAN_MCR_WAK_MSK;
            }

            Dem_ReportErrorStatus(CAN_E_TIMEOUT, DEM_EVENT_STATUS_FAILED);
            Ret = CAN_NOT_OK;
        }
        else
        {
            Can_Cntrl[Controller].CntrlMode = CAN_CS_SLEEP;
            Ret = CAN_OK;
        }
        #else
        Can_Cntrl[Controller].CntrlMode = CAN_CS_SLEEP;
        Ret = CAN_OK;
        #endif
    }
    else
    {
        #if(STD_ON == CAN_DEV_ERROR_DETECT)
        Det_ReportError(CAN_MODULE_ID, CAN_INSTANCE, CAN_SETCONTROLLERMODE_ID, CAN_E_TRANSITION);
        #endif
        Ret = CAN_NOT_OK;
    }

    return Ret;
}


/******************************************************************************/
/*
 * Brief               <This function performs controller wakeup Mode  process>
 * Param-Name[in]      <Controller- CAN controller to be Mode process>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
STATIC FUNC(Can_ReturnType, CAN_CODE) Can_WakeupMode(uint8 Controller)
{
    Can_ReturnType Ret = CAN_NOT_OK;
    
    /*  Don't Support Sleep & Wakeup in MPC5602D */
    if (CAN_CS_STOPPED == Can_Cntrl[Controller].CntrlMode)
    {
        Ret = CAN_OK;
    }
    else if(CAN_CS_SLEEP == Can_Cntrl[Controller].CntrlMode)  
    {
        #if (STD_ON == CAN_WAKEUP_SUPPORT)
        P2VAR(volatile Can_FlexCanRegType, AUTOMATIC, AUTOMATIC) CanRegs;
        uint32 HaltValue = 0;
        uint16 Timeout  = 0;

        CanRegs = CAN_CNTRL_REG(Controller);
        HaltValue = CAN_CNTRL_PCCFG(Controller).SiuHltStp;

        Timeout = CAN_TIMEOUT_DURATION;
        SIU_HLT &= ~HaltValue;
        while((Timeout > 0) && (1 == (SIU_HLTACK & HaltValue)))
        {
            Timeout--;
        }
        if(Timeout == 0)
        {
            Dem_ReportErrorStatus(CAN_E_TIMEOUT, DEM_EVENT_STATUS_FAILED);
            Ret = CAN_NOT_OK;
        }
        else
        {
            /* Disable Wakeup Interrupt */
            Can_Cntrl[Controller].SwIntFlag &= ~SWF_WAK_MASK;
            if (0 == Can_Cntrl[Controller].IntLockCount)
            {
                CanRegs->mcr &= ~CAN_MCR_WAK_MSK;
            }

            Can_Cntrl[Controller].CntrlMode = CAN_CS_STOPPED;
            Ret = CAN_OK;
        }
        #else
        Can_Cntrl[Controller].CntrlMode = CAN_CS_STOPPED;
        Ret = CAN_OK;
        #endif

    }
    else
    {
        #if(STD_ON == CAN_DEV_ERROR_DETECT)
        Det_ReportError(CAN_MODULE_ID, CAN_INSTANCE, CAN_SETCONTROLLERMODE_ID, CAN_E_TRANSITION);
        #endif
        Ret = CAN_NOT_OK;                
    }

    return Ret;
}


/******************************************************************************/
/*
 * Brief               <This function performs controller wakeup Mode  process>
 * Param-Name[in]      <Controller- CAN controller to be Mode process>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/

STATIC FUNC(void, CAN_CODE) Can_InitHwCntrl
(
    uint8 Controller, 
    P2CONST(Can_ControllerConfigType, AUTOMATIC, CAN_CONST_PBCFG) Config
)
{
    P2VAR(volatile Can_FlexCanRegType, AUTOMATIC, AUTOMATIC) CanRegs;
    uint16  timeout = 0,i;


    
 
 /*  	
    CanRegs->mcr = (~CAN_MCR_MDIS) | CAN_MCR_SOFTRST;
    timeout = CAN_TIMEOUT_DURATION;
    while ((timeout > 0) && (CAN_MCR_SOFTRST == (CanRegs->mcr & CAN_MCR_SOFTRST)))
    {
        timeout--;
    }
    if (0 == timeout)
    {
       Dem_ReportErrorStatus(CAN_E_TIMEOUT, DEM_EVENT_STATUS_FAILED);
    }

   
    CanRegs->mcr  |= CAN_MCR_MDIS;
    if (CAN_CLOCKSRC_TYPE_OSC == CAN_CNTRL_PCCFG(Controller).CanClockSource)
    {
        CanRegs->ctrl &= (~CAN_CTRL_CLKSRC);  
    }
    else   
    {
        CanRegs->ctrl |= CAN_CTRL_CLKSRC;  
    }
    CanRegs->mcr  &= (~CAN_MCR_MDIS);

    
    CanRegs->mcr = CAN_MCR_FRZ | CAN_MCR_HALT;
    timeout = CAN_TIMEOUT_DURATION;
    while ((timeout > 0) && (CAN_MCR_FRZACK != (CanRegs->mcr & CAN_MCR_FRZACK)))
    {
        timeout--;
    }
    if (0 == timeout)
    {
       Dem_ReportErrorStatus(CAN_E_TIMEOUT, DEM_EVENT_STATUS_FAILED);
    }


    CanRegs->mcr |= CAN_MCR_SRXDIS 
                    #if (CAN_WAKEUP_SUPPORT == STD_ON)
                    | CAN_MCR_DOZE
                    | CAN_MCR_SLF_WAK
                    #endif
                    | CAN_MCR_BCC 
                    #if(STD_ON == CAN_HW_TRANSMIT_CANCELLATION)
                    | CAN_MCR_AEN 
                    #endif ;
                    | (Config->CanRxHwObjCount + Config->CanTxHwObjCount);

   
 //   CanRegs->ctrl |= Config->CanCtrlValue;

    
  //  CanRegs->ctrl | = (0 | FLEXCAN_CTRL_PROPSEG(7) | FLEXCAN_CTRL_RJW(3) | FLEXCAN_CTRL_PSEG1(7)| FLEXCAN_CTRL_PSEG2(7) | FLEXCAN_CTRL_PRESDIV(7));   
    CanRegs->ctrl = (0 | FLEXCAN_CTRL_PROPSEG(7)
					| FLEXCAN_CTRL_RJW(3) | FLEXCAN_CTRL_PSEG1(7)
					| FLEXCAN_CTRL_PSEG2(7)
					| FLEXCAN_CTRL_PRESDIV(7));
    
    CanRegs->ctrl |= CAN_CTRL_BOFFMSK;

 */
    if(Controller==1)//先就测试单个CAN通道
    {
    	Controller=0;
        Can_InitMB(Controller);
    }

    return;

}

/******************************************************************************/
/*
 * Brief               <This function performs controller init message buffer  process>
 * Param-Name[in]      <Controller- CAN controller to be Mode process>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
STATIC FUNC(void, CAN_CODE) Can_InitMB(uint8 Controller)
{

	 uint16 HwObjId   = 0;
	 uint16 endHwObj  = 0;
	 uint16 mbId      = 0;
	 uint16 RxFirst=0;
	 uint16  RxCount=0;
	    
   // P2VAR(volatile Can_FlexCanRegType, AUTOMATIC, AUTOMATIC) CanRegs;
   // CanRegs = CAN_CNTRL_REG(Controller);
    

    flexcan_data_info_t dataInfo =
    {
            .data_length = 8U,
           // .msg_id_type = FLEXCAN_MSG_ID_STD,
			.msg_id_type =CAN_ID_TYPE_STANDARD,
            .enable_brs  = false,
            .fd_enable   = false,
            .fd_padding  = 0U
    };


    /* Configure RX message buffer with index RX_MSG_ID and RX_MAILBOX */
  //  FLEXCAN_DRV_ConfigRxMb(INST_CANCOM1, RX_MAILBOX, &dataInfo, RX_MSG_ID);

  //  FLEXCAN_DRV_ConfigTxMb(INST_CANCOM1, 1, &dataInfo, 1);
    
    /* 
     * Init Receive Hardware Object 
     */
    RxFirst= Can_ControllerConfigData[Controller].CanRxHwObjFirst;
    RxCount= Can_ControllerConfigData[Controller].CanRxHwObjCount;
    
    //endHwObj = (uint16)(CAN_CNTRL_CFG(Controller).CanRxHwObjFirst + CAN_CNTRL_CFG(Controller).CanRxHwObjCount);
    endHwObj=RxFirst+RxCount;
  //  for (HwObjId = CAN_CNTRL_CFG(Controller).CanRxHwObjFirst; HwObjId < endHwObj; HwObjId++)
    for (HwObjId = RxFirst; HwObjId < endHwObj; HwObjId++)
    {
        /* mailbox Id is Hardware Object Id plus offset of controller */
        mbId = CAN_MB_ID(HwObjId);

        /* set ID & Filter */
        if(CAN_ID_TYPE_EXTENDED == CAN_HWOBJ_CFG(HwObjId).CanIdType)
        {
        	dataInfo.msg_id_type=CAN_ID_TYPE_EXTENDED;
        }
        else if(CAN_ID_TYPE_STANDARD == CAN_HWOBJ_CFG(mbId).CanIdType)
        {
        	dataInfo.msg_id_type=CAN_ID_TYPE_STANDARD;
        }
        else /* mixed type */
        {

        }

   ///     FLEXCAN_DRV_ConfigRxMb(Controller, mbId, &dataInfo, CAN_HWOBJ_CFG(HwObjId).CanIdValue);  先放这里

       // CanRegs->mb[mbId].cs |= CAN_MBCS_CODERXEMPTY | CAN_MBCS_SRR;
        Can_Cntrl[Controller].RxMask[1-(mbId>>5)] |= (uint32)1 << (mbId&0x1F);

        /* clear iflag */
    }

    FLEXCAN_DRV_ConfigRxMb(INST_CANCOM1, 0, &dataInfo, 0x7df);
    FLEXCAN_DRV_ConfigRxMb(INST_CANCOM1, 1, &dataInfo, 0x70D);
    /* 
     * Init Transmit Hardware Object
     */
    endHwObj = (uint16)(CAN_CNTRL_CFG(Controller).CanTxHwObjFirst
                      + CAN_CNTRL_CFG(Controller).CanTxHwObjCount);
    for (HwObjId = CAN_CNTRL_CFG(Controller).CanTxHwObjFirst; HwObjId < endHwObj; HwObjId++)
    {
        /* mailbox Id is Hardware Object Id plus offset of controller */
        mbId = CAN_MB_ID(HwObjId);

        FLEXCAN_DRV_ConfigTxMb(Controller, mbId, &dataInfo, CAN_HWOBJ_CFG(HwObjId).CanIdValue);
        Can_Cntrl[Controller].TxMask[1-(mbId>>5)] |= (uint32)1<<(mbId&0x1F);

        /* clear iflag */
    }




    return;
}

#if(STD_ON == CAN_MULTIPLEXED_TRANSMISSION)
/******************************************************************************/
/*
 * Brief               <find lowest priority mailbox for same hth>
 * Param-Name[in]      <Hth - HW-transmit handle>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
STATIC FUNC(uint16, CAN_CODE) Can_FindLowPriorityMb(uint8 Hth)
{
    uint16 HwObjId  = 0;
    uint16 retHwObj = CAN_HWOBJ_ID(Hth);
    uint32 CanId    = 0; 

    if (FALSE == Can_IsTxMbFree(retHwObj))
    {
        CanId = Can_GetMBCanId(retHwObj);
        HwObjId = (uint16)(retHwObj+1);
        while ((HwObjId < CAN_HWOBJ_NUM) && (Hth == CAN_HWOBJ_CFG(HwObjId).CanObjectId))
        {
            if (TRUE == Can_IsTxMbFree(HwObjId))    
            {
                retHwObj = HwObjId;
                break;
            }
            else if (TRUE == Can_PriorityHigher(CanId, Can_GetMBCanId(HwObjId)))
            {
                retHwObj = HwObjId;
                CanId = Can_GetMBCanId(HwObjId);
            }
            else
            {
                /* do nothing */
            }

            HwObjId++;
        }
    }

    return retHwObj;
}
#endif /* STD_ON == CAN_MULTIPLEXED_TRANSMISSION */

/******************************************************************************/
/*
 * Brief               <check a mailbox if free, can transmit pdu>
 * Param-Name[in]      <HwObjId - hardware object index>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
STATIC FUNC(boolean, CAN_CODE) Can_IsTxMbFree(uint16 HwObjId)
{
    P2VAR(volatile Can_FlexCanRegType, AUTOMATIC, AUTOMATIC) CanRegs;
    uint16 mbId = CAN_MB_ID(HwObjId);
    boolean retVal = FALSE;

 //   CanRegs = CAN_CNTRL_REG(CAN_HWOBJ_CFG(HwObjId).CanControllerRef);
 //   if((CAN_MBCS_CODETX == (CanRegs->mb[mbId].cs & CAN_MBCS_CODE))
  //          && (0 == (CanRegs->iflag[1-(mbId>>5)] & ((uint32)0x1<<(mbId&0x1F)))))
    {
        retVal = TRUE;
    }
    return retVal;
}

/******************************************************************************/
/*
 * Brief               <Write PDU into mailbox to Transmit request>
 * Param-Name[in]      <mbId    - mailbox index>
 * Param-Name[in]      <PduInfo - pdu information>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
STATIC FUNC(void, CAN_CODE) Can_WriteMb(uint16 HwObjId, 
        P2CONST(Can_PduType, AUTOMATIC, CAN_APPL_DATA) PduInfo)
{
    P2VAR(volatile Can_FlexCanRegType, AUTOMATIC, AUTOMATIC) CanRegs;
    uint8  Controller = 0;
    uint16 mbId = 0;
    uint8  i = 0;

    flexcan_data_info_t dataInfo =
    {
            .data_length = 8,
            .msg_id_type = CAN_ID_TYPE_STANDARD,
            .enable_brs  = false,
            .fd_enable   = false,
            .fd_padding  = 0U
    };

    Controller = CAN_HWOBJ_CFG(HwObjId).CanControllerRef;

    if(Controller==1)//先设置单个CAN通道
    	Controller=0;
 //   CanRegs = CAN_CNTRL_REG(Controller);

    mbId = CAN_MB_ID(HwObjId);
  
    dataInfo.data_length=PduInfo->length;
    FLEXCAN_DRV_Send(Controller, mbId, &dataInfo, PduInfo->id, PduInfo->sdu);
    Can_Cntrl[Controller].TxPduHandles[mbId] = PduInfo->swPduHandle;

    return;
}

/******************************************************************************/
/*
 * Brief               <get can id from specific mailbox hardware>
 * Param-Name[in]      <mbId    - mailbox index>
 * Param-Name[in/out]  <None>
 * Return              <Can ID>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
STATIC FUNC(uint32, CAN_CODE) Can_GetMBCanId(uint16 HwObjId)
{
    P2VAR(volatile Can_FlexCanRegType, AUTOMATIC, AUTOMATIC) CanRegs;
    uint32 CanId = 0;
    uint16 mbId  = 0;

    CanRegs = CAN_CNTRL_REG(CAN_HWOBJ_CFG(HwObjId).CanControllerRef);
    mbId = CAN_MB_ID(HwObjId);

    /* extended frame */
    if(0 != (CanRegs->mb[mbId].cs & CAN_MBCS_IDE))   
    {
        CanId = CanRegs->mb[mbId].id & CAN_MBID_ID_EXTENDED;
        CanId |= 0x80000000U;  /* CanIf need extended Canid set 31 bit */
    } 
    /* standard frame */
    else
    {
        CanId = (CanRegs->mb[mbId].id & CAN_MBID_ID_STANDARD) >> 18;
    }

    return CanId;
}

/******************************************************************************/
/*
 * Brief               <get PDU from specific mailbox hardware>
 * Param-Name[in]      <mbId    - mailbox index>
 * Param-Name[in/out]  <pdu     - pdu>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/


 

STATIC FUNC(void, CAN_CODE) Can_GetMBInfo(uint16 HwObjId, 
                                P2VAR(Can_PduType, AUTOMATIC, AUTOMATIC) pdu)
{
    P2VAR(volatile Can_FlexCanRegType, AUTOMATIC, AUTOMATIC) CanRegs;
    uint16 mbId = 0;
    uint8 i = 0;
    uint8  Controller = 0;

    Controller = CAN_HWOBJ_CFG(HwObjId).CanControllerRef;

   // CanRegs = CAN_CNTRL_REG(CAN_HWOBJ_CFG(HwObjId).CanControllerRef);
    mbId = CAN_MB_ID(HwObjId);

    /* Define receive buffer */
    flexcan_msgbuff_t recvBuff;

    /* Start receiving data in RX_MAILBOX. */
    FLEXCAN_DRV_Receive(Controller, mbId, &recvBuff);

    
    pdu->id= recvBuff.msgId;   /* CanIf need extended Canid set 31 bit */
    pdu->length = recvBuff.dataLen;
    
    for (i = 0; i < pdu->length; i++) /* @req <CAN299> */
    {
        /* MISRA RULE 17.4:491 VIOLATION: Array subscripting applied to an object pointer */
        pdu->sdu[i] = recvBuff.data[i];
    }
    

    return;
}

/******************************************************************************/
/*
 * Brief               <apply software interrupt flag to hardware>
 * Param-Name[in]      <Controller - controller id>
 * Param-Name[in/out]  <SwIntFlag - software interrupt flag>
 * Return              <mailbox index>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
STATIC FUNC(void, CAN_CODE) Can_ApplySWF(uint8 Controller, uint8 SwIntFlag)
{
    P2VAR(volatile Can_FlexCanRegType, AUTOMATIC, AUTOMATIC) CanRegs;

 ///   CanRegs = CAN_CNTRL_REG(Controller);

//    CanRegs->imask[CAN_IMASK1_INDEX] = 0;
    if(CAN_CNTRL_HOH_NUM(Controller) > CAN_LOW_MB_NUMBER)
    {
//        CanRegs->imask[CAN_IMASK2_INDEX] = 0;
    }

    /* Rx Interrupt */
    if (SWF_RX_MASK == (SwIntFlag & SWF_RX_MASK))
    {
 //       CanRegs->imask[CAN_IMASK1_INDEX] |= Can_Cntrl[Controller].RxMask[CAN_IMASK1_INDEX];
        if(CAN_CNTRL_HOH_NUM(Controller) > CAN_LOW_MB_NUMBER)
        {
  //          CanRegs->imask[CAN_IMASK2_INDEX] |= Can_Cntrl[Controller].RxMask[CAN_IMASK2_INDEX];
        }
    }

    /* Tx Interrupt */
    if (SWF_TX_MASK == (SwIntFlag & SWF_TX_MASK))
    {
  //      CanRegs->imask[CAN_IMASK1_INDEX] |= Can_Cntrl[Controller].TxMask[CAN_IMASK1_INDEX];
        if(CAN_CNTRL_HOH_NUM(Controller) > CAN_LOW_MB_NUMBER)
        {
  //          CanRegs->imask[CAN_IMASK2_INDEX] |= Can_Cntrl[Controller].TxMask[CAN_IMASK2_INDEX];
        }
    }

    /* RWEN Interrupt */
    if (SWF_RWRN_MASK == (SwIntFlag & SWF_RWRN_MASK))
    {
 //       CanRegs->ctrl |= CAN_CTRL_RWRNMSK;
    }
    else
    {
  //      CanRegs->ctrl &= ~CAN_CTRL_RWRNMSK;
    }

    /* TWEN Interrupt */
    if (SWF_TWRN_MASK == (SwIntFlag & SWF_TWRN_MASK))
    {
 //       CanRegs->ctrl |= CAN_CTRL_TWRNMSK;
    }
    else
    {
  //      CanRegs->ctrl &= ~CAN_CTRL_TWRNMSK;
    }

    /* ERR Interrup */
    if (SWF_ERR_MASK == (SwIntFlag & SWF_ERR_MASK))
    {
 //       CanRegs->ctrl |= CAN_CTRL_ERRMSK;
    }
    else
    {
 //       CanRegs->ctrl &= ~CAN_CTRL_ERRMSK;
    }

    /* BUSOFF Interrupt */
    if (SWF_BOFF_MASK == (SwIntFlag & SWF_BOFF_MASK))
    {
 //       CanRegs->ctrl |= CAN_CTRL_BOFFMSK;
    }
    else
    {
  //      CanRegs->ctrl &= ~CAN_CTRL_BOFFMSK;
    }

    #if(STD_ON == CAN_WAKEUP_SUPPORT)
    /* WAK Interrupt */
    if (SWF_WAK_MASK == (SwIntFlag & SWF_WAK_MASK))
    {
 //       CanRegs->mcr |= CAN_MCR_WAK_MSK;
    }
    else
    {
 //       CanRegs->mcr &= ~CAN_MCR_WAK_MSK;
    }
    #endif /* #if(STD_ON == CAN_WAKEUP_SUPPORT) */

    return;
}

#if ((STD_ON==CAN_HW_TRANSMIT_CANCELLATION)&&(STD_ON==CAN_MULTIPLEXED_TRANSMISSION))
/******************************************************************************/
/*
 * Brief               <abort check >
 * Param-Name[in]      <Controller - controller id>
 * Param-Name[in/out]  <Config - Pointer to Controller Config>
 * Return              <mailbox index>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
STATIC FUNC(void, CAN_CODE) Can_CheckAbortMb(uint8 Controller, uint8 Hth)
{
    P2VAR(volatile Can_FlexCanRegType, AUTOMATIC, AUTOMATIC) CanRegs;
    uint16 HwObjId  = CAN_HWOBJ_ID(Hth);
    uint16 mbId = 0;

    CanRegs = CAN_CNTRL_REG(Controller);

    while ((HwObjId < CAN_HWOBJ_NUM) && (Hth == CAN_HWOBJ_CFG(HwObjId).CanObjectId))
    {
        mbId = CAN_MB_ID(HwObjId);

        if((CAN_MBCS_CODETX|CAN_MBCS_CODETXABORT) == (CanRegs->mb[mbId].cs&CAN_MBCS_CODE))
        {
            uint8  RxData[8];
            Can_PduType pdu;

            pdu.sdu = RxData;
            Can_GetMBInfo(HwObjId, &pdu);
            pdu.swPduHandle = Can_Cntrl[Controller].TxPduHandles[mbId];

            /* clear abort flag */
            CanRegs->mb[mbId].cs &= ~CAN_MBCS_CODETXABORT;

            CanIf_CancelTxConfirmation(&pdu);
        }

        HwObjId++;
    }
    return;
}
#endif /* (STD_ON==CAN_HW_TRANSMIT_CANCELLATION)&&(STD_ON==CAN_MULTIPLEXED_TRANSMISSION) */

#if ((STD_ON==CAN_HW_TRANSMIT_CANCELLATION)||(STD_ON==CAN_MULTIPLEXED_TRANSMISSION))
/******************************************************************************/
/*
 * Brief               <compare two canid priority>
 * Param-Name[in]      <destId - destination Can ID>
 * Param-Name[in]      <srcId  - source Can ID>
 * Return              <TRUE: destId higher than srcId, 
 *                      FALSE: destId not higher than srcId>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
STATIC FUNC(boolean, CAN_CODE) Can_PriorityHigher(Can_IdType destId, Can_IdType srcId)
{
    boolean ret = FALSE;

    /* clear bit 29, 30 */
    destId &= 0x9FFFFFFFU;
    srcId  &= 0x9FFFFFFFU;

    /* low 11 bit same, then compare high 21 bit */
    if ((destId & 0x7FFU) == (srcId & 0x7FFU)) 
    {
        /* compare high 21 bit */
        if ((destId & 0xFFFFF800U) < (srcId & 0xFFFFF800U))
        {
            ret = TRUE;    
        }
        else
        {
            ret = FALSE;
        }
    }
    else if ((destId & 0x7FFU) < (srcId & 0x7FFU))
    {
        ret = TRUE;
    }
    else
    {
        ret = FALSE;
    }

    return ret;
}
#endif /* (STD_ON==CAN_HW_TRANSMIT_CANCELLATION)||(STD_ON==CAN_MULTIPLEXED_TRANSMISSION) */

#define CAN_STOP_SEC_CODE
#include "Can_MemMap.h"
/*=======[E N D   O F   F I L E]==============================================*/

