/*============================================================================*/
/*  Copyright (C) 2009-2014,  INFRASTRUCTURE SOFTWARE CO.,LTD.
 *  
 *  All rights reserved. This software is  property. Duplication
 *  or disclosure without  written authorization is prohibited.
 *  
 *  @file       <Can_PBcfg.h>
 *  @brief      <>
 *  
 *  <MCU:MPC563x>
 *  
 *  @author     <>
 *  @date       <2014-06-10 09:41:05>
 */
/*============================================================================*/

	
#ifndef CAN_PBCFG_H
#define CAN_PBCFG_H

/*=======[I N C L U D E S]====================================================*/
/*@req <CAN047>*/
#include "Can.h"

/*=======[V E R S I O N   I N F O R M A T I O N]===============================*/
#define CAN_PBCFG_H_AR_MAJOR_VERSION     2
#define CAN_PBCFG_H_AR_MINOR_VERSION     4
#define CAN_PBCFG_H_AR_PATCH_VERSION     0
#define CAN_PBCFG_H_SW_MAJOR_VERSION     1
#define CAN_PBCFG_H_SW_MINOR_VERSION     0
#define CAN_PBCFG_H_SW_PATCH_VERSION     0


#endif /* #define CAN_PBCFG_H */

/*=======[E N D   O F   F I L E]==============================================*/
