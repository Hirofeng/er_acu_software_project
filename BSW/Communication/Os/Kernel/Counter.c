
/*============================================================================*/
/** Copyright (C) 2009-2011,  INFRASTRUCTURE SOFTWARE CO.,LTD.
 *
 *  All rights reserved. This software is  property. Duplication
 *  or disclosure without  written authorization is prohibited.
 *
 *  @file       <Counter.c>
 *  @brief      <>
 *
 *  <Detailed description of the file(multi-line)>
 *
 *  @author     <Yaoxuan.zhang>
 *  @date       <2013-06-08>
 */
/*============================================================================*/
/*=======[R E V I S I O N   H I S T O R Y]====================================*/
/*  <VERSION>    <DATE>    <AUTHOR>    <REVISION LOG>
 *  V0.1.0       20130415  yaoxuan.zhang       Initial version
 *  V0.2.0       20130709  huaming             modified the format of Os_CounterCfg,change the included file
 *  V0.3.0       20130723  yaoxuan.zhang       Modified the code to adapt MISRA C specification.
 */
/*============================================================================*/

/*=======[M I S R A C  R U L E  V I O L A T I O N]============================*/
/*  <MESSAGE ID>    <CODE LINE>    <REASON>
 */
/*============================================================================*/

/*=======[I N C L U D E S]====================================================*/
#include "Os.h"

#include "Kernel.h"
/*=======[M A C R O S]========================================================*/


/*=======[T Y P E   D E F I N I T I O N S]====================================*/


/*=======[E X T E R N A L   D A T A]==========================================*/


/*=======[E X T E R N A L   F U N C T I O N   D E C L A R A T I O N S]========*/


/*=======[I N T E R N A L   D A T A]==========================================*/


/*=======[I N T E R N A L   F U N C T I O N   D E C L A R A T I O N S]========*/
#if (CFG_ALARM_MAX > 0)
static void OSWorkAlarm(Os_CounterType CounterID) ;
#endif

/*=======[F U N C T I O N   I M P L E M E N T A T I O N S]====================*/

#if (CFG_COUNTER_MAX > 0)
/******************************************************************************/
/*
 * Brief                <Init the counter control block>
 * Service ID           <None>
 * Sync/Async           <Synchronous>
 * Reentrancy           <Non Reentrant>
 * param-id[in]         <counter control block id>
 * Param-Name[out]      <None>
 * Param-Name[in/out]   <None>
 * return               <None>
 * PreCondition         <None>
 * CallByAPI            <None>
 */
/******************************************************************************/
void Os_InitCounterCB(Os_CounterType id)
{
	Os_CCB[id].counterCurVal = 0;
	Os_CCB[id].counterLastVal = 0;
	Os_CCB[id].counterAlmQue = INVALID_ALARM_ID;
}
#endif
/******************************************************************************/
/*
 * Brief                <Add the tick value>
 * Service ID           <None>
 * Sync/Async           <Synchronous>
 * Reentrancy           <Reentrant>
 * param-value[in]      <current tick value>
 * param-base[in]       <time base of the tick>
 * param-maxticks[in]   <max tick allow to add>
 * Param-Name[out]      <None>
 * Param-Name[in/out]   <None>
 * return               <Os_TickType>
 * PreCondition         <None>
 * CallByAPI            <None>
 */
/******************************************************************************/
Os_TickType Os_AddTicks(Os_TickType value,Os_TickType base,Os_TickType maxticks)
{
	if (base <= (maxticks - value))
	{
		return (value + base);
	}

	return ((value + base) - (maxticks + 1));
}

/******************************************************************************/
/*
 * Brief                <Sub the tick value>
 * Service ID           <None>
 * Sync/Async           <Synchronous>
 * Reentrancy           <Reentrant>
 * param-newvalue[in]   <new tick value>
 * param-oldvalue[in]   <old tick value>
 * param-maxticks[in]   <max tick allow to add>
 * Param-Name[out]      <None>
 * Param-Name[in/out]   <None>
 * return               <Os_TickType>
 * PreCondition         <None>
 * CallByAPI            <None>
 */
/******************************************************************************/
Os_TickType Os_SubTicks(Os_TickType newvalue,Os_TickType oldvalue,Os_TickType maxticks)
{
	if (newvalue >= oldvalue)
	{
		return (newvalue - oldvalue);
	}

	return ((newvalue - oldvalue) + (maxticks +1));
}

/******************************************************************************/
/*
 * Brief                <>
 * Service ID           <None>
 * Sync/Async           <Synchronous>
 * Reentrancy           <Reentrant>
 * param-CounterID[in]  <reference to counter>
 * Param-Name[out]      <None>
 * Param-Name[in/out]   <None>
 * return               <None>
 * PreCondition         <None>
 * CallByAPI            <None>
 */
/******************************************************************************/
#if (CFG_ALARM_MAX > 0)
static void OSWorkAlarm(Os_CounterType CounterID)
{
	
}
#endif
#if (CFG_COUNTER_MAX > 0)
/******************************************************************************/
/*
 * Brief                <Increment counter>
 * Service ID           <None>
 * Sync/Async           <Synchronous>
 * Reentrancy           <Reentrant>
 * param-CounterID[in]  <reference to counter>
 * Param-Name[out]      <None>
 * Param-Name[in/out]   <None>
 * return               <None>
 * PreCondition         <None>
 * CallByAPI            <None>
 */
/******************************************************************************/
StatusType IncrementCounter(CounterType CounterID)
{
	Os_TickType counterMaxTicks;

	#if (CFG_STATUS == STATUS_EXTENDED)
	if (CounterID >= CFG_COUNTER_MAX)
	{
		return E_OS_ID;
	}
	#endif

	counterMaxTicks = (Os_CounterCfg[CounterID].osCounterMaxAllowedValue *2) +1;

	Os_CCB[CounterID].counterLastVal = Os_CCB[CounterID].counterCurVal;
	Os_CCB[CounterID].counterCurVal = Os_AddTicks(Os_CCB[CounterID].counterCurVal,1,counterMaxTicks);

	#if (CFG_ALARM_MAX > 0)
  	OSWorkAlarm(CounterID);
	#endif

	return E_OK;
}

/******************************************************************************/
/*
 * Brief                <Get current counter value>
 * Service ID           <None>
 * Sync/Async           <Synchronous>
 * Reentrancy           <Reentrant>
 * param-CounterID[in]  <reference to counter>
 * Param-Value[out]     <current value of the counter>
 * Param-Name[in/out]   <None>
 * return               <None>
 * PreCondition         <None>
 * CallByAPI            <None>
 */
/******************************************************************************/
StatusType GetCounterValue(CounterType CounterID,TickRefType Value)
{
	#if (CFG_STATUS == STATUS_EXTENDED)
	if (CounterID >= CFG_COUNTER_MAX)
	{
		return E_OS_ID;
	}
	#endif

	*Value = Os_CCB[CounterID].counterCurVal % (Os_CounterCfg[CounterID].osCounterMaxAllowedValue + 1);
	
	return E_OK;
}

/******************************************************************************/
/*
 * Brief                    <Get elapsed counter value>
 * Service ID               <None>
 * Sync/Async               <Synchronous>
 * Reentrancy               <Reentrant>
 * param-CounterID[in]      <reference to counter>
 * Param-Value[out]         <current value of the counter>
 * Param-ElapsedValue[out]  <Elapsed value of the counter>
 * Param-Name[in/out]       <None>
 * return                   <StatusType>
 * PreCondition             <None>
 * CallByAPI                <None>
 */
/******************************************************************************/
StatusType GetElapsedCounterValue(CounterType CounterID,TickRefType Value,TickRefType ElapsedValue)
{
	Os_TickType counterCurval;
	Os_TickType counterMaxAllowedValue;

	#if (CFG_STATUS == STATUS_EXTENDED)
	if (CounterID >= CFG_COUNTER_MAX)
	{
		return E_OS_ID;
	}

	if (*Value > Os_CounterCfg[CounterID].osCounterMaxAllowedValue)
	{
		return E_OS_VALUE;
	}
	#endif

	counterCurval = Os_CCB[CounterID].counterCurVal;
	counterMaxAllowedValue = Os_CounterCfg[CounterID].osCounterMaxAllowedValue;

	*ElapsedValue = ((counterCurval + counterMaxAllowedValue) - *Value) % counterMaxAllowedValue;
	*Value = counterCurval;

	return E_OK;
}
#endif
/*=======[E N D   O F   F I L E]==============================================*/
