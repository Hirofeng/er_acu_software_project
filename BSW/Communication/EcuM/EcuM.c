/*============================================================================*/
/*  Copyright (C) 2009-2013,  INFRASTRUCTURE SOFTWARE CO.,LTD.
 *  
 *  All rights reserved. This software is  property. Duplication
 *  or disclosure without  written authorization is prohibited.
 *  
 *  @file       <EcuM.c>
 *  @brief      <EcuM>
 *  
 *  <Compiler: CodeWarrior2.8 MCU:MPC5634>
 *  
 *  @author     <Tommy>
 *  @date       <2013-06-25>
 */
/*============================================================================*/

/*=======[R E V I S I O N   H I S T O R Y]====================================*/
/*  <VERSION>    <DATE>    <AUTHOR>    <REVISION LOG>
 *  V1.0.0       20130625   Tommy      Initial version
 */
/*============================================================================*/

/*=======[I N C L U D E S]====================================================*/
#include "EcuM.h"
#include "Can.h"
#include "CanIf.h"
#include "EcuM_Cbk.h"
/* #include "CanSM.h"*/


/*=======[F U N C T I O N   I M P L E M E N T A T I O N S]====================*/
boolean EcuM_CanWakeupEvent;

FUNC(void, EcuM_CODE)
EcuM_Init(void)
{
  EcuM_CanWakeupEvent = FALSE;
}

FUNC(void, EcuM_CODE)
EcuM_SetWakeupEvent(EcuM_WakeupSourceType WakeupSource)
{

  EcuM_CanWakeupEvent = TRUE;
}


FUNC(void, EcuM_CODE)
EcuM_ValidateWakeupEvent(EcuM_WakeupSourceType WakeupSource)
{
   return ;
}

FUNC(void, EcuM_CODE) EcuM_CheckWakeup(EcuM_WakeupSourceType WakeupSource)
{
	(void)CanIf_CheckWakeup(WakeupSource);
}

FUNC(void, EcuM_CODE)
EcuM_MainFunction(void)
{

  if(EcuM_CanWakeupEvent == TRUE)
  {
    
     EcuM_CanWakeupEvent = FALSE;    
  
     CanIf_SetControllerMode(CAN_CONTROLLER_C,CANIF_CS_STARTED);
  }
}
