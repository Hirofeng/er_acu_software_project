/*============================================================================*/
/*  Copyright (C) 2009-2013,  INFRASTRUCTURE SOFTWARE CO.,LTD.
 *  
 *  All rights reserved. This software is  property. Duplication
 *  or disclosure without  written authorization is prohibited.
 *  
 *  @file       <EcuM.h>
 *  @brief      <EcuM>
 *  
 *  <Compiler: CodeWarrior2.10  MCU:MPC5634>
 *  
 *  @author     <Tommy>
 *  @date       <2013-05-10>
 */
/*============================================================================*/



#ifndef _ECUM_H
#define _ECUM_H



#include "Std_Types.h"


typedef enum
{
	ECUM_WKSOURCE_INTERNAL_RESET = 0x04,

	/** Reset by external watchdog (bit 4), if
	 *  detection supported by hardware */
	ECUM_WKSOURCE_EXTERNAL_WDG = 0x10,

	/** Reset by internal watchdog (bit 3) */
	ECUM_WKSOURCE_INTERNAL_WDG = 0x08,

	/** Power cycle (bit 0) */
	ECUM_WKSOURCE_POWER = 0x01,

	/** ~0 to the power of 29 */
	ECUM_WKSOURCE_ALL_SOURCES1 = 0x3FFF,
    ECUM_WKSOURCE_ALL_SOURCES2 = 0xFF,
    ECUM_WKSOURCE_ALL_SOURCES3 =0xFF,
	/** Hardware reset (bit 1).
	 *  If hardware cannot distinguish between a
	 *  power cycle and a reset reason, then this
	 *  shall be the default wakeup source */
	ECUM_WKSOURCE_RESET = 0x02
}EcuM_WakeupSourceType;



extern FUNC(void, EcuM_CODE)
EcuM_Init(void);


extern FUNC(void, EcuM_CODE)
EcuM_MainFunction(void);


extern FUNC(void, EcuM_CODE)
EcuM_CheckWakeup(EcuM_WakeupSourceType WakeupSource);

#endif  /* #ifndef _ECUM_H*/

