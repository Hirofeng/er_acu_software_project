/*============================================================================*/
/*  Copyright (C) 2009-2014,  INFRASTRUCTURE SOFTWARE CO.,LTD.
 *  
 *  All rights reserved. This software is  property. Duplication
 *  or disclosure without  written authorization is prohibited.
 *  
 *  @file       <Com_Cfg.c>
 *  @brief      <>
 *  
 *  <MCU:MPC563x>
 *  
 *  @author     <>
 *  @date       <2014-06-23 17:29:01>
 */
/*============================================================================*/


/*=======[V E R S I O N  I N F O R M A T I O N]===============================*/
#define COM_PBCFG_C_AR_MAJOR_VERSION  3U
#define COM_PBCFG_C_AR_MINOR_VERSION  0U
#define COM_PBCFG_C_AR_PATCH_VERSION  3U
#define COM_PBCFG_C_SW_MAJOR_VERSION  1U
#define COM_PBCFG_C_SW_MINOR_VERSION  0U
#define COM_PBCFG_C_SW_PATCH_VERSION  0U
/*=======[I N C L U D E S]====================================================*/
#include "Com_CfgType.h"
/* #include "Rte_Com.h" */  /* no RTE present,so comments,tommy 20140610 */
#include "Com_Callout.h"

/*=======[E X T E R N A L   D A T A]==========================================*/


#if (OSEK_COM_CCC == OSEK_COM_CCC1) || (OSEK_COM_CCC == OSEK_COM_CCC0)
#define COM_START_CONST_PBCFG
#include "Com_MemMap.h"
CONST(Com_IPduType,COM_CONST_PBCFG) Com_IPdu[] =
{
	{
		/* ACCM_VCU_statusinformationl */
		0xd,							/*IPdu Id*/   //CANID=341
		64,							/*IPdu Size In Bits*/
		COM_IPDU_RECEIVE,				/*IPdu Direction*/
	#if (OSEK_COM_CCC == OSEK_COM_CCC1)
		COM_IPDU_TRANSMIT_DIRECT,	/*IPdu Transmit Mode*/
		0,							/*IPdu Transmit Period time*/
		0,							/*IPdu Transmit Offset time*/
		0,							/*Transmit MDT*/
		10,							/*Transmit or Receive DM*/
		10,							/*Receive First Timeout*/
	#if (STD_OFF == COM_OPTIME_SUPPORT)
		NULL_PTR					/*IPdu CallOut*/
	#endif
	#endif
	},
	{
		/* BMS_General */
		0xe,							/*IPdu Id*/
		64,							/*IPdu Size In Bits*/
		COM_IPDU_RECEIVE,				/*IPdu Direction*/
	#if (OSEK_COM_CCC == OSEK_COM_CCC1)
		COM_IPDU_TRANSMIT_DIRECT,	/*IPdu Transmit Mode*/
		0,							/*IPdu Transmit Period time*/
		0,							/*IPdu Transmit Offset time*/
		0,							/*Transmit MDT*/
		10,							/*Transmit or Receive DM*/
		10,							/*Receive First Timeout*/
	#if (STD_OFF == COM_OPTIME_SUPPORT)
		NULL_PTR					/*IPdu CallOut*/
	#endif
	#endif
	},
	{
		/* VCU_Eptcm_SHIFT */
		0x8,							/*IPdu Id*/ // 3A2
		64,							/*IPdu Size In Bits*/
		COM_IPDU_SENT,				/*IPdu Direction*/
	#if (OSEK_COM_CCC == OSEK_COM_CCC1)
		COM_IPDU_TRANSMIT_PERIODIC,	/*IPdu Transmit Mode*/
		100,							/*IPdu Transmit Period time*/
		10,							/*IPdu Transmit Offset time*/
		0,							/*Transmit MDT*/
		10,							/*Transmit or Receive DM*/
		0,							/*Receive First Timeout*/
	#if (STD_OFF == COM_OPTIME_SUPPORT)
		NULL_PTR					/*IPdu CallOut*/
	#endif
	#endif
	},
	
};
#define COM_STOP_CONST_PBCFG
#include "Com_MemMap.h"
#endif

#if (OSEK_COM_CCC == OSEK_COM_CCC1) || (OSEK_COM_CCC == OSEK_COM_CCC0)
#define COM_START_CONST_PBCFG
#include "Com_MemMap.h"
CONST(Com_NWMsgType,COM_CONST_PBCFG) Com_NWMessage[] =
{
	{
		/* ACCM_VCU_error_NetMsg ,PEPS_PowerPmode*/
		3,							/*SizeInBits*/
		2,							/*BitPosition*/
	#if (OSEK_COM_CCC == OSEK_COM_CCC1)
	#if (STD_OFF == COM_OPTIME_SUPPORT)
		0,							/*MaximumSizeInBits*/
	#endif
	#endif
		&Com_IPdu[0],			/*IPduRef*/
	#if (OSEK_COM_CCC == OSEK_COM_CCC1)
		COM_MSG_STATIC,				/*NetworkMessageClass*/
	#endif
		COM_MSG_BIG_ENDIAN,		/*BitOrder*/
		COM_MSG_UNSIGNEDINTEGER,	/*DataInterpretation*/
		COM_MSG_RECEIVED				/*Direction*/
	},
	{
		/* ACCM_VCU_power_NetMsg  PEPS_FobPairingSts */
		1,							/*SizeInBits*/
		11,							/*BitPosition*/
	#if (OSEK_COM_CCC == OSEK_COM_CCC1)
	#if (STD_OFF == COM_OPTIME_SUPPORT)
		0,							/*MaximumSizeInBits*/
	#endif
	#endif
		&Com_IPdu[0],			/*IPduRef*/
	#if (OSEK_COM_CCC == OSEK_COM_CCC1)
		COM_MSG_STATIC,				/*NetworkMessageClass*/
	#endif
		COM_MSG_BIG_ENDIAN,		/*BitOrder*/
		COM_MSG_UNSIGNEDINTEGER,	/*DataInterpretation*/
		COM_MSG_RECEIVED				/*Direction*/
	},
	{
		/* ACCM_VCU_RPM_NetMsg  BCM_DriverDoorStatus */
		1,							/*SizeInBits*/
		12,							/*BitPosition*/
	#if (OSEK_COM_CCC == OSEK_COM_CCC1)
	#if (STD_OFF == COM_OPTIME_SUPPORT)
		0,							/*MaximumSizeInBits*/
	#endif
	#endif
		&Com_IPdu[1],			/*IPduRef*/
	#if (OSEK_COM_CCC == OSEK_COM_CCC1)
		COM_MSG_STATIC,				/*NetworkMessageClass*/
	#endif
		COM_MSG_BIG_ENDIAN,		/*BitOrder*/
		COM_MSG_UNSIGNEDINTEGER,	/*DataInterpretation*/
		COM_MSG_RECEIVED				/*Direction*/
	},
	{
		/* VCU_DCDC_iActHvMax_NetMsg  WCM_ChargingStatus */
		2,							/*SizeInBits*/
		1,							/*BitPosition*/
	#if (OSEK_COM_CCC == OSEK_COM_CCC1)
	#if (STD_OFF == COM_OPTIME_SUPPORT)
		0,							/*MaximumSizeInBits*/
	#endif
	#endif
		&Com_IPdu[2],			/*IPduRef*/
	#if (OSEK_COM_CCC == OSEK_COM_CCC1)
		COM_MSG_STATIC,				/*NetworkMessageClass*/
	#endif
		COM_MSG_BIG_ENDIAN,		/*BitOrder*/
		COM_MSG_UNSIGNEDINTEGER,	/*DataInterpretation*/
		COM_MSG_SENT				/*Direction*/
	},
	{
		/* VCU_DCDC_stCtrlReq_NetMsg  WCM_PhoneReminder  */
		1,							/*SizeInBits*/
		2,							/*BitPosition*/
	#if (OSEK_COM_CCC == OSEK_COM_CCC1)
	#if (STD_OFF == COM_OPTIME_SUPPORT)
		0,							/*MaximumSizeInBits*/
	#endif
	#endif
		&Com_IPdu[2],			/*IPduRef*/
	#if (OSEK_COM_CCC == OSEK_COM_CCC1)
		COM_MSG_STATIC,				/*NetworkMessageClass*/
	#endif
		COM_MSG_BIG_ENDIAN,		/*BitOrder*/
		COM_MSG_UNSIGNEDINTEGER,	/*DataInterpretation*/
		COM_MSG_SENT				/*Direction*/
	},
	{
		/* VCU_DCDC_StModeReq_NetMsg  WCM_ShutdownFeedback */
		1,							/*SizeInBits*/
		3,							/*BitPosition*/
	#if (OSEK_COM_CCC == OSEK_COM_CCC1)
	#if (STD_OFF == COM_OPTIME_SUPPORT)
		0,							/*MaximumSizeInBits*/
	#endif
	#endif
		&Com_IPdu[2],			/*IPduRef*/
	#if (OSEK_COM_CCC == OSEK_COM_CCC1)
		COM_MSG_STATIC,				/*NetworkMessageClass*/
	#endif
		COM_MSG_BIG_ENDIAN,		/*BitOrder*/
		COM_MSG_UNSIGNEDINTEGER,	/*DataInterpretation*/
		COM_MSG_SENT				/*Direction*/
	},
	{
		/* VCU_DCDC_udcLvSetP_NetMsg   WCM_SwMemorySts */
		1,							/*SizeInBits*/
		4,							/*BitPosition*/
	#if (OSEK_COM_CCC == OSEK_COM_CCC1)
	#if (STD_OFF == COM_OPTIME_SUPPORT)
		0,							/*MaximumSizeInBits*/
	#endif
	#endif
		&Com_IPdu[2],			/*IPduRef*/
	#if (OSEK_COM_CCC == OSEK_COM_CCC1)
		COM_MSG_STATIC,				/*NetworkMessageClass*/
	#endif
		COM_MSG_BIG_ENDIAN,		/*BitOrder*/
		COM_MSG_UNSIGNEDINTEGER,	/*DataInterpretation*/
		COM_MSG_SENT				/*Direction*/
	},
	{
		/* VCU_MotorSpd_NetMsg  WCM_FODWarming  */
		1,							/*SizeInBits*/
		5,							/*BitPosition*/
	#if (OSEK_COM_CCC == OSEK_COM_CCC1)
	#if (STD_OFF == COM_OPTIME_SUPPORT)
		0,							/*MaximumSizeInBits*/
	#endif
	#endif
		&Com_IPdu[2],			/*IPduRef*/
	#if (OSEK_COM_CCC == OSEK_COM_CCC1)
		COM_MSG_STATIC,				/*NetworkMessageClass*/
	#endif
		COM_MSG_BIG_ENDIAN,		/*BitOrder*/
		COM_MSG_UNSIGNEDINTEGER,	/*DataInterpretation*/
		COM_MSG_SENT				/*Direction*/
	},
	{
		/* VCU_Parkrequest_NetMsg WCM_ChargingErr */
		3,							/*SizeInBits*/
		10,							/*BitPosition*/
	#if (OSEK_COM_CCC == OSEK_COM_CCC1)
	#if (STD_OFF == COM_OPTIME_SUPPORT)
		0,							/*MaximumSizeInBits*/
	#endif
	#endif
		&Com_IPdu[2],			/*IPduRef*/
	#if (OSEK_COM_CCC == OSEK_COM_CCC1)
		COM_MSG_STATIC,				/*NetworkMessageClass*/
	#endif
		COM_MSG_BIG_ENDIAN,		/*BitOrder*/
		COM_MSG_UNSIGNEDINTEGER,	/*DataInterpretation*/
		COM_MSG_SENT				/*Direction*/
	},
	
};
#define COM_STOP_CONST_PBCFG
#include "Com_MemMap.h"
#endif

#if (OSEK_COM_CCC == OSEK_COM_CCC1)
#if (STD_ON == COM_FILTER_SUPPORT)
#define COM_START_CONST_PBCFG
#include "Com_MemMap.h"
CONST(Com_MsgFilterType,COM_CONST_PBCFG) Com_Filter[] =
{
	{
		/* Message : ACCM_VCU_error */
		0x0,							/*Mask*/
		0x0,							/*Max*/
		0x0,							/*Min*/
		0x0,							/*Offset*/
		0x0,							/*PeriodFactor*/
		0x0,							/*FilterX*/
		COM_F_Always				/*FilterAlgorithm*/
	},
	{
		/* Message : ACCM_VCU_power */
		0x0,							/*Mask*/
		0x0,							/*Max*/
		0x0,							/*Min*/
		0x0,							/*Offset*/
		0x0,							/*PeriodFactor*/
		0x0,							/*FilterX*/
		COM_F_Always				/*FilterAlgorithm*/
	},
	{
		/* Message : ACCM_VCU_RPM */
		0x0,							/*Mask*/
		0x0,							/*Max*/
		0x0,							/*Min*/
		0x0,							/*Offset*/
		0x0,							/*PeriodFactor*/
		0x0,							/*FilterX*/
		COM_F_Always				/*FilterAlgorithm*/
	},
	{
		/* Message : ACCM_VCU_status */
		0x0,							/*Mask*/
		0x0,							/*Max*/
		0x0,							/*Min*/
		0x0,							/*Offset*/
		0x0,							/*PeriodFactor*/
		0x0,							/*FilterX*/
		COM_F_Always				/*FilterAlgorithm*/
	},
	{
		/* Message : BMS_bExtCoReq */
		0x0,							/*Mask*/
		0x0,							/*Max*/
		0x0,							/*Min*/
		0x0,							/*Offset*/
		0x0,							/*PeriodFactor*/
		0x0,							/*FilterX*/
		COM_F_Always				/*FilterAlgorithm*/
	},
	{
		/* Message : BMS_bServCon */
		0x0,							/*Mask*/
		0x0,							/*Max*/
		0x0,							/*Min*/
		0x0,							/*Offset*/
		0x0,							/*PeriodFactor*/
		0x0,							/*FilterX*/
		COM_F_Always				/*FilterAlgorithm*/
	},
	{
		/* Message : BMS_ChargePlugConnected */
		0x0,							/*Mask*/
		0x0,							/*Max*/
		0x0,							/*Min*/
		0x0,							/*Offset*/
		0x0,							/*PeriodFactor*/
		0x0,							/*FilterX*/
		COM_F_Always				/*FilterAlgorithm*/
	},
	{
		/* Message : BMS_FailureLvl */
		0x0,							/*Mask*/
		0x0,							/*Max*/
		0x0,							/*Min*/
		0x0,							/*Offset*/
		0x0,							/*PeriodFactor*/
		0x0,							/*FilterX*/
		COM_F_Always				/*FilterAlgorithm*/
	},
	{
		/* Message : BMS_General_ckSt */
		0x0,							/*Mask*/
		0x0,							/*Max*/
		0x0,							/*Min*/
		0x0,							/*Offset*/
		0x0,							/*PeriodFactor*/
		0x0,							/*FilterX*/
		COM_F_Always				/*FilterAlgorithm*/
	},
	
};
#define COM_STOP_CONST_PBCFG
#include "Com_MemMap.h"
#endif
#endif

#define COM_START_CONST_PBCFG
#include "Com_MemMap.h"
CONST(Com_MsgType,COM_CONST_PBCFG) Com_Message[] =
{
	{
		/* ACCM_VCU_error */
	#if ((OSEK_COM_CCC == OSEK_COM_CCCB) || (OSEK_COM_CCC == OSEK_COM_CCC1))
		0,							 /*QueuedSize*/
	#endif
		{0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0},        /*InitValue*/
	#if (STD_OFF == COM_OPTIME_SUPPORT)
		NULL_PTR,					 /*SendMessageRef*/
	#endif
	#if ((OSEK_COM_CCC == OSEK_COM_CCC1) || (OSEK_COM_CCC == OSEK_COM_CCC0))
	#if (STD_OFF == COM_OPTIME_SUPPORT)
		NULL_PTR,					/*LinkMessageRef*/
	#endif
		&Com_NWMessage[0],		    /*NWMsgRef*/
	#endif
	#if (OSEK_COM_CCC == OSEK_COM_CCC1)
	#if (STD_ON == COM_FILTER_SUPPORT)
		&Com_Filter[0],			/*FilterRef*/
	#endif
		NULL_PTR,					/*DMErrorNotification*/
	#if (STD_OFF == COM_OPTIME_SUPPORT)
		NULL_PTR,					/*NetworkorderCallout*/
		NULL_PTR,					/*CpuOrderCallout*/
	#endif
		COM_MSG_TRIGGERED,			/*TransferProperty*/
	#endif
		NULL_PTR,					/*Notification*/
		COM_MSG_RECEIVE_UNQUEUED_EXTERNAL,	      /*MsgProperty*/
		COM_MSG_UINT8,				/*CDataType*/
	},
	{
		/* ACCM_VCU_power */
	#if ((OSEK_COM_CCC == OSEK_COM_CCCB) || (OSEK_COM_CCC == OSEK_COM_CCC1))
		0,							 /*QueuedSize*/
	#endif
		{0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0},        /*InitValue*/
	#if (STD_OFF == COM_OPTIME_SUPPORT)
		NULL_PTR,					 /*SendMessageRef*/
	#endif
	#if ((OSEK_COM_CCC == OSEK_COM_CCC1) || (OSEK_COM_CCC == OSEK_COM_CCC0))
	#if (STD_OFF == COM_OPTIME_SUPPORT)
		NULL_PTR,					/*LinkMessageRef*/
	#endif
		&Com_NWMessage[1],		    /*NWMsgRef*/
	#endif
	#if (OSEK_COM_CCC == OSEK_COM_CCC1)
	#if (STD_ON == COM_FILTER_SUPPORT)
		&Com_Filter[1],			/*FilterRef*/
	#endif
		NULL_PTR,					/*DMErrorNotification*/
	#if (STD_OFF == COM_OPTIME_SUPPORT)
		NULL_PTR,					/*NetworkorderCallout*/
		NULL_PTR,					/*CpuOrderCallout*/
	#endif
		COM_MSG_TRIGGERED,			/*TransferProperty*/
	#endif
		NULL_PTR,					/*Notification*/
		COM_MSG_RECEIVE_UNQUEUED_EXTERNAL,	      /*MsgProperty*/
		COM_MSG_UINT8,				/*CDataType*/
	},
	{
		/* ACCM_VCU_RPM */
	#if ((OSEK_COM_CCC == OSEK_COM_CCCB) || (OSEK_COM_CCC == OSEK_COM_CCC1))
		0,							 /*QueuedSize*/
	#endif
		{0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0},        /*InitValue*/
	#if (STD_OFF == COM_OPTIME_SUPPORT)
		NULL_PTR,					 /*SendMessageRef*/
	#endif
	#if ((OSEK_COM_CCC == OSEK_COM_CCC1) || (OSEK_COM_CCC == OSEK_COM_CCC0))
	#if (STD_OFF == COM_OPTIME_SUPPORT)
		NULL_PTR,					/*LinkMessageRef*/
	#endif
		&Com_NWMessage[2],		    /*NWMsgRef*/
	#endif
	#if (OSEK_COM_CCC == OSEK_COM_CCC1)
	#if (STD_ON == COM_FILTER_SUPPORT)
		&Com_Filter[2],			/*FilterRef*/
	#endif
		NULL_PTR,					/*DMErrorNotification*/
	#if (STD_OFF == COM_OPTIME_SUPPORT)
		NULL_PTR,					/*NetworkorderCallout*/
		NULL_PTR,					/*CpuOrderCallout*/
	#endif
		COM_MSG_TRIGGERED,			/*TransferProperty*/
	#endif
		NULL_PTR,					/*Notification*/
		COM_MSG_RECEIVE_UNQUEUED_EXTERNAL,	      /*MsgProperty*/
		COM_MSG_UINT16,				/*CDataType*/
	},
	{
		/* VCU_DCDC_iActHvMax */  /* VCU_DCDC_iActHvMax_NetMsg  WCM_ChargingStatus */
	#if ((OSEK_COM_CCC == OSEK_COM_CCCB) || (OSEK_COM_CCC == OSEK_COM_CCC1))
		0,							 /*QueuedSize*/
	#endif
		{0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0},        /*InitValue*/
	#if (STD_OFF == COM_OPTIME_SUPPORT)
		NULL_PTR,					 /*SendMessageRef*/
	#endif
	#if ((OSEK_COM_CCC == OSEK_COM_CCC1) || (OSEK_COM_CCC == OSEK_COM_CCC0))
	#if (STD_OFF == COM_OPTIME_SUPPORT)
		NULL_PTR,					/*LinkMessageRef*/
	#endif
		&Com_NWMessage[3],		    /*NWMsgRef*/
	#endif
	#if (OSEK_COM_CCC == OSEK_COM_CCC1)
	#if (STD_ON == COM_FILTER_SUPPORT)
		&Com_Filter[3],			/*FilterRef*/
	#endif
		NULL_PTR,					/*DMErrorNotification*/
	#if (STD_OFF == COM_OPTIME_SUPPORT)
		NULL_PTR,					/*NetworkorderCallout*/
		NULL_PTR,					/*CpuOrderCallout*/
	#endif
		COM_MSG_PENDING,			/*TransferProperty*/
	#endif
		NULL_PTR,					/*Notification*/
		COM_MSG_SEND_STATIC_EXTERNAL,	      /*MsgProperty*/
		COM_MSG_UINT8,				/*CDataType*/
	},
	{
		/* VCU_DCDC_stCtrlReq */  /* VCU_DCDC_stCtrlReq_NetMsg  WCM_PhoneReminder  */
	#if ((OSEK_COM_CCC == OSEK_COM_CCCB) || (OSEK_COM_CCC == OSEK_COM_CCC1))
		0,							 /*QueuedSize*/
	#endif
		{0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0},        /*InitValue*/
	#if (STD_OFF == COM_OPTIME_SUPPORT)
		NULL_PTR,					 /*SendMessageRef*/
	#endif
	#if ((OSEK_COM_CCC == OSEK_COM_CCC1) || (OSEK_COM_CCC == OSEK_COM_CCC0))
	#if (STD_OFF == COM_OPTIME_SUPPORT)
		NULL_PTR,					/*LinkMessageRef*/
	#endif
		&Com_NWMessage[4],		    /*NWMsgRef*/
	#endif
	#if (OSEK_COM_CCC == OSEK_COM_CCC1)
	#if (STD_ON == COM_FILTER_SUPPORT)
		&Com_Filter[4],			/*FilterRef*/
	#endif
		NULL_PTR,					/*DMErrorNotification*/
	#if (STD_OFF == COM_OPTIME_SUPPORT)
		NULL_PTR,					/*NetworkorderCallout*/
		NULL_PTR,					/*CpuOrderCallout*/
	#endif
		COM_MSG_PENDING,			/*TransferProperty*/
	#endif
		NULL_PTR,					/*Notification*/
		COM_MSG_SEND_STATIC_EXTERNAL,	      /*MsgProperty*/
		COM_MSG_UINT8,				/*CDataType*/
	},
	{
		/* VCU_DCDC_StModeReq */  /* VCU_DCDC_StModeReq_NetMsg  WCM_ShutdownFeedback */
	#if ((OSEK_COM_CCC == OSEK_COM_CCCB) || (OSEK_COM_CCC == OSEK_COM_CCC1))
		0,							 /*QueuedSize*/
	#endif
		{0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0},        /*InitValue*/
	#if (STD_OFF == COM_OPTIME_SUPPORT)
		NULL_PTR,					 /*SendMessageRef*/
	#endif
	#if ((OSEK_COM_CCC == OSEK_COM_CCC1) || (OSEK_COM_CCC == OSEK_COM_CCC0))
	#if (STD_OFF == COM_OPTIME_SUPPORT)
		NULL_PTR,					/*LinkMessageRef*/
	#endif
		&Com_NWMessage[5],		    /*NWMsgRef*/
	#endif
	#if (OSEK_COM_CCC == OSEK_COM_CCC1)
	#if (STD_ON == COM_FILTER_SUPPORT)
		&Com_Filter[5],			/*FilterRef*/
	#endif
		NULL_PTR,					/*DMErrorNotification*/
	#if (STD_OFF == COM_OPTIME_SUPPORT)
		NULL_PTR,					/*NetworkorderCallout*/
		NULL_PTR,					/*CpuOrderCallout*/
	#endif
		COM_MSG_PENDING,			/*TransferProperty*/
	#endif
		NULL_PTR,					/*Notification*/
		COM_MSG_SEND_STATIC_EXTERNAL,	      /*MsgProperty*/
		COM_MSG_UINT8,				/*CDataType*/
	},
	{
		/* VCU_DCDC_udcLvSetP */   /* VCU_DCDC_udcLvSetP_NetMsg   WCM_SwMemorySts */
	#if ((OSEK_COM_CCC == OSEK_COM_CCCB) || (OSEK_COM_CCC == OSEK_COM_CCC1))
		0,							 /*QueuedSize*/
	#endif
		{0x1c,0x0,0x0,0x0,0x0,0x0,0x0,0x0},        /*InitValue*/
	#if (STD_OFF == COM_OPTIME_SUPPORT)
		NULL_PTR,					 /*SendMessageRef*/
	#endif
	#if ((OSEK_COM_CCC == OSEK_COM_CCC1) || (OSEK_COM_CCC == OSEK_COM_CCC0))
	#if (STD_OFF == COM_OPTIME_SUPPORT)
		NULL_PTR,					/*LinkMessageRef*/
	#endif
		&Com_NWMessage[6],		    /*NWMsgRef*/
	#endif
	#if (OSEK_COM_CCC == OSEK_COM_CCC1)
	#if (STD_ON == COM_FILTER_SUPPORT)
		&Com_Filter[6],			/*FilterRef*/
	#endif
		NULL_PTR,					/*DMErrorNotification*/
	#if (STD_OFF == COM_OPTIME_SUPPORT)
		NULL_PTR,					/*NetworkorderCallout*/
		NULL_PTR,					/*CpuOrderCallout*/
	#endif
		COM_MSG_PENDING,			/*TransferProperty*/
	#endif
		NULL_PTR,					/*Notification*/
		COM_MSG_SEND_STATIC_EXTERNAL,	      /*MsgProperty*/
		COM_MSG_UINT8,				/*CDataType*/
	},
	{
		/* VCU_MotorSpd */   /* VCU_MotorSpd_NetMsg  WCM_FODWarming  */
	#if ((OSEK_COM_CCC == OSEK_COM_CCCB) || (OSEK_COM_CCC == OSEK_COM_CCC1))
		0,							 /*QueuedSize*/
	#endif
		{0x00,0x00,0x0,0x0,0x0,0x0,0x0,0x0},        /*InitValue*/
	#if (STD_OFF == COM_OPTIME_SUPPORT)
		NULL_PTR,					 /*SendMessageRef*/
	#endif
	#if ((OSEK_COM_CCC == OSEK_COM_CCC1) || (OSEK_COM_CCC == OSEK_COM_CCC0))
	#if (STD_OFF == COM_OPTIME_SUPPORT)
		NULL_PTR,					/*LinkMessageRef*/
	#endif
		&Com_NWMessage[7],		    /*NWMsgRef*/
	#endif
	#if (OSEK_COM_CCC == OSEK_COM_CCC1)
	#if (STD_ON == COM_FILTER_SUPPORT)
		&Com_Filter[7],			/*FilterRef*/
	#endif
		NULL_PTR,					/*DMErrorNotification*/
	#if (STD_OFF == COM_OPTIME_SUPPORT)
		NULL_PTR,					/*NetworkorderCallout*/
		NULL_PTR,					/*CpuOrderCallout*/
	#endif
		COM_MSG_PENDING,			/*TransferProperty*/
	#endif
		NULL_PTR,					/*Notification*/
		COM_MSG_SEND_STATIC_EXTERNAL,	      /*MsgProperty*/
		COM_MSG_UINT8,				/*CDataType*/
	},
	{
		/* VCU_Parkrequest */   /* VCU_Parkrequest_NetMsg WCM_ChargingErr */
	#if ((OSEK_COM_CCC == OSEK_COM_CCCB) || (OSEK_COM_CCC == OSEK_COM_CCC1))
		0,							 /*QueuedSize*/
	#endif
		{0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0},        /*InitValue*/
	#if (STD_OFF == COM_OPTIME_SUPPORT)
		NULL_PTR,					 /*SendMessageRef*/
	#endif
	#if ((OSEK_COM_CCC == OSEK_COM_CCC1) || (OSEK_COM_CCC == OSEK_COM_CCC0))
	#if (STD_OFF == COM_OPTIME_SUPPORT)
		NULL_PTR,					/*LinkMessageRef*/
	#endif
		&Com_NWMessage[8],		    /*NWMsgRef*/
	#endif
	#if (OSEK_COM_CCC == OSEK_COM_CCC1)
	#if (STD_ON == COM_FILTER_SUPPORT)
		&Com_Filter[8],			/*FilterRef*/
	#endif
		NULL_PTR,					/*DMErrorNotification*/
	#if (STD_OFF == COM_OPTIME_SUPPORT)
		NULL_PTR,					/*NetworkorderCallout*/
		NULL_PTR,					/*CpuOrderCallout*/
	#endif
		COM_MSG_PENDING,			/*TransferProperty*/
	#endif
		NULL_PTR,					/*Notification*/
		COM_MSG_SEND_STATIC_EXTERNAL,	      /*MsgProperty*/
		COM_MSG_UINT8,				/*CDataType*/
	},

};
#define COM_STOP_CONST_PBCFG
#include "Com_MemMap.h"

#define COM_START_CONST_PBCFG_ROOT
#include "Com_MemMap.h"
CONST(Com_ConfigType,COM_CONST_PBCFG) Com_Config =
{
#if ((OSEK_COM_CCC == OSEK_COM_CCC0) || (OSEK_COM_CCC == OSEK_COM_CCC1))
	(Com_IPduType*)&Com_IPdu[0],		/*	ipdRef*/
#endif
	(Com_MsgType*)&Com_Message[0],		/*messagePtr*/
	NULL_PTR,						/*StartComExtension*/
#if (OSEK_COM_CCC == OSEK_COM_CCC1)
	10u,							/*TimeBase*/
#endif
};
#define COM_STOP_CONST_PBCFG_ROOT
#include "Com_MemMap.h"
