/*
 * Copyright 2017 NXP
 * All rights reserved.
 *
 * THIS SOFTWARE IS PROVIDED BY NXP "AS IS" AND ANY EXPRESSED OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL NXP OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
/* ###################################################################
**     Filename    : main.c
**     Project     : freertos_mpc5748g
**     Processor   : MPC5748G_324
**     Version     : Driver 01.00
**     Compiler    : GNU C Compiler
**     Date/Time   : 2017-03-14, 14:08, # CodeGen: 1
**     Abstract    :
**         Main module.
**         This module contains user's application code.
**     Settings    :
**     Contents    :
**         No public methods
**
** ###################################################################*/
/*!
** @file main.c
** @version 01.00
** @brief
**         Main module.
**         This module contains user's application code.
*/
/*!
**  @addtogroup main_module main module documentation
**  @{
*/
/* MODULE main */


/* Including needed modules to compile this module/procedure */


/*!!!!!!Warning: users can not alter any code of main.c.!!!!!!1*/
#include "Cpu.h"
#include "FreeRTOS.h"
#include <stdint.h>
#include <stdbool.h>
#include "task.h"

#include "osif.h"

#define USR_TASK_INCLUDE
#include "usr_task_container.h"
#undef  USR_TASK_INCLUDE
//#include "os_task_container.h"
#include "project_cfg.h"

#include "app_demo.h"

/*Task period time definition*/
#define OS_TASK_PERIOD_1MS                   1u
#define OS_TASK_PERIOD_2MS                   2u
#define OS_TASK_PERIOD_5MS                   5u
#define OS_TASK_PERIOD_10MS                  10u
#define OS_TASK_PERIOD_20MS                  20u
#define OS_TASK_PERIOD_50MS                  50u
#define OS_TASK_PERIOD_100MS                 100u

/*Task priority  definition*/

#define OS_1MS_TASK_PRIORITY                          (tskIDLE_PRIORITY + 6)
#define OS_2MS_TASK_PRIORITY                          (tskIDLE_PRIORITY + 5)
#define OS_5MS_TASK_PRIORITY                          (tskIDLE_PRIORITY + 4)
#define OS_10MS_TASK_PRIORITY                         (tskIDLE_PRIORITY + 3)
#define OS_20MS_TASK_PRIORITY                         (tskIDLE_PRIORITY + 2)
#define OS_50MS_TASK_PRIORITY                         (tskIDLE_PRIORITY + 1)
#define OS_100MS_TASK_PRIORITY                        (tskIDLE_PRIORITY + 0)

/*Task stack size definition*/
#define OS_Init_TASK_STACK_SIZE                              configMINIMAL_STACK_SIZE
#define OS_1MS_TASK_STACK_SIZE                               configMINIMAL_STACK_SIZE
#define OS_2MS_TASK_STACK_SIZE                               configMINIMAL_STACK_SIZE
#define OS_5MS_TASK_STACK_SIZE                               configMINIMAL_STACK_SIZE
#define OS_10MS_TASK_STACK_SIZE                              configMINIMAL_STACK_SIZE
#define OS_20MS_TASK_STACK_SIZE                              configMINIMAL_STACK_SIZE
#define OS_50MS_TASK_STACK_SIZE                              configMINIMAL_STACK_SIZE
#define OS_100MS_TASK_STACK_SIZE                             configMINIMAL_STACK_SIZE


/*OS task prototype */

static void OS_Task_1ms( void *pvParameters );
static void OS_Task_2ms( void *pvParameters );
static void OS_Task_5ms( void *pvParameters );
static void OS_Task_10ms( void *pvParameters );
static void OS_Task_20ms( void *pvParameters );
static void OS_Task_50ms( void *pvParameters );
static void OS_Task_100ms( void *pvParameters );


/*!
  \brief The main function for the project.
  \details The startup initialization sequence is the following:
 * - startup asm routine
 * - main()
*/
int main(void)
{

    /*------MCU bsp init--------------*/
#define USR_MCU_INIT_TAG
#include "usr_task_container.h"
#undef USR_MCU_INIT_TAG



    /*------BSW stack init-------------*/


    /*------FreeRTOS Task Creation--------*/

	xTaskCreate( OS_Task_1ms, ( const char * const ) "ACU_1ms_task", OS_1MS_TASK_STACK_SIZE, NULL, OS_1MS_TASK_PRIORITY, NULL );
	xTaskCreate( OS_Task_2ms, ( const char * const ) "ACU_2ms_task", OS_2MS_TASK_STACK_SIZE, NULL, OS_2MS_TASK_PRIORITY, NULL );
	xTaskCreate( OS_Task_5ms, ( const char * const ) "ACU_5ms_task", OS_5MS_TASK_STACK_SIZE, NULL, OS_5MS_TASK_PRIORITY, NULL );
	xTaskCreate( OS_Task_10ms, ( const char * const ) "ACU_10ms_task", OS_10MS_TASK_STACK_SIZE, NULL, OS_10MS_TASK_PRIORITY, NULL );
	xTaskCreate( OS_Task_20ms, ( const char * const ) "ACU_20ms_task", OS_20MS_TASK_STACK_SIZE, NULL, OS_20MS_TASK_PRIORITY, NULL );
	xTaskCreate( OS_Task_50ms, ( const char * const ) "ACU_50ms_task", OS_50MS_TASK_STACK_SIZE, NULL, OS_50MS_TASK_PRIORITY, NULL );
	xTaskCreate( OS_Task_100ms, ( const char * const ) "ACU_100ms_task", OS_100MS_TASK_STACK_SIZE, NULL, OS_100MS_TASK_PRIORITY, NULL );

	vTaskStartScheduler();
    for(;;)
    {

    }
    return 0;
   
} /*** End of main routine. DO NOT MODIFY THIS TEXT!!! ***/


/*os task object with 1ms period*/
static void OS_Task_1ms( void *pvParameters )
{
	portTickType xLastWakeTime;

	xLastWakeTime = xTaskGetTickCount();
    for( ;; )
    {

#define USR_TASK_1MS_TAG
#include "usr_task_container.h"
#undef  USR_TASK_1MS_TAG


    	vTaskDelayUntil(&xLastWakeTime, OS_TASK_PERIOD_1MS);
    }

}




/*os task object with 2ms period*/
static void OS_Task_2ms( void *pvParameters )
{
	portTickType xLastWakeTime;
	xLastWakeTime = xTaskGetTickCount();
    for( ;; )
    {

#define USR_TASK_2MS_TAG
#include "usr_task_container.h"
#undef  USR_TASK_2MS_TAG

    	vTaskDelayUntil(&xLastWakeTime, OS_TASK_PERIOD_2MS);

    }

}


/*os task object with 5ms period*/
static void OS_Task_5ms( void *pvParameters )
{
	portTickType xLastWakeTime;
	xLastWakeTime = xTaskGetTickCount();
    for( ;; )
    {

#define USR_TASK_5MS_TAG
#include "usr_task_container.h"
#undef  USR_TASK_5MS_TAG

    	vTaskDelayUntil(&xLastWakeTime, OS_TASK_PERIOD_5MS);
    }

}

/*os task object with 10ms period*/
static void OS_Task_10ms( void *pvParameters )
{
	portTickType xLastWakeTime;
	xLastWakeTime = xTaskGetTickCount();
    for( ;; )
    {

#define USR_TASK_10MS_TAG
#include "usr_task_container.h"
#undef  USR_TASK_10MS_TAG


    	vTaskDelayUntil(&xLastWakeTime, OS_TASK_PERIOD_10MS);
    }
}
/*os task object with 20ms period*/
static void OS_Task_20ms( void *pvParameters )
{

	portTickType xLastWakeTime;
	xLastWakeTime = xTaskGetTickCount();
    for( ;; )
    {

#define USR_TASK_20MS_TAG
#include "usr_task_container.h"
#undef  USR_TASK_20MS_TAG

    	vTaskDelayUntil(&xLastWakeTime, OS_TASK_PERIOD_20MS);

    }
}



/*os task object with 50ms period*/
static void OS_Task_50ms( void *pvParameters )
{
	portTickType xLastWakeTime;
	xLastWakeTime = xTaskGetTickCount();
    for( ;; )
    {

#define USR_TASK_50MS_TAG
#include "usr_task_container.h"
#undef  USR_TASK_50MS_TAG

    	vTaskDelayUntil(&xLastWakeTime, OS_TASK_PERIOD_50MS);

    }
}



/*os task object with 100ms period*/
static void OS_Task_100ms( void *pvParameters )
{
	portTickType xLastWakeTime;
	xLastWakeTime = xTaskGetTickCount();

    for( ;; )
    {

#define USR_TASK_100MS_TAG
#include "usr_task_container.h"
#undef  USR_TASK_100MS_TAG

    	vTaskDelayUntil(&xLastWakeTime, OS_TASK_PERIOD_100MS);

    }
}


/* END main */
/*!
** @}
*/
