/*============================================================================*/
/*  Copyright (C) 2009-2013,  INFRASTRUCTURE SOFTWARE CO.,LTD.
 *  
 *  All rights reserved. This software is  property. Duplication
 *  or disclosure without  written authorization is prohibited.
 *  
 *  
 *  @file       PduR_CanTp.h
 *  @brief      header file for DCM module from PDUR
 *  
 *  
 *  @author     stanley
 *  @date       2013-4-7
 */
/*============================================================================*/

#ifndef PDUR_DCM_H
#define PDUR_DCM_H

/****************************** references *********************************/
#include "CanTp.h"

#ifdef __cplusplus
extern "C" {
#endif  /* __cplusplus */

#define PDUR_CANTP_H_AR_MAJOR_VERSION  2
#define PDUR_CANTP_H_AR_MINOR_VERSION  3
/****************************** declarations *********************************/

#define PduR_DcmTransmit CanTp_Transmit

/****************************** definitions *********************************/

#ifdef __cplusplus
}
#endif  /* __cplusplus */

#endif /* PDUR_DCM_H */
