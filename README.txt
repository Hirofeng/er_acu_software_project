﻿*** 2019.4.12   Author: Fengyuan   Version: 1.0.0****
1.已集成FreeRTOS,为用户提供任务分配模版文件usr_task_container.h
2.本版软件不支持Processor expert代码自动生成
3.Doc目录内添加ER_ACU软件集成手册.docx手册，指导用户如何使用基础软件；ACU_UDS_CAN接口需求.doc手册描述了UDS协议栈用到的Can接口，需要SVW来实现并提交can.c文件



*** 2019.5.13   Author: Fengyuan   Version: 1.1.0****
1.集成UDS/COM通信协议栈软件、UDSnCOM用户集成手册
2.FreeRTOS中每个周期任务的定时函数由vTaskDelay更改为vTaskDelayUntil，实现精准周期定时
