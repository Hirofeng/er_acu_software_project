/************************************************************************
*				        ESEC UESTC
* 	 Copyright (C) 2005-2011 ESEC UESTC. All Rights Reserved.
***********************************************************************/

/**
 * Log:  
 * Revision 1.0 2011-5-6����08:37:24 stanley
 * description: create
 *
 */

/**
 * @file 	Dem_IntEvtId.h
 * @brief
 *	<li>Function��event identifiers definition for diagnostic events </li>
 *  <li>Design Points��</li>
 *  <p>
 *  
 *  </p>
 * @author 	stanley
 * @date 	2011-5-6
 * 
 */

#ifndef DEM_INTEVTID_H_
#define DEM_INTEVTID_H_

/****************************** references *********************************/

#ifdef __cplusplus
extern "C" {
#endif  /* __cplusplus */

/****************************** declarations *********************************/

/****************************** definitions *********************************/

#ifdef __cplusplus
}
#endif  /* __cplusplus */

#endif /* DEM_INTEVTID_H_ */
