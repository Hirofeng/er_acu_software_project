/*===========================================================================
        Copyright (C) 2009-2011,  INFRASTRUCTURE SOFTWARE CO.,LTD.

        All rights reserved. This software is  property. Duplication
        or disclosure without  written authorization is prohibited.

 ===========================================================================*/
#ifndef MEMIF_TYPES_H_
#define MEMIF_TYPES_H_

/*@req <MEMIF009>
*/
/*@req <MEMIF010>
*/


#include "Std_Types.h"

/*==========================================================================*/
/*		G L O B A L   D E F I N I T I O N S												  */
/*==========================================================================*/

/*==========================================================================*/
/*
	ITEM NAME:		<MemIf_StatusType>
	SCOPE:			<MemIf Module>
	DESCRIPTION:
		Denotes the current status of the underlying abstraction module and device drive.It shall be used 
		as the return value of the corresponding driver's "GetStatus" function.
*/

/*@req <MEMIF015>
*/

typedef enum{
	/*	The underlying abstraction module or device driver has not been initialized (yet).*/
	MEMIF_UNINIT,
	
	/* The underlying abstraction module or device driver is currently idle.*/
	MEMIF_IDLE,
	
	/* The underlying abstraction module or device driver is currently busy.*/
	MEMIF_BUSY,
	
	/*	The underlying abstraction module is busy with internal management operations. The underlying 
	device driver can be busy or idle.*/
	MEMIF_BUSY_INTERNAL              
}MemIf_StatusType;

/*==========================================================================*/
/*
	ITEM NAME:		<MemIf_JobResultType>
	SCOPE:			<MemIf Module>
	DESCRIPTION:
		Denotes the result of the last job.
*/

/*@req <MEMIF016>
*/

typedef enum {
	/*The job has been finished successfully.*/
	MEMIF_JOB_OK,
	
	/* The job has not been finished successfully.*/
	MEMIF_JOB_FAILED,
	
	/*	The job has not yet been finished.*/
	MEMIF_JOB_PENDING,
	
	/*	The job has been cancelled.*/
	MEMIF_JOB_CANCELLED,

	/*	The compare job is not equal*/
	MEMIF_COMPARE_UNEQUAL,
	
	/*	The requested block is inconsistent, it may contain corrupted data.*/
	MEMIF_BLOCK_INCONSISTENT,
	
	/* The requested block has been marked as invalid,	the requested operation can not be performed.*/
	MEMIF_BLOCK_INVALID, 
} MemIf_JobResultType; 

/*==========================================================================*/
/*
	ITEM NAME:		<MemIf_ModeType>
	SCOPE:			<MemIf Module>
	DESCRIPTION:
		Denotes the operation mode of the underlying abstraction modules and device drivers.
*/

/*@req <MEMIF021>
*/

typedef enum {
	/*    The underlying memory abstraction modules and drivers are working in slow mode.*/
	MEMIF_MODE_SLOW,
	
	/* The underlying memory abstraction modules and drivers are working in fast mode.*/
    MEMIF_MODE_FAST,
} MemIf_ModeType;

/*==========================================================================*/
/*
	ITEM NAME:		<MEMIF_BROADCAST_ID>
	SCOPE:			<MemIf Module>
	DESCRIPTION:
		define MemIf Module broadcast ID of device ID.  	
*/

/*@req <MEMIF036>
*/

#define MEMIF_BROADCAST_ID		255



#endif

/*==========================================================================*/
/*		E N D   O F   F I L E															  */
/*==========================================================================*/

