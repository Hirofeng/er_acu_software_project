/*============================================================================*/
/*  Copyright (C) 2009-2011,  INFRASTRUCTURE SOFTWARE CO.,LTD.
 *  
 *  All rights reserved. This software is  property. Duplication
 *  or disclosure without  written authorization is prohibited.
 *  
 *  
 *  @file       <filename>
 *  @brief      <Briefly describe file(one line)>
 *  
 *  <Detailed description of the file(multi-line)>
 *  
 *  @author     <yuxin.zhao>
 *  @date       <14-03-2013>
 */
/*============================================================================*/


#ifndef ECUM_CBK_H
#define ECUM_CBK_H

/*=======[I N C L U D E S]====================================================*/
#include "Std_Types.h"     /* To include AUTOSAR standard types */
#include "EcuM.h"          /* EcuM Header File */

/*=======[M A C R O S]========================================================*/
#define ECUM_AR_MAJOR_VERSION  1
#define ECUM_AR_MINOR_VERSION  2


#define  EcumWkSourcePower           (EcuM_WakeupSourceType)0  
#define  EcumWkSourceReset           (EcuM_WakeupSourceType)16
#define  ECUM_WKSOURCE_TIMER_TAA00   (EcuM_WakeupSourceType)32         
#define  CAN_WKP_SRC_1               (EcuM_WakeupSourceType)2
#define  CAN_WKP_SRC_2               (EcuM_WakeupSourceType)4
#define  CAN_WKP_SRC_3               (EcuM_WakeupSourceType)6
#define  CAN_WKP_SRC_4               (EcuM_WakeupSourceType)8    

/*=======[T Y P E   D E F I N I T I O N S]====================================*/


/*=======[I N T E R N A L   D A T A]==========================================*/



/*=======[I N T E R N A L   F U N C T I O N   D E C L A R A T I O N S]========*/

         

/*=======[E X T E R N A L   F U N C T I O N   D E C L A R A T I O N S]========*/

extern FUNC(void, EcuM_CODE)EcuM_SetWakeupEvent(EcuM_WakeupSourceType WakeupSource);

extern FUNC(void, EcuM_CODE)EcuM_ValidateWakeupEvent(EcuM_WakeupSourceType WakeupSource);

extern FUNC(void, EcuM_CODE) EcuM_CheckWakeup(EcuM_WakeupSourceType WakeupSource);



#endif /* ECUM_CBK_H */

/*=======[R E V I S I O N   H I S T O R Y]====================================*/
/*  <VERSION>  <DATE>  <AUTHOR>     <REVISION LOG>
 *  V1.0    20081127    James       Initial version
 * 
 *  V1.1    20090316    Z.BG      <Long descritpion can be multi-lined as below> 
 *          The following changes are made as per ADC127:
 *          1. Adc_GstHwUnitRamData is changed to Adc_GpHwUnitRamData and 
 *             Adc_GstGroupRamData is changed to Adc_GpGroupRamData.
 *          2. In API Adc_StopGroupConversion, check for the HW unit busy 
 *             status is corrected.
 *          3. Updated comments for APIs Adc_ReadGroup, Adc_SetupResultBuffer,
 *             Adc_DisableHardwareTrigger and Adc_EnableGroupNotification.
 * 
 *  V1.2    20091012    Michael     As per ADC171, ADC_E_NO_DB is changed
 *                                  to ADC_E_INVALID_DATABASE.
 * 
 *  V2.0    20100210    Z.YX        As per SCR 186, Adc_Init and Adc_DeInit
 *                                  are updated to use IMRm register instead
 *                                  of xxICn register.
 */
/*=======[E N D   O F   F I L E]==============================================*/
