/*
 * Copyright 2017 NXP
 * All rights reserved.
 *
 * THIS SOFTWARE IS PROVIDED BY NXP "AS IS" AND ANY EXPRESSED OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL NXP OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * @page misra_violations MISRA-C:2012 violations
 *
 * @section [global]
 * Violates MISRA 2012 Advisory Rule 8.13,  Pointer parameter 'icParam' could be declared as pointing to const
 * This is a pointer to the driver context structure which is for internal use only, and the application
 * must make no assumption about the content of this structure. Therefore it is irrelevant for the application
 * whether the structure is changed in the function or not. The fact that in a particular implementation of some
 * functions there is no write in the context structure is an implementation detail and there is no reason to
 * propagate it in the interface. That would compromise the stability of the interface, if this implementation
 * detail is changed in later releases or on other platforms.
 *
 * @section [global]
 * Violates MISRA 2012 Advisory Rule 8.7, External could be made static.
 * Function is defined for usage by application code.
 *
 */

#include "emios_ic_driver.h"
#include "emios_mc_driver.h"
#include "emios_hw_access.h"
/*******************************************************************************
 * Definitions
 ******************************************************************************/

/******************************************************************************
* API
******************************************************************************/

/*FUNCTION**********************************************************************
 *
 * Function Name : EMIOS_DRV_IC_InitInputCaptureMode
 * Description   : Initialize Input Measurement Mode or Single Action Input Capture mode
 * - Input Measurement Mode
 * This mode allows the measurement of the width of a positive or negative pulse or period of an input signal by
 * capturing two consecutive rising edges or two consecutive falling edges.
 * In period measurement mode: Successive input captures are done on consecutive edges of the same polarity.
 * The input signal must have at least four system clock cycles period in order to be properly
 * captured by the synchronization logic at the channel input even if the input filter is in by-pass mode.
 * - Single Action Input Capture mode
 * In SAIC mode, when a triggering event occurs on the input pin, the value on the selected time base is captured.
 * Implements    : EMIOS_DRV_IC_InitInputCaptureMode_Activity
 *END**************************************************************************/
status_t EMIOS_DRV_IC_InitInputCaptureMode(uint8_t emiosGroup,
                                           uint8_t channel,
                                           emios_input_capture_param_t *icParam)
{
    DEV_ASSERT(emiosGroup < EMIOS_NUMBER_GROUP_MAX);
    DEV_ASSERT(channel < EMIOS_NUMBER_CHANNEL_MAX);
    DEV_ASSERT(icParam != NULL);
    DEV_ASSERT(icParam->mode == EMIOS_MODE_IC);
    status_t ret = STATUS_SUCCESS;
    uint32_t value = 0U;

#if defined (CUSTOM_DEVASSERT) || defined (DEV_ERROR_DETECT)
    bool validMCMode = false;
    bool validMCBMode = false;
    uint32_t temp;
    /* Valid MC or MCB with channel supported */
    validMCMode  = EMIOS_ValidateMode(emiosGroup, channel, (uint8_t)EMIOS_GMODE_MC);
    validMCBMode = EMIOS_ValidateMode(emiosGroup, channel, (uint8_t)EMIOS_GMODE_MCB);
    temp = EMIOS_GetUCRegCMode( emiosGroup, channel);

    /* Check that device was initialized */
    DEV_ASSERT((eMIOS[emiosGroup]->UC[channel].C == 0UL) ||
               (((validMCMode == true) || (validMCBMode == true)) && (temp == (uint32_t)EMIOS_MODE_MCB_UP_COUNTER_INT_CLK)) ||
               (temp == (uint32_t)EMIOS_MODE_PULSE_WIDTH) ||
               (temp == (uint32_t)EMIOS_MODE_PERIOD) ||
               (temp == (uint32_t)EMIOS_MODE_SAIC));
#endif /* defined (CUSTOM_DEVASSERT) || defined (DEV_ERROR_DETECT) */

    if ((icParam->inputCaptureMode == EMIOS_PERIOD_ON_MEASUREMENT) ||
        (icParam->inputCaptureMode == EMIOS_PERIOD_OFF_MEASUREMENT))
    {
        /* Valid Input Pulse Width Measurement with channels supported */
        if (EMIOS_ValidateMode(emiosGroup, channel, (uint8_t)EMIOS_GMODE_IPWM) == false)
        {
             ret = STATUS_ERROR;
        }
        else
        {
            /* Configure registers */
            if (icParam->inputCaptureMode == EMIOS_PERIOD_ON_MEASUREMENT)
            {
                EMIOS_SetUCRegCEdpol(emiosGroup, channel, (uint32_t)EMIOS_POSITIVE_PULSE);
            }
            else
            {
                EMIOS_SetUCRegCEdpol(emiosGroup, channel, (uint32_t)EMIOS_NEGATIVE_PULSE);
            }

            if (icParam->timebase == EMIOS_BUS_SEL_INTERNAL)
            {
                /* Choose internal counter, this channel mode should be Type X or G */
                if (EMIOS_ValidateInternalCnt(emiosGroup, channel) == false)
                {
                    ret = STATUS_ERROR;
                }
            }
            else
            {
                /* Choose external counter */
            }

            value = (uint32_t)EMIOS_MODE_PULSE_WIDTH;
        }
    }
    else if ((icParam->inputCaptureMode == EMIOS_RISING_EDGE_PERIOD_MEASUREMENT) ||
             (icParam->inputCaptureMode == EMIOS_FALLING_EDGE_PERIOD_MEASUREMENT))
    {
        /* Valid Input Period Measurement with channels supported */
        if (EMIOS_ValidateMode(emiosGroup, channel, (uint8_t)EMIOS_GMODE_IPM) == false)
        {
            ret = STATUS_ERROR;
        }
        else
        {
            /* Validate 4 system clock cycles period at least in non by-pass mode */
            /* Configure registers */
            if (icParam->inputCaptureMode == EMIOS_RISING_EDGE_PERIOD_MEASUREMENT)
            {
                EMIOS_SetUCRegCEdpol(emiosGroup, channel, EMIOS_RISING_EDGE);
            }
            else
            {
                EMIOS_SetUCRegCEdpol(emiosGroup, channel, EMIOS_FALLING_EDGE);
            }

            if (icParam->timebase == EMIOS_BUS_SEL_INTERNAL)
            {
                /* Choose internal counter, this channel mode should be Type X or G */
                if (EMIOS_ValidateInternalCnt(emiosGroup, channel) == false)
                {
                    ret = STATUS_ERROR;
                }
            }
            else
            {
                /* Choose external counter */
            }

            value = (uint32_t)EMIOS_MODE_PERIOD;
        }
    }
    else
    {
        /* Configure registers */
        eMIOS[emiosGroup]->UC[channel].C = 0UL; /* Disable channel pre-scaler (reset default) */
        EMIOS_SetUCRegCEdsel(emiosGroup, channel, ((((uint8_t)(icParam->inputCaptureMode) & 0x02U) == 0U) ? 0UL : 1UL));
        EMIOS_SetUCRegCEdpol(emiosGroup, channel, ((((uint8_t)(icParam->inputCaptureMode) & 0x01U) == 0U) ? 0UL : 1UL));

        /* Validate internal counter with selected channel */
        if (icParam->timebase == EMIOS_BUS_SEL_INTERNAL)
        {
            /* Choose internal counter, this channel mode should be Type X or G */
            if (EMIOS_ValidateInternalCnt(emiosGroup, channel) == false)
            {
                ret = STATUS_ERROR;
            }
        }
        else
        {
            /* Choose external counter */
        }
        value = (uint32_t)EMIOS_MODE_SAIC;
    }

    if (ret == STATUS_SUCCESS)
    {
        EMIOS_SetUCRegCBsl(emiosGroup, channel, (uint32_t)icParam->timebase);
        EMIOS_SetUCRegCMode(emiosGroup, channel, value);
        EMIOS_SetUCRegCIf(emiosGroup, channel, (uint32_t)icParam->filterInput);
        EMIOS_SetUCRegCFck(emiosGroup, channel, icParam->filterEn ? 0UL : 1UL);
        EMIOS_SetUCRegCUcpren(emiosGroup, channel, 1U);
    }

    return ret;
}

/*FUNCTION**********************************************************************
 *
 * Function Name : EMIOS_DRV_IC_GetLastMeasurement
 * Description   : Get last measurement value in input capture measurement mode or
 * get Input capture value in SAIC mode.
 * Implements    : EMIOS_DRV_IC_GetLastMeasurement_Activity
 *END**************************************************************************/
status_t EMIOS_DRV_IC_GetLastMeasurement(uint8_t emiosGroup,
                                         uint8_t channel,
                                         uint32_t *retValue)
{
    uint8_t busSelect;
    uint32_t temp;
    status_t retStatus = STATUS_SUCCESS;

    DEV_ASSERT(emiosGroup < EMIOS_NUMBER_GROUP_MAX);
    DEV_ASSERT(channel < EMIOS_NUMBER_CHANNEL_MAX);
    DEV_ASSERT(retValue != NULL);

    /* Checking mode was input capture mode*/
    temp = EMIOS_GetUCRegCMode(emiosGroup, channel);
    if((temp != (uint32_t)EMIOS_MODE_PERIOD) && (temp != (uint32_t)EMIOS_MODE_PULSE_WIDTH) && (temp != (uint32_t)EMIOS_MODE_SAIC))
    {
        retStatus = STATUS_ERROR;
    }
    else
    {
        if (temp == EMIOS_MODE_SAIC)
        {
            *retValue = EMIOS_GetUCRegA(emiosGroup, channel);
        }
        else
        {
            uint32_t secondCnt = EMIOS_GetUCRegA(emiosGroup, channel);
            uint32_t firstCnt = EMIOS_GetUCRegB(emiosGroup, channel);

            /* Normal Case */
            if (firstCnt <= secondCnt)
            {
                *retValue = secondCnt - firstCnt;
            }
            else
            {
                busSelect = (uint8_t)EMIOS_CNT_BUSA_DRIVEN;

                if (EMIOS_GetUCRegCBsl(emiosGroup, channel) == (uint32_t)EMIOS_BUS_SEL_A)
                {
                    busSelect = (uint8_t)EMIOS_CNT_BUSA_DRIVEN;
                }
                else if (EMIOS_GetUCRegCBsl(emiosGroup, channel) == (uint32_t)EMIOS_BUS_SEL_F)
                {
                    busSelect = (uint8_t)EMIOS_CNT_BUSF_DRIVEN;
                }
                else if (EMIOS_GetUCRegCBsl(emiosGroup, channel) == (uint32_t)EMIOS_BUS_SEL_BCDE)
                {
                    busSelect = (uint8_t)(channel & 0xF8U);
                }
                else
                {
                    busSelect = channel;
                }
                /* if counter bus selected is Up mode */
                *retValue = (EMIOS_GetUCRegA(emiosGroup, busSelect) - firstCnt) + secondCnt;
            }
        }
    }

    return retStatus;
}

/*******************************************************************************
* EOF
******************************************************************************/
