/*============================================================================*/
/*  Copyright (C) 2009-2013, iSOFT INFRASTRUCTURE SOFTWARE CO.,LTD.
 *  
 *  All rights reserved. This software is iSOFT property. Duplication
 *  or disclosure without iSOFT written authorization is prohibited.
 *  
 *  
 *  @file       
 *  @brief      
 *  
 *  
 *  @author     chenms
 *  @date       2013-4-7
 */
/*============================================================================*/
/******************************* references ************************************/
#include "Rte_Dcm.h"
#include "Dem.h"
#include "FreeRTimer.h"


static uint32 bl_extds_seed= 0xbeafdada;
/*******function for Protocol Start and stop***********/
Std_ReturnType  Rte_StartProtocol(Dcm_ProtocolType  ProtocolID)
{
	  return E_OK;
}

Std_ReturnType  Rte_StopProtocol(Dcm_ProtocolType  ProtocolID)
{
	  return E_OK;
}

uint8 SXOR[4]={0x21,0X10,0XD1,0XC6};
/*******function for security access***********/
/*安全级1*/
Std_ReturnType  Rte_CompareKey1(uint8 *key)
{

	uint8 Seed[4]={0},i;
	uint8 Cal[4]={0};
	uint8 Key[4]={0};


	Seed[0] = (uint8)(bl_extds_seed>>24);
	Seed[1] = (uint8)(bl_extds_seed>>16);
	Seed[2] = (uint8)(bl_extds_seed>>8);
	Seed[3] = (uint8)(bl_extds_seed);




	for(i=0;i<4;i++)
	{
		Cal[i]=Seed[i]^SXOR[i];
	}
	Key[0]=((Cal[0]&0x0f)<<4)|(Cal[1]&0xf0);
	Key[1]=((Cal[1]&0x0f)<<4)|((Cal[2]&0xf0)>>4);
	Key[2]=(Cal[2]&0xf0)|((Cal[3]&0xf0)>>4);
	Key[3]=((Cal[3]&0x0f)<<4)|(Cal[0]&0x0f);

	if((key[0]==Key[0])&&
		key[1]==Key[1]&&
	    key[2]==Key[2]&&
	    key[3]==Key[3])
	{


	}
	else
	{
		return E_COMPARE_KEY_FAILED;
	}

	  return E_OK;
}



Std_ReturnType  Rte_GetSeed1(uint8 *SecurityAccessRecord,uint8 *Seed,Dcm_NegativeResponseCodeType *ErrorCode)
{


	uint32 u32Seed; /* intentional not initialized to use the stack random value 用RAM随机数*/
	uint32 u32Time;
//	*ErrorCode = DCM_E_POSITIVE_RESPONSE;
//	(void)securityAccessDataRecord;
//	*ErrorCode = DCM_E_POSITIVE_RESPONSE;


	u32Time = Frt_ReadOutTicks();
	bl_extds_seed = bl_extds_seed ^ u32Seed ^ u32Time ^ 0x95774321;



	Seed[0] = (uint8)(bl_extds_seed>>24);
	Seed[1] = (uint8)(bl_extds_seed>>16);
	Seed[2] = (uint8)(bl_extds_seed>>8);
	Seed[3] = (uint8)(bl_extds_seed);



	return E_OK;

}
/*安全级3*/
Std_ReturnType  Rte_CompareKey3(uint8 *key)
{
      if(  (0x33 != key[0])
         ||(0x44 != key[1])
         ||(0x55 != key[2])
         ||(0x66 != key[3]))
      {
      	return E_COMPARE_KEY_FAILED;
      }
	  return E_OK;
}
Std_ReturnType  Rte_GetSeed3(uint8 *SecurityAccessRecord,uint8 *Seed,Dcm_NegativeResponseCodeType *ErrorCode)
{
      Seed[0] = 0x03;
      Seed[1] = 0x04;
      Seed[2] = 0x05;
      Seed[3] = 0x06;      
	  return E_OK;
}
/*安全级11*/
Std_ReturnType  Rte_CompareKey11(uint8 *key)
{
      //key=0x778899AA
	  return E_OK;
}
Std_ReturnType  Rte_GetSeed11(uint8 *SecurityAccessRecord,uint8 *Seed,Dcm_NegativeResponseCodeType *ErrorCode)
{
      Seed[0] = 0x11;
      Seed[1] = 0x12;
      Seed[2] = 0x13;
      Seed[3] = 0x14;      
	  return E_OK;
}
/*存储连续请求种子和密钥比较错误的错误计数到Flash或EEPROM*/
Std_ReturnType  Rte_StoreFalseCounter(uint8  falseCounter)
{
   return E_OK;
}

/*从Flash或EEP读取连续请求种子和密钥比较错误的错误计数*/
Std_ReturnType  Rte_ReadFalseCounter(uint8  *pfalseCounter)
{
   (*pfalseCounter) = 0u;
   return E_OK;
}

/*************functions for session control service*****************/
Std_ReturnType  Rte_DcmSessChgIndication(Dcm_SesType  SesCtrlTypeOld,   Dcm_SesType  SesCtrlTypeNew)
{
	  return E_OK;
}


Std_ReturnType  Rte_DcmGetSessChgPermission(Dcm_SesType  SesCtrlTypeActive,Dcm_SesType  SesCtrlTypeNew)
{
      if(0x02 == SesCtrlTypeNew)
      {
      	/*接收到编程会话请求*/

      	 return E_FORCE_RCRRP;
      }
	  return E_OK;
}

/***********call back functions called by DCM when a service request received******/
Std_ReturnType  Rte_DcmServiceReqIndication(uint8 SID,uint8 *RequestData,uint16 DataSize)
{
	return E_OK;
}

/*************callback functions for DID services*************/

/*DID = 0xF189*/
Std_ReturnType Rte_DidConditionCheckRead_F189(Dcm_NegativeResponseCodeType* ErrorCode)
{
	return E_OK;
}

Std_ReturnType Rte_DidConditionCheckWrite_F189(Dcm_NegativeResponseCodeType* ErrorCode)
{
	return E_OK;
}

uint8  vehicleManufacturerECUSoftw[21] = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21};
//uint8  vehicleManufacturerECUSoftw[21] = {0};
Std_ReturnType Rte_DidReadData_F189(uint8* data)
{
    data[0] = vehicleManufacturerECUSoftw[0];
    data[1] = vehicleManufacturerECUSoftw[1];
    data[2] = vehicleManufacturerECUSoftw[2];
    data[3] = vehicleManufacturerECUSoftw[3];
    data[4] = vehicleManufacturerECUSoftw[4];
    data[5] = vehicleManufacturerECUSoftw[5];
    data[6] = vehicleManufacturerECUSoftw[6];
    data[7] = vehicleManufacturerECUSoftw[7];
    data[8] = vehicleManufacturerECUSoftw[8];
    data[9] = vehicleManufacturerECUSoftw[9];
    data[10] = vehicleManufacturerECUSoftw[10];
    data[11] = vehicleManufacturerECUSoftw[11];
    data[12] = vehicleManufacturerECUSoftw[12];
    data[13] = vehicleManufacturerECUSoftw[13];
    data[14] = vehicleManufacturerECUSoftw[14];
    data[15] = vehicleManufacturerECUSoftw[15];
    data[16] = vehicleManufacturerECUSoftw[16];
    data[17] = vehicleManufacturerECUSoftw[17];
    data[18] = vehicleManufacturerECUSoftw[18];
    data[19] = vehicleManufacturerECUSoftw[19];
    data[20] = vehicleManufacturerECUSoftw[20];
   
	return E_OK;
}

Std_ReturnType Rte_DidWriteData_F189(	uint8* data,
            							uint16 dataLength,
            							Dcm_NegativeResponseCodeType* ErrorCode)
{
    uint16  Index;
#if 0
    if(20 != dataLength)
    {
        (*ErrorCode) = 0x22;
    	return E_NOT_OK;
    }
    if(	  (0x10 != data[0]) 
    	&&(0x1A != data[10])
    	&&(0x23 != data[19]))
    {
        (*ErrorCode) = 0x22;
    	return E_NOT_OK;
	
    }
#endif
    for(Index=0;Index<dataLength;Index++)
    {
    	vehicleManufacturerECUSoftw[Index] = 	data[Index];
    }
    return E_OK;
}
/*DID = 0xF089*/
Std_ReturnType Rte_DidConditionCheckRead_F089(Dcm_NegativeResponseCodeType* ErrorCode)
{
	return E_OK;
}

Std_ReturnType Rte_DidConditionCheckWrite_F089(Dcm_NegativeResponseCodeType* ErrorCode)
{
	return E_OK;
}

uint8  vehicleManufacturerECUHardw[8] = {1,2,3,4,5,6,7,8};
//uint8  vehicleManufacturerECUHardw[8] = {0};
Std_ReturnType Rte_DidReadData_F089(uint8* data)
{
    data[0] = vehicleManufacturerECUHardw[0];
    data[1] = vehicleManufacturerECUHardw[1];
    data[2] = vehicleManufacturerECUHardw[2];
    data[3] = vehicleManufacturerECUHardw[3];
    data[4] = vehicleManufacturerECUHardw[4];
    data[5] = vehicleManufacturerECUHardw[5];
    data[6] = vehicleManufacturerECUHardw[6];
    data[7] = vehicleManufacturerECUHardw[7];
	return E_OK;
}

Std_ReturnType Rte_DidWriteData_F089(	uint8* data,
            							uint16 dataLength,
            							Dcm_NegativeResponseCodeType* ErrorCode)
{
    uint16  Index;
#if 0
    if(20 != dataLength)
    {
        (*ErrorCode) = 0x22;
    	return E_NOT_OK;
    }
    if(	  (0x10 != data[0]) 
    	&&(0x1A != data[10])
    	&&(0x23 != data[19]))
    {
        (*ErrorCode) = 0x22;
    	return E_NOT_OK;
	
    }
#endif
    for(Index=0;Index<dataLength;Index++)
    {
    	vehicleManufacturerECUHardw[Index] = 	data[Index];
    }
    return E_OK;
}
/*DID = 0xF187*/
Std_ReturnType Rte_DidConditionCheckRead_F187(Dcm_NegativeResponseCodeType* ErrorCode)
{
	return E_OK;
}

Std_ReturnType Rte_DidConditionCheckWrite_F187(Dcm_NegativeResponseCodeType* ErrorCode)
{
	return E_OK;
}

uint8  vehicleManufacturerSparePartNumberDataIdentifier[12] = {0,11,21,22,23,44,55,66,77,88,89,12};
//uint8  vehicleManufacturerSparePartNumberDataIdentifier[12] = {0};
Std_ReturnType Rte_DidReadData_F187(uint8* data)
{
	    data[0] = vehicleManufacturerSparePartNumberDataIdentifier[0];
	    data[1] = vehicleManufacturerSparePartNumberDataIdentifier[1];
	    data[2] = vehicleManufacturerSparePartNumberDataIdentifier[2];
	    data[3] = vehicleManufacturerSparePartNumberDataIdentifier[3];
	    data[4] = vehicleManufacturerSparePartNumberDataIdentifier[4];
	    data[5] = vehicleManufacturerSparePartNumberDataIdentifier[5];
	    data[6] = vehicleManufacturerSparePartNumberDataIdentifier[6];
	    data[7] = vehicleManufacturerSparePartNumberDataIdentifier[7];
	    data[8] = vehicleManufacturerSparePartNumberDataIdentifier[8];
	    data[9] = vehicleManufacturerSparePartNumberDataIdentifier[9];
	    data[10] = vehicleManufacturerSparePartNumberDataIdentifier[10];
	    data[11] = vehicleManufacturerSparePartNumberDataIdentifier[11];
	return E_OK;
}

Std_ReturnType Rte_DidWriteData_F187(	uint8* data,
            							uint16 dataLength,
            							Dcm_NegativeResponseCodeType* ErrorCode)
{
    uint16  Index;
#if 0
    if(20 != dataLength)
    {
        (*ErrorCode) = 0x22;
    	return E_NOT_OK;
    }
    if(	  (0x10 != data[0]) 
    	&&(0x1A != data[10])
    	&&(0x23 != data[19]))
    {
        (*ErrorCode) = 0x22;
    	return E_NOT_OK;
	
    }
#endif
    for(Index=0;Index<dataLength;Index++)
    {
    	vehicleManufacturerSparePartNumberDataIdentifier[Index] = 	data[Index];
    }
    return E_OK;
}

/*DID = 0xF18A*/
Std_ReturnType Rte_DidConditionCheckRead_F18A(Dcm_NegativeResponseCodeType* ErrorCode)
{
	return E_OK;
}

Std_ReturnType Rte_DidConditionCheckWrite_F18A(Dcm_NegativeResponseCodeType* ErrorCode)
{
	return E_OK;
}

uint8  systemSupplierIdentifierDataIdentifier[10] = {11,22,33,44,53,65,78,87,90,11};
//uint8  systemSupplierIdentifierDataIdentifier[10] = {0};
Std_ReturnType Rte_DidReadData_F18A(uint8* data)
{
	    data[0] = systemSupplierIdentifierDataIdentifier[0];
	    data[1] = systemSupplierIdentifierDataIdentifier[1];
	    data[2] = systemSupplierIdentifierDataIdentifier[2];
	    data[3] = systemSupplierIdentifierDataIdentifier[3];
	    data[4] = systemSupplierIdentifierDataIdentifier[4];
	    data[5] = systemSupplierIdentifierDataIdentifier[5];
	    data[6] = systemSupplierIdentifierDataIdentifier[6];
	    data[7] = systemSupplierIdentifierDataIdentifier[7];
	    data[8] = systemSupplierIdentifierDataIdentifier[8];
	    data[9] = systemSupplierIdentifierDataIdentifier[9];

	return E_OK;
}

Std_ReturnType Rte_DidWriteData_F18A(	uint8* data,
            							uint16 dataLength,
            							Dcm_NegativeResponseCodeType* ErrorCode)
{
    uint16  Index;
#if 0
    if(20 != dataLength)
    {
        (*ErrorCode) = 0x22;
    	return E_NOT_OK;
    }
    if(	  (0x10 != data[0]) 
    	&&(0x1A != data[10])
    	&&(0x23 != data[19]))
    {
        (*ErrorCode) = 0x22;
    	return E_NOT_OK;
	
    }
#endif
    for(Index=0;Index<dataLength;Index++)
    {
    	systemSupplierIdentifierDataIdentifier[Index] = 	data[Index];
    }
    return E_OK;
}

/*DID = 0xF197*/
Std_ReturnType Rte_DidConditionCheckRead_F197(Dcm_NegativeResponseCodeType* ErrorCode)
{
	return E_OK;
}

Std_ReturnType Rte_DidConditionCheckWrite_F197(Dcm_NegativeResponseCodeType* ErrorCode)
{
	return E_OK;
}

uint8  SystemName[10] = {12,23,34,45,54,66,79,88,91,22};
//uint8  SystemName[10] = {0};
Std_ReturnType Rte_DidReadData_F197(uint8* data)
{
	    data[0] = SystemName[0];
	    data[1] = SystemName[1];
	    data[2] = SystemName[2];
	    data[3] = SystemName[3];
	    data[4] = SystemName[4];
	    data[5] = SystemName[5];
	    data[6] = SystemName[6];
	    data[7] = SystemName[7];
	    data[8] = SystemName[8];
	    data[9] = SystemName[9];

	return E_OK;
}

Std_ReturnType Rte_DidWriteData_F197(	uint8* data,
            							uint16 dataLength,
            							Dcm_NegativeResponseCodeType* ErrorCode)
{
    uint16  Index;
#if 0
    if(20 != dataLength)
    {
        (*ErrorCode) = 0x22;
    	return E_NOT_OK;
    }
    if(	  (0x10 != data[0]) 
    	&&(0x1A != data[10])
    	&&(0x23 != data[19]))
    {
        (*ErrorCode) = 0x22;
    	return E_NOT_OK;
	
    }
#endif
    for(Index=0;Index<dataLength;Index++)
    {
    	SystemName[Index] = 	data[Index];
    }
    return E_OK;
}

/*DID = 0xF193*/
Std_ReturnType Rte_DidConditionCheckRead_F193(Dcm_NegativeResponseCodeType* ErrorCode)
{
	return E_OK;
}

Std_ReturnType Rte_DidConditionCheckWrite_F193(Dcm_NegativeResponseCodeType* ErrorCode)
{
	return E_OK;
}

uint8  SystemSupplierECUHardwareVersionNumberDataIdentifier[10] = {31,32,33,54,53,65,78,87,90,37};
//uint8  SystemSupplierECUHardwareVersionNumberDataIdentifier[10] = {0};
Std_ReturnType Rte_DidReadData_F193(uint8* data)
{
	    data[0] = SystemSupplierECUHardwareVersionNumberDataIdentifier[0];
	    data[1] = SystemSupplierECUHardwareVersionNumberDataIdentifier[1];
	    data[2] = SystemSupplierECUHardwareVersionNumberDataIdentifier[2];
	    data[3] = SystemSupplierECUHardwareVersionNumberDataIdentifier[3];
	    data[4] = SystemSupplierECUHardwareVersionNumberDataIdentifier[4];
	    data[5] = SystemSupplierECUHardwareVersionNumberDataIdentifier[5];
	    data[6] = SystemSupplierECUHardwareVersionNumberDataIdentifier[6];
	    data[7] = SystemSupplierECUHardwareVersionNumberDataIdentifier[7];
	    data[8] = SystemSupplierECUHardwareVersionNumberDataIdentifier[8];
	    data[9] = SystemSupplierECUHardwareVersionNumberDataIdentifier[9];

	return E_OK;
}

Std_ReturnType Rte_DidWriteData_F193(	uint8* data,
            							uint16 dataLength,
            							Dcm_NegativeResponseCodeType* ErrorCode)
{
    uint16  Index;
#if 0
    if(20 != dataLength)
    {
        (*ErrorCode) = 0x22;
    	return E_NOT_OK;
    }
    if(	  (0x10 != data[0]) 
    	&&(0x1A != data[10])
    	&&(0x23 != data[19]))
    {
        (*ErrorCode) = 0x22;
    	return E_NOT_OK;
	
    }
#endif
    for(Index=0;Index<dataLength;Index++)
    {
    	SystemSupplierECUHardwareVersionNumberDataIdentifier[Index] = 	data[Index];
    }
    return E_OK;
}

/*DID = 0xF195*/
Std_ReturnType Rte_DidConditionCheckRead_F195(Dcm_NegativeResponseCodeType* ErrorCode)
{
	return E_OK;
}

Std_ReturnType Rte_DidConditionCheckWrite_F195(Dcm_NegativeResponseCodeType* ErrorCode)
{
	return E_OK;
}

uint8  SystemSupplierECUSoftwareVersionNumberDataIdentifier[10] = {11,22,33,44,53,65,78,87,90,11};
//uint8  SystemSupplierECUSoftwareVersionNumberDataIdentifier[10] = {0};
Std_ReturnType Rte_DidReadData_F195(uint8* data)
{
	    data[0] = SystemSupplierECUSoftwareVersionNumberDataIdentifier[0];
	    data[1] = SystemSupplierECUSoftwareVersionNumberDataIdentifier[1];
	    data[2] = SystemSupplierECUSoftwareVersionNumberDataIdentifier[2];
	    data[3] = SystemSupplierECUSoftwareVersionNumberDataIdentifier[3];
	    data[4] = SystemSupplierECUSoftwareVersionNumberDataIdentifier[4];
	    data[5] = SystemSupplierECUSoftwareVersionNumberDataIdentifier[5];
	    data[6] = SystemSupplierECUSoftwareVersionNumberDataIdentifier[6];
	    data[7] = SystemSupplierECUSoftwareVersionNumberDataIdentifier[7];
	    data[8] = SystemSupplierECUSoftwareVersionNumberDataIdentifier[8];
	    data[9] = SystemSupplierECUSoftwareVersionNumberDataIdentifier[9];

	return E_OK;
}

Std_ReturnType Rte_DidWriteData_F195(	uint8* data,
            							uint16 dataLength,
            							Dcm_NegativeResponseCodeType* ErrorCode)
{
    uint16  Index;
#if 0
    if(20 != dataLength)
    {
        (*ErrorCode) = 0x22;
    	return E_NOT_OK;
    }
    if(	  (0x10 != data[0]) 
    	&&(0x1A != data[10])
    	&&(0x23 != data[19]))
    {
        (*ErrorCode) = 0x22;
    	return E_NOT_OK;
	
    }
#endif
    for(Index=0;Index<dataLength;Index++)
    {
    	SystemSupplierECUSoftwareVersionNumberDataIdentifier[Index] = 	data[Index];
    }
    return E_OK;
}

/*DID = 0xF18C*/
Std_ReturnType Rte_DidConditionCheckRead_F18C(Dcm_NegativeResponseCodeType* ErrorCode)
{
	return E_OK;
}

Std_ReturnType Rte_DidConditionCheckWrite_F18C(Dcm_NegativeResponseCodeType* ErrorCode)
{
	return E_OK;
}

uint8  ECUSerialNumberDataIdentifier[8] = {18,22,33,44,53,65,78,87};
//uint8  ECUSerialNumberDataIdentifier[8] = {0};
Std_ReturnType Rte_DidReadData_F18C(uint8* data)
{
	    data[0] = ECUSerialNumberDataIdentifier[0];
	    data[1] = ECUSerialNumberDataIdentifier[1];
	    data[2] = ECUSerialNumberDataIdentifier[2];
	    data[3] = ECUSerialNumberDataIdentifier[3];
	    data[4] = ECUSerialNumberDataIdentifier[4];
	    data[5] = ECUSerialNumberDataIdentifier[5];
	    data[6] = ECUSerialNumberDataIdentifier[6];
	    data[7] = ECUSerialNumberDataIdentifier[7];


	return E_OK;
}

Std_ReturnType Rte_DidWriteData_F18C(	uint8* data,
            							uint16 dataLength,
            							Dcm_NegativeResponseCodeType* ErrorCode)
{
    uint16  Index;
#if 0
    if(20 != dataLength)
    {
        (*ErrorCode) = 0x22;
    	return E_NOT_OK;
    }
    if(	  (0x10 != data[0]) 
    	&&(0x1A != data[10])
    	&&(0x23 != data[19]))
    {
        (*ErrorCode) = 0x22;
    	return E_NOT_OK;
	
    }
#endif
    for(Index=0;Index<dataLength;Index++)
    {
    	ECUSerialNumberDataIdentifier[Index] = 	data[Index];
    }
    return E_OK;
}

/*DID = 0xF010*/
Std_ReturnType Rte_DidConditionCheckRead_F010(Dcm_NegativeResponseCodeType* ErrorCode)
{
	return E_OK;
}

Std_ReturnType Rte_DidConditionCheckWrite_F010(Dcm_NegativeResponseCodeType* ErrorCode)
{
	return E_OK;
}

uint8  ECUVariantCode[10] = {0};
Std_ReturnType Rte_DidReadData_F010(uint8* data)
{
	    data[0] = ECUVariantCode[0];
	    data[1] = ECUVariantCode[1];
	    data[2] = ECUVariantCode[2];
	    data[3] = ECUVariantCode[3];
	    data[4] = ECUVariantCode[4];
	    data[5] = ECUVariantCode[5];
	    data[6] = ECUVariantCode[6];
	    data[7] = ECUVariantCode[7];
	    data[8] = ECUVariantCode[8];
	    data[9] = ECUVariantCode[9];

	return E_OK;
}

Std_ReturnType Rte_DidWriteData_F010(	uint8* data,
            							uint16 dataLength,
            							Dcm_NegativeResponseCodeType* ErrorCode)
{
    uint16  Index;
#if 0
    if(20 != dataLength)
    {
        (*ErrorCode) = 0x22;
    	return E_NOT_OK;
    }
    if(	  (0x10 != data[0]) 
    	&&(0x1A != data[10])
    	&&(0x23 != data[19]))
    {
        (*ErrorCode) = 0x22;
    	return E_NOT_OK;
	
    }
#endif
    for(Index=0;Index<dataLength;Index++)
    {
    	ECUVariantCode[Index] = 	data[Index];
    }
    return E_OK;
}

/*DID = 0xF190*/
Std_ReturnType Rte_DidConditionCheckRead_F190(Dcm_NegativeResponseCodeType* ErrorCode)
{
	return E_OK;
}

Std_ReturnType Rte_DidConditionCheckWrite_F190(Dcm_NegativeResponseCodeType* ErrorCode)
{
	return E_OK;
}

uint8  VINDataIdentifier[17] = {0};
Std_ReturnType Rte_DidReadData_F190(uint8* data)
{
	   // VINDataIdentifier[0]=EE_Log_Data.v[1]&0xFF00;


	  

	return E_OK;
}

Std_ReturnType Rte_DidWriteData_F190(	uint8* data,
            							uint16 dataLength,
            							Dcm_NegativeResponseCodeType* ErrorCode)
{
    uint16  Index;
#if 0
    if(20 != dataLength)
    {
        (*ErrorCode) = 0x22;
    	return E_NOT_OK;
    }
    if(	  (0x10 != data[0]) 
    	&&(0x1A != data[10])
    	&&(0x23 != data[19]))
    {
        (*ErrorCode) = 0x22;
    	return E_NOT_OK;
	
    }
#endif
    for(Index=0;Index<dataLength;Index++)
    {
    //	VINDataIdentifier[Index] = 	data[Index];

    }

  //  NVMWriteData(E_NVM_LOG_DATA_Object);
    return E_OK;
}


/*DID = 0xF184*/
Std_ReturnType Rte_DidConditionCheckRead_F184(Dcm_NegativeResponseCodeType* ErrorCode)
{
	return E_OK;
}

Std_ReturnType Rte_DidConditionCheckWrite_F184(Dcm_NegativeResponseCodeType* ErrorCode)
{
	return E_OK;
}

uint8  applicationSoftwareFingerprintDataIdentifier[10] = {0};
Std_ReturnType Rte_DidReadData_F184(uint8* data)
{
	   data[0] = applicationSoftwareFingerprintDataIdentifier[0];
	    data[1] = applicationSoftwareFingerprintDataIdentifier[1];
	    data[2] = applicationSoftwareFingerprintDataIdentifier[2];
	    data[3] = applicationSoftwareFingerprintDataIdentifier[3];
	    data[4] = applicationSoftwareFingerprintDataIdentifier[4];
	    data[5] = applicationSoftwareFingerprintDataIdentifier[5];
	    data[6] = applicationSoftwareFingerprintDataIdentifier[6];
	    data[7] = applicationSoftwareFingerprintDataIdentifier[7];
	    data[8] = applicationSoftwareFingerprintDataIdentifier[8];
	    data[9] = applicationSoftwareFingerprintDataIdentifier[9];

	  

	return E_OK;
}

Std_ReturnType Rte_DidWriteData_F184(	uint8* data,
            							uint16 dataLength,
            							Dcm_NegativeResponseCodeType* ErrorCode)
{
    uint16  Index;
#if 0
    if(20 != dataLength)
    {
        (*ErrorCode) = 0x22;
    	return E_NOT_OK;
    }
    if(	  (0x10 != data[0]) 
    	&&(0x1A != data[10])
    	&&(0x23 != data[19]))
    {
        (*ErrorCode) = 0x22;
    	return E_NOT_OK;
	
    }
#endif
    for(Index=0;Index<dataLength;Index++)
    {
    	applicationSoftwareFingerprintDataIdentifier[Index] = 	data[Index];
    }
    return E_OK;
}


/*DID = 0x0003*/
Std_ReturnType Rte_DidShortTermAdjustment_0003(	uint8 *ControlOptionRecord,
                                       			uint8 *ControlEnableMaskRecord,
                                       			uint8 *ControlStatusRecord,
                                       			Dcm_NegativeResponseCodeType  *ErrorCode)
{
		return E_OK;
}

Std_ReturnType Rte_DidReadData_0003(uint8* data)
{
    data[0] = 0x30;
    data[1] = 0x31;
    data[2] = 0x32;
    data[3] = 0x33;
    data[4] = 0x34;
    data[5] = 0x35;
    data[6] = 0x36;
    data[7] = 0x37;
	return E_OK;
}

/**************************************************************************
 ********************* callback function for ECU Reset********************
 *************************************************************************/
Std_ReturnType Rte_DcmEcuReset1(uint8 ResetType,Dcm_NegativeResponseCodeType* ErrorCode)
{
	return E_OK;
}


/**************************************************************************
 *********************callback functions for Routine control
 *************************************************************************/
/*FFF2*/
Std_ReturnType Rte_DcmRoutineStartDA00(	uint8* InBuffer,
										uint8* OutBuffer,
										Dcm_NegativeResponseCodeType* ErrorCode)
{
	return E_OK;
}

Std_ReturnType Rte_DcmRoutineStopDA00(	uint8* InBuffer,
            							uint8* OutBuffer,
										Dcm_NegativeResponseCodeType* ErrorCode)
{
	return E_OK;
}

Std_ReturnType Rte_DcmRoutineResultDA00(uint8* OutBuffer,
										Dcm_NegativeResponseCodeType* ErrorCode)
{
	return E_OK;
}

/*********callback function for Communication Control 0x28 Service*********/
Std_ReturnType  Rte_CommunicaitonControl(uint8  controlType,uint8 communicationType)
{
	return E_OK;
}



/*********callback function for Communication Control 0xB0 Service*********/
Std_ReturnType Rte_TestService(	uint8  *pInData,
								uint16 InDataLength,
                               	uint8  *pOutDta,
                               	uint16 *pOutDataLength,
                                Dcm_NegativeResponseCodeType *Error)
{
    (*pOutDataLength) = 0u;
    return E_OK;
}
/***************************************************************************
     会话切换从非默认会话到默认会话，则恢复DTC的记录功能
 ***************************************************************************/
void Rte_EnableAllDtcsRecord(void)
{
   /*The update of the DTC status bit information shall continue once a ControlDTCSetting request is performed
     with sub-function set to on or a session layer timeout occurs (server transitions to defaultSession. */
    (void)Dem_EnableDTCStorage(DEM_DTC_GROUP_ALL_DTCS, DEM_DTC_KIND_ALL_DTCS);
}

