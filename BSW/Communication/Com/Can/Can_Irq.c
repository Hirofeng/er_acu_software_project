/*============================================================================*/
/*  Copyright (C) 2009-2011,  INFRASTRUCTURE SOFTWARE CO.,LTD.
 *
 *  All rights reserved. This software is  property. Duplication
 *  or disclosure without  written authorization is prohibited.
 *
 *  @file       <Can_Irq.c>
 *  @brief      <Can Irq Isr Source code>
 *
 * <Compiler: CodeWarrior V2.7    MCU:MPC55XX>
 *
 *  @author     <bo.zeng>
 *  @date       <15-07-2013>
 */
/*============================================================================*/

/*=======[R E V I S I O N   H I S T O R Y]====================================*/
/*  <VERSION>    <DATE>    <AUTHOR>    <REVISION LOG>
 *  V1.0.0       20130715   bo.zeng     Initial version
 */
/*============================================================================*/

/*=======[V E R S I O N  I N F O R M A T I O N]===============================*/
#define CAN_IRQ_C_AR_MAJOR_VERSION      2
#define CAN_IRQ_C_AR_MINOR_VERSION      4
#define CAN_IRQ_C_AR_PATCH_VERSION      0
#define CAN_IRQ_C_SW_MAJOR_VERSION      3
#define CAN_IRQ_C_SW_MINOR_VERSION      1
#define CAN_IRQ_C_SW_PATCH_VERSION      0

/*=======[I N C L U D E S]====================================================*/
#include "Can.h"
#include "Can_Irq.h"

/*=======[V E R S I O N  C H E C K]===========================================*/
#if (CAN_IRQ_C_AR_MAJOR_VERSION != CAN_IRQ_H_AR_MAJOR_VERSION)
    #error "Can_Irq.c:Mismatch in Specification Major Version"
#endif 
#if (CAN_IRQ_C_AR_MINOR_VERSION != CAN_IRQ_H_AR_MINOR_VERSION)
    #error "Can_Irq.c:Mismatch in Specification Minor Version"
#endif
#if (CAN_IRQ_C_AR_PATCH_VERSION != CAN_IRQ_H_AR_PATCH_VERSION)
    #error "Can_Irq.c:Mismatch in Specification Patch Version"
#endif
#if (CAN_IRQ_C_SW_MAJOR_VERSION != CAN_IRQ_H_SW_MAJOR_VERSION)
    #error "Can_Irq.c:Mismatch in Specification Major Version"
#endif
#if (CAN_IRQ_C_SW_MINOR_VERSION != CAN_IRQ_H_SW_MINOR_VERSION)
    #error "Can_Irq.c:Mismatch in Specification Minor Version"
#endif

/*=======[F U N C T I O N   I M P L E M E N T A T I O N S]====================*/
#define CAN_START_SEC_CODE_FAST
#include "Can_MemMap.h"



#if (STD_ON == CAN_CONTROLLERA_BUSOFF_INTERRUPT)
/******************************************************************************/
/*
 * Brief               <CAN A bus-off Interrupt Service Routine process>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
FUNC(void, CAN_CODE_FAST) Can_A_TwRwBusOff(void)
{
    /* bus-off Interrupt Service Routine process */
    Can_TwRwBusOff_Handler(CAN_CONTROLLER_A);
    return;
}
#endif /* STD_ON == CAN_CONTROLLERA_BUSOFF_INTERRUPT */

/******************************************************************************/
/*
 * Brief               <CAN A Error Interrupt Service Routine process>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
FUNC(void, CAN_CODE_FAST) Can_A_Err(void)
{
    /* Error Interrupt Service Routine process */
    Can_Error_Handler(CAN_CONTROLLER_A);
    return;
}

#if (STD_ON == CAN_WAKEUP_SUPPORT)
#if (STD_ON == CAN_CONTROLLERA_WAKEUP_INTERRUPT)
/******************************************************************************/
/*
 * Brief               <CAN A Wakeup Interrupt Service Routine process>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
FUNC(void, CAN_CODE_FAST) Can_A_Wakeup(void)
{
    /* Wakeup Interrupt Service Routine process */
    Can_Wakeup_Handler(CAN_CONTROLLER_A);
    return;
}
#endif /* STD_ON == CAN_CONTROLLERA_WAKEUP_INTERRUPT */
#endif /* STD_ON == CAN_WAKEUP_SUPPORT */

/******************************************************************************/
/*
 * Brief               <CAN C Tx/Rx (00MB) Interrupt Service Routine process>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
FUNC(void, CAN_CODE_FAST) Can_C_TxRxMb00ISR(void)
{
   // Can_RxTxInt_Handler(CAN_CONTROLLER_C, 0, 0);
    return;
}

/******************************************************************************/
/*
 * Brief               <CAN C Tx/Rx (01MB) Interrupt Service Routine process>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/


/******************************************************************************/
/*
 * Brief               <CAN C Tx/Rx (02MB) Interrupt Service Routine process>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/


/******************************************************************************/
/*
 * Brief               <CAN C Tx/Rx (03MB) Interrupt Service Routine process>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/


/******************************************************************************/
/*
 * Brief               <CAN C Tx/Rx (04MB) Interrupt Service Routine process>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/


/******************************************************************************/
/*
 * Brief               <CAN C Tx/Rx (05MB) Interrupt Service Routine process>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/


/******************************************************************************/
/*
 * Brief               <CAN C Tx/Rx (05MB) Interrupt Service Routine process>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/


/******************************************************************************/
/*
 * Brief               <CAN C Tx/Rx (07MB) Interrupt Service Routine process>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/


/******************************************************************************/
/*
 * Brief               <CAN C Tx/Rx (08MB) Interrupt Service Routine process>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/


/******************************************************************************/
/*
 * Brief               <CAN C Tx/Rx (09MB) Interrupt Service Routine process>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/


/******************************************************************************/
/*
 * Brief               <CAN C Tx/Rx (10MB) Interrupt Service Routine process>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/


/******************************************************************************/
/*
 * Brief               <CAN C Tx/Rx (11MB) Interrupt Service Routine process>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/


/******************************************************************************/
/*
 * Brief               <CAN C Tx/Rx (12MB) Interrupt Service Routine process>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/


/******************************************************************************/
/*
 * Brief               <CAN C Tx/Rx (13MB) Interrupt Service Routine process>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/

/******************************************************************************/
/*
 * Brief               <CAN C Tx/Rx (14MB) Interrupt Service Routine process>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/


/******************************************************************************/
/*
 * Brief               <CAN C Tx/Rx (15MB) Interrupt Service Routine process>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/


/******************************************************************************/
/*
 * Brief               <CAN C Tx/Rx (16-31 MB) Interrupt Service Routine process>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/


#if (STD_ON == CAN_CONTROLLERC_BUSOFF_INTERRUPT)
/******************************************************************************/
/*
 * Brief               <CAN C bus-off Interrupt Service Routine process>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
FUNC(void, CAN_CODE_FAST) Can_C_TwRwBusOff(void)
{
    /* bus-off Interrupt Service Routine process */
    Can_TwRwBusOff_Handler(CAN_CONTROLLER_C);
    return;
}
#endif /* STD_ON == CAN_CONTROLLERC_BUSOFF_INTERRUPT */

/******************************************************************************/
/*
 * Brief               <CAN C Error Interrupt Service Routine process>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
FUNC(void, CAN_CODE_FAST) Can_C_Err(void)
{
    /* Error Interrupt Service Routine process */
    Can_Error_Handler(CAN_CONTROLLER_C);
    return;
}

#if (STD_ON == CAN_WAKEUP_SUPPORT)
#if (STD_ON == CAN_CONTROLLERC_WAKEUP_INTERRUPT)
/******************************************************************************/
/*
 * Brief               <CAN C Wakeup Interrupt Service Routine process>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
FUNC(void, CAN_CODE_FAST) Can_C_Wakeup(void)
{
    /* Wakeup Interrupt Service Routine process */
    Can_Wakeup_Handler(CAN_CONTROLLER_C);
    return;
}
#endif /* STD_ON == CAN_CONTROLLERC_WAKEUP_INTERRUPT */
#endif /* STD_ON == CAN_WAKEUP_SUPPORT */

#define CAN_STOP_SEC_CODE_FAST
#include "Can_MemMap.h"

/*=======[E N D   O F   F I L E]==============================================*/

