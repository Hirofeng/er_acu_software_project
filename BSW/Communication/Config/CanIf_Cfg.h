/*============================================================================*/
/*  Copyright (C) 2009-2014,  INFRASTRUCTURE SOFTWARE CO.,LTD.
 *  
 *  All rights reserved. This software is  property. Duplication
 *  or disclosure without  written authorization is prohibited.
 *  
 *  @file       <CanIf_Cfg.h>
 *  @brief      <>
 *  
 *  <MCU:MPC563x>
 *  
 *  @author     <>
 *  @date       <2014-06-23 17:28:58>
 */
/*============================================================================*/


#ifndef CANIF_CFG_H
#define CANIF_CFG_H

/*=======[V E R S I O N  I N F O R M A T I O N]===============================*/
#define CANIF_CFG_H_AR_MAJOR_VERSION    3
#define CANIF_CFG_H_AR_MINOR_VERSION    2
#define CANIF_CFG_H_AR_PATCH_VERSION    0
#define CANIF_CFG_H_SW_MAJOR_VERSION    1
#define CANIF_CFG_H_SW_MINOR_VERSION    0
#define CANIF_CFG_H_SW_PATCH_VERSION    1


/*=======[I N C L U D E S]====================================================*/
#include "ComStack_Types.h"


/*=======[M A C R O S]========================================================*/
/* define CANIF_VARIANT TYPE */
#define CANIF_VARIANT_PC             VARIANT_PRE_COMPILE
#define CANIF_VARIANT_LT             VARIANT_LINK_TIME
#define CANIF_VARIANT_PB             VARIANT_POST_BUILD

/* define CanIf Software Filter Type */
#define CANIF_BINARY                0
#define CANIF_INDEX                 1
#define CANIF_LINEAR                2
#define CANIF_TABLE                 3      

/* define the Variant of the CanIf Module */
#define CANIF_VARIANT_CFG           CANIF_VARIANT_PC

/* CanIfPrivateConfiguration,contains the private configuration (parameters)
 * of the CAN Interface. 
 */
#define CANIF_DLC_CHECK             STD_ON

/* Define TxBuffer number*/
#define CANIF_NUMBER_OF_TXBUFFERS   0

/* Enable or disable Tx Buffer,depend on 
 * CANIF_NUMBER_OF_TXBUFFERS which configed above.
 * If bigger than 0, then configuration tool should set STD_ON
 */
#define CANIF_TX_BUFFER_USED        STD_OFF

/* define software filter method 
 * CANIF_BINARY;CANIF_INDEX;CANIF_LINEAR;CANIF_TABLE
 */
#define CANIF_SOFTWARE_FILTER_TYPE  CANIF_LINEAR

/* CanIfPublicConfiguration */
#define CANIF_DEV_ERROR_DETECT                  STD_OFF
#define CANIF_PUBLIC_TXCONFIRM_POLLING_SUPPORT  STD_OFF
#define CANIF_READRXPDU_DATA_API                STD_OFF
#define CANIF_READRXPDU_NOTIFY_STATUS_API       STD_OFF
#define CANIF_READTXPDU_NOTIFY_STATUS_API       STD_OFF
#define CANIF_SETDYNAMICTXID_API                STD_OFF
#define CANIF_VERSION_INFO_API                  STD_ON

#define CANIF_WAKEUP_VALIDATION                 STD_ON
#define CANIF_TRANSMIT_CANCELLATION             STD_OFF

/* user defined macros */
#define CANIF_DEM_ERROR_DETECT                  STD_OFF


/* define Controller configuration number,configured by config tool while Variant 1*/
#define CANIF_MAX_CONTROLLER                    2


/* define Rx L-Pdu number,configured by config tool while Variant 1*/
#define CANIF_MAX_NUMBER_OF_CANRXPDUIDS             21

/* define Tx L-Pdu number,configured by config tool while Variant 1*/
#define CANIF_MAX_NUMBER_OF_CANTXPDUIDS             13

/* dynamic ID number,configured by config tool while Variant 1*/
#define CANIF_MAX_NUMBER_OF_DYNAMIC_CANTXPDUIDS     0

/* define RxBuffer number,config tool generate automatic when Rx Pdu config finished */
#define CANIF_MAX_NUMBER_OF_RXBUFFERS               0

#define CANIF_NUMBER_OF_HTHUSED                     10

#define CANIF_NUMBER_OF_HRHUSED                     18



#endif

/*=======[E N D   O F   F I L E]==============================================*/
