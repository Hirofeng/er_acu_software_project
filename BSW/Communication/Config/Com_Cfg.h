/*============================================================================*/
/*  Copyright (C) 2009-2014,  INFRASTRUCTURE SOFTWARE CO.,LTD.
 *  
 *  All rights reserved. This software is  property. Duplication
 *  or disclosure without  written authorization is prohibited.
 *  
 *  @file       <Com_Cfg.h>
 *  @brief      <>
 *  
 *  <MCU:MPC563x>
 *  
 *  @author     <>
 *  @date       <2014-06-23 17:29:01>
 */
/*============================================================================*/

/*==========================================================================*/
#ifndef COM_CFG_H
#define COM_CFG_H

/*=======[V E R S I O N  I N F O R M A T I O N]===============================*/
#define COM_CFG_H_AR_MAJOR_VERSION  3U
#define COM_CFG_H_AR_MINOR_VERSION  0U
#define COM_CFG_H_AR_PATCH_VERSION  3U
#define COM_CFG_H_SW_MAJOR_VERSION  1U
#define COM_CFG_H_SW_MINOR_VERSION  0U
#define COM_CFG_H_SW_PATCH_VERSION  0U

/*=======[M A C R O S]========================================================*/
/** @req COM203 */
#define OSEK_COM_CCCA    0x00            /*Conformance Classes: CCCA*/
#define OSEK_COM_CCCB    0x01            /*Conformance Classes: CCCB*/
#define OSEK_COM_CCC0    0x02            /*Conformance Classes: CCC0*/
#define OSEK_COM_CCC1    0x03            /*Conformance Classes: CCC1*/
#define OSEK_COM_CCC     OSEK_COM_CCC1   /*Selected Conformance Classes*/

/*version information check API*/
#define COM_VERSION_INFO_API      STD_ON 

/*Development error trace switch*/
#define COM_CONFIGURATION_USE_DET STD_OFF

/*
 * The message indications/confirmations are performed
 * in Com_RxIndication/Com_TxConfirmation
 */
#define COM_MSGUNPACK_IMMEDIATE   STD_ON

/*I-Pdu most number of OSEK COM allowed to use*/
#if ((OSEK_COM_CCC == OSEK_COM_CCC1) || (OSEK_COM_CCC == OSEK_COM_CCC0))
#define COM_IPDU_NUM        3
#endif

/*Message most number of OSEK COM allowed to use*/
#define COM_MSG_NUM         9

/*memory space of OSEK COM allowed to use*/
#define COM_BUFFER_SIZE     500//390u

/* function optimize switch*/
#define COM_OPTIME_SUPPORT  STD_ON

/* filter support switch */
#define COM_FILTER_SUPPORT  STD_ON

/* send PDU group ID */
#define COM_SENDPDU_GROUPID      0u

/* receive PDU group ID */
#define COM_RECEIVEPDU_GROUPID   1u

/*Message Id*/
#define ACCM_VCU_error   0
#define ACCM_VCU_power   1
#define ACCM_VCU_RPM   2

#define Test_S_Sig1   3    /* VCU_DCDC_iActHvMax_NetMsg   */
#define Test_S_Sig2   4    /* VCU_DCDC_stCtrlReq_NetMsg    */
#define Test_S_Sig3   5    /* VCU_DCDC_StModeReq_NetMsg  */
#define Test_S_Sig4   6    /* VCU_DCDC_udcLvSetP_NetMsg    */
#define Test_S_Sig5   7          /* VCU_MotorSpd_NetMsg    */
#define Test_S_Sig6   8       /* VCU_Parkrequest_NetMsg  */


#endif /* COM_CFG_H*/

/*=======[E N D   O F   F I L E]==============================================*/

