/*============================================================================*/
/*  Copyright (C) 2009-2011,  INFRASTRUCTURE SOFTWARE CO.,LTD.
 *
 *  All rights reserved. This software is  property. Duplication
 *  or disclosure without  written authorization is prohibited.
 *
 *
 *  @file       <kernel.h>
 *  @brief      <>
 *
 *  <Compiler: CodeWarrior    MCU:freescale xep 100>
 *
 *  @author     <yaoxuan.zhang>
 *  @date        <15-04-2013>
 */
/*============================================================================*/

#ifndef KERNEL_H
#define KERNEL_H

/*=======[R E V I S I O N   H I S T O R Y]====================================*/
/*  <VERSION>    <DATE>    <AUTHOR>    <REVISION LOG>
 *  V0.1.0       20130415  yaoxuan.zhang       Initial version
 *  V0.2.0       20130619  huaming             modified the format of data type and macro definition,
 *											   and added the information about MISRAC rule
 *  V0.3.0       20130702  yaoxuan.zhang       Modified error function definition
 *  V0.4.0       20130709  huaming             modified the format of parameter base on rule of i-soft
 *  V0.5.0       20130723  yaoxuan.zhang       Modified the code to adapt MISRA C specification.
 */
/*============================================================================*/

/*=======[M I S R A C  R U L E  V I O L A T I O N]============================*/
/*  <MESSAGE ID>    <CODE LINE>    <REASON>
 */
/*============================================================================*/


/*=======[V E R S I O N  I N F O R M A T I O N]===============================*/
#define KERNEL_H_VENDOR_ID              0
#define KERNEL_H_MODULE_ID              0
#define KERNEL_H_AR_MAJOR_VERSION       1
#define KERNEL_H_AR_MINOR_VERSION       0
#define KERNEL_H_AR_PATCH_VERSION       0
#define KERNEL_H_SW_MAJOR_VERSION       1
#define KERNEL_H_SW_MINOR_VERSION       0
#define KERNEL_H_SW_PATCH_VERSION       0
#define KERNEL_H_VENDOR_API_INFIX       0


/*=======[I N C L U D E S]====================================================*/
#include "Os_Types.h"
//#include "Arch.h"
#include "Os_Cfg.h"
/*=======[M A C R O S]========================================================*/


/*=======[T Y P E   D E F I N I T I O N S]====================================*/

/*=======[E X T E R N A L   D A T A]==========================================*/






Os_PriorityType Os_GetHighPrio(void);
void Os_ReadyQueueInsert(Os_TaskType object,Os_CallLevelType level,Os_PriorityType prio);
void Os_ReadyQueueRemove(Os_TaskType object,Os_CallLevelType level,Os_PriorityType prio);


Os_TaskType Os_ReadyQueueGetFirst(Os_PriorityType prio) ;

Os_IPLType Os_PrioToIpl(Os_PriorityType prio);
Os_PriorityType Os_IplToPrio(Os_IPLType ipl);

void Os_Dispatch(void);


/* Counter */
#if (CFG_COUNTER_MAX > 0)
void Os_InitCounterCB(Os_CounterType id);
#endif
Os_TickType Os_AddTicks(Os_TickType value,Os_TickType base,Os_TickType maxticks);
Os_TickType Os_SubTicks(Os_TickType newvalue,Os_TickType oldvalue,Os_TickType maxticks);

/* Alarm */
#if (CFG_ALARM_MAX > 0)
void Os_InitAlarmCB(Os_AlarmType alarmid);
void Os_InsertAlarm(Os_AlarmType alarmid,Os_AlarmRefType almque,Os_TickType curTick);
void Os_DeleteAlarm(Os_AlarmType alarmid,Os_AlarmRefType almque);
#endif

/* Event */
#if (CFG_EXTENDED_TASK_MAX > 0)
void Os_InitEventCB(Os_EventType eventId);
#endif

/* Resource */
#if (CFG_STD_RESOURCE_MAX > 0)
void Os_GetInternalResource(void);
void Os_ReleaseInternalResource(void);
#endif
/*=======[M A C R O S]========================================================*/


#if (CFG_STARTUPHOOK == TRUE)
void Call_StartupHook(void); 
#endif
#if (CFG_SHUTDOWNHOOK == TRUE)
void Call_ShutDownHook(StatusType Error);
#endif
#if (CFG_ERRORHOOK == TRUE)
void Call_ErrorHook(StatusType Error);
#endif

#if (CFG_POSTTASKHOOK == TRUE)
void Call_PostTaskHook(void);
#endif
#if (CFG_PRETASKHOOK == TRUE)
void Call_PreTaskHook(void);
#endif
/*=======[I N T E R N A L   D A T A]==========================================*/


/*=======[I N T E R N A L   F U N C T I O N   D E C L A R A T I O N S]========*/

#endif  /* end KERNEL_H */
/*=======[E N D   O F   F I L E]==============================================*/


