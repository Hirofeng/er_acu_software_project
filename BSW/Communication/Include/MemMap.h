/*============================================================================*/
/*  Copyright (C) 2009-2011,  INFRASTRUCTURE SOFTWARE CO.,LTD.
 *  
 *  All rights reserved. This software is  property. Duplication
 *  or disclosure without  written authorization is prohibited.
 *  
 *  @file       <MemMap.h>
 *  @brief      <Briefly describe file(one line)>
 *  
 *  <Compiler: CodeWarriar    MCU:MC9S12>
 *  
 *  @author     <chen xue hua>
 *  @date       <2013-02-27>
 */
/*============================================================================*/
/* @req MEMMAP020 @req MEMMAP001 @req MEMMAP002 @req MEMMAP005 @req MEMMAP015 */
/* @req MEMMAP016 */

/*=======[R E V I S I O N   H I S T O R Y]====================================*/
/*  <VERSION>    <DATE>    <AUTHOR>    <REVISION LOG>
 *  V1.0.0       20130227  chenxuehua  Initial version
 */
/*============================================================================*/

/*=======[V E R S I O N  I N F O R M A T I O N]===============================*/
#define MEMMAP_VENDOR_ID  62
#define MEMMAP_MODULE_ID  0
#define MEMMAP_AR_MAJOR_VERSION  1
#define MEMMAP_AR_MINOR_VERSION  1
#define MEMMAP_AR_PATCH_VERSION  2
#define MEMMAP_SW_MAJOR_VERSION  1
#define MEMMAP_SW_MINOR_VERSION  0
#define MEMMAP_SW_PATCH_VERSION  0
#define MEMMAP_VENDOR_API_INFIX  0

/* 
 * <SIZE> -- BOOLEAN/8BIT/16BIT/32BIT/UNSPECIFIED 
 */

/* 
 * <MSN>_START_SEC_CODE
 * <MSN>_STOP_SEC_CODE
 * To be used for mapping code to applicaiton block,boot block,external flash etc. 
 */

/* 
 * <MSN>_START_SEC_CODE_FAST
 * <MSN>_STOP_SEC_CODE_FAST
 * To be used for mapping code to local block,boot block of ISR API. 
 */

/* 
 * <MSN>_START_SEC_VAR_NOINIT_<SIZE>
 * <MSN>_STOP_SEC_VAR_NOINIT_<SIZE>
 * To be used for all global or static variables that are never initialized. 
 */

/* 
 * <MSN>_START_SEC_VAR_POWER_ON_INIT_<SIZE>
 * <MSN>_STOP_SEC_VAR_POWER_ON_INIT_<SIZE>
 * To be used for all global or static variables that are initialized only after power on reset. 
 */

/*
 * <MSN>_START_SEC_VAR_<SIZE>
 * <MSN>_STOP_SEC_VAR_<SIZE>
 * To be used for all global or static variables that are initialized after every reset(the normal case).
 */

/* 
 * <MSN>_START_SEC_VAR_FAST_NOINIT_<SIZE>
 * <MSN>_STOP_SEC_VAR_FAST_NOINIT_<SIZE>
 * To be used for all global or static variables that have at least one of the following properties:
 *      *accessed bitwise
 *      *frequently used
 *      *high number of accesses in source code
 *  Some platforms allow the use of bit instructions for variables located in this specific RAM area as
 *  well as shorter addressing instructions.This saves code and runtime. 
 */

/* 
 * <MSN>_START_SEC_VAR_FAST_POWER_ON_INIT_<SIZE>
 * <MSN>_STOP_SEC_VAR_FAST_POWER_ON_INIT_<SIZE>
 * To be used for all global or static variables that have at least one of the following properties:
 *      *accessed bitwise
 *      *frequently used
 *      *high number of accesses in source code
 *  Some platforms allow the use of bit instructions for variables located in this specific RAM area as
 *  well as shorter addressing instructions.This saves code and runtime. 
 */

/* 
 * <MSN>_START_SEC_VAR_FAST_<SIZE>
 * <MSN>_STOP_SEC_VAR_FAST_<SIZE>
 * To be used for all global or static variables that have at least one of the following properties:
 *      *accessed bitwise
 *      *frequently used
 *      *high number of accesses in source code
 *  Some platforms allow the use of bit instructions for variables located in this specific RAM area as
 *  well as shorter addressing instructions.This saves code and runtime. 
 */

/*
 * <MSN>_START_SEC_CONST_<SIZE>
 * <MSN>_STOP_SEC_CONST_<SIZE>
 * To be used for all global or static constants.
 */

/*
 * <MSN>_START_SEC_CONST_FAST_<SIZE>
 * <MSN>_STOP_SEC_CONST_FAST_<SIZE>
 * To be used for all global or static constants in fast segment block.
 */

/*
 * <MSN>_START_CONST_PBCFG
 * <MSN>_STOP_CONST_PBCFG
 * Constants with attributes that show that they reside in one segment for module configuration.
 */

/*
 * <MSN>_START_CONST_PBCFG_ROOT
 * <MSN>_STOP_CONST_PBCFG_ROOT
 * Constants with attributes that show that they reside in one segment for module configuration for root structure.
 */

/* @req MEMMAP010 @req MEMMAP004 @req MEMMAP021 @req MEMMAP003 @req MEMMAP018 */
/*=======[M A C R O S]========================================================*/

#define START_WITH_IF
/* @req MEMMAP006  @req MEMMAP013 */
/*=======[M E M M A P  S Y M B O L  D E F I N E]==============================*/
#define MEMMAP_ERROR

/****************************************/
#if defined (START_WITH_IF)
    #undef MEMMAP_ERROR

#elif defined (START_SEC_CODE)
    #undef START_SEC_CODE
    #undef MEMMAP_ERROR
#elif defined (STOP_SEC_CODE)
    #undef STOP_SEC_CODE
    #undef MEMMAP_ERROR
#endif /* START_SEC_CODE */

/****************************************/
#if defined (START_WITH_IF)
    #undef MEMMAP_ERROR

#elif defined (START_SEC_CODE_FAST)
    #undef START_SEC_CODE_FAST
    #undef MEMMAP_ERROR
    #pragma CODE_SEG __NEAR_SEG NON_BANKED
#elif defined (STOP_SEC_CODE_FAST)
    #undef STOP_SEC_CODE_FAST
    #undef MEMMAP_ERROR
    #pragma CODE_SEG DEFAULT
#endif /* START_SEC_CODE_FAST */

/****************************************/
#if defined (START_WITH_IF)

#elif defined (START_SEC_VAR_NOINIT_BOOLEAN)
    #undef START_SEC_VAR_NOINIT_BOOLEAN
    #undef MEMMAP_ERROR
#elif defined (STOP_SEC_VAR_NOINIT_BOOLEAN)
    #undef STOP_SEC_VAR_NOINIT_BOOLEAN
    #undef MEMMAP_ERROR

#elif defined (START_SEC_VAR_NOINIT_8BIT)
    #undef START_SEC_VAR_NOINIT_8BIT
    #undef MEMMAP_ERROR
#elif defined (STOP_SEC_VAR_NOINIT_8BIT)
    #undef STOP_SEC_VAR_NOINIT_8BIT
    #undef MEMMAP_ERROR

#elif defined (START_SEC_VAR_NOINIT_16BIT)
    #undef START_SEC_VAR_NOINIT_16BIT
    #undef MEMMAP_ERROR
#elif defined (STOP_SEC_VAR_NOINIT_16BIT)
    #undef STOP_SEC_VAR_NOINIT_16BIT
    #undef MEMMAP_ERROR

#elif defined (START_SEC_VAR_NOINIT_32BIT)
    #undef START_SEC_VAR_NOINIT_32BIT
    #undef MEMMAP_ERROR
#elif defined (STOP_SEC_VAR_NOINIT_32BIT)
    #undef STOP_SEC_VAR_NOINIT_32BIT
    #undef MEMMAP_ERROR

#elif defined (START_SEC_VAR_NOINIT_UNSPECIFIED)
    #undef START_SEC_VAR_NOINIT_UNSPECIFIED
    #undef MEMMAP_ERROR
#elif defined (STOP_SEC_VAR_NOINIT_UNSPECIFIED)
    #undef STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #undef MEMMAP_ERROR

#endif /* START_SEC_VAR_NOINIT_<SIZE> */

/****************************************/
#if defined (START_WITH_IF)

#elif defined (START_SEC_VAR_POWER_ON_INIT_BOOLEAN)
    #undef START_SEC_VAR_POWER_ON_INIT_BOOLEAN
    #undef MEMMAP_ERROR
#elif defined (STOP_SEC_VAR_POWER_ON_INIT_BOOLEAN)
    #undef STOP_SEC_VAR_POWER_ON_INIT_BOOLEAN
    #undef MEMMAP_ERROR

#elif defined (START_SEC_VAR_POWER_ON_INIT_8BIT)
    #undef START_SEC_VAR_POWER_ON_INIT_8BIT
    #undef MEMMAP_ERROR
#elif defined (STOP_SEC_VAR_POWER_ON_INIT_8BIT)
    #undef STOP_SEC_VAR_POWER_ON_INIT_8BIT
    #undef MEMMAP_ERROR

#elif defined (START_SEC_VAR_POWER_ON_INIT_16BIT)
    #undef START_SEC_VAR_POWER_ON_INIT_16BIT
    #undef MEMMAP_ERROR
#elif defined (STOP_SEC_VAR_POWER_ON_INIT_16BIT)
    #undef STOP_SEC_VAR_POWER_ON_INIT_16BIT
    #undef MEMMAP_ERROR

#elif defined (START_SEC_VAR_POWER_ON_INIT_32BIT)
    #undef START_SEC_VAR_POWER_ON_INIT_32BIT
    #undef MEMMAP_ERROR
#elif defined (STOP_SEC_VAR_POWER_ON_INIT_32BIT)
    #undef STOP_SEC_VAR_POWER_ON_INIT_32BIT
    #undef MEMMAP_ERROR

#elif defined (START_SEC_VAR_POWER_ON_INIT_UNSPECIFIED)
    #undef START_SEC_VAR_POWER_ON_INIT_UNSPECIFIED
    #undef MEMMAP_ERROR
#elif defined (STOP_SEC_VAR_POWER_ON_INIT_UNSPECIFIED)
    #undef STOP_SEC_VAR_POWER_ON_INIT_UNSPECIFIED
    #undef MEMMAP_ERROR

#endif /* START_SEC_VAR_POWER_ON_INIT_<SIZE> */

/****************************************/
#if defined (START_WITH_IF)

#elif defined (START_SEC_VAR_BOOLEAN)
    #undef START_SEC_VAR_BOOLEAN
    #undef MEMMAP_ERROR
#elif defined (STOP_SEC_VAR_BOOLEAN)
    #undef STOP_SEC_VAR_BOOLEAN
    #undef MEMMAP_ERROR

#elif defined (START_SEC_VAR_8BIT)
    #undef START_SEC_VAR_8BIT
    #undef MEMMAP_ERROR
#elif defined (STOP_SEC_VAR_8BIT)
    #undef STOP_SEC_VAR_8BIT
    #undef MEMMAP_ERROR

#elif defined (START_SEC_VAR_16BIT)
    #undef START_SEC_VAR_16BIT
    #undef MEMMAP_ERROR
#elif defined (STOP_SEC_VAR_16BIT)
    #undef STOP_SEC_VAR_16BIT
    #undef MEMMAP_ERROR

#elif defined (START_SEC_VAR_32BIT)
    #undef START_SEC_VAR_32BIT
    #undef MEMMAP_ERROR
#elif defined (STOP_SEC_VAR_32BIT)
    #undef STOP_SEC_VAR_32BIT
    #undef MEMMAP_ERROR

#elif defined (START_SEC_VAR_UNSPECIFIED)
    #undef START_SEC_VAR_UNSPECIFIED
    #undef MEMMAP_ERROR
#elif defined (STOP_SEC_VAR_UNSPECIFIED)
    #undef STOP_SEC_VAR_UNSPECIFIED
    #undef MEMMAP_ERROR

#endif /* START_SEC_VAR_<SIZE> */

/****************************************/
#if defined (START_WITH_IF)

#elif defined (START_SEC_VAR_FAST_NOINIT_BOOLEAN)
    #undef START_SEC_VAR_FAST_NOINIT_BOOLEAN
    #undef MEMMAP_ERROR
#elif defined (STOP_SEC_VAR_FAST_NOINIT_BOOLEAN)
    #undef STOP_SEC_VAR_FAST_NOINIT_BOOLEAN
    #undef MEMMAP_ERROR

#elif defined (START_SEC_VAR_FAST_NOINIT_8BIT)
    #undef START_SEC_VAR_FAST_NOINIT_8BIT
    #undef MEMMAP_ERROR
#elif defined (STOP_SEC_VAR_FAST_NOINIT_8BIT)
    #undef STOP_SEC_VAR_FAST_NOINIT_8BIT
    #undef MEMMAP_ERROR

#elif defined (START_SEC_VAR_FAST_NOINIT_16BIT)
    #undef START_SEC_VAR_FAST_NOINIT_16BIT
    #undef MEMMAP_ERROR
#elif defined (STOP_SEC_VAR_FAST_NOINIT_16BIT)
    #undef STOP_SEC_VAR_FAST_NOINIT_16BIT
    #undef MEMMAP_ERROR

#elif defined (START_SEC_VAR_FAST_NOINIT_32BIT)
    #undef START_SEC_VAR_FAST_NOINIT_32BIT
    #undef MEMMAP_ERROR
#elif defined (STOP_SEC_VAR_FAST_NOINIT_32BIT)
    #undef STOP_SEC_VAR_FAST_NOINIT_32BIT
    #undef MEMMAP_ERROR

#elif defined (START_SEC_VAR_FAST_NOINIT_UNSPECIFIED)
    #undef START_SEC_VAR_FAST_NOINIT_UNSPECIFIED
    #undef MEMMAP_ERROR
#elif defined (STOP_SEC_VAR_FAST_NOINIT_UNSPECIFIED)
    #undef STOP_SEC_VAR_FAST_NOINIT_UNSPECIFIED
    #undef MEMMAP_ERROR

#endif /* START_SEC_VAR_FAST_NOINIT_<SIZE> */

/****************************************/
#if defined (START_WITH_IF)

#elif defined (START_SEC_VAR_FAST_POWER_ON_INIT_BOOLEAN)
    #undef START_SEC_VAR_FAST_POWER_ON_INIT_BOOLEAN
    #undef MEMMAP_ERROR
#elif defined (STOP_SEC_VAR_FAST_POWER_ON_INIT_BOOLEAN)
    #undef STOP_SEC_VAR_FAST_POWER_ON_INIT_BOOLEAN
    #undef MEMMAP_ERROR

#elif defined (START_SEC_VAR_FAST_POWER_ON_INIT_8BIT)
    #undef START_SEC_VAR_FAST_POWER_ON_INIT_8BIT
    #undef MEMMAP_ERROR
#elif defined (STOP_SEC_VAR_FAST_POWER_ON_INIT_8BIT)
    #undef STOP_SEC_VAR_FAST_POWER_ON_INIT_8BIT
    #undef MEMMAP_ERROR

#elif defined (START_SEC_VAR_FAST_POWER_ON_INIT_16BIT)
    #undef START_SEC_VAR_FAST_POWER_ON_INIT_16BIT
    #undef MEMMAP_ERROR
#elif defined (STOP_SEC_VAR_FAST_POWER_ON_INIT_16BIT)
    #undef STOP_SEC_VAR_FAST_POWER_ON_INIT_16BIT
    #undef MEMMAP_ERROR

#elif defined (START_SEC_VAR_FAST_POWER_ON_INIT_32BIT)
    #undef START_SEC_VAR_FAST_POWER_ON_INIT_32BIT
    #undef MEMMAP_ERROR
#elif defined (STOP_SEC_VAR_FAST_POWER_ON_INIT_32BIT)
    #undef STOP_SEC_VAR_FAST_POWER_ON_INIT_32BIT
    #undef MEMMAP_ERROR

#elif defined (START_SEC_VAR_FAST_POWER_ON_INIT_UNSPECIFIED)
    #undef START_SEC_VAR_FAST_POWER_ON_INIT_UNSPECIFIED
    #undef MEMMAP_ERROR
#elif defined (STOP_SEC_VAR_FAST_POWER_ON_INIT_UNSPECIFIED)
    #undef STOP_SEC_VAR_FAST_POWER_ON_INIT_UNSPECIFIED
    #undef MEMMAP_ERROR

#endif /* START_SEC_VAR_FAST_POWER_ON_INIT_<SIZE> */

/****************************************/
#if defined (START_WITH_IF)

#elif defined (START_SEC_VAR_FAST_BOOLEAN)
    #undef START_SEC_VAR_FAST_BOOLEAN
    #undef MEMMAP_ERROR
#elif defined (STOP_SEC_VAR_FAST_BOOLEAN)
    #undef STOP_SEC_VAR_FAST_BOOLEAN
    #undef MEMMAP_ERROR

#elif defined (START_SEC_VAR_FAST_8BIT)
    #undef START_SEC_VAR_FAST_8BIT
    #undef MEMMAP_ERROR
#elif defined (STOP_SEC_VAR_FAST_8BIT)
    #undef STOP_SEC_VAR_FAST_8BIT
    #undef MEMMAP_ERROR

#elif defined (START_SEC_VAR_FAST_16BIT)
    #undef START_SEC_VAR_FAST_16BIT
    #undef MEMMAP_ERROR
#elif defined (STOP_SEC_VAR_FAST_16BIT)
    #undef STOP_SEC_VAR_FAST_16BIT
    #undef MEMMAP_ERROR

#elif defined (START_SEC_VAR_FAST_32BIT)
    #undef START_SEC_VAR_FAST_32BIT
    #undef MEMMAP_ERROR
#elif defined (STOP_SEC_VAR_FAST_32BIT)
    #undef STOP_SEC_VAR_FAST_32BIT
    #undef MEMMAP_ERROR

#elif defined (START_SEC_VAR_FAST_UNSPECIFIED)
    #undef START_SEC_VAR_FAST_UNSPECIFIED
    #undef MEMMAP_ERROR
#elif defined (STOP_SEC_VAR_FAST_UNSPECIFIED)
    #undef STOP_SEC_VAR_FAST_UNSPECIFIED
    #undef MEMMAP_ERROR

#endif /* START_SEC_VAR_FAST_<SIZE> */


/****************************************/
#if defined (START_WITH_IF)

#elif defined (START_SEC_CONST_BOOLEAN)
    #undef START_SEC_CONST_BOOLEAN
    #undef MEMMAP_ERROR
#elif defined (STOP_SEC_CONST_BOOLEAN)
    #undef STOP_SEC_CONST_BOOLEAN
    #undef MEMMAP_ERROR

#elif defined (START_SEC_CONST_8BIT)
    #undef START_SEC_CONST_8BIT
    #undef MEMMAP_ERROR
#elif defined (STOP_SEC_CONST_8BIT)
    #undef STOP_SEC_CONST_8BIT
    #undef MEMMAP_ERROR

#elif defined (START_SEC_CONST_16BIT)
    #undef START_SEC_CONST_16BIT
    #undef MEMMAP_ERROR
#elif defined (STOP_SEC_CONST_16BIT)
    #undef STOP_SEC_CONST_16BIT
    #undef MEMMAP_ERROR

#elif defined (START_SEC_CONST_32BIT)
    #undef START_SEC_CONST_32BIT
    #undef MEMMAP_ERROR
#elif defined (STOP_SEC_CONST_32BIT)
    #undef STOP_SEC_CONST_32BIT
    #undef MEMMAP_ERROR

#elif defined (START_SEC_CONST_UNSPECIFIED)
    #undef START_SEC_CONST_UNSPECIFIED
    #undef MEMMAP_ERROR
#elif defined (STOP_SEC_CONST_UNSPECIFIED)
    #undef STOP_SEC_CONST_UNSPECIFIED
    #undef MEMMAP_ERROR

#endif /* START_SEC_CONST_<SIZE> */

/****************************************/
#if defined (START_WITH_IF)

#elif defined (START_SEC_CONST_FAST_BOOLEAN)
    #undef START_SEC_CONST_FAST_BOOLEAN
    #undef MEMMAP_ERROR
#elif defined (STOP_SEC_CONST_FAST_BOOLEAN)
    #undef STOP_SEC_CONST_FAST_BOOLEAN
    #undef MEMMAP_ERROR

#elif defined (START_SEC_CONST_FAST_8BIT)
    #undef START_SEC_CONST_FAST_8BIT
    #undef MEMMAP_ERROR
#elif defined (STOP_SEC_CONST_FAST_8BIT)
    #undef STOP_SEC_CONST_FAST_8BIT
    #undef MEMMAP_ERROR

#elif defined (START_SEC_CONST_FAST_16BIT)
    #undef START_SEC_CONST_FAST_16BIT
    #undef MEMMAP_ERROR
#elif defined (STOP_SEC_CONST_FAST_16BIT)
    #undef STOP_SEC_CONST_FAST_16BIT
    #undef MEMMAP_ERROR

#elif defined (START_SEC_CONST_FAST_32BIT)
    #undef START_SEC_CONST_FAST_32BIT
    #undef MEMMAP_ERROR
#elif defined (STOP_SEC_CONST_FAST_32BIT)
    #undef STOP_SEC_CONST_FAST_32BIT
    #undef MEMMAP_ERROR

#elif defined (START_SEC_CONST_FAST_UNSPECIFIED)
    #undef START_SEC_CONST_FAST_UNSPECIFIED
    #undef MEMMAP_ERROR
#elif defined (STOP_SEC_CONST_FAST_UNSPECIFIED)
    #undef STOP_SEC_CONST_FAST_UNSPECIFIED
    #undef MEMMAP_ERROR

#endif /* START_SEC_CONST_FAST_<SIZE> */

/****************************************/
#if defined (START_WITH_IF)

#elif defined (START_CONST_PBCFG)
    #undef START_CONST_PBCFG
    #undef MEMMAP_ERROR
#elif defined (STOP_CONST_PBCFG)
    #undef STOP_CONST_PBCFG
    #undef MEMMAP_ERROR

#endif /* START_CONST_PBCFG */

/****************************************/
#if defined (START_WITH_IF)

#elif defined (START_CONST_PBCFG_ROOT)
    #undef START_CONST_PBCFG_ROOT
    #undef MEMMAP_ERROR
#elif defined (STOP_CONST_PBCFG_ROOT)
    #undef STOP_CONST_PBCFG_ROOT
    #undef MEMMAP_ERROR

#endif /* START_CONST_PBCFG */







/******************************************************************************/
/************* add 20140610 for Mcal Intergration,by  tommy *******************/
/******************************************************************************/

#if defined (START_WITH_IF)

/* -------------------------------------------------------------------------- */
/*             MCU                                                            */
/* -------------------------------------------------------------------------- */
#elif defined (MCU_START_SEC_VAR_NOINIT_1BIT)
   #undef      MCU_START_SEC_VAR_NOINIT_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (MCU_STOP_SEC_VAR_NOINIT_1BIT)
   #undef      MCU_STOP_SEC_VAR_NOINIT_1BIT
   #pragma pop
   
#elif defined (MCU_START_SEC_VAR_NOINIT_8BIT)
   #undef      MCU_START_SEC_VAR_NOINIT_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (MCU_STOP_SEC_VAR_NOINIT_8BIT)
   #undef      MCU_STOP_SEC_VAR_NOINIT_8BIT
   #pragma pop
   
#elif defined (MCU_START_SEC_VAR_NOINIT_16BIT)
   #undef      MCU_START_SEC_VAR_NOINIT_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (MCU_STOP_SEC_VAR_NOINIT_16BIT)
   #undef      MCU_STOP_SEC_VAR_NOINIT_16BIT
   #pragma pop
   
#elif defined (MCU_START_SEC_VAR_NOINIT_32BIT)
   #undef      MCU_START_SEC_VAR_NOINIT_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (MCU_STOP_SEC_VAR_NOINIT_32BIT)
   #undef      MCU_STOP_SEC_VAR_NOINIT_32BIT
   #pragma pop   

#elif defined (MCU_START_SEC_VAR_NOINIT_UNSPECIFIED)
   #undef      MCU_START_SEC_VAR_NOINIT_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (MCU_STOP_SEC_VAR_NOINIT_UNSPECIFIED)
   #undef      MCU_STOP_SEC_VAR_NOINIT_UNSPECIFIED
   #pragma pop   
   
#elif defined (MCU_START_SEC_VAR_POWER_ON_INIT_1BIT)
   #undef      MCU_START_SEC_VAR_POWER_ON_INIT_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (MCU_STOP_SEC_VAR_POWER_ON_INIT_1BIT)
   #undef      MCU_STOP_SEC_VAR_POWER_ON_INIT_1BIT
   #pragma pop
   
#elif defined (MCU_START_SEC_VAR_POWER_ON_INIT_8BIT)
   #undef      MCU_START_SEC_VAR_POWER_ON_INIT_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (MCU_STOP_SEC_VAR_POWER_ON_INIT_8BIT)
   #undef      MCU_STOP_SEC_VAR_POWER_ON_INIT_8BIT
   #pragma pop
   
#elif defined (MCU_START_SEC_VAR_POWER_ON_INIT_16BIT)
   #undef      MCU_START_SEC_VAR_POWER_ON_INIT_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (MCU_STOP_SEC_VAR_POWER_ON_INIT_16BIT)
   #undef      MCU_STOP_SEC_VAR_POWER_ON_INIT_16BIT
   #pragma pop
   
#elif defined (MCU_START_SEC_VAR_POWER_ON_INIT_32BIT)
   #undef      MCU_START_SEC_VAR_POWER_ON_INIT_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (MCU_STOP_SEC_VAR_POWER_ON_INIT_32BIT)
   #undef      MCU_STOP_SEC_VAR_POWER_ON_INIT_32BIT
   #pragma pop  

#elif defined (MCU_START_SEC_VAR_POWER_ON_INIT_UNSPECIFIED)
   #undef      MCU_START_SEC_VAR_POWER_ON_INIT_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (MCU_STOP_SEC_VAR_POWER_ON_INIT_UNSPECIFIED)
   #undef      MCU_STOP_SEC_VAR_POWER_ON_INIT_UNSPECIFIED
   #pragma pop   
   
#elif defined (MCU_START_SEC_VAR_1BIT)
   #undef      MCU_START_SEC_VAR_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (MCU_STOP_SEC_VAR_1BIT)
   #undef      MCU_STOP_SEC_VAR_1BIT
   #pragma pop
   
#elif defined (MCU_START_SEC_VAR_8BIT)
   #undef      MCU_START_SEC_VAR_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (MCU_STOP_SEC_VAR_8BIT)
   #undef      MCU_STOP_SEC_VAR_8BIT
   #pragma pop
   
#elif defined (MCU_START_SEC_VAR_16BIT)
   #undef      MCU_START_SEC_VAR_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (MCU_STOP_SEC_VAR_16BIT)
   #undef      MCU_STOP_SEC_VAR_16BIT
   #pragma pop
   
#elif defined (MCU_START_SEC_VAR_32BIT)
   #undef      MCU_START_SEC_VAR_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (MCU_STOP_SEC_VAR_32BIT)
   #undef      MCU_STOP_SEC_VAR_32BIT
   #pragma pop   
   
#elif defined (MCU_START_SEC_VAR_UNSPECIFIED)
   #undef      MCU_START_SEC_VAR_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (MCU_STOP_SEC_VAR_UNSPECIFIED)
   #undef      MCU_STOP_SEC_VAR_UNSPECIFIED
   #pragma pop   
   
#elif defined (MCU_START_SEC_VAR_FAST_NOINIT_1BIT)
   #undef      MCU_START_SEC_VAR_FAST_NOINIT_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (MCU_STOP_SEC_VAR_FAST_NOINIT_1BIT)
   #undef      MCU_STOP_SEC_VAR_FAST_NOINIT_1BIT
   #pragma pop
   
#elif defined (MCU_START_SEC_VAR_FAST_NOINIT_8BIT)
   #undef      MCU_START_SEC_VAR_FAST_NOINIT_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (MCU_STOP_SEC_VAR_FAST_NOINIT_8BIT)
   #undef      MCU_STOP_SEC_VAR_FAST_NOINIT_8BIT
   #pragma pop
   
#elif defined (MCU_START_SEC_VAR_FAST_NOINIT_16BIT)
   #undef      MCU_START_SEC_VAR_FAST_NOINIT_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (MCU_STOP_SEC_VAR_FAST_NOINIT_16BIT)
   #undef      MCU_STOP_SEC_VAR_FAST_NOINIT_16BIT
   #pragma pop
   
#elif defined (MCU_START_SEC_VAR_FAST_NOINIT_32BIT)
   #undef      MCU_START_SEC_VAR_FAST_NOINIT_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (MCU_STOP_SEC_VAR_FAST_NOINIT_32BIT)
   #undef      MCU_STOP_SEC_VAR_FAST_NOINIT_32BIT
   #pragma pop   
    
#elif defined (MCU_START_SEC_VAR_FAST_NOINIT_UNSPECIFIED)
   #undef      MCU_START_SEC_VAR_FAST_NOINIT_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (MCU_STOP_SEC_VAR_FAST_NOINIT_UNSPECIFIED)
   #undef      MCU_STOP_SEC_VAR_FAST_NOINIT_UNSPECIFIED
   #pragma pop     
   
#elif defined (MCU_START_SEC_VAR_FAST_POWER_ON_INIT_1BIT)
   #undef      MCU_START_SEC_VAR_FAST_POWER_ON_INIT_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (MCU_STOP_SEC_VAR_FAST_POWER_ON_INIT_1BIT)
   #undef      MCU_STOP_SEC_VAR_FAST_POWER_ON_INIT_1BIT
   #pragma pop
   
#elif defined (MCU_START_SEC_VAR_FAST_POWER_ON_INIT_8BIT)
   #undef      MCU_START_SEC_VAR_FAST_POWER_ON_INIT_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (MCU_STOP_SEC_VAR_FAST_POWER_ON_INIT_8BIT)
   #undef      MCU_STOP_SEC_VAR_FAST_POWER_ON_INIT_8BIT
   #pragma pop
   
#elif defined (MCU_START_SEC_VAR_FAST_POWER_ON_INIT_16BIT)
   #undef      MCU_START_SEC_VAR_FAST_POWER_ON_INIT_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (MCU_STOP_SEC_VAR_FAST_POWER_ON_INIT_16BIT)
   #undef      MCU_STOP_SEC_VAR_FAST_POWER_ON_INIT_16BIT
   #pragma pop
   
#elif defined (MCU_START_SEC_VAR_FAST_POWER_ON_INIT_32BIT)
   #undef      MCU_START_SEC_VAR_FAST_POWER_ON_INIT_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (MCU_STOP_SEC_VAR_FAST_POWER_ON_INIT_32BIT)
   #undef      MCU_STOP_SEC_VAR_FAST_POWER_ON_INIT_32BIT
   #pragma pop   
    
#elif defined (MCU_START_SEC_VAR_FAST_POWER_ON_INIT_UNSPECIFIED)
   #undef      MCU_START_SEC_VAR_FAST_POWER_ON_INIT_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (MCU_STOP_SEC_VAR_FAST_POWER_ON_INIT_UNSPECIFIED)
   #undef      MCU_STOP_SEC_VAR_FAST_POWER_ON_INIT_UNSPECIFIED
   #pragma pop     
   
#elif defined (MCU_START_SEC_VAR_FAST_1BIT)
   #undef      MCU_START_SEC_VAR_FAST_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (MCU_STOP_SEC_VAR_FAST_1BIT)
   #undef      MCU_STOP_SEC_VAR_FAST_1BIT
   #pragma pop
   
#elif defined (MCU_START_SEC_VAR_FAST_8BIT)
   #undef      MCU_START_SEC_VAR_FAST_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (MCU_STOP_SEC_VAR_FAST_8BIT)
   #undef      MCU_STOP_SEC_VAR_FAST_8BIT
   #pragma pop
   
#elif defined (MCU_START_SEC_VAR_FAST_16BIT)
   #undef      MCU_START_SEC_VAR_FAST_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (MCU_STOP_SEC_VAR_FAST_16BIT)
   #undef      MCU_STOP_SEC_VAR_FAST_16BIT
   #pragma pop
   
#elif defined (MCU_START_SEC_VAR_FAST_32BIT)
   #undef      MCU_START_SEC_VAR_FAST_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (MCU_STOP_SEC_VAR_FAST_32BIT)
   #undef      MCU_STOP_SEC_VAR_FAST_32BIT
   #pragma pop   
   
#elif defined (MCU_START_SEC_VAR_FAST_UNSPECIFIED)
   #undef      MCU_START_SEC_VAR_FAST_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (MCU_STOP_SEC_VAR_FAST_UNSPECIFIED)
   #undef      MCU_STOP_SEC_VAR_FAST_UNSPECIFIED
   #pragma pop   
   
#elif defined (MCU_START_SEC_CONST_1BIT)
   #undef      MCU_START_SEC_CONST_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (MCU_STOP_SEC_CONST_1BIT)
   #undef      MCU_STOP_SEC_CONST_1BIT
   #pragma pop
   
#elif defined (MCU_START_SEC_CONST_8BIT)
   #undef      MCU_START_SEC_CONST_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (MCU_STOP_SEC_CONST_8BIT)
   #undef      MCU_STOP_SEC_CONST_8BIT
   #pragma pop
   
#elif defined (MCU_START_SEC_CONST_16BIT)
   #undef      MCU_START_SEC_CONST_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (MCU_STOP_SEC_CONST_16BIT)
   #undef      MCU_STOP_SEC_CONST_16BIT
   #pragma pop
   
#elif defined (MCU_START_SEC_CONST_32BIT)
   #undef      MCU_START_SEC_CONST_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (MCU_STOP_SEC_CONST_32BIT)
   #undef      MCU_STOP_SEC_CONST_32BIT
   #pragma pop   
      
#elif defined (MCU_START_SEC_CONST_UNSPECIFIED)
   #undef      MCU_START_SEC_CONST_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (MCU_STOP_SEC_CONST_UNSPECIFIED)
   #undef      MCU_STOP_SEC_CONST_UNSPECIFIED
   #pragma pop   
   
#elif defined (MCU_START_SEC_CONST_FAST_1BIT)
   #undef      MCU_START_SEC_CONST_FAST_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (MCU_STOP_SEC_CONST_FAST_1BIT)
   #undef      MCU_STOP_SEC_CONST_FAST_1BIT
   #pragma pop
   
#elif defined (MCU_START_SEC_CONST_FAST_8BIT)
   #undef      MCU_START_SEC_CONST_FAST_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (MCU_STOP_SEC_CONST_FAST_8BIT)
   #undef      MCU_STOP_SEC_CONST_FAST_8BIT
   #pragma pop
   
#elif defined (MCU_START_SEC_CONST_FAST_16BIT)
   #undef      MCU_START_SEC_CONST_FAST_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (MCU_STOP_SEC_CONST_FAST_16BIT)
   #undef      MCU_STOP_SEC_CONST_FAST_16BIT
   #pragma pop
   
#elif defined (MCU_START_SEC_CONST_FAST_32BIT)
   #undef      MCU_START_SEC_CONST_FAST_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (MCU_STOP_SEC_CONST_FAST_32BIT)
   #undef      MCU_STOP_SEC_CONST_FAST_32BIT
   #pragma pop   
      
#elif defined (MCU_START_SEC_CONST_FAST_UNSPECIFIED)
   #undef      MCU_START_SEC_CONST_FAST_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (MCU_STOP_SEC_CONST_FAST_UNSPECIFIED)
   #undef      MCU_STOP_SEC_CONST_FAST_UNSPECIFIED
   #pragma pop   
   
#elif defined (MCU_START_SEC_CONST_PBCFG)
   #undef      MCU_START_SEC_CONST_PBCFG
   //#pragma push
   //#pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel
#elif defined (MCU_STOP_SEC_CONST_PBCFG)
   #undef      MCU_STOP_SEC_CONST_PBCFG
   //#pragma pop
   
#elif defined (MCU_START_SEC_CONST_PBCFG_ROOT)
   #undef      MCU_START_SEC_CONST_PBCFG_ROOT
   //#pragma push
   //#pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel
#elif defined (MCU_STOP_SEC_CONST_PBCFG_ROOT)
   #undef      MCU_STOP_SEC_CONST_PBCFG_ROOT
   //#pragma pop

#elif defined (MCU_START_SEC_CODE)
   #undef      MCU_START_SEC_CODE
   //#pragma push
   __declspec (section ".MCAL_CODE")
#elif defined (MCU_STOP_SEC_CODE)
   #undef      MCU_STOP_SEC_CODE
   //#pragma pop
   
#elif defined (MCU_START_SEC_CODE_FAST)
   #undef      MCU_START_SEC_CODE_FAST
   #pragma push
   __declspec (section ".MCAL_CODE")
#elif defined (MCU_STOP_SEC_CODE_FAST)
   #undef      MCU_STOP_SEC_CODE_FAST
   #pragma pop
 
#elif defined (MCU_START_SEC_APPL_CODE)
   #undef      MCU_START_SEC_APPL_CODE
   #pragma push
   __declspec (section ".MCU_APPL_CODE_ROM")
#elif defined (MCU_STOP_SEC_APPL_CODE)
   #undef      MCU_STOP_SEC_APPL_CODE
   #pragma pop


/* -------------------------------------------------------------------------- */
/*             GPT                                                            */
/* -------------------------------------------------------------------------- */
#elif defined (GPT_START_SEC_VAR_NOINIT_1BIT)
   #undef      GPT_START_SEC_VAR_NOINIT_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (GPT_STOP_SEC_VAR_NOINIT_1BIT)
   #undef      GPT_STOP_SEC_VAR_NOINIT_1BIT
   #pragma pop
   
#elif defined (GPT_START_SEC_VAR_NOINIT_8BIT)
   #undef      GPT_START_SEC_VAR_NOINIT_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (GPT_STOP_SEC_VAR_NOINIT_8BIT)
   #undef      GPT_STOP_SEC_VAR_NOINIT_8BIT
   #pragma pop
   
#elif defined (GPT_START_SEC_VAR_NOINIT_16BIT)
   #undef      GPT_START_SEC_VAR_NOINIT_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (GPT_STOP_SEC_VAR_NOINIT_16BIT)
   #undef      GPT_STOP_SEC_VAR_NOINIT_16BIT
   #pragma pop
   
#elif defined (GPT_START_SEC_VAR_NOINIT_32BIT)
   #undef      GPT_START_SEC_VAR_NOINIT_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (GPT_STOP_SEC_VAR_NOINIT_32BIT)
   #undef      GPT_STOP_SEC_VAR_NOINIT_32BIT
   #pragma pop   

#elif defined (GPT_START_SEC_VAR_NOINIT_UNSPECIFIED)
   #undef      GPT_START_SEC_VAR_NOINIT_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (GPT_STOP_SEC_VAR_NOINIT_UNSPECIFIED)
   #undef      GPT_STOP_SEC_VAR_NOINIT_UNSPECIFIED
   #pragma pop   
   
#elif defined (GPT_START_SEC_VAR_POWER_ON_INIT_1BIT)
   #undef      GPT_START_SEC_VAR_POWER_ON_INIT_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (GPT_STOP_SEC_VAR_POWER_ON_INIT_1BIT)
   #undef      GPT_STOP_SEC_VAR_POWER_ON_INIT_1BIT
   #pragma pop
   
#elif defined (GPT_START_SEC_VAR_POWER_ON_INIT_8BIT)
   #undef      GPT_START_SEC_VAR_POWER_ON_INIT_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (GPT_STOP_SEC_VAR_POWER_ON_INIT_8BIT)
   #undef      GPT_STOP_SEC_VAR_POWER_ON_INIT_8BIT
   #pragma pop
   
#elif defined (GPT_START_SEC_VAR_POWER_ON_INIT_16BIT)
   #undef      GPT_START_SEC_VAR_POWER_ON_INIT_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (GPT_STOP_SEC_VAR_POWER_ON_INIT_16BIT)
   #undef      GPT_STOP_SEC_VAR_POWER_ON_INIT_16BIT
   #pragma pop
   
#elif defined (GPT_START_SEC_VAR_POWER_ON_INIT_32BIT)
   #undef      GPT_START_SEC_VAR_POWER_ON_INIT_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (GPT_STOP_SEC_VAR_POWER_ON_INIT_32BIT)
   #undef      GPT_STOP_SEC_VAR_POWER_ON_INIT_32BIT
   #pragma pop  
   
#elif defined (GPT_START_SEC_VAR_POWER_ON_INIT_UNSPECIFIED)
   #undef      GPT_START_SEC_VAR_POWER_ON_INIT_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (GPT_STOP_SEC_VAR_POWER_ON_INIT_UNSPECIFIED)
   #undef      GPT_STOP_SEC_VAR_POWER_ON_INIT_UNSPECIFIED
   #pragma pop   
   
#elif defined (GPT_START_SEC_VAR_1BIT)
   #undef      GPT_START_SEC_VAR_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (GPT_STOP_SEC_VAR_1BIT)
   #undef      GPT_STOP_SEC_VAR_1BIT
   #pragma pop
   
#elif defined (GPT_START_SEC_VAR_8BIT)
   #undef      GPT_START_SEC_VAR_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (GPT_STOP_SEC_VAR_8BIT)
   #undef      GPT_STOP_SEC_VAR_8BIT
   #pragma pop
   
#elif defined (GPT_START_SEC_VAR_16BIT)
   #undef      GPT_START_SEC_VAR_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (GPT_STOP_SEC_VAR_16BIT)
   #undef      GPT_STOP_SEC_VAR_16BIT
   #pragma pop
   
#elif defined (GPT_START_SEC_VAR_32BIT)
   #undef      GPT_START_SEC_VAR_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (GPT_STOP_SEC_VAR_32BIT)
   #undef      GPT_STOP_SEC_VAR_32BIT
   #pragma pop   
   
#elif defined (GPT_START_SEC_VAR_UNSPECIFIED)
   #undef      GPT_START_SEC_VAR_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (GPT_STOP_SEC_VAR_UNSPECIFIED)
   #undef      GPT_STOP_SEC_VAR_UNSPECIFIED
   #pragma pop   
   
#elif defined (GPT_START_SEC_VAR_FAST_NOINIT_1BIT)
   #undef      GPT_START_SEC_VAR_FAST_NOINIT_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (GPT_STOP_SEC_VAR_FAST_NOINIT_1BIT)
   #undef      GPT_STOP_SEC_VAR_FAST_NOINIT_1BIT
   #pragma pop
   
#elif defined (GPT_START_SEC_VAR_FAST_NOINIT_8BIT)
   #undef      GPT_START_SEC_VAR_FAST_NOINIT_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (GPT_STOP_SEC_VAR_FAST_NOINIT_8BIT)
   #undef      GPT_STOP_SEC_VAR_FAST_NOINIT_8BIT
   #pragma pop
   
#elif defined (GPT_START_SEC_VAR_FAST_NOINIT_16BIT)
   #undef      GPT_START_SEC_VAR_FAST_NOINIT_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (GPT_STOP_SEC_VAR_FAST_NOINIT_16BIT)
   #undef      GPT_STOP_SEC_VAR_FAST_NOINIT_16BIT
   #pragma pop
   
#elif defined (GPT_START_SEC_VAR_FAST_NOINIT_32BIT)
   #undef      GPT_START_SEC_VAR_FAST_NOINIT_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (GPT_STOP_SEC_VAR_FAST_NOINIT_32BIT)
   #undef      GPT_STOP_SEC_VAR_FAST_NOINIT_32BIT
   #pragma pop   
    
#elif defined (GPT_START_SEC_VAR_FAST_NOINIT_UNSPECIFIED)
   #undef      GPT_START_SEC_VAR_FAST_NOINIT_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (GPT_STOP_SEC_VAR_FAST_NOINIT_UNSPECIFIED)
   #undef      GPT_STOP_SEC_VAR_FAST_NOINIT_UNSPECIFIED
   #pragma pop     
   
#elif defined (GPT_START_SEC_VAR_FAST_POWER_ON_INIT_1BIT)
   #undef      GPT_START_SEC_VAR_FAST_POWER_ON_INIT_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (GPT_STOP_SEC_VAR_FAST_POWER_ON_INIT_1BIT)
   #undef      GPT_STOP_SEC_VAR_FAST_POWER_ON_INIT_1BIT
   #pragma pop
   
#elif defined (GPT_START_SEC_VAR_FAST_POWER_ON_INIT_8BIT)
   #undef      GPT_START_SEC_VAR_FAST_POWER_ON_INIT_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (GPT_STOP_SEC_VAR_FAST_POWER_ON_INIT_8BIT)
   #undef      GPT_STOP_SEC_VAR_FAST_POWER_ON_INIT_8BIT
   #pragma pop
   
#elif defined (GPT_START_SEC_VAR_FAST_POWER_ON_INIT_16BIT)
   #undef      GPT_START_SEC_VAR_FAST_POWER_ON_INIT_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (GPT_STOP_SEC_VAR_FAST_POWER_ON_INIT_16BIT)
   #undef      GPT_STOP_SEC_VAR_FAST_POWER_ON_INIT_16BIT
   #pragma pop
   
#elif defined (GPT_START_SEC_VAR_FAST_POWER_ON_INIT_32BIT)
   #undef      GPT_START_SEC_VAR_FAST_POWER_ON_INIT_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (GPT_STOP_SEC_VAR_FAST_POWER_ON_INIT_32BIT)
   #undef      GPT_STOP_SEC_VAR_FAST_POWER_ON_INIT_32BIT
   #pragma pop   
    
#elif defined (GPT_START_SEC_VAR_FAST_POWER_ON_INIT_UNSPECIFIED)
   #undef      GPT_START_SEC_VAR_FAST_POWER_ON_INIT_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (GPT_STOP_SEC_VAR_FAST_POWER_ON_INIT_UNSPECIFIED)
   #undef      GPT_STOP_SEC_VAR_FAST_POWER_ON_INIT_UNSPECIFIED
   #pragma pop     
   
#elif defined (GPT_START_SEC_VAR_FAST_1BIT)
   #undef      GPT_START_SEC_VAR_FAST_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (GPT_STOP_SEC_VAR_FAST_1BIT)
   #undef      GPT_STOP_SEC_VAR_FAST_1BIT
   #pragma pop
   
#elif defined (GPT_START_SEC_VAR_FAST_8BIT)
   #undef      GPT_START_SEC_VAR_FAST_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (GPT_STOP_SEC_VAR_FAST_8BIT)
   #undef      GPT_STOP_SEC_VAR_FAST_8BIT
   #pragma pop
   
#elif defined (GPT_START_SEC_VAR_FAST_16BIT)
   #undef      GPT_START_SEC_VAR_FAST_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (GPT_STOP_SEC_VAR_FAST_16BIT)
   #undef      GPT_STOP_SEC_VAR_FAST_16BIT
   #pragma pop
   
#elif defined (GPT_START_SEC_VAR_FAST_32BIT)
   #undef      GPT_START_SEC_VAR_FAST_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (GPT_STOP_SEC_VAR_FAST_32BIT)
   #undef      GPT_STOP_SEC_VAR_FAST_32BIT
   #pragma pop   
   
#elif defined (GPT_START_SEC_VAR_FAST_UNSPECIFIED)
   #undef      GPT_START_SEC_VAR_FAST_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (GPT_STOP_SEC_VAR_FAST_UNSPECIFIED)
   #undef      GPT_STOP_SEC_VAR_FAST_UNSPECIFIED
   #pragma pop   
   
#elif defined (GPT_START_SEC_CONST_1BIT)
   #undef      GPT_START_SEC_CONST_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (GPT_STOP_SEC_CONST_1BIT)
   #undef      GPT_STOP_SEC_CONST_1BIT
   #pragma pop
   
#elif defined (GPT_START_SEC_CONST_8BIT)
   #undef      GPT_START_SEC_CONST_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (GPT_STOP_SEC_CONST_8BIT)
   #undef      GPT_STOP_SEC_CONST_8BIT
   #pragma pop
   
#elif defined (GPT_START_SEC_CONST_16BIT)
   #undef      GPT_START_SEC_CONST_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (GPT_STOP_SEC_CONST_16BIT)
   #undef      GPT_STOP_SEC_CONST_16BIT
   #pragma pop
   
#elif defined (GPT_START_SEC_CONST_32BIT)
   #undef      GPT_START_SEC_CONST_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (GPT_STOP_SEC_CONST_32BIT)
   #undef      GPT_STOP_SEC_CONST_32BIT
   #pragma pop   
      
#elif defined (GPT_START_SEC_CONST_UNSPECIFIED)
   #undef      GPT_START_SEC_CONST_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (GPT_STOP_SEC_CONST_UNSPECIFIED)
   #undef      GPT_STOP_SEC_CONST_UNSPECIFIED
   #pragma pop   

   
#elif defined (GPT_START_SEC_CONST_FAST_1BIT)
   #undef      GPT_START_SEC_CONST_FAST_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (GPT_STOP_SEC_CONST_FAST_1BIT)
   #undef      GPT_STOP_SEC_CONST_FAST_1BIT
   #pragma pop
   
#elif defined (GPT_START_SEC_CONST_FAST_8BIT)
   #undef      GPT_START_SEC_CONST_FAST_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (GPT_STOP_SEC_CONST_FAST_8BIT)
   #undef      GPT_STOP_SEC_CONST_FAST_8BIT
   #pragma pop
   
#elif defined (GPT_START_SEC_CONST_FAST_16BIT)
   #undef      GPT_START_SEC_CONST_FAST_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (GPT_STOP_SEC_CONST_FAST_16BIT)
   #undef      GPT_STOP_SEC_CONST_FAST_16BIT
   #pragma pop
   
#elif defined (GPT_START_SEC_CONST_FAST_32BIT)
   #undef      GPT_START_SEC_CONST_FAST_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (GPT_STOP_SEC_CONST_FAST_32BIT)
   #undef      GPT_STOP_SEC_CONST_FAST_32BIT
   #pragma pop   
      
#elif defined (GPT_START_SEC_CONST_FAST_UNSPECIFIED)
   #undef      GPT_START_SEC_CONST_FAST_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (GPT_STOP_SEC_CONST_FAST_UNSPECIFIED)
   #undef      GPT_STOP_SEC_CONST_FAST_UNSPECIFIED
   #pragma pop   
   
#elif defined (GPT_START_SEC_CONST_PBCFG)
   #undef      GPT_START_SEC_CONST_PBCFG
   //#pragma push
   //#pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel
#elif defined (GPT_STOP_SEC_CONST_PBCFG)
   #undef      GPT_STOP_SEC_CONST_PBCFG
   //#pragma pop

#elif defined (GPT_START_SEC_CONST_PBCFG_ROOT)
   #undef      GPT_START_SEC_CONST_PBCFG_ROOT
   //#pragma push
   //#pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel
#elif defined (GPT_STOP_SEC_CONST_PBCFG_ROOT)
   #undef      GPT_STOP_SEC_CONST_PBCFG_ROOT
   //#pragma pop
   
#elif defined (GPT_START_SEC_CODE)
   #undef      GPT_START_SEC_CODE
   //__declspec (section ".GPT_CODE_ROM")
   //#pragma push
   //__declspec (section ".MCAL_CODE")
#elif defined (GPT_STOP_SEC_CODE)
   #undef      GPT_STOP_SEC_CODE
   //#pragma pop
  

#elif defined (GPT_START_SEC_APPL_CODE)
   #undef      GPT_START_SEC_APPL_CODE
   #pragma push
   __declspec (section ".GPT_APPL_CODE_ROM")
#elif defined (GPT_STOP_SEC_APPL_CODE)
   #undef      GPT_STOP_SEC_APPL_CODE
   #pragma pop


/* -------------------------------------------------------------------------- */
/*             WDG                                                            */
/* -------------------------------------------------------------------------- */
#elif defined (WDG_START_SEC_VAR_NOINIT_1BIT)
   #undef      WDG_START_SEC_VAR_NOINIT_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (WDG_STOP_SEC_VAR_NOINIT_1BIT)
   #undef      WDG_STOP_SEC_VAR_NOINIT_1BIT
   #pragma pop
   
#elif defined (WDG_START_SEC_VAR_NOINIT_8BIT)
   #undef      WDG_START_SEC_VAR_NOINIT_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (WDG_STOP_SEC_VAR_NOINIT_8BIT)
   #undef      WDG_STOP_SEC_VAR_NOINIT_8BIT
   #pragma pop
   
#elif defined (WDG_START_SEC_VAR_NOINIT_16BIT)
   #undef      WDG_START_SEC_VAR_NOINIT_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (WDG_STOP_SEC_VAR_NOINIT_16BIT)
   #undef      WDG_STOP_SEC_VAR_NOINIT_16BIT
   #pragma pop
   
#elif defined (WDG_START_SEC_VAR_NOINIT_32BIT)
   #undef      WDG_START_SEC_VAR_NOINIT_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (WDG_STOP_SEC_VAR_NOINIT_32BIT)
   #undef      WDG_STOP_SEC_VAR_NOINIT_32BIT
   #pragma pop   

#elif defined (WDG_START_SEC_VAR_NOINIT_UNSPECIFIED)
   #undef      WDG_START_SEC_VAR_NOINIT_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (WDG_STOP_SEC_VAR_NOINIT_UNSPECIFIED)
   #undef      WDG_STOP_SEC_VAR_NOINIT_UNSPECIFIED
   #pragma pop   
   
#elif defined (WDG_START_SEC_VAR_POWER_ON_INIT_1BIT)
   #undef      WDG_START_SEC_VAR_POWER_ON_INIT_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (WDG_STOP_SEC_VAR_POWER_ON_INIT_1BIT)
   #undef      WDG_STOP_SEC_VAR_POWER_ON_INIT_1BIT
   #pragma pop
   
#elif defined (WDG_START_SEC_VAR_POWER_ON_INIT_8BIT)
   #undef      WDG_START_SEC_VAR_POWER_ON_INIT_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (WDG_STOP_SEC_VAR_POWER_ON_INIT_8BIT)
   #undef      WDG_STOP_SEC_VAR_POWER_ON_INIT_8BIT
   #pragma pop
   
#elif defined (WDG_START_SEC_VAR_POWER_ON_INIT_16BIT)
   #undef      WDG_START_SEC_VAR_POWER_ON_INIT_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (WDG_STOP_SEC_VAR_POWER_ON_INIT_16BIT)
   #undef      WDG_STOP_SEC_VAR_POWER_ON_INIT_16BIT
   #pragma pop
   
#elif defined (WDG_START_SEC_VAR_POWER_ON_INIT_32BIT)
   #undef      WDG_START_SEC_VAR_POWER_ON_INIT_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (WDG_STOP_SEC_VAR_POWER_ON_INIT_32BIT)
   #undef      WDG_STOP_SEC_VAR_POWER_ON_INIT_32BIT
   #pragma pop  

 
#elif defined (WDG_START_SEC_VAR_POWER_ON_INIT_UNSPECIFIED)
   #undef      WDG_START_SEC_VAR_POWER_ON_INIT_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (WDG_STOP_SEC_VAR_POWER_ON_INIT_UNSPECIFIED)
   #undef      WDG_STOP_SEC_VAR_POWER_ON_INIT_UNSPECIFIED
   #pragma pop   
   
#elif defined (WDG_START_SEC_VAR_1BIT)
   #undef      WDG_START_SEC_VAR_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (WDG_STOP_SEC_VAR_1BIT)
   #undef      WDG_STOP_SEC_VAR_1BIT
   #pragma pop
   
#elif defined (WDG_START_SEC_VAR_8BIT)
   #undef      WDG_START_SEC_VAR_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (WDG_STOP_SEC_VAR_8BIT)
   #undef      WDG_STOP_SEC_VAR_8BIT
   #pragma pop
   
#elif defined (WDG_START_SEC_VAR_16BIT)
   #undef      WDG_START_SEC_VAR_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (WDG_STOP_SEC_VAR_16BIT)
   #undef      WDG_STOP_SEC_VAR_16BIT
   #pragma pop
   
#elif defined (WDG_START_SEC_VAR_32BIT)
   #undef      WDG_START_SEC_VAR_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (WDG_STOP_SEC_VAR_32BIT)
   #undef      WDG_STOP_SEC_VAR_32BIT
   #pragma pop   
   
#elif defined (WDG_START_SEC_VAR_UNSPECIFIED)
   #undef      WDG_START_SEC_VAR_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (WDG_STOP_SEC_VAR_UNSPECIFIED)
   #undef      WDG_STOP_SEC_VAR_UNSPECIFIED
   #pragma pop   
   
#elif defined (WDG_START_SEC_VAR_FAST_NOINIT_1BIT)
   #undef      WDG_START_SEC_VAR_FAST_NOINIT_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (WDG_STOP_SEC_VAR_FAST_NOINIT_1BIT)
   #undef      WDG_STOP_SEC_VAR_FAST_NOINIT_1BIT
   #pragma pop
   
#elif defined (WDG_START_SEC_VAR_FAST_NOINIT_8BIT)
   #undef      WDG_START_SEC_VAR_FAST_NOINIT_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (WDG_STOP_SEC_VAR_FAST_NOINIT_8BIT)
   #undef      WDG_STOP_SEC_VAR_FAST_NOINIT_8BIT
   #pragma pop
   
#elif defined (WDG_START_SEC_VAR_FAST_NOINIT_16BIT)
   #undef      WDG_START_SEC_VAR_FAST_NOINIT_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (WDG_STOP_SEC_VAR_FAST_NOINIT_16BIT)
   #undef      WDG_STOP_SEC_VAR_FAST_NOINIT_16BIT
   #pragma pop
   
#elif defined (WDG_START_SEC_VAR_FAST_NOINIT_32BIT)
   #undef      WDG_START_SEC_VAR_FAST_NOINIT_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (WDG_STOP_SEC_VAR_FAST_NOINIT_32BIT)
   #undef      WDG_STOP_SEC_VAR_FAST_NOINIT_32BIT
   #pragma pop   
    
#elif defined (WDG_START_SEC_VAR_FAST_NOINIT_UNSPECIFIED)
   #undef      WDG_START_SEC_VAR_FAST_NOINIT_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (WDG_STOP_SEC_VAR_FAST_NOINIT_UNSPECIFIED)
   #undef      WDG_STOP_SEC_VAR_FAST_NOINIT_UNSPECIFIED
   #pragma pop     
   
#elif defined (WDG_START_SEC_VAR_FAST_POWER_ON_INIT_1BIT)
   #undef      WDG_START_SEC_VAR_FAST_POWER_ON_INIT_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (WDG_STOP_SEC_VAR_FAST_POWER_ON_INIT_1BIT)
   #undef      WDG_STOP_SEC_VAR_FAST_POWER_ON_INIT_1BIT
   #pragma pop
   
#elif defined (WDG_START_SEC_VAR_FAST_POWER_ON_INIT_8BIT)
   #undef      WDG_START_SEC_VAR_FAST_POWER_ON_INIT_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (WDG_STOP_SEC_VAR_FAST_POWER_ON_INIT_8BIT)
   #undef      WDG_STOP_SEC_VAR_FAST_POWER_ON_INIT_8BIT
   #pragma pop
   
#elif defined (WDG_START_SEC_VAR_FAST_POWER_ON_INIT_16BIT)
   #undef      WDG_START_SEC_VAR_FAST_POWER_ON_INIT_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (WDG_STOP_SEC_VAR_FAST_POWER_ON_INIT_16BIT)
   #undef      WDG_STOP_SEC_VAR_FAST_POWER_ON_INIT_16BIT
   #pragma pop
   
#elif defined (WDG_START_SEC_VAR_FAST_POWER_ON_INIT_32BIT)
   #undef      WDG_START_SEC_VAR_FAST_POWER_ON_INIT_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (WDG_STOP_SEC_VAR_FAST_POWER_ON_INIT_32BIT)
   #undef      WDG_STOP_SEC_VAR_FAST_POWER_ON_INIT_32BIT
   #pragma pop   
    
#elif defined (WDG_START_SEC_VAR_FAST_POWER_ON_INIT_UNSPECIFIED)
   #undef      WDG_START_SEC_VAR_FAST_POWER_ON_INIT_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (WDG_STOP_SEC_VAR_FAST_POWER_ON_INIT_UNSPECIFIED)
   #undef      WDG_STOP_SEC_VAR_FAST_POWER_ON_INIT_UNSPECIFIED
   #pragma pop     
   
#elif defined (WDG_START_SEC_VAR_FAST_1BIT)
   #undef      WDG_START_SEC_VAR_FAST_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (WDG_STOP_SEC_VAR_FAST_1BIT)
   #undef      WDG_STOP_SEC_VAR_FAST_1BIT
   #pragma pop
   
#elif defined (WDG_START_SEC_VAR_FAST_8BIT)
   #undef      WDG_START_SEC_VAR_FAST_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (WDG_STOP_SEC_VAR_FAST_8BIT)
   #undef      WDG_STOP_SEC_VAR_FAST_8BIT
   #pragma pop
   
#elif defined (WDG_START_SEC_VAR_FAST_16BIT)
   #undef      WDG_START_SEC_VAR_FAST_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (WDG_STOP_SEC_VAR_FAST_16BIT)
   #undef      WDG_STOP_SEC_VAR_FAST_16BIT
   #pragma pop
   
#elif defined (WDG_START_SEC_VAR_FAST_32BIT)
   #undef      WDG_START_SEC_VAR_FAST_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (WDG_STOP_SEC_VAR_FAST_32BIT)
   #undef      WDG_STOP_SEC_VAR_FAST_32BIT
   #pragma pop   
   
#elif defined (WDG_START_SEC_VAR_FAST_UNSPECIFIED)
   #undef      WDG_START_SEC_VAR_FAST_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (WDG_STOP_SEC_VAR_FAST_UNSPECIFIED)
   #undef      WDG_STOP_SEC_VAR_FAST_UNSPECIFIED
   #pragma pop   
   
#elif defined (WDG_START_SEC_CONST_1BIT)
   #undef      WDG_START_SEC_CONST_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (WDG_STOP_SEC_CONST_1BIT)
   #undef      WDG_STOP_SEC_CONST_1BIT
   #pragma pop
   
#elif defined (WDG_START_SEC_CONST_8BIT)
   #undef      WDG_START_SEC_CONST_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (WDG_STOP_SEC_CONST_8BIT)
   #undef      WDG_STOP_SEC_CONST_8BIT
   #pragma pop
   
#elif defined (WDG_START_SEC_CONST_16BIT)
   #undef      WDG_START_SEC_CONST_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (WDG_STOP_SEC_CONST_16BIT)
   #undef      WDG_STOP_SEC_CONST_16BIT
   #pragma pop
   
#elif defined (WDG_START_SEC_CONST_32BIT)
   #undef      WDG_START_SEC_CONST_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (WDG_STOP_SEC_CONST_32BIT)
   #undef      WDG_STOP_SEC_CONST_32BIT
   #pragma pop   
      
#elif defined (WDG_START_SEC_CONST_UNSPECIFIED)
   #undef      WDG_START_SEC_CONST_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (WDG_STOP_SEC_CONST_UNSPECIFIED)
   #undef      WDG_STOP_SEC_CONST_UNSPECIFIED
   #pragma pop   

   
#elif defined (WDG_START_SEC_CONST_FAST_1BIT)
   #undef      WDG_START_SEC_CONST_FAST_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (WDG_STOP_SEC_CONST_FAST_1BIT)
   #undef      WDG_STOP_SEC_CONST_FAST_1BIT
   #pragma pop
   
#elif defined (WDG_START_SEC_CONST_FAST_8BIT)
   #undef      WDG_START_SEC_CONST_FAST_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (WDG_STOP_SEC_CONST_FAST_8BIT)
   #undef      WDG_STOP_SEC_CONST_FAST_8BIT
   #pragma pop
   
#elif defined (WDG_START_SEC_CONST_FAST_16BIT)
   #undef      WDG_START_SEC_CONST_FAST_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (WDG_STOP_SEC_CONST_FAST_16BIT)
   #undef      WDG_STOP_SEC_CONST_FAST_16BIT
   #pragma pop
   
#elif defined (WDG_START_SEC_CONST_FAST_32BIT)
   #undef      WDG_START_SEC_CONST_FAST_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (WDG_STOP_SEC_CONST_FAST_32BIT)
   #undef      WDG_STOP_SEC_CONST_FAST_32BIT
   #pragma pop   
      
#elif defined (WDG_START_SEC_CONST_FAST_UNSPECIFIED)
   #undef      WDG_START_SEC_CONST_FAST_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (WDG_STOP_SEC_CONST_FAST_UNSPECIFIED)
   #undef      WDG_STOP_SEC_CONST_FAST_UNSPECIFIED
   #pragma pop   
   
#elif defined (WDG_START_SEC_CONST_PBCFG)
   #undef      WDG_START_SEC_CONST_PBCFG
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel
#elif defined (WDG_STOP_SEC_CONST_PBCFG)
   #undef      WDG_STOP_SEC_CONST_PBCFG
   #pragma pop

#elif defined (WDG_START_SEC_CONST_PBCFG_ROOT)
   #undef      WDG_START_SEC_CONST_PBCFG_ROOT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel
#elif defined (WDG_STOP_SEC_CONST_PBCFG_ROOT)
   #undef      WDG_STOP_SEC_CONST_PBCFG_ROOT
   #pragma pop

#elif defined (WDG_START_SEC_CODE)
   #undef      WDG_START_SEC_CODE
   #pragma push
   __declspec (section ".MCAL_CODE")
   //__declspec (section ".WDG_CODE_ROM")
#elif defined (WDG_STOP_SEC_CODE)
   #undef      WDG_STOP_SEC_CODE
   #pragma pop

#elif defined (WDG_START_SEC_APPL_CODE)
   #undef      WDG_START_SEC_APPL_CODE
   #pragma push
   __declspec (section ".WDG_APPL_CODE_ROM")
#elif defined (WDG_STOP_SEC_APPL_CODE)
   #undef      WDG_STOP_SEC_APPL_CODE
   #pragma pop


/* -------------------------------------------------------------------------- */
/*             WDGIF                                                          */
/* -------------------------------------------------------------------------- */




/* -------------------------------------------------------------------------- */
/*             PORT                                                           */
/* -------------------------------------------------------------------------- */
#elif defined (PORT_START_SEC_VAR_NOINIT_1BIT)
   #undef      PORT_START_SEC_VAR_NOINIT_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (PORT_STOP_SEC_VAR_NOINIT_1BIT)
   #undef      PORT_STOP_SEC_VAR_NOINIT_1BIT
   #pragma pop
   
#elif defined (PORT_START_SEC_VAR_NOINIT_8BIT)
   #undef      PORT_START_SEC_VAR_NOINIT_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (PORT_STOP_SEC_VAR_NOINIT_8BIT)
   #undef      PORT_STOP_SEC_VAR_NOINIT_8BIT
   #pragma pop
   
#elif defined (PORT_START_SEC_VAR_NOINIT_16BIT)
   #undef      PORT_START_SEC_VAR_NOINIT_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (PORT_STOP_SEC_VAR_NOINIT_16BIT)
   #undef      PORT_STOP_SEC_VAR_NOINIT_16BIT
   #pragma pop
   
#elif defined (PORT_START_SEC_VAR_NOINIT_32BIT)
   #undef      PORT_START_SEC_VAR_NOINIT_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (PORT_STOP_SEC_VAR_NOINIT_32BIT)
   #undef      PORT_STOP_SEC_VAR_NOINIT_32BIT
   #pragma pop   

#elif defined (PORT_START_SEC_VAR_NOINIT_UNSPECIFIED)
   #undef      PORT_START_SEC_VAR_NOINIT_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (PORT_STOP_SEC_VAR_NOINIT_UNSPECIFIED)
   #undef      PORT_STOP_SEC_VAR_NOINIT_UNSPECIFIED
   #pragma pop   
   
#elif defined (PORT_START_SEC_VAR_POWER_ON_INIT_1BIT)
   #undef      PORT_START_SEC_VAR_POWER_ON_INIT_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (PORT_STOP_SEC_VAR_POWER_ON_INIT_1BIT)
   #undef      PORT_STOP_SEC_VAR_POWER_ON_INIT_1BIT
   #pragma pop
   
#elif defined (PORT_START_SEC_VAR_POWER_ON_INIT_8BIT)
   #undef      PORT_START_SEC_VAR_POWER_ON_INIT_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (PORT_STOP_SEC_VAR_POWER_ON_INIT_8BIT)
   #undef      PORT_STOP_SEC_VAR_POWER_ON_INIT_8BIT
   #pragma pop
   
#elif defined (PORT_START_SEC_VAR_POWER_ON_INIT_16BIT)
   #undef      PORT_START_SEC_VAR_POWER_ON_INIT_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (PORT_STOP_SEC_VAR_POWER_ON_INIT_16BIT)
   #undef      PORT_STOP_SEC_VAR_POWER_ON_INIT_16BIT
   #pragma pop
   
#elif defined (PORT_START_SEC_VAR_POWER_ON_INIT_32BIT)
   #undef      PORT_START_SEC_VAR_POWER_ON_INIT_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (PORT_STOP_SEC_VAR_POWER_ON_INIT_32BIT)
   #undef      PORT_STOP_SEC_VAR_POWER_ON_INIT_32BIT
   #pragma pop  

 
#elif defined (PORT_START_SEC_VAR_POWER_ON_INIT_UNSPECIFIED)
   #undef      PORT_START_SEC_VAR_POWER_ON_INIT_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (PORT_STOP_SEC_VAR_POWER_ON_INIT_UNSPECIFIED)
   #undef      PORT_STOP_SEC_VAR_POWER_ON_INIT_UNSPECIFIED
   #pragma pop   
   
#elif defined (PORT_START_SEC_VAR_1BIT)
   #undef      PORT_START_SEC_VAR_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (PORT_STOP_SEC_VAR_1BIT)
   #undef      PORT_STOP_SEC_VAR_1BIT
   #pragma pop
   
#elif defined (PORT_START_SEC_VAR_8BIT)
   #undef      PORT_START_SEC_VAR_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (PORT_STOP_SEC_VAR_8BIT)
   #undef      PORT_STOP_SEC_VAR_8BIT
   #pragma pop
   
#elif defined (PORT_START_SEC_VAR_16BIT)
   #undef      PORT_START_SEC_VAR_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (PORT_STOP_SEC_VAR_16BIT)
   #undef      PORT_STOP_SEC_VAR_16BIT
   #pragma pop
   
#elif defined (PORT_START_SEC_VAR_32BIT)
   #undef      PORT_START_SEC_VAR_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (PORT_STOP_SEC_VAR_32BIT)
   #undef      PORT_STOP_SEC_VAR_32BIT
   #pragma pop   
   
#elif defined (PORT_START_SEC_VAR_UNSPECIFIED)
   #undef      PORT_START_SEC_VAR_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (PORT_STOP_SEC_VAR_UNSPECIFIED)
   #undef      PORT_STOP_SEC_VAR_UNSPECIFIED
   #pragma pop   
   
#elif defined (PORT_START_SEC_VAR_FAST_NOINIT_1BIT)
   #undef      PORT_START_SEC_VAR_FAST_NOINIT_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (PORT_STOP_SEC_VAR_FAST_NOINIT_1BIT)
   #undef      PORT_STOP_SEC_VAR_FAST_NOINIT_1BIT
   #pragma pop
   
#elif defined (PORT_START_SEC_VAR_FAST_NOINIT_8BIT)
   #undef      PORT_START_SEC_VAR_FAST_NOINIT_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (PORT_STOP_SEC_VAR_FAST_NOINIT_8BIT)
   #undef      PORT_STOP_SEC_VAR_FAST_NOINIT_8BIT
   #pragma pop
   
#elif defined (PORT_START_SEC_VAR_FAST_NOINIT_16BIT)
   #undef      PORT_START_SEC_VAR_FAST_NOINIT_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (PORT_STOP_SEC_VAR_FAST_NOINIT_16BIT)
   #undef      PORT_STOP_SEC_VAR_FAST_NOINIT_16BIT
   #pragma pop
   
#elif defined (PORT_START_SEC_VAR_FAST_NOINIT_32BIT)
   #undef      PORT_START_SEC_VAR_FAST_NOINIT_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (PORT_STOP_SEC_VAR_FAST_NOINIT_32BIT)
   #undef      PORT_STOP_SEC_VAR_FAST_NOINIT_32BIT
   #pragma pop   
    
#elif defined (PORT_START_SEC_VAR_FAST_NOINIT_UNSPECIFIED)
   #undef      PORT_START_SEC_VAR_FAST_NOINIT_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (PORT_STOP_SEC_VAR_FAST_NOINIT_UNSPECIFIED)
   #undef      PORT_STOP_SEC_VAR_FAST_NOINIT_UNSPECIFIED
   #pragma pop     
   
#elif defined (PORT_START_SEC_VAR_FAST_POWER_ON_INIT_1BIT)
   #undef      PORT_START_SEC_VAR_FAST_POWER_ON_INIT_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (PORT_STOP_SEC_VAR_FAST_POWER_ON_INIT_1BIT)
   #undef      PORT_STOP_SEC_VAR_FAST_POWER_ON_INIT_1BIT
   #pragma pop
   
#elif defined (PORT_START_SEC_VAR_FAST_POWER_ON_INIT_8BIT)
   #undef      PORT_START_SEC_VAR_FAST_POWER_ON_INIT_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (PORT_STOP_SEC_VAR_FAST_POWER_ON_INIT_8BIT)
   #undef      PORT_STOP_SEC_VAR_FAST_POWER_ON_INIT_8BIT
   #pragma pop
   
#elif defined (PORT_START_SEC_VAR_FAST_POWER_ON_INIT_16BIT)
   #undef      PORT_START_SEC_VAR_FAST_POWER_ON_INIT_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (PORT_STOP_SEC_VAR_FAST_POWER_ON_INIT_16BIT)
   #undef      PORT_STOP_SEC_VAR_FAST_POWER_ON_INIT_16BIT
   #pragma pop
   
#elif defined (PORT_START_SEC_VAR_FAST_POWER_ON_INIT_32BIT)
   #undef      PORT_START_SEC_VAR_FAST_POWER_ON_INIT_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (PORT_STOP_SEC_VAR_FAST_POWER_ON_INIT_32BIT)
   #undef      PORT_STOP_SEC_VAR_FAST_POWER_ON_INIT_32BIT
   #pragma pop   
    
#elif defined (PORT_START_SEC_VAR_FAST_POWER_ON_INIT_UNSPECIFIED)
   #undef      PORT_START_SEC_VAR_FAST_POWER_ON_INIT_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (PORT_STOP_SEC_VAR_FAST_POWER_ON_INIT_UNSPECIFIED)
   #undef      PORT_STOP_SEC_VAR_FAST_POWER_ON_INIT_UNSPECIFIED
   #pragma pop     
   
#elif defined (PORT_START_SEC_VAR_FAST_1BIT)
   #undef      PORT_START_SEC_VAR_FAST_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (PORT_STOP_SEC_VAR_FAST_1BIT)
   #undef      PORT_STOP_SEC_VAR_FAST_1BIT
   #pragma pop
   
#elif defined (PORT_START_SEC_VAR_FAST_8BIT)
   #undef      PORT_START_SEC_VAR_FAST_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (PORT_STOP_SEC_VAR_FAST_8BIT)
   #undef      PORT_STOP_SEC_VAR_FAST_8BIT
   #pragma pop
   
#elif defined (PORT_START_SEC_VAR_FAST_16BIT)
   #undef      PORT_START_SEC_VAR_FAST_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (PORT_STOP_SEC_VAR_FAST_16BIT)
   #undef      PORT_STOP_SEC_VAR_FAST_16BIT
   #pragma pop
   
#elif defined (PORT_START_SEC_VAR_FAST_32BIT)
   #undef      PORT_START_SEC_VAR_FAST_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (PORT_STOP_SEC_VAR_FAST_32BIT)
   #undef      PORT_STOP_SEC_VAR_FAST_32BIT
   #pragma pop   
   
#elif defined (PORT_START_SEC_VAR_FAST_UNSPECIFIED)
   #undef      PORT_START_SEC_VAR_FAST_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (PORT_STOP_SEC_VAR_FAST_UNSPECIFIED)
   #undef      PORT_STOP_SEC_VAR_FAST_UNSPECIFIED
   #pragma pop   
   
#elif defined (PORT_START_SEC_CONST_1BIT)
   #undef      PORT_START_SEC_CONST_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (PORT_STOP_SEC_CONST_1BIT)
   #undef      PORT_STOP_SEC_CONST_1BIT
   #pragma pop
   
#elif defined (PORT_START_SEC_CONST_8BIT)
   #undef      PORT_START_SEC_CONST_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (PORT_STOP_SEC_CONST_8BIT)
   #undef      PORT_STOP_SEC_CONST_8BIT
   #pragma pop
   
#elif defined (PORT_START_SEC_CONST_16BIT)
   #undef      PORT_START_SEC_CONST_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (PORT_STOP_SEC_CONST_16BIT)
   #undef      PORT_STOP_SEC_CONST_16BIT
   #pragma pop
   
#elif defined (PORT_START_SEC_CONST_32BIT)
   #undef      PORT_START_SEC_CONST_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (PORT_STOP_SEC_CONST_32BIT)
   #undef      PORT_STOP_SEC_CONST_32BIT
   #pragma pop   
      
#elif defined (PORT_START_SEC_CONST_UNSPECIFIED)
   #undef      PORT_START_SEC_CONST_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (PORT_STOP_SEC_CONST_UNSPECIFIED)
   #undef      PORT_STOP_SEC_CONST_UNSPECIFIED
   #pragma pop   

   
#elif defined (PORT_START_SEC_CONST_FAST_1BIT)
   #undef      PORT_START_SEC_CONST_FAST_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (PORT_STOP_SEC_CONST_FAST_1BIT)
   #undef      PORT_STOP_SEC_CONST_FAST_1BIT
   #pragma pop
   
#elif defined (PORT_START_SEC_CONST_FAST_8BIT)
   #undef      PORT_START_SEC_CONST_FAST_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (PORT_STOP_SEC_CONST_FAST_8BIT)
   #undef      PORT_STOP_SEC_CONST_FAST_8BIT
   #pragma pop
   
#elif defined (PORT_START_SEC_CONST_FAST_16BIT)
   #undef      PORT_START_SEC_CONST_FAST_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (PORT_STOP_SEC_CONST_FAST_16BIT)
   #undef      PORT_STOP_SEC_CONST_FAST_16BIT
   #pragma pop
   
#elif defined (PORT_START_SEC_CONST_FAST_32BIT)
   #undef      PORT_START_SEC_CONST_FAST_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (PORT_STOP_SEC_CONST_FAST_32BIT)
   #undef      PORT_STOP_SEC_CONST_FAST_32BIT
   #pragma pop   
      
#elif defined (PORT_START_SEC_CONST_FAST_UNSPECIFIED)
   #undef      PORT_START_SEC_CONST_FAST_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (PORT_STOP_SEC_CONST_FAST_UNSPECIFIED)
   #undef      PORT_STOP_SEC_CONST_FAST_UNSPECIFIED
   #pragma pop   
   
#elif defined (PORT_START_SEC_CONST_PBCFG)
   #undef      PORT_START_SEC_CONST_PBCFG
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel
#elif defined (PORT_STOP_SEC_CONST_PBCFG)
   #undef      PORT_STOP_SEC_CONST_PBCFG
   #pragma pop
   
#elif defined (PORT_START_SEC_CONST_PBCFG_ROOT)
   #undef      PORT_START_SEC_CONST_PBCFG_ROOT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel
#elif defined (PORT_STOP_SEC_CONST_PBCFG_ROOT)
   #undef      PORT_STOP_SEC_CONST_PBCFG_ROOT
   #pragma pop   

#elif defined (PORT_START_SEC_CONFIG_DATA)
   #undef      PORT_START_SEC_CONFIG_DATA
   #pragma push
   #pragma section data_type ".PORT_CFG_DATA" data_mode = far_abs 
#elif defined (PORT_STOP_SEC_CONFIG_DATA)
   #undef      PORT_STOP_SEC_CONFIG_DATA
   #pragma pop

#elif defined (PORT_START_SEC_CODE)
   #undef      PORT_START_SEC_CODE
   #pragma push
   __declspec (section ".MCAL_CODE")
   //__declspec (section ".PORT_CODE_ROM")
#elif defined (PORT_STOP_SEC_CODE)
   #undef      PORT_STOP_SEC_CODE
   #pragma pop

#elif defined (PORT_START_SEC_APPL_CODE)
   #undef      PORT_START_SEC_APPL_CODE
   #pragma push
   __declspec (section ".PORT_APPL_CODE_ROM")
#elif defined (PORT_STOP_SEC_APPL_CODE)
   #undef      PORT_STOP_SEC_APPL_CODE
   #pragma pop



/* -------------------------------------------------------------------------- */
/*             DIO                                                            */
/* -------------------------------------------------------------------------- */
#elif defined (DIO_START_SEC_VAR_NOINIT_1BIT)
   #undef      DIO_START_SEC_VAR_NOINIT_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (DIO_STOP_SEC_VAR_NOINIT_1BIT)
   #undef      DIO_STOP_SEC_VAR_NOINIT_1BIT
   #pragma pop
   
#elif defined (DIO_START_SEC_VAR_NOINIT_8BIT)
   #undef      DIO_START_SEC_VAR_NOINIT_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (DIO_STOP_SEC_VAR_NOINIT_8BIT)
   #undef      DIO_STOP_SEC_VAR_NOINIT_8BIT
   #pragma pop
   
#elif defined (DIO_START_SEC_VAR_NOINIT_16BIT)
   #undef      DIO_START_SEC_VAR_NOINIT_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (DIO_STOP_SEC_VAR_NOINIT_16BIT)
   #undef      DIO_STOP_SEC_VAR_NOINIT_16BIT
   #pragma pop
   
#elif defined (DIO_START_SEC_VAR_NOINIT_32BIT)
   #undef      DIO_START_SEC_VAR_NOINIT_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (DIO_STOP_SEC_VAR_NOINIT_32BIT)
   #undef      DIO_STOP_SEC_VAR_NOINIT_32BIT
   #pragma pop   

#elif defined (DIO_START_SEC_VAR_NOINIT_UNSPECIFIED)
   #undef      DIO_START_SEC_VAR_NOINIT_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (DIO_STOP_SEC_VAR_NOINIT_UNSPECIFIED)
   #undef      DIO_STOP_SEC_VAR_NOINIT_UNSPECIFIED
   #pragma pop   
   
#elif defined (DIO_START_SEC_VAR_POWER_ON_INIT_1BIT)
   #undef      DIO_START_SEC_VAR_POWER_ON_INIT_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (DIO_STOP_SEC_VAR_POWER_ON_INIT_1BIT)
   #undef      DIO_STOP_SEC_VAR_POWER_ON_INIT_1BIT
   #pragma pop
   
#elif defined (DIO_START_SEC_VAR_POWER_ON_INIT_8BIT)
   #undef      DIO_START_SEC_VAR_POWER_ON_INIT_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (DIO_STOP_SEC_VAR_POWER_ON_INIT_8BIT)
   #undef      DIO_STOP_SEC_VAR_POWER_ON_INIT_8BIT
   #pragma pop
   
#elif defined (DIO_START_SEC_VAR_POWER_ON_INIT_16BIT)
   #undef      DIO_START_SEC_VAR_POWER_ON_INIT_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (DIO_STOP_SEC_VAR_POWER_ON_INIT_16BIT)
   #undef      DIO_STOP_SEC_VAR_POWER_ON_INIT_16BIT
   #pragma pop
   
#elif defined (DIO_START_SEC_VAR_POWER_ON_INIT_32BIT)
   #undef      DIO_START_SEC_VAR_POWER_ON_INIT_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (DIO_STOP_SEC_VAR_POWER_ON_INIT_32BIT)
   #undef      DIO_STOP_SEC_VAR_POWER_ON_INIT_32BIT
   #pragma pop  

 
#elif defined (DIO_START_SEC_VAR_POWER_ON_INIT_UNSPECIFIED)
   #undef      DIO_START_SEC_VAR_POWER_ON_INIT_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (DIO_STOP_SEC_VAR_POWER_ON_INIT_UNSPECIFIED)
   #undef      DIO_STOP_SEC_VAR_POWER_ON_INIT_UNSPECIFIED
   #pragma pop   
   
#elif defined (DIO_START_SEC_VAR_1BIT)
   #undef      DIO_START_SEC_VAR_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (DIO_STOP_SEC_VAR_1BIT)
   #undef      DIO_STOP_SEC_VAR_1BIT
   #pragma pop
   
#elif defined (DIO_START_SEC_VAR_8BIT)
   #undef      DIO_START_SEC_VAR_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (DIO_STOP_SEC_VAR_8BIT)
   #undef      DIO_STOP_SEC_VAR_8BIT
   #pragma pop
   
#elif defined (DIO_START_SEC_VAR_16BIT)
   #undef      DIO_START_SEC_VAR_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (DIO_STOP_SEC_VAR_16BIT)
   #undef      DIO_STOP_SEC_VAR_16BIT
   #pragma pop
   
#elif defined (DIO_START_SEC_VAR_32BIT)
   #undef      DIO_START_SEC_VAR_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (DIO_STOP_SEC_VAR_32BIT)
   #undef      DIO_STOP_SEC_VAR_32BIT
   #pragma pop   
   
#elif defined (DIO_START_SEC_VAR_UNSPECIFIED)
   #undef      DIO_START_SEC_VAR_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (DIO_STOP_SEC_VAR_UNSPECIFIED)
   #undef      DIO_STOP_SEC_VAR_UNSPECIFIED
   #pragma pop   
   
#elif defined (DIO_START_SEC_VAR_FAST_NOINIT_1BIT)
   #undef      DIO_START_SEC_VAR_FAST_NOINIT_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (DIO_STOP_SEC_VAR_FAST_NOINIT_1BIT)
   #undef      DIO_STOP_SEC_VAR_FAST_NOINIT_1BIT
   #pragma pop
   
#elif defined (DIO_START_SEC_VAR_FAST_NOINIT_8BIT)
   #undef      DIO_START_SEC_VAR_FAST_NOINIT_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (DIO_STOP_SEC_VAR_FAST_NOINIT_8BIT)
   #undef      DIO_STOP_SEC_VAR_FAST_NOINIT_8BIT
   #pragma pop
   
#elif defined (DIO_START_SEC_VAR_FAST_NOINIT_16BIT)
   #undef      DIO_START_SEC_VAR_FAST_NOINIT_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (DIO_STOP_SEC_VAR_FAST_NOINIT_16BIT)
   #undef      DIO_STOP_SEC_VAR_FAST_NOINIT_16BIT
   #pragma pop
   
#elif defined (DIO_START_SEC_VAR_FAST_NOINIT_32BIT)
   #undef      DIO_START_SEC_VAR_FAST_NOINIT_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (DIO_STOP_SEC_VAR_FAST_NOINIT_32BIT)
   #undef      DIO_STOP_SEC_VAR_FAST_NOINIT_32BIT
   #pragma pop   
    
#elif defined (DIO_START_SEC_VAR_FAST_NOINIT_UNSPECIFIED)
   #undef      DIO_START_SEC_VAR_FAST_NOINIT_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (DIO_STOP_SEC_VAR_FAST_NOINIT_UNSPECIFIED)
   #undef      DIO_STOP_SEC_VAR_FAST_NOINIT_UNSPECIFIED
   #pragma pop     
   
#elif defined (DIO_START_SEC_VAR_FAST_POWER_ON_INIT_1BIT)
   #undef      DIO_START_SEC_VAR_FAST_POWER_ON_INIT_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (DIO_STOP_SEC_VAR_FAST_POWER_ON_INIT_1BIT)
   #undef      DIO_STOP_SEC_VAR_FAST_POWER_ON_INIT_1BIT
   #pragma pop
   
#elif defined (DIO_START_SEC_VAR_FAST_POWER_ON_INIT_8BIT)
   #undef      DIO_START_SEC_VAR_FAST_POWER_ON_INIT_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (DIO_STOP_SEC_VAR_FAST_POWER_ON_INIT_8BIT)
   #undef      DIO_STOP_SEC_VAR_FAST_POWER_ON_INIT_8BIT
   #pragma pop
   
#elif defined (DIO_START_SEC_VAR_FAST_POWER_ON_INIT_16BIT)
   #undef      DIO_START_SEC_VAR_FAST_POWER_ON_INIT_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (DIO_STOP_SEC_VAR_FAST_POWER_ON_INIT_16BIT)
   #undef      DIO_STOP_SEC_VAR_FAST_POWER_ON_INIT_16BIT
   #pragma pop
   
#elif defined (DIO_START_SEC_VAR_FAST_POWER_ON_INIT_32BIT)
   #undef      DIO_START_SEC_VAR_FAST_POWER_ON_INIT_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (DIO_STOP_SEC_VAR_FAST_POWER_ON_INIT_32BIT)
   #undef      DIO_STOP_SEC_VAR_FAST_POWER_ON_INIT_32BIT
   #pragma pop   
    
#elif defined (DIO_START_SEC_VAR_FAST_POWER_ON_INIT_UNSPECIFIED)
   #undef      DIO_START_SEC_VAR_FAST_POWER_ON_INIT_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (DIO_STOP_SEC_VAR_FAST_POWER_ON_INIT_UNSPECIFIED)
   #undef      DIO_STOP_SEC_VAR_FAST_POWER_ON_INIT_UNSPECIFIED
   #pragma pop     
   
#elif defined (DIO_START_SEC_VAR_FAST_1BIT)
   #undef      DIO_START_SEC_VAR_FAST_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (DIO_STOP_SEC_VAR_FAST_1BIT)
   #undef      DIO_STOP_SEC_VAR_FAST_1BIT
   #pragma pop
   
#elif defined (DIO_START_SEC_VAR_FAST_8BIT)
   #undef      DIO_START_SEC_VAR_FAST_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (DIO_STOP_SEC_VAR_FAST_8BIT)
   #undef      DIO_STOP_SEC_VAR_FAST_8BIT
   #pragma pop
   
#elif defined (DIO_START_SEC_VAR_FAST_16BIT)
   #undef      DIO_START_SEC_VAR_FAST_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (DIO_STOP_SEC_VAR_FAST_16BIT)
   #undef      DIO_STOP_SEC_VAR_FAST_16BIT
   #pragma pop
   
#elif defined (DIO_START_SEC_VAR_FAST_32BIT)
   #undef      DIO_START_SEC_VAR_FAST_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (DIO_STOP_SEC_VAR_FAST_32BIT)
   #undef      DIO_STOP_SEC_VAR_FAST_32BIT
   #pragma pop   
   
#elif defined (DIO_START_SEC_VAR_FAST_UNSPECIFIED)
   #undef      DIO_START_SEC_VAR_FAST_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (DIO_STOP_SEC_VAR_FAST_UNSPECIFIED)
   #undef      DIO_STOP_SEC_VAR_FAST_UNSPECIFIED
   #pragma pop   
   
#elif defined (DIO_START_SEC_CONST_1BIT)
   #undef      DIO_START_SEC_CONST_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (DIO_STOP_SEC_CONST_1BIT)
   #undef      DIO_STOP_SEC_CONST_1BIT
   #pragma pop
   
#elif defined (DIO_START_SEC_CONST_8BIT)
   #undef      DIO_START_SEC_CONST_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (DIO_STOP_SEC_CONST_8BIT)
   #undef      DIO_STOP_SEC_CONST_8BIT
   #pragma pop
   
#elif defined (DIO_START_SEC_CONST_16BIT)
   #undef      DIO_START_SEC_CONST_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (DIO_STOP_SEC_CONST_16BIT)
   #undef      DIO_STOP_SEC_CONST_16BIT
   #pragma pop
   
#elif defined (DIO_START_SEC_CONST_32BIT)
   #undef      DIO_START_SEC_CONST_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (DIO_STOP_SEC_CONST_32BIT)
   #undef      DIO_STOP_SEC_CONST_32BIT
   #pragma pop   
      
#elif defined (DIO_START_SEC_CONST_UNSPECIFIED)
   #undef      DIO_START_SEC_CONST_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (DIO_STOP_SEC_CONST_UNSPECIFIED)
   #undef      DIO_STOP_SEC_CONST_UNSPECIFIED
   #pragma pop   

   
#elif defined (DIO_START_SEC_CONST_FAST_1BIT)
   #undef      DIO_START_SEC_CONST_FAST_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (DIO_STOP_SEC_CONST_FAST_1BIT)
   #undef      DIO_STOP_SEC_CONST_FAST_1BIT
   #pragma pop
   
#elif defined (DIO_START_SEC_CONST_FAST_8BIT)
   #undef      DIO_START_SEC_CONST_FAST_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (DIO_STOP_SEC_CONST_FAST_8BIT)
   #undef      DIO_STOP_SEC_CONST_FAST_8BIT
   #pragma pop
   
#elif defined (DIO_START_SEC_CONST_FAST_16BIT)
   #undef      DIO_START_SEC_CONST_FAST_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (DIO_STOP_SEC_CONST_FAST_16BIT)
   #undef      DIO_STOP_SEC_CONST_FAST_16BIT
   #pragma pop
   
#elif defined (DIO_START_SEC_CONST_FAST_32BIT)
   #undef      DIO_START_SEC_CONST_FAST_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (DIO_STOP_SEC_CONST_FAST_32BIT)
   #undef      DIO_STOP_SEC_CONST_FAST_32BIT
   #pragma pop   
      
#elif defined (DIO_START_SEC_CONST_FAST_UNSPECIFIED)
   #undef      DIO_START_SEC_CONST_FAST_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (DIO_STOP_SEC_CONST_FAST_UNSPECIFIED)
   #undef      DIO_STOP_SEC_CONST_FAST_UNSPECIFIED
   #pragma pop   
   
#elif defined (DIO_START_SEC_CONST_PBCFG)
   #undef      DIO_START_SEC_CONST_PBCFG
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel
#elif defined (DIO_STOP_SEC_CONST_PBCFG)
   #undef      DIO_STOP_SEC_CONST_PBCFG
   #pragma pop
 
#elif defined (DIO_START_SEC_CONST_PBCFG_ROOT)
   #undef      DIO_START_SEC_CONST_PBCFG_ROOT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel
#elif defined (DIO_STOP_SEC_CONST_PBCFG_ROOT)
   #undef      DIO_STOP_SEC_CONST_PBCFG_ROOT
   #pragma pop

#elif defined (DIO_START_SEC_CODE)
   #undef      DIO_START_SEC_CODE
   #pragma push
   __declspec (section ".MCAL_CODE")
   //__declspec (section ".DIO_CODE_ROM")
#elif defined (DIO_STOP_SEC_CODE)
   #undef      DIO_STOP_SEC_CODE
   #pragma pop

#elif defined (DIO_START_SEC_APPL_CODE)
   #undef      DIO_START_SEC_APPL_CODE
   #pragma push
   __declspec (section ".DIO_APPL_CODE_ROM")
#elif defined (DIO_STOP_SEC_APPL_CODE)
   #undef      DIO_STOP_SEC_APPL_CODE
   #pragma pop


/* -------------------------------------------------------------------------- */
/*             MEMIF                                                          */
/* -------------------------------------------------------------------------- */



/* ---------------------------------------------------------------------------*/
/*                   FEE                                                      */
/* ---------------------------------------------------------------------------*/
#elif defined (FEE_START_SEC_VAR_NOINIT_1BIT)
   #undef      FEE_START_SEC_VAR_NOINIT_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (FEE_STOP_SEC_VAR_NOINIT_1BIT)
   #undef      FEE_STOP_SEC_VAR_NOINIT_1BIT
   #pragma pop
   
#elif defined (FEE_START_SEC_VAR_NOINIT_8BIT)
   #undef      FEE_START_SEC_VAR_NOINIT_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (FEE_STOP_SEC_VAR_NOINIT_8BIT)
   #undef      FEE_STOP_SEC_VAR_NOINIT_8BIT
   #pragma pop
   
#elif defined (FEE_START_SEC_VAR_NOINIT_16BIT)
   #undef      FEE_START_SEC_VAR_NOINIT_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (FEE_STOP_SEC_VAR_NOINIT_16BIT)
   #undef      FEE_STOP_SEC_VAR_NOINIT_16BIT
   #pragma pop
   
#elif defined (FEE_START_SEC_VAR_NOINIT_32BIT)
   #undef      FEE_START_SEC_VAR_NOINIT_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (FEE_STOP_SEC_VAR_NOINIT_32BIT)
   #undef      FEE_STOP_SEC_VAR_NOINIT_32BIT
   #pragma pop   

#elif defined (FEE_START_SEC_VAR_NOINIT_UNSPECIFIED)
   #undef      FEE_START_SEC_VAR_NOINIT_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (FEE_STOP_SEC_VAR_NOINIT_UNSPECIFIED)
   #undef      FEE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
   #pragma pop   
   
#elif defined (FEE_START_SEC_VAR_POWER_ON_INIT_1BIT)
   #undef      FEE_START_SEC_VAR_POWER_ON_INIT_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (FEE_STOP_SEC_VAR_POWER_ON_INIT_1BIT)
   #undef      FEE_STOP_SEC_VAR_POWER_ON_INIT_1BIT
   #pragma pop
   
#elif defined (FEE_START_SEC_VAR_POWER_ON_INIT_8BIT)
   #undef      FEE_START_SEC_VAR_POWER_ON_INIT_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (FEE_STOP_SEC_VAR_POWER_ON_INIT_8BIT)
   #undef      FEE_STOP_SEC_VAR_POWER_ON_INIT_8BIT
   #pragma pop
   
#elif defined (FEE_START_SEC_VAR_POWER_ON_INIT_16BIT)
   #undef      FEE_START_SEC_VAR_POWER_ON_INIT_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (FEE_STOP_SEC_VAR_POWER_ON_INIT_16BIT)
   #undef      FEE_STOP_SEC_VAR_POWER_ON_INIT_16BIT
   #pragma pop
   
#elif defined (FEE_START_SEC_VAR_POWER_ON_INIT_32BIT)
   #undef      FEE_START_SEC_VAR_POWER_ON_INIT_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (FEE_STOP_SEC_VAR_POWER_ON_INIT_32BIT)
   #undef      FEE_STOP_SEC_VAR_POWER_ON_INIT_32BIT
   #pragma pop  

#elif defined (FEE_START_SEC_VAR_POWER_ON_INIT_UNSPECIFIED)
   #undef      FEE_START_SEC_VAR_POWER_ON_INIT_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (FEE_STOP_SEC_VAR_POWER_ON_INIT_UNSPECIFIED)
   #undef      FEE_STOP_SEC_VAR_POWER_ON_INIT_UNSPECIFIED
   #pragma pop   
   
#elif defined (FEE_START_SEC_VAR_1BIT)
   #undef      FEE_START_SEC_VAR_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (FEE_STOP_SEC_VAR_1BIT)
   #undef      FEE_STOP_SEC_VAR_1BIT
   #pragma pop
   
#elif defined (FEE_START_SEC_VAR_8BIT)
   #undef      FEE_START_SEC_VAR_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (FEE_STOP_SEC_VAR_8BIT)
   #undef      FEE_STOP_SEC_VAR_8BIT
   #pragma pop
   
#elif defined (FEE_START_SEC_VAR_16BIT)
   #undef      FEE_START_SEC_VAR_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (FEE_STOP_SEC_VAR_16BIT)
   #undef      FEE_STOP_SEC_VAR_16BIT
   #pragma pop
   
#elif defined (FEE_START_SEC_VAR_32BIT)
   #undef      FEE_START_SEC_VAR_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (FEE_STOP_SEC_VAR_32BIT)
   #undef      FEE_STOP_SEC_VAR_32BIT
   #pragma pop   
   
#elif defined (FEE_START_SEC_VAR_UNSPECIFIED)
   #undef      FEE_START_SEC_VAR_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (FEE_STOP_SEC_VAR_UNSPECIFIED)
   #undef      FEE_STOP_SEC_VAR_UNSPECIFIED
   #pragma pop   
   
#elif defined (FEE_START_SEC_VAR_FAST_NOINIT_1BIT)
   #undef      FEE_START_SEC_VAR_FAST_NOINIT_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (FEE_STOP_SEC_VAR_FAST_NOINIT_1BIT)
   #undef      FEE_STOP_SEC_VAR_FAST_NOINIT_1BIT
   #pragma pop
   
#elif defined (FEE_START_SEC_VAR_FAST_NOINIT_8BIT)
   #undef      FEE_START_SEC_VAR_FAST_NOINIT_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (FEE_STOP_SEC_VAR_FAST_NOINIT_8BIT)
   #undef      FEE_STOP_SEC_VAR_FAST_NOINIT_8BIT
   #pragma pop
   
#elif defined (FEE_START_SEC_VAR_FAST_NOINIT_16BIT)
   #undef      FEE_START_SEC_VAR_FAST_NOINIT_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (FEE_STOP_SEC_VAR_FAST_NOINIT_16BIT)
   #undef      FEE_STOP_SEC_VAR_FAST_NOINIT_16BIT
   #pragma pop
   
#elif defined (FEE_START_SEC_VAR_FAST_NOINIT_32BIT)
   #undef      FEE_START_SEC_VAR_FAST_NOINIT_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (FEE_STOP_SEC_VAR_FAST_NOINIT_32BIT)
   #undef      FEE_STOP_SEC_VAR_FAST_NOINIT_32BIT
   #pragma pop   
    
#elif defined (FEE_START_SEC_VAR_FAST_NOINIT_UNSPECIFIED)
   #undef      FEE_START_SEC_VAR_FAST_NOINIT_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (FEE_STOP_SEC_VAR_FAST_NOINIT_UNSPECIFIED)
   #undef      FEE_STOP_SEC_VAR_FAST_NOINIT_UNSPECIFIED
   #pragma pop     
   
#elif defined (FEE_START_SEC_VAR_FAST_POWER_ON_INIT_1BIT)
   #undef      FEE_START_SEC_VAR_FAST_POWER_ON_INIT_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (FEE_STOP_SEC_VAR_FAST_POWER_ON_INIT_1BIT)
   #undef      FEE_STOP_SEC_VAR_FAST_POWER_ON_INIT_1BIT
   #pragma pop
   
#elif defined (FEE_START_SEC_VAR_FAST_POWER_ON_INIT_8BIT)
   #undef      FEE_START_SEC_VAR_FAST_POWER_ON_INIT_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (FEE_STOP_SEC_VAR_FAST_POWER_ON_INIT_8BIT)
   #undef      FEE_STOP_SEC_VAR_FAST_POWER_ON_INIT_8BIT
   #pragma pop
   
#elif defined (FEE_START_SEC_VAR_FAST_POWER_ON_INIT_16BIT)
   #undef      FEE_START_SEC_VAR_FAST_POWER_ON_INIT_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (FEE_STOP_SEC_VAR_FAST_POWER_ON_INIT_16BIT)
   #undef      FEE_STOP_SEC_VAR_FAST_POWER_ON_INIT_16BIT
   #pragma pop
   
#elif defined (FEE_START_SEC_VAR_FAST_POWER_ON_INIT_32BIT)
   #undef      FEE_START_SEC_VAR_FAST_POWER_ON_INIT_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (FEE_STOP_SEC_VAR_FAST_POWER_ON_INIT_32BIT)
   #undef      FEE_STOP_SEC_VAR_FAST_POWER_ON_INIT_32BIT
   #pragma pop   
    
#elif defined (FEE_START_SEC_VAR_FAST_POWER_ON_INIT_UNSPECIFIED)
   #undef      FEE_START_SEC_VAR_FAST_POWER_ON_INIT_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (FEE_STOP_SEC_VAR_FAST_POWER_ON_INIT_UNSPECIFIED)
   #undef      FEE_STOP_SEC_VAR_FAST_POWER_ON_INIT_UNSPECIFIED
   #pragma pop     
   
#elif defined (FEE_START_SEC_VAR_FAST_1BIT)
   #undef      FEE_START_SEC_VAR_FAST_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (FEE_STOP_SEC_VAR_FAST_1BIT)
   #undef      FEE_STOP_SEC_VAR_FAST_1BIT
   #pragma pop
   
#elif defined (FEE_START_SEC_VAR_FAST_8BIT)
   #undef      FEE_START_SEC_VAR_FAST_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (FEE_STOP_SEC_VAR_FAST_8BIT)
   #undef      FEE_STOP_SEC_VAR_FAST_8BIT
   #pragma pop
   
#elif defined (FEE_START_SEC_VAR_FAST_16BIT)
   #undef      FEE_START_SEC_VAR_FAST_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (FEE_STOP_SEC_VAR_FAST_16BIT)
   #undef      FEE_STOP_SEC_VAR_FAST_16BIT
   #pragma pop
   
#elif defined (FEE_START_SEC_VAR_FAST_32BIT)
   #undef      FEE_START_SEC_VAR_FAST_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (FEE_STOP_SEC_VAR_FAST_32BIT)
   #undef      FEE_STOP_SEC_VAR_FAST_32BIT
   #pragma pop   
   
#elif defined (FEE_START_SEC_VAR_FAST_UNSPECIFIED)
   #undef      FEE_START_SEC_VAR_FAST_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (FEE_STOP_SEC_VAR_FAST_UNSPECIFIED)
   #undef      FEE_STOP_SEC_VAR_FAST_UNSPECIFIED
   #pragma pop   
   
#elif defined (FEE_START_SEC_CONST_1BIT)
   #undef      FEE_START_SEC_CONST_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (FEE_STOP_SEC_CONST_1BIT)
   #undef      FEE_STOP_SEC_CONST_1BIT
   #pragma pop
   
#elif defined (FEE_START_SEC_CONST_8BIT)
   #undef      FEE_START_SEC_CONST_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (FEE_STOP_SEC_CONST_8BIT)
   #undef      FEE_STOP_SEC_CONST_8BIT
   #pragma pop
   
#elif defined (FEE_START_SEC_CONST_16BIT)
   #undef      FEE_START_SEC_CONST_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (FEE_STOP_SEC_CONST_16BIT)
   #undef      FEE_STOP_SEC_CONST_16BIT
   #pragma pop
   
#elif defined (FEE_START_SEC_CONST_32BIT)
   #undef      FEE_START_SEC_CONST_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (FEE_STOP_SEC_CONST_32BIT)
   #undef      FEE_STOP_SEC_CONST_32BIT
   #pragma pop   
      
#elif defined (FEE_START_SEC_CONST_UNSPECIFIED)
   #undef      FEE_START_SEC_CONST_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (FEE_STOP_SEC_CONST_UNSPECIFIED)
   #undef      FEE_STOP_SEC_CONST_UNSPECIFIED
   #pragma pop   

#elif defined (FEE_START_SEC_CONST_FAST_1BIT)
   #undef      FEE_START_SEC_CONST_FAST_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (FEE_STOP_SEC_CONST_FAST_1BIT)
   #undef      FEE_STOP_SEC_CONST_FAST_1BIT
   #pragma pop
   
#elif defined (FEE_START_SEC_CONST_FAST_8BIT)
   #undef      FEE_START_SEC_CONST_FAST_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (FEE_STOP_SEC_CONST_FAST_8BIT)
   #undef      FEE_STOP_SEC_CONST_FAST_8BIT
   #pragma pop
   
#elif defined (FEE_START_SEC_CONST_FAST_16BIT)
   #undef      FEE_START_SEC_CONST_FAST_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (FEE_STOP_SEC_CONST_FAST_16BIT)
   #undef      FEE_STOP_SEC_CONST_FAST_16BIT
   #pragma pop
   
#elif defined (FEE_START_SEC_CONST_FAST_32BIT)
   #undef      FEE_START_SEC_CONST_FAST_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (FEE_STOP_SEC_CONST_FAST_32BIT)
   #undef      FEE_STOP_SEC_CONST_FAST_32BIT
   #pragma pop   
      
#elif defined (FEE_START_SEC_CONST_FAST_UNSPECIFIED)
   #undef      FEE_START_SEC_CONST_FAST_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (FEE_STOP_SEC_CONST_FAST_UNSPECIFIED)
   #undef      FEE_STOP_SEC_CONST_FAST_UNSPECIFIED
   #pragma pop   
   
#elif defined (FEE_START_SEC_CONST_PBCFG)
   #undef      FEE_START_SEC_CONST_PBCFG
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel
#elif defined (FEE_STOP_SEC_CONST_PBCFG)
   #undef      FEE_STOP_SEC_CONST_PBCFG
   #pragma pop

#elif defined (FEE_START_SEC_CONST_PBCFG_ROOT)
   #undef      FEE_START_SEC_CONST_PBCFG_ROOT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel
#elif defined (FEE_STOP_SEC_CONST_PBCFG_ROOT)
   #undef      FEE_STOP_SEC_CONST_PBCFG_ROOT
   #pragma pop
   
#elif defined (FEE_START_SEC_CODE)
   #undef      FEE_START_SEC_CODE
   #pragma push
   __declspec (section ".MCAL_CODE")
   //__declspec (section ".FEE_CODE_ROM")
#elif defined (FEE_STOP_SEC_CODE)
   #undef      FEE_STOP_SEC_CODE
   #pragma pop

#elif defined (FEE_START_SEC_APPL_CODE)
   #undef      FEE_START_SEC_APPL_CODE
   #pragma push
   __declspec (section ".FEE_APPL_CODE_ROM")
#elif defined (FEE_STOP_SEC_APPL_CODE)
   #undef      FEE_STOP_SEC_APPL_CODE
   #pragma pop


/* -------------------------------------------------------------------------- */
/*             PWM                                                            */
/* -------------------------------------------------------------------------- */
#elif defined (PWM_START_SEC_VAR_NOINIT_1BIT)
   #undef      PWM_START_SEC_VAR_NOINIT_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (PWM_STOP_SEC_VAR_NOINIT_1BIT)
   #undef      PWM_STOP_SEC_VAR_NOINIT_1BIT
   #pragma pop
   
#elif defined (PWM_START_SEC_VAR_NOINIT_8BIT)
   #undef      PWM_START_SEC_VAR_NOINIT_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (PWM_STOP_SEC_VAR_NOINIT_8BIT)
   #undef      PWM_STOP_SEC_VAR_NOINIT_8BIT
   #pragma pop
   
#elif defined (PWM_START_SEC_VAR_NOINIT_16BIT)
   #undef      PWM_START_SEC_VAR_NOINIT_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (PWM_STOP_SEC_VAR_NOINIT_16BIT)
   #undef      PWM_STOP_SEC_VAR_NOINIT_16BIT
   #pragma pop
   
#elif defined (PWM_START_SEC_VAR_NOINIT_32BIT)
   #undef      PWM_START_SEC_VAR_NOINIT_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (PWM_STOP_SEC_VAR_NOINIT_32BIT)
   #undef      PWM_STOP_SEC_VAR_NOINIT_32BIT
   #pragma pop   

#elif defined (PWM_START_SEC_VAR_NOINIT_UNSPECIFIED)
   #undef      PWM_START_SEC_VAR_NOINIT_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (PWM_STOP_SEC_VAR_NOINIT_UNSPECIFIED)
   #undef      PWM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
   #pragma pop   
   
#elif defined (PWM_START_SEC_VAR_POWER_ON_INIT_1BIT)
   #undef      PWM_START_SEC_VAR_POWER_ON_INIT_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (PWM_STOP_SEC_VAR_POWER_ON_INIT_1BIT)
   #undef      PWM_STOP_SEC_VAR_POWER_ON_INIT_1BIT
   #pragma pop
   
#elif defined (PWM_START_SEC_VAR_POWER_ON_INIT_8BIT)
   #undef      PWM_START_SEC_VAR_POWER_ON_INIT_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (PWM_STOP_SEC_VAR_POWER_ON_INIT_8BIT)
   #undef      PWM_STOP_SEC_VAR_POWER_ON_INIT_8BIT
   #pragma pop
   
#elif defined (PWM_START_SEC_VAR_POWER_ON_INIT_16BIT)
   #undef      PWM_START_SEC_VAR_POWER_ON_INIT_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (PWM_STOP_SEC_VAR_POWER_ON_INIT_16BIT)
   #undef      PWM_STOP_SEC_VAR_POWER_ON_INIT_16BIT
   #pragma pop
   
#elif defined (PWM_START_SEC_VAR_POWER_ON_INIT_32BIT)
   #undef      PWM_START_SEC_VAR_POWER_ON_INIT_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (PWM_STOP_SEC_VAR_POWER_ON_INIT_32BIT)
   #undef      PWM_STOP_SEC_VAR_POWER_ON_INIT_32BIT
   #pragma pop  

 
#elif defined (PWM_START_SEC_VAR_POWER_ON_INIT_UNSPECIFIED)
   #undef      PWM_START_SEC_VAR_POWER_ON_INIT_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (PWM_STOP_SEC_VAR_POWER_ON_INIT_UNSPECIFIED)
   #undef      PWM_STOP_SEC_VAR_POWER_ON_INIT_UNSPECIFIED
   #pragma pop   
   
#elif defined (PWM_START_SEC_VAR_1BIT)
   #undef      PWM_START_SEC_VAR_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (PWM_STOP_SEC_VAR_1BIT)
   #undef      PWM_STOP_SEC_VAR_1BIT
   #pragma pop
   
#elif defined (PWM_START_SEC_VAR_8BIT)
   #undef      PWM_START_SEC_VAR_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (PWM_STOP_SEC_VAR_8BIT)
   #undef      PWM_STOP_SEC_VAR_8BIT
   #pragma pop
   
#elif defined (PWM_START_SEC_VAR_16BIT)
   #undef      PWM_START_SEC_VAR_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (PWM_STOP_SEC_VAR_16BIT)
   #undef      PWM_STOP_SEC_VAR_16BIT
   #pragma pop
   
#elif defined (PWM_START_SEC_VAR_32BIT)
   #undef      PWM_START_SEC_VAR_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (PWM_STOP_SEC_VAR_32BIT)
   #undef      PWM_STOP_SEC_VAR_32BIT
   #pragma pop   
   
#elif defined (PWM_START_SEC_VAR_UNSPECIFIED)
   #undef      PWM_START_SEC_VAR_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (PWM_STOP_SEC_VAR_UNSPECIFIED)
   #undef      PWM_STOP_SEC_VAR_UNSPECIFIED
   #pragma pop   
   
#elif defined (PWM_START_SEC_VAR_FAST_NOINIT_1BIT)
   #undef      PWM_START_SEC_VAR_FAST_NOINIT_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (PWM_STOP_SEC_VAR_FAST_NOINIT_1BIT)
   #undef      PWM_STOP_SEC_VAR_FAST_NOINIT_1BIT
   #pragma pop
   
#elif defined (PWM_START_SEC_VAR_FAST_NOINIT_8BIT)
   #undef      PWM_START_SEC_VAR_FAST_NOINIT_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (PWM_STOP_SEC_VAR_FAST_NOINIT_8BIT)
   #undef      PWM_STOP_SEC_VAR_FAST_NOINIT_8BIT
   #pragma pop
   
#elif defined (PWM_START_SEC_VAR_FAST_NOINIT_16BIT)
   #undef      PWM_START_SEC_VAR_FAST_NOINIT_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (PWM_STOP_SEC_VAR_FAST_NOINIT_16BIT)
   #undef      PWM_STOP_SEC_VAR_FAST_NOINIT_16BIT
   #pragma pop
   
#elif defined (PWM_START_SEC_VAR_FAST_NOINIT_32BIT)
   #undef      PWM_START_SEC_VAR_FAST_NOINIT_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (PWM_STOP_SEC_VAR_FAST_NOINIT_32BIT)
   #undef      PWM_STOP_SEC_VAR_FAST_NOINIT_32BIT
   #pragma pop   
    
#elif defined (PWM_START_SEC_VAR_FAST_NOINIT_UNSPECIFIED)
   #undef      PWM_START_SEC_VAR_FAST_NOINIT_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (PWM_STOP_SEC_VAR_FAST_NOINIT_UNSPECIFIED)
   #undef      PWM_STOP_SEC_VAR_FAST_NOINIT_UNSPECIFIED
   #pragma pop     
   
#elif defined (PWM_START_SEC_VAR_FAST_POWER_ON_INIT_1BIT)
   #undef      PWM_START_SEC_VAR_FAST_POWER_ON_INIT_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (PWM_STOP_SEC_VAR_FAST_POWER_ON_INIT_1BIT)
   #undef      PWM_STOP_SEC_VAR_FAST_POWER_ON_INIT_1BIT
   #pragma pop
   
#elif defined (PWM_START_SEC_VAR_FAST_POWER_ON_INIT_8BIT)
   #undef      PWM_START_SEC_VAR_FAST_POWER_ON_INIT_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (PWM_STOP_SEC_VAR_FAST_POWER_ON_INIT_8BIT)
   #undef      PWM_STOP_SEC_VAR_FAST_POWER_ON_INIT_8BIT
   #pragma pop
   
#elif defined (PWM_START_SEC_VAR_FAST_POWER_ON_INIT_16BIT)
   #undef      PWM_START_SEC_VAR_FAST_POWER_ON_INIT_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (PWM_STOP_SEC_VAR_FAST_POWER_ON_INIT_16BIT)
   #undef      PWM_STOP_SEC_VAR_FAST_POWER_ON_INIT_16BIT
   #pragma pop
   
#elif defined (PWM_START_SEC_VAR_FAST_POWER_ON_INIT_32BIT)
   #undef      PWM_START_SEC_VAR_FAST_POWER_ON_INIT_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (PWM_STOP_SEC_VAR_FAST_POWER_ON_INIT_32BIT)
   #undef      PWM_STOP_SEC_VAR_FAST_POWER_ON_INIT_32BIT
   #pragma pop   
    
#elif defined (PWM_START_SEC_VAR_FAST_POWER_ON_INIT_UNSPECIFIED)
   #undef      PWM_START_SEC_VAR_FAST_POWER_ON_INIT_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (PWM_STOP_SEC_VAR_FAST_POWER_ON_INIT_UNSPECIFIED)
   #undef      PWM_STOP_SEC_VAR_FAST_POWER_ON_INIT_UNSPECIFIED
   #pragma pop     
   
#elif defined (PWM_START_SEC_VAR_FAST_1BIT)
   #undef      PWM_START_SEC_VAR_FAST_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (PWM_STOP_SEC_VAR_FAST_1BIT)
   #undef      PWM_STOP_SEC_VAR_FAST_1BIT
   #pragma pop
   
#elif defined (PWM_START_SEC_VAR_FAST_8BIT)
   #undef      PWM_START_SEC_VAR_FAST_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (PWM_STOP_SEC_VAR_FAST_8BIT)
   #undef      PWM_STOP_SEC_VAR_FAST_8BIT
   #pragma pop
   
#elif defined (PWM_START_SEC_VAR_FAST_16BIT)
   #undef      PWM_START_SEC_VAR_FAST_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (PWM_STOP_SEC_VAR_FAST_16BIT)
   #undef      PWM_STOP_SEC_VAR_FAST_16BIT
   #pragma pop
   
#elif defined (PWM_START_SEC_VAR_FAST_32BIT)
   #undef      PWM_START_SEC_VAR_FAST_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (PWM_STOP_SEC_VAR_FAST_32BIT)
   #undef      PWM_STOP_SEC_VAR_FAST_32BIT
   #pragma pop   
   
#elif defined (PWM_START_SEC_VAR_FAST_UNSPECIFIED)
   #undef      PWM_START_SEC_VAR_FAST_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (PWM_STOP_SEC_VAR_FAST_UNSPECIFIED)
   #undef      PWM_STOP_SEC_VAR_FAST_UNSPECIFIED
   #pragma pop   
   
#elif defined (PWM_START_SEC_CONST_1BIT)
   #undef      PWM_START_SEC_CONST_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (PWM_STOP_SEC_CONST_1BIT)
   #undef      PWM_STOP_SEC_CONST_1BIT
   #pragma pop
   
#elif defined (PWM_START_SEC_CONST_8BIT)
   #undef      PWM_START_SEC_CONST_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (PWM_STOP_SEC_CONST_8BIT)
   #undef      PWM_STOP_SEC_CONST_8BIT
   #pragma pop
   
#elif defined (PWM_START_SEC_CONST_16BIT)
   #undef      PWM_START_SEC_CONST_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (PWM_STOP_SEC_CONST_16BIT)
   #undef      PWM_STOP_SEC_CONST_16BIT
   #pragma pop
   
#elif defined (PWM_START_SEC_CONST_32BIT)
   #undef      PWM_START_SEC_CONST_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (PWM_STOP_SEC_CONST_32BIT)
   #undef      PWM_STOP_SEC_CONST_32BIT
   #pragma pop   
      
#elif defined (PWM_START_SEC_CONST_UNSPECIFIED)
   #undef      PWM_START_SEC_CONST_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (PWM_STOP_SEC_CONST_UNSPECIFIED)
   #undef      PWM_STOP_SEC_CONST_UNSPECIFIED
   #pragma pop   

   
#elif defined (PWM_START_SEC_CONST_FAST_1BIT)
   #undef      PWM_START_SEC_CONST_FAST_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (PWM_STOP_SEC_CONST_FAST_1BIT)
   #undef      PWM_STOP_SEC_CONST_FAST_1BIT
   #pragma pop
   
#elif defined (PWM_START_SEC_CONST_FAST_8BIT)
   #undef      PWM_START_SEC_CONST_FAST_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (PWM_STOP_SEC_CONST_FAST_8BIT)
   #undef      PWM_STOP_SEC_CONST_FAST_8BIT
   #pragma pop
   
#elif defined (PWM_START_SEC_CONST_FAST_16BIT)
   #undef      PWM_START_SEC_CONST_FAST_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (PWM_STOP_SEC_CONST_FAST_16BIT)
   #undef      PWM_STOP_SEC_CONST_FAST_16BIT
   #pragma pop
   
#elif defined (PWM_START_SEC_CONST_FAST_32BIT)
   #undef      PWM_START_SEC_CONST_FAST_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (PWM_STOP_SEC_CONST_FAST_32BIT)
   #undef      PWM_STOP_SEC_CONST_FAST_32BIT
   #pragma pop   
      
#elif defined (PWM_START_SEC_CONST_FAST_UNSPECIFIED)
   #undef      PWM_START_SEC_CONST_FAST_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (PWM_STOP_SEC_CONST_FAST_UNSPECIFIED)
   #undef      PWM_STOP_SEC_CONST_FAST_UNSPECIFIED
   #pragma pop 
   
#elif defined (MCU_START_SEC_CONST_PBCFG)
   #undef      MCU_START_SEC_CONST_PBCFG
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel
#elif defined (MCU_STOP_SEC_CONST_PBCFG)
   #undef      MCU_STOP_SEC_CONST_PBCFG
   #pragma pop

#elif defined (MCU_START_SEC_CONST_PBCFG_ROOT)
   #undef      MCU_START_SEC_CONST_PBCFG_ROOT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel
#elif defined (MCU_STOP_SEC_CONST_PBCFG_ROOT)
   #undef      MCU_STOP_SEC_CONST_PBCFG_ROOT
   #pragma pop

#elif defined (PWM_START_SEC_CODE)
   #undef      PWM_START_SEC_CODE
   #pragma push
   __declspec (section ".MCAL_CODE")
   //__declspec (section ".PWM_CODE_ROM")
#elif defined (PWM_STOP_SEC_CODE)
   #undef      PWM_STOP_SEC_CODE
   #pragma pop

#elif defined (PWM_START_SEC_APPL_CODE)
   #undef      PWM_START_SEC_APPL_CODE
   #pragma push
   __declspec (section ".PWM_APPL_CODE_ROM")
#elif defined (PWM_STOP_SEC_APPL_CODE)
   #undef      PWM_STOP_SEC_APPL_CODE
   #pragma pop


/* -------------------------------------------------------------------------- */
/*             SPI                                                            */
/* -------------------------------------------------------------------------- */
#elif defined (SPI_START_SEC_VAR_NOINIT_1BIT)
   #undef      SPI_START_SEC_VAR_NOINIT_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (SPI_STOP_SEC_VAR_NOINIT_1BIT)
   #undef      SPI_STOP_SEC_VAR_NOINIT_1BIT
   #pragma pop
   
#elif defined (SPI_START_SEC_VAR_NOINIT_8BIT)
   #undef      SPI_START_SEC_VAR_NOINIT_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (SPI_STOP_SEC_VAR_NOINIT_8BIT)
   #undef      SPI_STOP_SEC_VAR_NOINIT_8BIT
   #pragma pop
   
#elif defined (SPI_START_SEC_VAR_NOINIT_16BIT)
   #undef      SPI_START_SEC_VAR_NOINIT_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (SPI_STOP_SEC_VAR_NOINIT_16BIT)
   #undef      SPI_STOP_SEC_VAR_NOINIT_16BIT
   #pragma pop
   
#elif defined (SPI_START_SEC_VAR_NOINIT_32BIT)
   #undef      SPI_START_SEC_VAR_NOINIT_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (SPI_STOP_SEC_VAR_NOINIT_32BIT)
   #undef      SPI_STOP_SEC_VAR_NOINIT_32BIT
   #pragma pop   

#elif defined (SPI_START_SEC_VAR_NOINIT_UNSPECIFIED)
   #undef      SPI_START_SEC_VAR_NOINIT_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (SPI_STOP_SEC_VAR_NOINIT_UNSPECIFIED)
   #undef      SPI_STOP_SEC_VAR_NOINIT_UNSPECIFIED
   #pragma pop   
   
#elif defined (SPI_START_SEC_VAR_POWER_ON_INIT_1BIT)
   #undef      SPI_START_SEC_VAR_POWER_ON_INIT_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (SPI_STOP_SEC_VAR_POWER_ON_INIT_1BIT)
   #undef      SPI_STOP_SEC_VAR_POWER_ON_INIT_1BIT
   #pragma pop
   
#elif defined (SPI_START_SEC_VAR_POWER_ON_INIT_8BIT)
   #undef      SPI_START_SEC_VAR_POWER_ON_INIT_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (SPI_STOP_SEC_VAR_POWER_ON_INIT_8BIT)
   #undef      SPI_STOP_SEC_VAR_POWER_ON_INIT_8BIT
   #pragma pop
   
#elif defined (SPI_START_SEC_VAR_POWER_ON_INIT_16BIT)
   #undef      SPI_START_SEC_VAR_POWER_ON_INIT_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (SPI_STOP_SEC_VAR_POWER_ON_INIT_16BIT)
   #undef      SPI_STOP_SEC_VAR_POWER_ON_INIT_16BIT
   #pragma pop
   
#elif defined (SPI_START_SEC_VAR_POWER_ON_INIT_32BIT)
   #undef      SPI_START_SEC_VAR_POWER_ON_INIT_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (SPI_STOP_SEC_VAR_POWER_ON_INIT_32BIT)
   #undef      SPI_STOP_SEC_VAR_POWER_ON_INIT_32BIT
   #pragma pop  

 
#elif defined (SPI_START_SEC_VAR_POWER_ON_INIT_UNSPECIFIED)
   #undef      SPI_START_SEC_VAR_POWER_ON_INIT_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (SPI_STOP_SEC_VAR_POWER_ON_INIT_UNSPECIFIED)
   #undef      SPI_STOP_SEC_VAR_POWER_ON_INIT_UNSPECIFIED
   #pragma pop   
   
#elif defined (SPI_START_SEC_VAR_1BIT)
   #undef      SPI_START_SEC_VAR_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (SPI_STOP_SEC_VAR_1BIT)
   #undef      SPI_STOP_SEC_VAR_1BIT
   #pragma pop
   
#elif defined (SPI_START_SEC_VAR_8BIT)
   #undef      SPI_START_SEC_VAR_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (SPI_STOP_SEC_VAR_8BIT)
   #undef      SPI_STOP_SEC_VAR_8BIT
   #pragma pop
   
#elif defined (SPI_START_SEC_VAR_16BIT)
   #undef      SPI_START_SEC_VAR_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (SPI_STOP_SEC_VAR_16BIT)
   #undef      SPI_STOP_SEC_VAR_16BIT
   #pragma pop
   
#elif defined (SPI_START_SEC_VAR_32BIT)
   #undef      SPI_START_SEC_VAR_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (SPI_STOP_SEC_VAR_32BIT)
   #undef      SPI_STOP_SEC_VAR_32BIT
   #pragma pop   
   
#elif defined (SPI_START_SEC_VAR_UNSPECIFIED)
   #undef      SPI_START_SEC_VAR_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (SPI_STOP_SEC_VAR_UNSPECIFIED)
   #undef      SPI_STOP_SEC_VAR_UNSPECIFIED
   #pragma pop   
   
#elif defined (SPI_START_SEC_VAR_FAST_NOINIT_1BIT)
   #undef      SPI_START_SEC_VAR_FAST_NOINIT_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (SPI_STOP_SEC_VAR_FAST_NOINIT_1BIT)
   #undef      SPI_STOP_SEC_VAR_FAST_NOINIT_1BIT
   #pragma pop
   
#elif defined (SPI_START_SEC_VAR_FAST_NOINIT_8BIT)
   #undef      SPI_START_SEC_VAR_FAST_NOINIT_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (SPI_STOP_SEC_VAR_FAST_NOINIT_8BIT)
   #undef      SPI_STOP_SEC_VAR_FAST_NOINIT_8BIT
   #pragma pop
   
#elif defined (SPI_START_SEC_VAR_FAST_NOINIT_16BIT)
   #undef      SPI_START_SEC_VAR_FAST_NOINIT_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (SPI_STOP_SEC_VAR_FAST_NOINIT_16BIT)
   #undef      SPI_STOP_SEC_VAR_FAST_NOINIT_16BIT
   #pragma pop
   
#elif defined (SPI_START_SEC_VAR_FAST_NOINIT_32BIT)
   #undef      SPI_START_SEC_VAR_FAST_NOINIT_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (SPI_STOP_SEC_VAR_FAST_NOINIT_32BIT)
   #undef      SPI_STOP_SEC_VAR_FAST_NOINIT_32BIT
   #pragma pop   
    
#elif defined (SPI_START_SEC_VAR_FAST_NOINIT_UNSPECIFIED)
   #undef      SPI_START_SEC_VAR_FAST_NOINIT_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (SPI_STOP_SEC_VAR_FAST_NOINIT_UNSPECIFIED)
   #undef      SPI_STOP_SEC_VAR_FAST_NOINIT_UNSPECIFIED
   #pragma pop     
   
#elif defined (SPI_START_SEC_VAR_FAST_POWER_ON_INIT_1BIT)
   #undef      SPI_START_SEC_VAR_FAST_POWER_ON_INIT_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (SPI_STOP_SEC_VAR_FAST_POWER_ON_INIT_1BIT)
   #undef      SPI_STOP_SEC_VAR_FAST_POWER_ON_INIT_1BIT
   #pragma pop
   
#elif defined (SPI_START_SEC_VAR_FAST_POWER_ON_INIT_8BIT)
   #undef      SPI_START_SEC_VAR_FAST_POWER_ON_INIT_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (SPI_STOP_SEC_VAR_FAST_POWER_ON_INIT_8BIT)
   #undef      SPI_STOP_SEC_VAR_FAST_POWER_ON_INIT_8BIT
   #pragma pop
   
#elif defined (SPI_START_SEC_VAR_FAST_POWER_ON_INIT_16BIT)
   #undef      SPI_START_SEC_VAR_FAST_POWER_ON_INIT_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (SPI_STOP_SEC_VAR_FAST_POWER_ON_INIT_16BIT)
   #undef      SPI_STOP_SEC_VAR_FAST_POWER_ON_INIT_16BIT
   #pragma pop
   
#elif defined (SPI_START_SEC_VAR_FAST_POWER_ON_INIT_32BIT)
   #undef      SPI_START_SEC_VAR_FAST_POWER_ON_INIT_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (SPI_STOP_SEC_VAR_FAST_POWER_ON_INIT_32BIT)
   #undef      SPI_STOP_SEC_VAR_FAST_POWER_ON_INIT_32BIT
   #pragma pop   
    
#elif defined (SPI_START_SEC_VAR_FAST_POWER_ON_INIT_UNSPECIFIED)
   #undef      SPI_START_SEC_VAR_FAST_POWER_ON_INIT_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (SPI_STOP_SEC_VAR_FAST_POWER_ON_INIT_UNSPECIFIED)
   #undef      SPI_STOP_SEC_VAR_FAST_POWER_ON_INIT_UNSPECIFIED
   #pragma pop     
   
#elif defined (SPI_START_SEC_VAR_FAST_1BIT)
   #undef      SPI_START_SEC_VAR_FAST_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (SPI_STOP_SEC_VAR_FAST_1BIT)
   #undef      SPI_STOP_SEC_VAR_FAST_1BIT
   #pragma pop
   
#elif defined (SPI_START_SEC_VAR_FAST_8BIT)
   #undef      SPI_START_SEC_VAR_FAST_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (SPI_STOP_SEC_VAR_FAST_8BIT)
   #undef      SPI_STOP_SEC_VAR_FAST_8BIT
   #pragma pop
   
#elif defined (SPI_START_SEC_VAR_FAST_16BIT)
   #undef      SPI_START_SEC_VAR_FAST_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (SPI_STOP_SEC_VAR_FAST_16BIT)
   #undef      SPI_STOP_SEC_VAR_FAST_16BIT
   #pragma pop
   
#elif defined (SPI_START_SEC_VAR_FAST_32BIT)
   #undef      SPI_START_SEC_VAR_FAST_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (SPI_STOP_SEC_VAR_FAST_32BIT)
   #undef      SPI_STOP_SEC_VAR_FAST_32BIT
   #pragma pop   
   
#elif defined (SPI_START_SEC_VAR_FAST_UNSPECIFIED)
   #undef      SPI_START_SEC_VAR_FAST_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (SPI_STOP_SEC_VAR_FAST_UNSPECIFIED)
   #undef      SPI_STOP_SEC_VAR_FAST_UNSPECIFIED
   #pragma pop   
   
#elif defined (SPI_START_SEC_CONST_1BIT)
   #undef      SPI_START_SEC_CONST_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (SPI_STOP_SEC_CONST_1BIT)
   #undef      SPI_STOP_SEC_CONST_1BIT
   #pragma pop
   
#elif defined (SPI_START_SEC_CONST_8BIT)
   #undef      SPI_START_SEC_CONST_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (SPI_STOP_SEC_CONST_8BIT)
   #undef      SPI_STOP_SEC_CONST_8BIT
   #pragma pop
   
#elif defined (SPI_START_SEC_CONST_16BIT)
   #undef      SPI_START_SEC_CONST_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (SPI_STOP_SEC_CONST_16BIT)
   #undef      SPI_STOP_SEC_CONST_16BIT
   #pragma pop
   
#elif defined (SPI_START_SEC_CONST_32BIT)
   #undef      SPI_START_SEC_CONST_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (SPI_STOP_SEC_CONST_32BIT)
   #undef      SPI_STOP_SEC_CONST_32BIT
   #pragma pop   
      
#elif defined (SPI_START_SEC_CONST_UNSPECIFIED)
   #undef      SPI_START_SEC_CONST_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (SPI_STOP_SEC_CONST_UNSPECIFIED)
   #undef      SPI_STOP_SEC_CONST_UNSPECIFIED
   #pragma pop   

   
#elif defined (SPI_START_SEC_CONST_FAST_1BIT)
   #undef      SPI_START_SEC_CONST_FAST_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (SPI_STOP_SEC_CONST_FAST_1BIT)
   #undef      SPI_STOP_SEC_CONST_FAST_1BIT
   #pragma pop
   
#elif defined (SPI_START_SEC_CONST_FAST_8BIT)
   #undef      SPI_START_SEC_CONST_FAST_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (SPI_STOP_SEC_CONST_FAST_8BIT)
   #undef      SPI_STOP_SEC_CONST_FAST_8BIT
   #pragma pop
   
#elif defined (SPI_START_SEC_CONST_FAST_16BIT)
   #undef      SPI_START_SEC_CONST_FAST_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (SPI_STOP_SEC_CONST_FAST_16BIT)
   #undef      SPI_STOP_SEC_CONST_FAST_16BIT
   #pragma pop
   
#elif defined (SPI_START_SEC_CONST_FAST_32BIT)
   #undef      SPI_START_SEC_CONST_FAST_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (SPI_STOP_SEC_CONST_FAST_32BIT)
   #undef      SPI_STOP_SEC_CONST_FAST_32BIT
   #pragma pop   
      
#elif defined (SPI_START_SEC_CONST_FAST_UNSPECIFIED)
   #undef      SPI_START_SEC_CONST_FAST_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (SPI_STOP_SEC_CONST_FAST_UNSPECIFIED)
   #undef      SPI_STOP_SEC_CONST_FAST_UNSPECIFIED
   #pragma pop   
   
#elif defined (SPI_START_SEC_CONST_PBCFG)
   #undef      SPI_START_SEC_CONST_PBCFG
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel
#elif defined (SPI_STOP_SEC_CONST_PBCFG)
   #undef      SPI_STOP_SEC_CONST_PBCFG
   #pragma pop
   
#elif defined (SPI_START_SEC_CONST_PBCFG_ROOT)
   #undef      SPI_START_SEC_CONST_PBCFG_ROOT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel
#elif defined (SPI_STOP_SEC_CONST_PBCFG_ROOT)
   #undef      SPI_STOP_SEC_CONST_PBCFG_ROOT
   #pragma pop

#elif defined (SPI_START_SEC_CODE)
   #undef      SPI_START_SEC_CODE
   //#pragma push
   __declspec (section ".MCAL_CODE")
#elif defined (SPI_STOP_SEC_CODE)
   #undef      SPI_STOP_SEC_CODE
   //#pragma pop

#elif defined (SPI_START_SEC_APPL_CODE)
   #undef      SPI_START_SEC_APPL_CODE
   //#pragma push
   //__declspec (section ".SPI_APPL_CODE_ROM")
#elif defined (SPI_STOP_SEC_APPL_CODE)
   #undef      SPI_STOP_SEC_APPL_CODE
   //#pragma pop


/* -------------------------------------------------------------------------- */
/*             ADC                                                            */
/* -------------------------------------------------------------------------- */
#elif defined (ADC_START_SEC_VAR_NOINIT_1BIT)
   #undef      ADC_START_SEC_VAR_NOINIT_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (ADC_STOP_SEC_VAR_NOINIT_1BIT)
   #undef      ADC_STOP_SEC_VAR_NOINIT_1BIT
   #pragma pop
   
#elif defined (ADC_START_SEC_VAR_NOINIT_8BIT)
   #undef      ADC_START_SEC_VAR_NOINIT_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (ADC_STOP_SEC_VAR_NOINIT_8BIT)
   #undef      ADC_STOP_SEC_VAR_NOINIT_8BIT
   #pragma pop
   
#elif defined (ADC_START_SEC_VAR_NOINIT_16BIT)
   #undef      ADC_START_SEC_VAR_NOINIT_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (ADC_STOP_SEC_VAR_NOINIT_16BIT)
   #undef      ADC_STOP_SEC_VAR_NOINIT_16BIT
   #pragma pop
   
#elif defined (ADC_START_SEC_VAR_NOINIT_32BIT)
   #undef      ADC_START_SEC_VAR_NOINIT_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (ADC_STOP_SEC_VAR_NOINIT_32BIT)
   #undef      ADC_STOP_SEC_VAR_NOINIT_32BIT
   #pragma pop   

#elif defined (ADC_START_SEC_VAR_NOINIT_UNSPECIFIED)
   #undef      ADC_START_SEC_VAR_NOINIT_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (ADC_STOP_SEC_VAR_NOINIT_UNSPECIFIED)
   #undef      ADC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
   #pragma pop   
   
#elif defined (ADC_START_SEC_VAR_POWER_ON_INIT_1BIT)
   #undef      ADC_START_SEC_VAR_POWER_ON_INIT_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (ADC_STOP_SEC_VAR_POWER_ON_INIT_1BIT)
   #undef      ADC_STOP_SEC_VAR_POWER_ON_INIT_1BIT
   #pragma pop
   
#elif defined (ADC_START_SEC_VAR_POWER_ON_INIT_8BIT)
   #undef      ADC_START_SEC_VAR_POWER_ON_INIT_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (ADC_STOP_SEC_VAR_POWER_ON_INIT_8BIT)
   #undef      ADC_STOP_SEC_VAR_POWER_ON_INIT_8BIT
   #pragma pop
   
#elif defined (ADC_START_SEC_VAR_POWER_ON_INIT_16BIT)
   #undef      ADC_START_SEC_VAR_POWER_ON_INIT_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (ADC_STOP_SEC_VAR_POWER_ON_INIT_16BIT)
   #undef      ADC_STOP_SEC_VAR_POWER_ON_INIT_16BIT
   #pragma pop
   
#elif defined (ADC_START_SEC_VAR_POWER_ON_INIT_32BIT)
   #undef      ADC_START_SEC_VAR_POWER_ON_INIT_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (ADC_STOP_SEC_VAR_POWER_ON_INIT_32BIT)
   #undef      ADC_STOP_SEC_VAR_POWER_ON_INIT_32BIT
   #pragma pop  

#elif defined (ADC_START_SEC_VAR_POWER_ON_INIT_UNSPECIFIED)
   #undef      ADC_START_SEC_VAR_POWER_ON_INIT_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (ADC_STOP_SEC_VAR_POWER_ON_INIT_UNSPECIFIED)
   #undef      ADC_STOP_SEC_VAR_POWER_ON_INIT_UNSPECIFIED
   #pragma pop   
   
#elif defined (ADC_START_SEC_VAR_1BIT)
   #undef      ADC_START_SEC_VAR_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (ADC_STOP_SEC_VAR_1BIT)
   #undef      ADC_STOP_SEC_VAR_1BIT
   #pragma pop
   
#elif defined (ADC_START_SEC_VAR_8BIT)
   #undef      ADC_START_SEC_VAR_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (ADC_STOP_SEC_VAR_8BIT)
   #undef      ADC_STOP_SEC_VAR_8BIT
   #pragma pop
   
#elif defined (ADC_START_SEC_VAR_16BIT)
   #undef      ADC_START_SEC_VAR_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (ADC_STOP_SEC_VAR_16BIT)
   #undef      ADC_STOP_SEC_VAR_16BIT
   #pragma pop
   
#elif defined (ADC_START_SEC_VAR_32BIT)
   #undef      ADC_START_SEC_VAR_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (ADC_STOP_SEC_VAR_32BIT)
   #undef      ADC_STOP_SEC_VAR_32BIT
   #pragma pop   
   
#elif defined (ADC_START_SEC_VAR_UNSPECIFIED)
   #undef      ADC_START_SEC_VAR_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (ADC_STOP_SEC_VAR_UNSPECIFIED)
   #undef      ADC_STOP_SEC_VAR_UNSPECIFIED
   #pragma pop   
   
#elif defined (ADC_START_SEC_VAR_FAST_NOINIT_1BIT)
   #undef      ADC_START_SEC_VAR_FAST_NOINIT_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (ADC_STOP_SEC_VAR_FAST_NOINIT_1BIT)
   #undef      ADC_STOP_SEC_VAR_FAST_NOINIT_1BIT
   #pragma pop
   
#elif defined (ADC_START_SEC_VAR_FAST_NOINIT_8BIT)
   #undef      ADC_START_SEC_VAR_FAST_NOINIT_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (ADC_STOP_SEC_VAR_FAST_NOINIT_8BIT)
   #undef      ADC_STOP_SEC_VAR_FAST_NOINIT_8BIT
   #pragma pop
   
#elif defined (ADC_START_SEC_VAR_FAST_NOINIT_16BIT)
   #undef      ADC_START_SEC_VAR_FAST_NOINIT_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (ADC_STOP_SEC_VAR_FAST_NOINIT_16BIT)
   #undef      ADC_STOP_SEC_VAR_FAST_NOINIT_16BIT
   #pragma pop
   
#elif defined (ADC_START_SEC_VAR_FAST_NOINIT_32BIT)
   #undef      ADC_START_SEC_VAR_FAST_NOINIT_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (ADC_STOP_SEC_VAR_FAST_NOINIT_32BIT)
   #undef      ADC_STOP_SEC_VAR_FAST_NOINIT_32BIT
   #pragma pop   
    
#elif defined (ADC_START_SEC_VAR_FAST_NOINIT_UNSPECIFIED)
   #undef      ADC_START_SEC_VAR_FAST_NOINIT_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (ADC_STOP_SEC_VAR_FAST_NOINIT_UNSPECIFIED)
   #undef      ADC_STOP_SEC_VAR_FAST_NOINIT_UNSPECIFIED
   #pragma pop     
   
#elif defined (ADC_START_SEC_VAR_FAST_POWER_ON_INIT_1BIT)
   #undef      ADC_START_SEC_VAR_FAST_POWER_ON_INIT_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (ADC_STOP_SEC_VAR_FAST_POWER_ON_INIT_1BIT)
   #undef      ADC_STOP_SEC_VAR_FAST_POWER_ON_INIT_1BIT
   #pragma pop
   
#elif defined (ADC_START_SEC_VAR_FAST_POWER_ON_INIT_8BIT)
   #undef      ADC_START_SEC_VAR_FAST_POWER_ON_INIT_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (ADC_STOP_SEC_VAR_FAST_POWER_ON_INIT_8BIT)
   #undef      ADC_STOP_SEC_VAR_FAST_POWER_ON_INIT_8BIT
   #pragma pop
   
#elif defined (ADC_START_SEC_VAR_FAST_POWER_ON_INIT_16BIT)
   #undef      ADC_START_SEC_VAR_FAST_POWER_ON_INIT_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (ADC_STOP_SEC_VAR_FAST_POWER_ON_INIT_16BIT)
   #undef      ADC_STOP_SEC_VAR_FAST_POWER_ON_INIT_16BIT
   #pragma pop
   
#elif defined (ADC_START_SEC_VAR_FAST_POWER_ON_INIT_32BIT)
   #undef      ADC_START_SEC_VAR_FAST_POWER_ON_INIT_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (ADC_STOP_SEC_VAR_FAST_POWER_ON_INIT_32BIT)
   #undef      ADC_STOP_SEC_VAR_FAST_POWER_ON_INIT_32BIT
   #pragma pop   
    
#elif defined (ADC_START_SEC_VAR_FAST_POWER_ON_INIT_UNSPECIFIED)
   #undef      ADC_START_SEC_VAR_FAST_POWER_ON_INIT_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (ADC_STOP_SEC_VAR_FAST_POWER_ON_INIT_UNSPECIFIED)
   #undef      ADC_STOP_SEC_VAR_FAST_POWER_ON_INIT_UNSPECIFIED
   #pragma pop     
   
#elif defined (ADC_START_SEC_VAR_FAST_1BIT)
   #undef      ADC_START_SEC_VAR_FAST_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (ADC_STOP_SEC_VAR_FAST_1BIT)
   #undef      ADC_STOP_SEC_VAR_FAST_1BIT
   #pragma pop
   
#elif defined (ADC_START_SEC_VAR_FAST_8BIT)
   #undef      ADC_START_SEC_VAR_FAST_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (ADC_STOP_SEC_VAR_FAST_8BIT)
   #undef      ADC_STOP_SEC_VAR_FAST_8BIT
   #pragma pop
   
#elif defined (ADC_START_SEC_VAR_FAST_16BIT)
   #undef      ADC_START_SEC_VAR_FAST_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (ADC_STOP_SEC_VAR_FAST_16BIT)
   #undef      ADC_STOP_SEC_VAR_FAST_16BIT
   #pragma pop
   
#elif defined (ADC_START_SEC_VAR_FAST_32BIT)
   #undef      ADC_START_SEC_VAR_FAST_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (ADC_STOP_SEC_VAR_FAST_32BIT)
   #undef      ADC_STOP_SEC_VAR_FAST_32BIT
   #pragma pop   
   
#elif defined (ADC_START_SEC_VAR_FAST_UNSPECIFIED)
   #undef      ADC_START_SEC_VAR_FAST_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (ADC_STOP_SEC_VAR_FAST_UNSPECIFIED)
   #undef      ADC_STOP_SEC_VAR_FAST_UNSPECIFIED
   #pragma pop   
   
#elif defined (ADC_START_SEC_CONST_1BIT)
   #undef      ADC_START_SEC_CONST_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (ADC_STOP_SEC_CONST_1BIT)
   #undef      ADC_STOP_SEC_CONST_1BIT
   #pragma pop
   
#elif defined (ADC_START_SEC_CONST_8BIT)
   #undef      ADC_START_SEC_CONST_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (ADC_STOP_SEC_CONST_8BIT)
   #undef      ADC_STOP_SEC_CONST_8BIT
   #pragma pop
   
#elif defined (ADC_START_SEC_CONST_16BIT)
   #undef      ADC_START_SEC_CONST_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (ADC_STOP_SEC_CONST_16BIT)
   #undef      ADC_STOP_SEC_CONST_16BIT
   #pragma pop
   
#elif defined (ADC_START_SEC_CONST_32BIT)
   #undef      ADC_START_SEC_CONST_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (ADC_STOP_SEC_CONST_32BIT)
   #undef      ADC_STOP_SEC_CONST_32BIT
   #pragma pop   
      
#elif defined (ADC_START_SEC_CONST_UNSPECIFIED)
   #undef      ADC_START_SEC_CONST_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (ADC_STOP_SEC_CONST_UNSPECIFIED)
   #undef      ADC_STOP_SEC_CONST_UNSPECIFIED
   #pragma pop   
   
#elif defined (ADC_START_SEC_CONST_FAST_1BIT)
   #undef      ADC_START_SEC_CONST_FAST_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (ADC_STOP_SEC_CONST_FAST_1BIT)
   #undef      ADC_STOP_SEC_CONST_FAST_1BIT
   #pragma pop
   
#elif defined (ADC_START_SEC_CONST_FAST_8BIT)
   #undef      ADC_START_SEC_CONST_FAST_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (ADC_STOP_SEC_CONST_FAST_8BIT)
   #undef      ADC_STOP_SEC_CONST_FAST_8BIT
   #pragma pop
   
#elif defined (ADC_START_SEC_CONST_FAST_16BIT)
   #undef      ADC_START_SEC_CONST_FAST_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (ADC_STOP_SEC_CONST_FAST_16BIT)
   #undef      ADC_STOP_SEC_CONST_FAST_16BIT
   #pragma pop
   
#elif defined (ADC_START_SEC_CONST_FAST_32BIT)
   #undef      ADC_START_SEC_CONST_FAST_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (ADC_STOP_SEC_CONST_FAST_32BIT)
   #undef      ADC_STOP_SEC_CONST_FAST_32BIT
   #pragma pop   
      
#elif defined (ADC_START_SEC_CONST_FAST_UNSPECIFIED)
   #undef      ADC_START_SEC_CONST_FAST_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (ADC_STOP_SEC_CONST_FAST_UNSPECIFIED)
   #undef      ADC_STOP_SEC_CONST_FAST_UNSPECIFIED
   #pragma pop   
   
#elif defined (MCU_START_SEC_CONST_PBCFG_ROOT)
   #undef      MCU_START_SEC_CONST_PBCFG_ROOT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel
#elif defined (MCU_STOP_SEC_CONST_PBCFG_ROOT)
   #undef      MCU_STOP_SEC_CONST_PBCFG_ROOT
   #pragma pop

#elif defined (ADC_START_SEC_CODE)
   #undef      ADC_START_SEC_CODE
   #pragma push
   __declspec (section ".MCAL_CODE")
   //__declspec (section ".ADC_CODE_ROM")
#elif defined (ADC_STOP_SEC_CODE)
   #undef      ADC_STOP_SEC_CODE
   #pragma pop

#elif defined (ADC_START_SEC_APPL_CODE)
   #undef      ADC_START_SEC_APPL_CODE
   #pragma push
   __declspec (section ".ADC_APPL_CODE_ROM")
#elif defined (ADC_STOP_SEC_APPL_CODE)
   #undef      ADC_STOP_SEC_APPL_CODE
   #pragma pop

/* -------------------------------------------------------------------------- */
/*             ICU                                                            */
/* -------------------------------------------------------------------------- */
#elif defined (ICU_START_SEC_VAR_NOINIT_1BIT)
   #undef      ICU_START_SEC_VAR_NOINIT_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (ICU_STOP_SEC_VAR_NOINIT_1BIT)
   #undef      ICU_STOP_SEC_VAR_NOINIT_1BIT
   #pragma pop
   
#elif defined (ICU_START_SEC_VAR_NOINIT_8BIT)
   #undef      ICU_START_SEC_VAR_NOINIT_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (ICU_STOP_SEC_VAR_NOINIT_8BIT)
   #undef      ICU_STOP_SEC_VAR_NOINIT_8BIT
   #pragma pop
   
#elif defined (ICU_START_SEC_VAR_NOINIT_16BIT)
   #undef      ICU_START_SEC_VAR_NOINIT_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (ICU_STOP_SEC_VAR_NOINIT_16BIT)
   #undef      ICU_STOP_SEC_VAR_NOINIT_16BIT
   #pragma pop
   
#elif defined (ICU_START_SEC_VAR_NOINIT_32BIT)
   #undef      ICU_START_SEC_VAR_NOINIT_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (ICU_STOP_SEC_VAR_NOINIT_32BIT)
   #undef      ICU_STOP_SEC_VAR_NOINIT_32BIT
   #pragma pop   

#elif defined (ICU_START_SEC_VAR_NOINIT_UNSPECIFIED)
   #undef      ICU_START_SEC_VAR_NOINIT_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (ICU_STOP_SEC_VAR_NOINIT_UNSPECIFIED)
   #undef      ICU_STOP_SEC_VAR_NOINIT_UNSPECIFIED
   #pragma pop   
   
#elif defined (ICU_START_SEC_VAR_POWER_ON_INIT_1BIT)
   #undef      ICU_START_SEC_VAR_POWER_ON_INIT_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (ICU_STOP_SEC_VAR_POWER_ON_INIT_1BIT)
   #undef      ICU_STOP_SEC_VAR_POWER_ON_INIT_1BIT
   #pragma pop
   
#elif defined (ICU_START_SEC_VAR_POWER_ON_INIT_8BIT)
   #undef      ICU_START_SEC_VAR_POWER_ON_INIT_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (ICU_STOP_SEC_VAR_POWER_ON_INIT_8BIT)
   #undef      ICU_STOP_SEC_VAR_POWER_ON_INIT_8BIT
   #pragma pop
   
#elif defined (ICU_START_SEC_VAR_POWER_ON_INIT_16BIT)
   #undef      ICU_START_SEC_VAR_POWER_ON_INIT_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (ICU_STOP_SEC_VAR_POWER_ON_INIT_16BIT)
   #undef      ICU_STOP_SEC_VAR_POWER_ON_INIT_16BIT
   #pragma pop
   
#elif defined (ICU_START_SEC_VAR_POWER_ON_INIT_32BIT)
   #undef      ICU_START_SEC_VAR_POWER_ON_INIT_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (ICU_STOP_SEC_VAR_POWER_ON_INIT_32BIT)
   #undef      ICU_STOP_SEC_VAR_POWER_ON_INIT_32BIT
   #pragma pop  

 
#elif defined (ICU_START_SEC_VAR_POWER_ON_INIT_UNSPECIFIED)
   #undef      ICU_START_SEC_VAR_POWER_ON_INIT_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (ICU_STOP_SEC_VAR_POWER_ON_INIT_UNSPECIFIED)
   #undef      ICU_STOP_SEC_VAR_POWER_ON_INIT_UNSPECIFIED
   #pragma pop   
   
#elif defined (ICU_START_SEC_VAR_1BIT)
   #undef      ICU_START_SEC_VAR_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (ICU_STOP_SEC_VAR_1BIT)
   #undef      ICU_STOP_SEC_VAR_1BIT
   #pragma pop
   
#elif defined (ICU_START_SEC_VAR_8BIT)
   #undef      ICU_START_SEC_VAR_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (ICU_STOP_SEC_VAR_8BIT)
   #undef      ICU_STOP_SEC_VAR_8BIT
   #pragma pop
   
#elif defined (ICU_START_SEC_VAR_16BIT)
   #undef      ICU_START_SEC_VAR_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (ICU_STOP_SEC_VAR_16BIT)
   #undef      ICU_STOP_SEC_VAR_16BIT
   #pragma pop
   
#elif defined (ICU_START_SEC_VAR_32BIT)
   #undef      ICU_START_SEC_VAR_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (ICU_STOP_SEC_VAR_32BIT)
   #undef      ICU_STOP_SEC_VAR_32BIT
   #pragma pop   
   
#elif defined (ICU_START_SEC_VAR_UNSPECIFIED)
   #undef      ICU_START_SEC_VAR_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (ICU_STOP_SEC_VAR_UNSPECIFIED)
   #undef      ICU_STOP_SEC_VAR_UNSPECIFIED
   #pragma pop   
   
#elif defined (ICU_START_SEC_VAR_FAST_NOINIT_1BIT)
   #undef      ICU_START_SEC_VAR_FAST_NOINIT_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (ICU_STOP_SEC_VAR_FAST_NOINIT_1BIT)
   #undef      ICU_STOP_SEC_VAR_FAST_NOINIT_1BIT
   #pragma pop
   
#elif defined (ICU_START_SEC_VAR_FAST_NOINIT_8BIT)
   #undef      ICU_START_SEC_VAR_FAST_NOINIT_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (ICU_STOP_SEC_VAR_FAST_NOINIT_8BIT)
   #undef      ICU_STOP_SEC_VAR_FAST_NOINIT_8BIT
   #pragma pop
   
#elif defined (ICU_START_SEC_VAR_FAST_NOINIT_16BIT)
   #undef      ICU_START_SEC_VAR_FAST_NOINIT_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (ICU_STOP_SEC_VAR_FAST_NOINIT_16BIT)
   #undef      ICU_STOP_SEC_VAR_FAST_NOINIT_16BIT
   #pragma pop
   
#elif defined (ICU_START_SEC_VAR_FAST_NOINIT_32BIT)
   #undef      ICU_START_SEC_VAR_FAST_NOINIT_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (ICU_STOP_SEC_VAR_FAST_NOINIT_32BIT)
   #undef      ICU_STOP_SEC_VAR_FAST_NOINIT_32BIT
   #pragma pop   
    
#elif defined (ICU_START_SEC_VAR_FAST_NOINIT_UNSPECIFIED)
   #undef      ICU_START_SEC_VAR_FAST_NOINIT_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (ICU_STOP_SEC_VAR_FAST_NOINIT_UNSPECIFIED)
   #undef      ICU_STOP_SEC_VAR_FAST_NOINIT_UNSPECIFIED
   #pragma pop     
   
#elif defined (ICU_START_SEC_VAR_FAST_POWER_ON_INIT_1BIT)
   #undef      ICU_START_SEC_VAR_FAST_POWER_ON_INIT_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (ICU_STOP_SEC_VAR_FAST_POWER_ON_INIT_1BIT)
   #undef      ICU_STOP_SEC_VAR_FAST_POWER_ON_INIT_1BIT
   #pragma pop
   
#elif defined (ICU_START_SEC_VAR_FAST_POWER_ON_INIT_8BIT)
   #undef      ICU_START_SEC_VAR_FAST_POWER_ON_INIT_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (ICU_STOP_SEC_VAR_FAST_POWER_ON_INIT_8BIT)
   #undef      ICU_STOP_SEC_VAR_FAST_POWER_ON_INIT_8BIT
   #pragma pop
   
#elif defined (ICU_START_SEC_VAR_FAST_POWER_ON_INIT_16BIT)
   #undef      ICU_START_SEC_VAR_FAST_POWER_ON_INIT_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (ICU_STOP_SEC_VAR_FAST_POWER_ON_INIT_16BIT)
   #undef      ICU_STOP_SEC_VAR_FAST_POWER_ON_INIT_16BIT
   #pragma pop
   
#elif defined (ICU_START_SEC_VAR_FAST_POWER_ON_INIT_32BIT)
   #undef      ICU_START_SEC_VAR_FAST_POWER_ON_INIT_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (ICU_STOP_SEC_VAR_FAST_POWER_ON_INIT_32BIT)
   #undef      ICU_STOP_SEC_VAR_FAST_POWER_ON_INIT_32BIT
   #pragma pop   
    
#elif defined (ICU_START_SEC_VAR_FAST_POWER_ON_INIT_UNSPECIFIED)
   #undef      ICU_START_SEC_VAR_FAST_POWER_ON_INIT_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (ICU_STOP_SEC_VAR_FAST_POWER_ON_INIT_UNSPECIFIED)
   #undef      ICU_STOP_SEC_VAR_FAST_POWER_ON_INIT_UNSPECIFIED
   #pragma pop     
   
#elif defined (ICU_START_SEC_VAR_FAST_1BIT)
   #undef      ICU_START_SEC_VAR_FAST_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (ICU_STOP_SEC_VAR_FAST_1BIT)
   #undef      ICU_STOP_SEC_VAR_FAST_1BIT
   #pragma pop
   
#elif defined (ICU_START_SEC_VAR_FAST_8BIT)
   #undef      ICU_START_SEC_VAR_FAST_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (ICU_STOP_SEC_VAR_FAST_8BIT)
   #undef      ICU_STOP_SEC_VAR_FAST_8BIT
   #pragma pop
   
#elif defined (ICU_START_SEC_VAR_FAST_16BIT)
   #undef      ICU_START_SEC_VAR_FAST_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (ICU_STOP_SEC_VAR_FAST_16BIT)
   #undef      ICU_STOP_SEC_VAR_FAST_16BIT
   #pragma pop
   
#elif defined (ICU_START_SEC_VAR_FAST_32BIT)
   #undef      ICU_START_SEC_VAR_FAST_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (ICU_STOP_SEC_VAR_FAST_32BIT)
   #undef      ICU_STOP_SEC_VAR_FAST_32BIT
   #pragma pop   
   
#elif defined (ICU_START_SEC_VAR_FAST_UNSPECIFIED)
   #undef      ICU_START_SEC_VAR_FAST_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (ICU_STOP_SEC_VAR_FAST_UNSPECIFIED)
   #undef      ICU_STOP_SEC_VAR_FAST_UNSPECIFIED
   #pragma pop   
   
#elif defined (ICU_START_SEC_CONST_1BIT)
   #undef      ICU_START_SEC_CONST_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (ICU_STOP_SEC_CONST_1BIT)
   #undef      ICU_STOP_SEC_CONST_1BIT
   #pragma pop
   
#elif defined (ICU_START_SEC_CONST_8BIT)
   #undef      ICU_START_SEC_CONST_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (ICU_STOP_SEC_CONST_8BIT)
   #undef      ICU_STOP_SEC_CONST_8BIT
   #pragma pop
   
#elif defined (ICU_START_SEC_CONST_16BIT)
   #undef      ICU_START_SEC_CONST_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (ICU_STOP_SEC_CONST_16BIT)
   #undef      ICU_STOP_SEC_CONST_16BIT
   #pragma pop
   
#elif defined (ICU_START_SEC_CONST_32BIT)
   #undef      ICU_START_SEC_CONST_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (ICU_STOP_SEC_CONST_32BIT)
   #undef      ICU_STOP_SEC_CONST_32BIT
   #pragma pop   
      
#elif defined (ICU_START_SEC_CONST_UNSPECIFIED)
   #undef      ICU_START_SEC_CONST_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (ICU_STOP_SEC_CONST_UNSPECIFIED)
   #undef      ICU_STOP_SEC_CONST_UNSPECIFIED
   #pragma pop   

   
#elif defined (ICU_START_SEC_CONST_FAST_1BIT)
   #undef      ICU_START_SEC_CONST_FAST_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (ICU_STOP_SEC_CONST_FAST_1BIT)
   #undef      ICU_STOP_SEC_CONST_FAST_1BIT
   #pragma pop
   
#elif defined (ICU_START_SEC_CONST_FAST_8BIT)
   #undef      ICU_START_SEC_CONST_FAST_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (ICU_STOP_SEC_CONST_FAST_8BIT)
   #undef      ICU_STOP_SEC_CONST_FAST_8BIT
   #pragma pop
   
#elif defined (ICU_START_SEC_CONST_FAST_16BIT)
   #undef      ICU_START_SEC_CONST_FAST_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (ICU_STOP_SEC_CONST_FAST_16BIT)
   #undef      ICU_STOP_SEC_CONST_FAST_16BIT
   #pragma pop
   
#elif defined (ICU_START_SEC_CONST_FAST_32BIT)
   #undef      ICU_START_SEC_CONST_FAST_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (ICU_STOP_SEC_CONST_FAST_32BIT)
   #undef      ICU_STOP_SEC_CONST_FAST_32BIT
   #pragma pop   
      
#elif defined (ICU_START_SEC_CONST_FAST_UNSPECIFIED)
   #undef      ICU_START_SEC_CONST_FAST_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (ICU_STOP_SEC_CONST_FAST_UNSPECIFIED)
   #undef      ICU_STOP_SEC_CONST_FAST_UNSPECIFIED
   #pragma pop   
   
#elif defined (MCU_START_SEC_CONST_PBCFG_ROOT)
   #undef      MCU_START_SEC_CONST_PBCFG_ROOT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel
#elif defined (MCU_STOP_SEC_CONST_PBCFG_ROOT)
   #undef      MCU_STOP_SEC_CONST_PBCFG_ROOT
   #pragma pop

#elif defined (ICU_START_SEC_CODE)
   #undef      ICU_START_SEC_CODE
   #pragma push
   __declspec (section ".MCAL_CODE")
   //__declspec (section ".ICU_CODE_ROM")
#elif defined (ICU_STOP_SEC_CODE)
   #undef      ICU_STOP_SEC_CODE
   #pragma pop

#elif defined (ICU_START_SEC_APPL_CODE)
   #undef      ICU_START_SEC_APPL_CODE
   #pragma push
   __declspec (section ".ICU_APPL_CODE_ROM")
#elif defined (ICU_STOP_SEC_APPL_CODE)
   #undef      ICU_STOP_SEC_APPL_CODE
   #pragma pop

/* -------------------------------------------------------------------------- */
/*             FLS                                                            */
/* -------------------------------------------------------------------------- */
#elif defined (FLS_START_SEC_VAR_NOINIT_1BIT)
   #undef      FLS_START_SEC_VAR_NOINIT_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (FLS_STOP_SEC_VAR_NOINIT_1BIT)
   #undef      FLS_STOP_SEC_VAR_NOINIT_1BIT
   #pragma pop
   
#elif defined (FLS_START_SEC_VAR_NOINIT_8BIT)
   #undef      FLS_START_SEC_VAR_NOINIT_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (FLS_STOP_SEC_VAR_NOINIT_8BIT)
   #undef      FLS_STOP_SEC_VAR_NOINIT_8BIT
   #pragma pop
   
#elif defined (FLS_START_SEC_VAR_NOINIT_16BIT)
   #undef      FLS_START_SEC_VAR_NOINIT_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (FLS_STOP_SEC_VAR_NOINIT_16BIT)
   #undef      FLS_STOP_SEC_VAR_NOINIT_16BIT
   #pragma pop
   
#elif defined (FLS_START_SEC_VAR_NOINIT_32BIT)
   #undef      FLS_START_SEC_VAR_NOINIT_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (FLS_STOP_SEC_VAR_NOINIT_32BIT)
   #undef      FLS_STOP_SEC_VAR_NOINIT_32BIT
   #pragma pop   

#elif defined (FLS_START_SEC_VAR_NOINIT_UNSPECIFIED)
   #undef      FLS_START_SEC_VAR_NOINIT_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (FLS_STOP_SEC_VAR_NOINIT_UNSPECIFIED)
   #undef      FLS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
   #pragma pop   
   
#elif defined (FLS_START_SEC_VAR_POWER_ON_INIT_1BIT)
   #undef      FLS_START_SEC_VAR_POWER_ON_INIT_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (FLS_STOP_SEC_VAR_POWER_ON_INIT_1BIT)
   #undef      FLS_STOP_SEC_VAR_POWER_ON_INIT_1BIT
   #pragma pop
   
#elif defined (FLS_START_SEC_VAR_POWER_ON_INIT_8BIT)
   #undef      FLS_START_SEC_VAR_POWER_ON_INIT_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (FLS_STOP_SEC_VAR_POWER_ON_INIT_8BIT)
   #undef      FLS_STOP_SEC_VAR_POWER_ON_INIT_8BIT
   #pragma pop
   
#elif defined (FLS_START_SEC_VAR_POWER_ON_INIT_16BIT)
   #undef      FLS_START_SEC_VAR_POWER_ON_INIT_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (FLS_STOP_SEC_VAR_POWER_ON_INIT_16BIT)
   #undef      FLS_STOP_SEC_VAR_POWER_ON_INIT_16BIT
   #pragma pop
   
#elif defined (FLS_START_SEC_VAR_POWER_ON_INIT_32BIT)
   #undef      FLS_START_SEC_VAR_POWER_ON_INIT_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (FLS_STOP_SEC_VAR_POWER_ON_INIT_32BIT)
   #undef      FLS_STOP_SEC_VAR_POWER_ON_INIT_32BIT
   #pragma pop  

 
#elif defined (FLS_START_SEC_VAR_POWER_ON_INIT_UNSPECIFIED)
   #undef      FLS_START_SEC_VAR_POWER_ON_INIT_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (FLS_STOP_SEC_VAR_POWER_ON_INIT_UNSPECIFIED)
   #undef      FLS_STOP_SEC_VAR_POWER_ON_INIT_UNSPECIFIED
   #pragma pop   
   
#elif defined (FLS_START_SEC_VAR_1BIT)
   #undef      FLS_START_SEC_VAR_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (FLS_STOP_SEC_VAR_1BIT)
   #undef      FLS_STOP_SEC_VAR_1BIT
   #pragma pop
   
#elif defined (FLS_START_SEC_VAR_8BIT)
   #undef      FLS_START_SEC_VAR_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (FLS_STOP_SEC_VAR_8BIT)
   #undef      FLS_STOP_SEC_VAR_8BIT
   #pragma pop
   
#elif defined (FLS_START_SEC_VAR_16BIT)
   #undef      FLS_START_SEC_VAR_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (FLS_STOP_SEC_VAR_16BIT)
   #undef      FLS_STOP_SEC_VAR_16BIT
   #pragma pop
   
#elif defined (FLS_START_SEC_VAR_32BIT)
   #undef      FLS_START_SEC_VAR_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (FLS_STOP_SEC_VAR_32BIT)
   #undef      FLS_STOP_SEC_VAR_32BIT
   #pragma pop   
   
#elif defined (FLS_START_SEC_VAR_UNSPECIFIED)
   #undef      FLS_START_SEC_VAR_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (FLS_STOP_SEC_VAR_UNSPECIFIED)
   #undef      FLS_STOP_SEC_VAR_UNSPECIFIED
   #pragma pop   
   
#elif defined (FLS_START_SEC_VAR_FAST_NOINIT_1BIT)
   #undef      FLS_START_SEC_VAR_FAST_NOINIT_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (FLS_STOP_SEC_VAR_FAST_NOINIT_1BIT)
   #undef      FLS_STOP_SEC_VAR_FAST_NOINIT_1BIT
   #pragma pop
   
#elif defined (FLS_START_SEC_VAR_FAST_NOINIT_8BIT)
   #undef      FLS_START_SEC_VAR_FAST_NOINIT_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (FLS_STOP_SEC_VAR_FAST_NOINIT_8BIT)
   #undef      FLS_STOP_SEC_VAR_FAST_NOINIT_8BIT
   #pragma pop
   
#elif defined (FLS_START_SEC_VAR_FAST_NOINIT_16BIT)
   #undef      FLS_START_SEC_VAR_FAST_NOINIT_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (FLS_STOP_SEC_VAR_FAST_NOINIT_16BIT)
   #undef      FLS_STOP_SEC_VAR_FAST_NOINIT_16BIT
   #pragma pop
   
#elif defined (FLS_START_SEC_VAR_FAST_NOINIT_32BIT)
   #undef      FLS_START_SEC_VAR_FAST_NOINIT_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (FLS_STOP_SEC_VAR_FAST_NOINIT_32BIT)
   #undef      FLS_STOP_SEC_VAR_FAST_NOINIT_32BIT
   #pragma pop   
    
#elif defined (FLS_START_SEC_VAR_FAST_NOINIT_UNSPECIFIED)
   #undef      FLS_START_SEC_VAR_FAST_NOINIT_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (FLS_STOP_SEC_VAR_FAST_NOINIT_UNSPECIFIED)
   #undef      FLS_STOP_SEC_VAR_FAST_NOINIT_UNSPECIFIED
   #pragma pop     
   
#elif defined (FLS_START_SEC_VAR_FAST_POWER_ON_INIT_1BIT)
   #undef      FLS_START_SEC_VAR_FAST_POWER_ON_INIT_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (FLS_STOP_SEC_VAR_FAST_POWER_ON_INIT_1BIT)
   #undef      FLS_STOP_SEC_VAR_FAST_POWER_ON_INIT_1BIT
   #pragma pop
   
#elif defined (FLS_START_SEC_VAR_FAST_POWER_ON_INIT_8BIT)
   #undef      FLS_START_SEC_VAR_FAST_POWER_ON_INIT_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (FLS_STOP_SEC_VAR_FAST_POWER_ON_INIT_8BIT)
   #undef      FLS_STOP_SEC_VAR_FAST_POWER_ON_INIT_8BIT
   #pragma pop
   
#elif defined (FLS_START_SEC_VAR_FAST_POWER_ON_INIT_16BIT)
   #undef      FLS_START_SEC_VAR_FAST_POWER_ON_INIT_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (FLS_STOP_SEC_VAR_FAST_POWER_ON_INIT_16BIT)
   #undef      FLS_STOP_SEC_VAR_FAST_POWER_ON_INIT_16BIT
   #pragma pop
   
#elif defined (FLS_START_SEC_VAR_FAST_POWER_ON_INIT_32BIT)
   #undef      FLS_START_SEC_VAR_FAST_POWER_ON_INIT_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (FLS_STOP_SEC_VAR_FAST_POWER_ON_INIT_32BIT)
   #undef      FLS_STOP_SEC_VAR_FAST_POWER_ON_INIT_32BIT
   #pragma pop   
    
#elif defined (FLS_START_SEC_VAR_FAST_POWER_ON_INIT_UNSPECIFIED)
   #undef      FLS_START_SEC_VAR_FAST_POWER_ON_INIT_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (FLS_STOP_SEC_VAR_FAST_POWER_ON_INIT_UNSPECIFIED)
   #undef      FLS_STOP_SEC_VAR_FAST_POWER_ON_INIT_UNSPECIFIED
   #pragma pop     
   
#elif defined (FLS_START_SEC_VAR_FAST_1BIT)
   #undef      FLS_START_SEC_VAR_FAST_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (FLS_STOP_SEC_VAR_FAST_1BIT)
   #undef      FLS_STOP_SEC_VAR_FAST_1BIT
   #pragma pop
   
#elif defined (FLS_START_SEC_VAR_FAST_8BIT)
   #undef      FLS_START_SEC_VAR_FAST_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (FLS_STOP_SEC_VAR_FAST_8BIT)
   #undef      FLS_STOP_SEC_VAR_FAST_8BIT
   #pragma pop
   
#elif defined (FLS_START_SEC_VAR_FAST_16BIT)
   #undef      FLS_START_SEC_VAR_FAST_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (FLS_STOP_SEC_VAR_FAST_16BIT)
   #undef      FLS_STOP_SEC_VAR_FAST_16BIT
   #pragma pop
   
#elif defined (FLS_START_SEC_VAR_FAST_32BIT)
   #undef      FLS_START_SEC_VAR_FAST_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (FLS_STOP_SEC_VAR_FAST_32BIT)
   #undef      FLS_STOP_SEC_VAR_FAST_32BIT
   #pragma pop   
   
#elif defined (FLS_START_SEC_VAR_FAST_UNSPECIFIED)
   #undef      FLS_START_SEC_VAR_FAST_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (FLS_STOP_SEC_VAR_FAST_UNSPECIFIED)
   #undef      FLS_STOP_SEC_VAR_FAST_UNSPECIFIED
   #pragma pop   
   
#elif defined (FLS_START_SEC_CONST_1BIT)
   #undef      FLS_START_SEC_CONST_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (FLS_STOP_SEC_CONST_1BIT)
   #undef      FLS_STOP_SEC_CONST_1BIT
   #pragma pop
   
#elif defined (FLS_START_SEC_CONST_8BIT)
   #undef      FLS_START_SEC_CONST_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (FLS_STOP_SEC_CONST_8BIT)
   #undef      FLS_STOP_SEC_CONST_8BIT
   #pragma pop
   
#elif defined (FLS_START_SEC_CONST_16BIT)
   #undef      FLS_START_SEC_CONST_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (FLS_STOP_SEC_CONST_16BIT)
   #undef      FLS_STOP_SEC_CONST_16BIT
   #pragma pop
   
#elif defined (FLS_START_SEC_CONST_32BIT)
   #undef      FLS_START_SEC_CONST_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (FLS_STOP_SEC_CONST_32BIT)
   #undef      FLS_STOP_SEC_CONST_32BIT
   #pragma pop   
      
#elif defined (FLS_START_SEC_CONST_UNSPECIFIED)
   #undef      FLS_START_SEC_CONST_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (FLS_STOP_SEC_CONST_UNSPECIFIED)
   #undef      FLS_STOP_SEC_CONST_UNSPECIFIED
   #pragma pop   

   
#elif defined (FLS_START_SEC_CONST_FAST_1BIT)
   #undef      FLS_START_SEC_CONST_FAST_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (FLS_STOP_SEC_CONST_FAST_1BIT)
   #undef      FLS_STOP_SEC_CONST_FAST_1BIT
   #pragma pop
   
#elif defined (FLS_START_SEC_CONST_FAST_8BIT)
   #undef      FLS_START_SEC_CONST_FAST_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (FLS_STOP_SEC_CONST_FAST_8BIT)
   #undef      FLS_STOP_SEC_CONST_FAST_8BIT
   #pragma pop
   
#elif defined (FLS_START_SEC_CONST_FAST_16BIT)
   #undef      FLS_START_SEC_CONST_FAST_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (FLS_STOP_SEC_CONST_FAST_16BIT)
   #undef      FLS_STOP_SEC_CONST_FAST_16BIT
   #pragma pop
   
#elif defined (FLS_START_SEC_CONST_FAST_32BIT)
   #undef      FLS_START_SEC_CONST_FAST_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (FLS_STOP_SEC_CONST_FAST_32BIT)
   #undef      FLS_STOP_SEC_CONST_FAST_32BIT
   #pragma pop   
      
#elif defined (FLS_START_SEC_CONST_FAST_UNSPECIFIED)
   #undef      FLS_START_SEC_CONST_FAST_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (FLS_STOP_SEC_CONST_FAST_UNSPECIFIED)
   #undef      FLS_STOP_SEC_CONST_FAST_UNSPECIFIED
   #pragma pop   
   
#elif defined (FLS_START_SEC_CONST_PBCFG)
   #undef      FLS_START_SEC_CONST_PBCFG
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel
#elif defined (FLS_STOP_SEC_CONST_PBCFG)
   #undef      FLS_STOP_SEC_CONST_PBCFG
   #pragma pop
   
#elif defined (FLS_START_SEC_CONST_PBCFG_ROOT)
   #undef      FLS_START_SEC_CONST_PBCFG_ROOT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel
#elif defined (FLS_STOP_SEC_CONST_PBCFG_ROOT)
   #undef      FLS_STOP_SEC_CONST_PBCFG_ROOT
   #pragma pop

#elif defined (FLS_START_SEC_CODE)
   #undef      FLS_START_SEC_CODE
   #pragma push
   __declspec (section ".MCAL_CODE")
#elif defined (FLS_STOP_SEC_CODE)
   #undef      FLS_STOP_SEC_CODE
   #pragma pop

#elif defined (FLS_START_SEC_APPL_CODE)
   #undef      FLS_START_SEC_APPL_CODE
   #pragma push
   __declspec (section ".FLS_APPL_CODE_ROM")
#elif defined (FLS_STOP_SEC_APPL_CODE)
   #undef      FLS_STOP_SEC_APPL_CODE
   #pragma pop


/* -------------------------------------------------------------------------- */
/*             EEP                                                            */
/* -------------------------------------------------------------------------- */
#elif defined (EEP_START_SEC_VAR_NOINIT_1BIT)
   #undef      EEP_START_SEC_VAR_NOINIT_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (EEP_STOP_SEC_VAR_NOINIT_1BIT)
   #undef      EEP_STOP_SEC_VAR_NOINIT_1BIT
   #pragma pop
   
#elif defined (EEP_START_SEC_VAR_NOINIT_8BIT)
   #undef      EEP_START_SEC_VAR_NOINIT_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (EEP_STOP_SEC_VAR_NOINIT_8BIT)
   #undef      EEP_STOP_SEC_VAR_NOINIT_8BIT
   #pragma pop
   
#elif defined (EEP_START_SEC_VAR_NOINIT_16BIT)
   #undef      EEP_START_SEC_VAR_NOINIT_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (EEP_STOP_SEC_VAR_NOINIT_16BIT)
   #undef      EEP_STOP_SEC_VAR_NOINIT_16BIT
   #pragma pop
   
#elif defined (EEP_START_SEC_VAR_NOINIT_32BIT)
   #undef      EEP_START_SEC_VAR_NOINIT_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (EEP_STOP_SEC_VAR_NOINIT_32BIT)
   #undef      EEP_STOP_SEC_VAR_NOINIT_32BIT
   #pragma pop   

#elif defined (EEP_START_SEC_VAR_NOINIT_UNSPECIFIED)
   #undef      EEP_START_SEC_VAR_NOINIT_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (EEP_STOP_SEC_VAR_NOINIT_UNSPECIFIED)
   #undef      EEP_STOP_SEC_VAR_NOINIT_UNSPECIFIED
   #pragma pop   
   
#elif defined (EEP_START_SEC_VAR_POWER_ON_INIT_1BIT)
   #undef      EEP_START_SEC_VAR_POWER_ON_INIT_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (EEP_STOP_SEC_VAR_POWER_ON_INIT_1BIT)
   #undef      EEP_STOP_SEC_VAR_POWER_ON_INIT_1BIT
   #pragma pop
   
#elif defined (EEP_START_SEC_VAR_POWER_ON_INIT_8BIT)
   #undef      EEP_START_SEC_VAR_POWER_ON_INIT_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (EEP_STOP_SEC_VAR_POWER_ON_INIT_8BIT)
   #undef      EEP_STOP_SEC_VAR_POWER_ON_INIT_8BIT
   #pragma pop
   
#elif defined (EEP_START_SEC_VAR_POWER_ON_INIT_16BIT)
   #undef      EEP_START_SEC_VAR_POWER_ON_INIT_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (EEP_STOP_SEC_VAR_POWER_ON_INIT_16BIT)
   #undef      EEP_STOP_SEC_VAR_POWER_ON_INIT_16BIT
   #pragma pop
   
#elif defined (EEP_START_SEC_VAR_POWER_ON_INIT_32BIT)
   #undef      EEP_START_SEC_VAR_POWER_ON_INIT_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (EEP_STOP_SEC_VAR_POWER_ON_INIT_32BIT)
   #undef      EEP_STOP_SEC_VAR_POWER_ON_INIT_32BIT
   #pragma pop  
 
#elif defined (EEP_START_SEC_VAR_POWER_ON_INIT_UNSPECIFIED)
   #undef      EEP_START_SEC_VAR_POWER_ON_INIT_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (EEP_STOP_SEC_VAR_POWER_ON_INIT_UNSPECIFIED)
   #undef      EEP_STOP_SEC_VAR_POWER_ON_INIT_UNSPECIFIED
   #pragma pop   
   
#elif defined (EEP_START_SEC_VAR_1BIT)
   #undef      EEP_START_SEC_VAR_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (EEP_STOP_SEC_VAR_1BIT)
   #undef      EEP_STOP_SEC_VAR_1BIT
   #pragma pop
   
#elif defined (EEP_START_SEC_VAR_8BIT)
   #undef      EEP_START_SEC_VAR_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (EEP_STOP_SEC_VAR_8BIT)
   #undef      EEP_STOP_SEC_VAR_8BIT
   #pragma pop
   
#elif defined (EEP_START_SEC_VAR_16BIT)
   #undef      EEP_START_SEC_VAR_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (EEP_STOP_SEC_VAR_16BIT)
   #undef      EEP_STOP_SEC_VAR_16BIT
   #pragma pop
   
#elif defined (EEP_START_SEC_VAR_32BIT)
   #undef      EEP_START_SEC_VAR_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (EEP_STOP_SEC_VAR_32BIT)
   #undef      EEP_STOP_SEC_VAR_32BIT
   #pragma pop   
   
#elif defined (EEP_START_SEC_VAR_UNSPECIFIED)
   #undef      EEP_START_SEC_VAR_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (EEP_STOP_SEC_VAR_UNSPECIFIED)
   #undef      EEP_STOP_SEC_VAR_UNSPECIFIED
   #pragma pop   
   
#elif defined (EEP_START_SEC_VAR_FAST_NOINIT_1BIT)
   #undef      EEP_START_SEC_VAR_FAST_NOINIT_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (EEP_STOP_SEC_VAR_FAST_NOINIT_1BIT)
   #undef      EEP_STOP_SEC_VAR_FAST_NOINIT_1BIT
   #pragma pop
   
#elif defined (EEP_START_SEC_VAR_FAST_NOINIT_8BIT)
   #undef      EEP_START_SEC_VAR_FAST_NOINIT_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (EEP_STOP_SEC_VAR_FAST_NOINIT_8BIT)
   #undef      EEP_STOP_SEC_VAR_FAST_NOINIT_8BIT
   #pragma pop
   
#elif defined (EEP_START_SEC_VAR_FAST_NOINIT_16BIT)
   #undef      EEP_START_SEC_VAR_FAST_NOINIT_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (EEP_STOP_SEC_VAR_FAST_NOINIT_16BIT)
   #undef      EEP_STOP_SEC_VAR_FAST_NOINIT_16BIT
   #pragma pop
   
#elif defined (EEP_START_SEC_VAR_FAST_NOINIT_32BIT)
   #undef      EEP_START_SEC_VAR_FAST_NOINIT_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (EEP_STOP_SEC_VAR_FAST_NOINIT_32BIT)
   #undef      EEP_STOP_SEC_VAR_FAST_NOINIT_32BIT
   #pragma pop   
    
#elif defined (EEP_START_SEC_VAR_FAST_NOINIT_UNSPECIFIED)
   #undef      EEP_START_SEC_VAR_FAST_NOINIT_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (EEP_STOP_SEC_VAR_FAST_NOINIT_UNSPECIFIED)
   #undef      EEP_STOP_SEC_VAR_FAST_NOINIT_UNSPECIFIED
   #pragma pop     
   
#elif defined (EEP_START_SEC_VAR_FAST_POWER_ON_INIT_1BIT)
   #undef      EEP_START_SEC_VAR_FAST_POWER_ON_INIT_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (EEP_STOP_SEC_VAR_FAST_POWER_ON_INIT_1BIT)
   #undef      EEP_STOP_SEC_VAR_FAST_POWER_ON_INIT_1BIT
   #pragma pop
   
#elif defined (EEP_START_SEC_VAR_FAST_POWER_ON_INIT_8BIT)
   #undef      EEP_START_SEC_VAR_FAST_POWER_ON_INIT_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (EEP_STOP_SEC_VAR_FAST_POWER_ON_INIT_8BIT)
   #undef      EEP_STOP_SEC_VAR_FAST_POWER_ON_INIT_8BIT
   #pragma pop
   
#elif defined (EEP_START_SEC_VAR_FAST_POWER_ON_INIT_16BIT)
   #undef      EEP_START_SEC_VAR_FAST_POWER_ON_INIT_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (EEP_STOP_SEC_VAR_FAST_POWER_ON_INIT_16BIT)
   #undef      EEP_STOP_SEC_VAR_FAST_POWER_ON_INIT_16BIT
   #pragma pop
   
#elif defined (EEP_START_SEC_VAR_FAST_POWER_ON_INIT_32BIT)
   #undef      EEP_START_SEC_VAR_FAST_POWER_ON_INIT_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (EEP_STOP_SEC_VAR_FAST_POWER_ON_INIT_32BIT)
   #undef      EEP_STOP_SEC_VAR_FAST_POWER_ON_INIT_32BIT
   #pragma pop   
    
#elif defined (EEP_START_SEC_VAR_FAST_POWER_ON_INIT_UNSPECIFIED)
   #undef      EEP_START_SEC_VAR_FAST_POWER_ON_INIT_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (EEP_STOP_SEC_VAR_FAST_POWER_ON_INIT_UNSPECIFIED)
   #undef      EEP_STOP_SEC_VAR_FAST_POWER_ON_INIT_UNSPECIFIED
   #pragma pop     
   
#elif defined (EEP_START_SEC_VAR_FAST_1BIT)
   #undef      EEP_START_SEC_VAR_FAST_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (EEP_STOP_SEC_VAR_FAST_1BIT)
   #undef      EEP_STOP_SEC_VAR_FAST_1BIT
   #pragma pop
   
#elif defined (EEP_START_SEC_VAR_FAST_8BIT)
   #undef      EEP_START_SEC_VAR_FAST_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (EEP_STOP_SEC_VAR_FAST_8BIT)
   #undef      EEP_STOP_SEC_VAR_FAST_8BIT
   #pragma pop
   
#elif defined (EEP_START_SEC_VAR_FAST_16BIT)
   #undef      EEP_START_SEC_VAR_FAST_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (EEP_STOP_SEC_VAR_FAST_16BIT)
   #undef      EEP_STOP_SEC_VAR_FAST_16BIT
   #pragma pop
   
#elif defined (EEP_START_SEC_VAR_FAST_32BIT)
   #undef      EEP_START_SEC_VAR_FAST_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (EEP_STOP_SEC_VAR_FAST_32BIT)
   #undef      EEP_STOP_SEC_VAR_FAST_32BIT
   #pragma pop   
   
#elif defined (EEP_START_SEC_VAR_FAST_UNSPECIFIED)
   #undef      EEP_START_SEC_VAR_FAST_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (EEP_STOP_SEC_VAR_FAST_UNSPECIFIED)
   #undef      EEP_STOP_SEC_VAR_FAST_UNSPECIFIED
   #pragma pop   
   
#elif defined (EEP_START_SEC_CONST_1BIT)
   #undef      EEP_START_SEC_CONST_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (EEP_STOP_SEC_CONST_1BIT)
   #undef      EEP_STOP_SEC_CONST_1BIT
   #pragma pop
   
#elif defined (EEP_START_SEC_CONST_8BIT)
   #undef      EEP_START_SEC_CONST_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (EEP_STOP_SEC_CONST_8BIT)
   #undef      EEP_STOP_SEC_CONST_8BIT
   #pragma pop
   
#elif defined (EEP_START_SEC_CONST_16BIT)
   #undef      EEP_START_SEC_CONST_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (EEP_STOP_SEC_CONST_16BIT)
   #undef      EEP_STOP_SEC_CONST_16BIT
   #pragma pop
   
#elif defined (EEP_START_SEC_CONST_32BIT)
   #undef      EEP_START_SEC_CONST_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (EEP_STOP_SEC_CONST_32BIT)
   #undef      EEP_STOP_SEC_CONST_32BIT
   #pragma pop   
      
#elif defined (EEP_START_SEC_CONST_UNSPECIFIED)
   #undef      EEP_START_SEC_CONST_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (EEP_STOP_SEC_CONST_UNSPECIFIED)
   #undef      EEP_STOP_SEC_CONST_UNSPECIFIED
   #pragma pop   

   
#elif defined (EEP_START_SEC_CONST_FAST_1BIT)
   #undef      EEP_START_SEC_CONST_FAST_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (EEP_STOP_SEC_CONST_FAST_1BIT)
   #undef      EEP_STOP_SEC_CONST_FAST_1BIT
   #pragma pop
   
#elif defined (EEP_START_SEC_CONST_FAST_8BIT)
   #undef      EEP_START_SEC_CONST_FAST_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (EEP_STOP_SEC_CONST_FAST_8BIT)
   #undef      EEP_STOP_SEC_CONST_FAST_8BIT
   #pragma pop
   
#elif defined (EEP_START_SEC_CONST_FAST_16BIT)
   #undef      EEP_START_SEC_CONST_FAST_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (EEP_STOP_SEC_CONST_FAST_16BIT)
   #undef      EEP_STOP_SEC_CONST_FAST_16BIT
   #pragma pop
   
#elif defined (EEP_START_SEC_CONST_FAST_32BIT)
   #undef      EEP_START_SEC_CONST_FAST_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (EEP_STOP_SEC_CONST_FAST_32BIT)
   #undef      EEP_STOP_SEC_CONST_FAST_32BIT
   #pragma pop   
      
#elif defined (EEP_START_SEC_CONST_FAST_UNSPECIFIED)
   #undef      EEP_START_SEC_CONST_FAST_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (EEP_STOP_SEC_CONST_FAST_UNSPECIFIED)
   #undef      EEP_STOP_SEC_CONST_FAST_UNSPECIFIED
   #pragma pop   
   
#elif defined (EEP_START_SEC_CONST_PBCFG)
   #undef      EEP_START_SEC_CONST_PBCFG
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel
#elif defined (EEP_STOP_SEC_CONST_PBCFG)
   #undef      EEP_STOP_SEC_CONST_PBCFG
   #pragma pop
 
#elif defined (EEP_START_SEC_CONST_PBCFG_ROOT)
   #undef      EEP_START_SEC_CONST_PBCFG_ROOT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel
#elif defined (EEP_STOP_SEC_CONST_PBCFG_ROOT)
   #undef      EEP_STOP_SEC_CONST_PBCFG_ROOT
   #pragma pop

#elif defined (EEP_START_SEC_CODE)
   #undef      EEP_START_SEC_CODE
   #pragma push
   __declspec (section ".MCAL_CODE")
   //__declspec (section ".EEP_CODE_ROM")
#elif defined (EEP_STOP_SEC_CODE)
   #undef      EEP_STOP_SEC_CODE
   #pragma pop

#elif defined (EEP_START_SEC_APPL_CODE)
   #undef      EEP_START_SEC_APPL_CODE
   #pragma push
   __declspec (section ".EEP_APPL_CODE_ROM")
#elif defined (EEP_STOP_SEC_APPL_CODE)
   #undef      EEP_STOP_SEC_APPL_CODE
   #pragma pop

/* -------------------------------------------------------------------------- */
/*             DMA                                                            */
/* -------------------------------------------------------------------------- */
#elif defined (DMA_START_SEC_VAR_NOINIT_1BIT)
   #undef      DMA_START_SEC_VAR_NOINIT_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (DMA_STOP_SEC_VAR_NOINIT_1BIT)
   #undef      DMA_STOP_SEC_VAR_NOINIT_1BIT
   #pragma pop
   
#elif defined (DMA_START_SEC_VAR_NOINIT_8BIT)
   #undef      DMA_START_SEC_VAR_NOINIT_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (DMA_STOP_SEC_VAR_NOINIT_8BIT)
   #undef      DMA_STOP_SEC_VAR_NOINIT_8BIT
   #pragma pop
   
#elif defined (DMA_START_SEC_VAR_NOINIT_16BIT)
   #undef      DMA_START_SEC_VAR_NOINIT_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (DMA_STOP_SEC_VAR_NOINIT_16BIT)
   #undef      DMA_STOP_SEC_VAR_NOINIT_16BIT
   #pragma pop
   
#elif defined (DMA_START_SEC_VAR_NOINIT_32BIT)
   #undef      DMA_START_SEC_VAR_NOINIT_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (DMA_STOP_SEC_VAR_NOINIT_32BIT)
   #undef      DMA_STOP_SEC_VAR_NOINIT_32BIT
   #pragma pop   

#elif defined (DMA_START_SEC_VAR_NOINIT_UNSPECIFIED)
   #undef      DMA_START_SEC_VAR_NOINIT_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (DMA_STOP_SEC_VAR_NOINIT_UNSPECIFIED)
   #undef      DMA_STOP_SEC_VAR_NOINIT_UNSPECIFIED
   #pragma pop   
   
#elif defined (DMA_START_SEC_VAR_POWER_ON_INIT_1BIT)
   #undef      DMA_START_SEC_VAR_POWER_ON_INIT_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (DMA_STOP_SEC_VAR_POWER_ON_INIT_1BIT)
   #undef      DMA_STOP_SEC_VAR_POWER_ON_INIT_1BIT
   #pragma pop
   
#elif defined (DMA_START_SEC_VAR_POWER_ON_INIT_8BIT)
   #undef      DMA_START_SEC_VAR_POWER_ON_INIT_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (DMA_STOP_SEC_VAR_POWER_ON_INIT_8BIT)
   #undef      DMA_STOP_SEC_VAR_POWER_ON_INIT_8BIT
   #pragma pop
   
#elif defined (DMA_START_SEC_VAR_POWER_ON_INIT_16BIT)
   #undef      DMA_START_SEC_VAR_POWER_ON_INIT_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (DMA_STOP_SEC_VAR_POWER_ON_INIT_16BIT)
   #undef      DMA_STOP_SEC_VAR_POWER_ON_INIT_16BIT
   #pragma pop
   
#elif defined (DMA_START_SEC_VAR_POWER_ON_INIT_32BIT)
   #undef      DMA_START_SEC_VAR_POWER_ON_INIT_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (DMA_STOP_SEC_VAR_POWER_ON_INIT_32BIT)
   #undef      DMA_STOP_SEC_VAR_POWER_ON_INIT_32BIT
   #pragma pop  

 
#elif defined (DMA_START_SEC_VAR_POWER_ON_INIT_UNSPECIFIED)
   #undef      DMA_START_SEC_VAR_POWER_ON_INIT_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (DMA_STOP_SEC_VAR_POWER_ON_INIT_UNSPECIFIED)
   #undef      DMA_STOP_SEC_VAR_POWER_ON_INIT_UNSPECIFIED
   #pragma pop   
   
#elif defined (DMA_START_SEC_VAR_1BIT)
   #undef      DMA_START_SEC_VAR_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (DMA_STOP_SEC_VAR_1BIT)
   #undef      DMA_STOP_SEC_VAR_1BIT
   #pragma pop
   
#elif defined (DMA_START_SEC_VAR_8BIT)
   #undef      DMA_START_SEC_VAR_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (DMA_STOP_SEC_VAR_8BIT)
   #undef      DMA_STOP_SEC_VAR_8BIT
   #pragma pop
   
#elif defined (DMA_START_SEC_VAR_16BIT)
   #undef      DMA_START_SEC_VAR_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (DMA_STOP_SEC_VAR_16BIT)
   #undef      DMA_STOP_SEC_VAR_16BIT
   #pragma pop
   
#elif defined (DMA_START_SEC_VAR_32BIT)
   #undef      DMA_START_SEC_VAR_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (DMA_STOP_SEC_VAR_32BIT)
   #undef      DMA_STOP_SEC_VAR_32BIT
   #pragma pop   
   
#elif defined (DMA_START_SEC_VAR_UNSPECIFIED)
   #undef      DMA_START_SEC_VAR_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (DMA_STOP_SEC_VAR_UNSPECIFIED)
   #undef      DMA_STOP_SEC_VAR_UNSPECIFIED
   #pragma pop   
   
#elif defined (DMA_START_SEC_VAR_FAST_NOINIT_1BIT)
   #undef      DMA_START_SEC_VAR_FAST_NOINIT_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (DMA_STOP_SEC_VAR_FAST_NOINIT_1BIT)
   #undef      DMA_STOP_SEC_VAR_FAST_NOINIT_1BIT
   #pragma pop
   
#elif defined (DMA_START_SEC_VAR_FAST_NOINIT_8BIT)
   #undef      DMA_START_SEC_VAR_FAST_NOINIT_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (DMA_STOP_SEC_VAR_FAST_NOINIT_8BIT)
   #undef      DMA_STOP_SEC_VAR_FAST_NOINIT_8BIT
   #pragma pop
   
#elif defined (DMA_START_SEC_VAR_FAST_NOINIT_16BIT)
   #undef      DMA_START_SEC_VAR_FAST_NOINIT_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (DMA_STOP_SEC_VAR_FAST_NOINIT_16BIT)
   #undef      DMA_STOP_SEC_VAR_FAST_NOINIT_16BIT
   #pragma pop
   
#elif defined (DMA_START_SEC_VAR_FAST_NOINIT_32BIT)
   #undef      DMA_START_SEC_VAR_FAST_NOINIT_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (DMA_STOP_SEC_VAR_FAST_NOINIT_32BIT)
   #undef      DMA_STOP_SEC_VAR_FAST_NOINIT_32BIT
   #pragma pop   
    
#elif defined (DMA_START_SEC_VAR_FAST_NOINIT_UNSPECIFIED)
   #undef      DMA_START_SEC_VAR_FAST_NOINIT_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (DMA_STOP_SEC_VAR_FAST_NOINIT_UNSPECIFIED)
   #undef      DMA_STOP_SEC_VAR_FAST_NOINIT_UNSPECIFIED
   #pragma pop     
   
#elif defined (DMA_START_SEC_VAR_FAST_POWER_ON_INIT_1BIT)
   #undef      DMA_START_SEC_VAR_FAST_POWER_ON_INIT_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (DMA_STOP_SEC_VAR_FAST_POWER_ON_INIT_1BIT)
   #undef      DMA_STOP_SEC_VAR_FAST_POWER_ON_INIT_1BIT
   #pragma pop
   
#elif defined (DMA_START_SEC_VAR_FAST_POWER_ON_INIT_8BIT)
   #undef      DMA_START_SEC_VAR_FAST_POWER_ON_INIT_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (DMA_STOP_SEC_VAR_FAST_POWER_ON_INIT_8BIT)
   #undef      DMA_STOP_SEC_VAR_FAST_POWER_ON_INIT_8BIT
   #pragma pop
   
#elif defined (DMA_START_SEC_VAR_FAST_POWER_ON_INIT_16BIT)
   #undef      DMA_START_SEC_VAR_FAST_POWER_ON_INIT_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (DMA_STOP_SEC_VAR_FAST_POWER_ON_INIT_16BIT)
   #undef      DMA_STOP_SEC_VAR_FAST_POWER_ON_INIT_16BIT
   #pragma pop
   
#elif defined (DMA_START_SEC_VAR_FAST_POWER_ON_INIT_32BIT)
   #undef      DMA_START_SEC_VAR_FAST_POWER_ON_INIT_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (DMA_STOP_SEC_VAR_FAST_POWER_ON_INIT_32BIT)
   #undef      DMA_STOP_SEC_VAR_FAST_POWER_ON_INIT_32BIT
   #pragma pop   
    
#elif defined (DMA_START_SEC_VAR_FAST_POWER_ON_INIT_UNSPECIFIED)
   #undef      DMA_START_SEC_VAR_FAST_POWER_ON_INIT_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (DMA_STOP_SEC_VAR_FAST_POWER_ON_INIT_UNSPECIFIED)
   #undef      DMA_STOP_SEC_VAR_FAST_POWER_ON_INIT_UNSPECIFIED
   #pragma pop     
   
#elif defined (DMA_START_SEC_VAR_FAST_1BIT)
   #undef      DMA_START_SEC_VAR_FAST_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (DMA_STOP_SEC_VAR_FAST_1BIT)
   #undef      DMA_STOP_SEC_VAR_FAST_1BIT
   #pragma pop
   
#elif defined (DMA_START_SEC_VAR_FAST_8BIT)
   #undef      DMA_START_SEC_VAR_FAST_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (DMA_STOP_SEC_VAR_FAST_8BIT)
   #undef      DMA_STOP_SEC_VAR_FAST_8BIT
   #pragma pop
   
#elif defined (DMA_START_SEC_VAR_FAST_16BIT)
   #undef      DMA_START_SEC_VAR_FAST_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (DMA_STOP_SEC_VAR_FAST_16BIT)
   #undef      DMA_STOP_SEC_VAR_FAST_16BIT
   #pragma pop
   
#elif defined (DMA_START_SEC_VAR_FAST_32BIT)
   #undef      DMA_START_SEC_VAR_FAST_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (DMA_STOP_SEC_VAR_FAST_32BIT)
   #undef      DMA_STOP_SEC_VAR_FAST_32BIT
   #pragma pop   
   
#elif defined (DMA_START_SEC_VAR_FAST_UNSPECIFIED)
   #undef      DMA_START_SEC_VAR_FAST_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (DMA_STOP_SEC_VAR_FAST_UNSPECIFIED)
   #undef      DMA_STOP_SEC_VAR_FAST_UNSPECIFIED
   #pragma pop   
   
#elif defined (DMA_START_SEC_CONST_1BIT)
   #undef      DMA_START_SEC_CONST_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (DMA_STOP_SEC_CONST_1BIT)
   #undef      DMA_STOP_SEC_CONST_1BIT
   #pragma pop
   
#elif defined (DMA_START_SEC_CONST_8BIT)
   #undef      DMA_START_SEC_CONST_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (DMA_STOP_SEC_CONST_8BIT)
   #undef      DMA_STOP_SEC_CONST_8BIT
   #pragma pop
   
#elif defined (DMA_START_SEC_CONST_16BIT)
   #undef      DMA_START_SEC_CONST_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (DMA_STOP_SEC_CONST_16BIT)
   #undef      DMA_STOP_SEC_CONST_16BIT
   #pragma pop
   
#elif defined (DMA_START_SEC_CONST_32BIT)
   #undef      DMA_START_SEC_CONST_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (DMA_STOP_SEC_CONST_32BIT)
   #undef      DMA_STOP_SEC_CONST_32BIT
   #pragma pop   
      
#elif defined (DMA_START_SEC_CONST_UNSPECIFIED)
   #undef      DMA_START_SEC_CONST_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (DMA_STOP_SEC_CONST_UNSPECIFIED)
   #undef      DMA_STOP_SEC_CONST_UNSPECIFIED
   #pragma pop   

   
#elif defined (DMA_START_SEC_CONST_FAST_1BIT)
   #undef      DMA_START_SEC_CONST_FAST_1BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (DMA_STOP_SEC_CONST_FAST_1BIT)
   #undef      DMA_STOP_SEC_CONST_FAST_1BIT
   #pragma pop
   
#elif defined (DMA_START_SEC_CONST_FAST_8BIT)
   #undef      DMA_START_SEC_CONST_FAST_8BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (DMA_STOP_SEC_CONST_FAST_8BIT)
   #undef      DMA_STOP_SEC_CONST_FAST_8BIT
   #pragma pop
   
#elif defined (DMA_START_SEC_CONST_FAST_16BIT)
   #undef      DMA_START_SEC_CONST_FAST_16BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (DMA_STOP_SEC_CONST_FAST_16BIT)
   #undef      DMA_STOP_SEC_CONST_FAST_16BIT
   #pragma pop
   
#elif defined (DMA_START_SEC_CONST_FAST_32BIT)
   #undef      DMA_START_SEC_CONST_FAST_32BIT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (DMA_STOP_SEC_CONST_FAST_32BIT)
   #undef      DMA_STOP_SEC_CONST_FAST_32BIT
   #pragma pop   
      
#elif defined (DMA_START_SEC_CONST_FAST_UNSPECIFIED)
   #undef      DMA_START_SEC_CONST_FAST_UNSPECIFIED
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel 
#elif defined (DMA_STOP_SEC_CONST_FAST_UNSPECIFIED)
   #undef      DMA_STOP_SEC_CONST_FAST_UNSPECIFIED
   #pragma pop   
   
#elif defined (DMA_START_SEC_CONST_PBCFG)
   #undef      DMA_START_SEC_CONST_PBCFG
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel
#elif defined (DMA_STOP_SEC_CONST_PBCFG)
   #undef      DMA_STOP_SEC_CONST_PBCFG
   #pragma pop
 
#elif defined (DMA_START_SEC_CONST_PBCFG_ROOT)
   #undef      DMA_START_SEC_CONST_PBCFG_ROOT
   #pragma push
   #pragma section all_types ".CFG_DATA" ".CFG_DATA" data_mode = far_abs code_mode = pc_rel
#elif defined (DMA_STOP_SEC_CONST_PBCFG_ROOT)
   #undef      DMA_STOP_SEC_CONST_PBCFG_ROOT
   #pragma pop

#elif defined (DMA_START_SEC_CODE)
   #undef      DMA_START_SEC_CODE
   #pragma push
   __declspec (section ".MCAL_CODE")
   //__declspec (section ".DMA_CODE_ROM")
#elif defined (DMA_STOP_SEC_CODE)
   #undef      DMA_STOP_SEC_CODE
   #pragma pop

#elif defined (DMA_START_SEC_APPL_CODE)
   #undef      DMA_START_SEC_APPL_CODE
   #pragma push
   __declspec (section ".DMA_APPL_CODE_ROM")
#elif defined (DMA_STOP_SEC_APPL_CODE)
   #undef      DMA_STOP_SEC_APPL_CODE
   #pragma pop

/******************************************************************************/
/************* End of Add  for Mcal Intergration,by  tommy ********************/
/******************************************************************************/




/*=======[CANTP]==============================================================*/
#elif defined (CANTP_START_SEC_CODE)
   #undef      CANTP_START_SEC_CODE
   #define START_SEC_CODE
#elif defined (CANTP_STOP_SEC_CODE)
   #undef      CANTP_STOP_SEC_CODE
   #define STOP_SEC_CODE

#elif defined (CANTP_START_SEC_CODE_FAST)
   #undef      CANTP_START_SEC_CODE_FAST
   #define START_SEC_CODE_FAST
#elif defined (CANTP_STOP_SEC_CODE_FAST)
   #undef      CANTP_STOP_SEC_CODE_FAST
   #define STOP_SEC_CODE_FAST

#elif defined (CANTP_START_SEC_VAR_NOINIT_BOOLEAN)
   #undef      CANTP_START_SEC_VAR_NOINIT_BOOLEAN
   #define START_SEC_VAR_NOINIT_BOOLEAN
#elif defined (CANTP_STOP_SEC_VAR_NOINIT_BOOLEAN)
   #undef      CANTP_STOP_SEC_VAR_NOINIT_BOOLEAN
   #define STOP_SEC_VAR_NOINIT_BOOLEAN

#elif defined (CANTP_START_SEC_VAR_NOINIT_8BIT)
   #undef      CANTP_START_SEC_VAR_NOINIT_8BIT
   #define START_SEC_VAR_NOINIT_8BIT
#elif defined (CANTP_STOP_SEC_VAR_NOINIT_8BIT)
   #undef      CANTP_STOP_SEC_VAR_NOINIT_8BIT
   #define STOP_SEC_VAR_NOINIT_8BIT

#elif defined (CANTP_START_SEC_VAR_NOINIT_16BIT)
   #undef      CANTP_START_SEC_VAR_NOINIT_16BIT
   #define START_SEC_VAR_NOINIT_16BIT
#elif defined (CANTP_STOP_SEC_VAR_NOINIT_16BIT)
   #undef      CANTP_STOP_SEC_VAR_NOINIT_16BIT
   #define STOP_SEC_VAR_NOINIT_16BIT

#elif defined (CANTP_START_SEC_VAR_NOINIT_32BIT)
   #undef      CANTP_START_SEC_VAR_NOINIT_32BIT
   #define START_SEC_VAR_NOINIT_32BIT
#elif defined (CANTP_STOP_SEC_VAR_NOINIT_32BIT)
   #undef      CANTP_STOP_SEC_VAR_NOINIT_32BIT
   #define STOP_SEC_VAR_NOINIT_32BIT

#elif defined (CANTP_START_SEC_VAR_NOINIT_UNSPECIFIED)
   #undef      CANTP_START_SEC_VAR_NOINIT_UNSPECIFIED
   #define START_SEC_VAR_NOINIT_UNSPECIFIED
#elif defined (CANTP_STOP_SEC_VAR_NOINIT_UNSPECIFIED)
   #undef      CANTP_STOP_SEC_VAR_NOINIT_UNSPECIFIED
   #define STOP_SEC_VAR_NOINIT_UNSPECIFIED

#elif defined (CANTP_START_SEC_VAR_POWER_ON_INIT_BOOLEAN)
   #undef      CANTP_START_SEC_VAR_POWER_ON_INIT_BOOLEAN
   #define START_SEC_VAR_POWER_ON_INIT_BOOLEAN
#elif defined (CANTP_STOP_SEC_VAR_POWER_ON_INIT_BOOLEAN)
   #undef      CANTP_STOP_SEC_VAR_POWER_ON_INIT_BOOLEAN
   #define STOP_SEC_VAR_POWER_ON_INIT_BOOLEAN

#elif defined (CANTP_START_SEC_VAR_POWER_ON_INIT_8BIT)
   #undef      CANTP_START_SEC_VAR_POWER_ON_INIT_8BIT
   #define START_SEC_VAR_POWER_ON_INIT_8BIT
#elif defined (CANTP_STOP_SEC_VAR_POWER_ON_INIT_8BIT)
   #undef      CANTP_STOP_SEC_VAR_POWER_ON_INIT_8BIT
   #define STOP_SEC_VAR_POWER_ON_INIT_8BIT

#elif defined (CANTP_START_SEC_VAR_POWER_ON_INIT_16BIT)
   #undef      CANTP_START_SEC_VAR_POWER_ON_INIT_16BIT
   #define START_SEC_VAR_POWER_ON_INIT_16BIT
#elif defined (CANTP_STOP_SEC_VAR_POWER_ON_INIT_16BIT)
   #undef      CANTP_STOP_SEC_VAR_POWER_ON_INIT_16BIT
   #define STOP_SEC_VAR_POWER_ON_INIT_16BIT

#elif defined (CANTP_START_SEC_VAR_POWER_ON_INIT_32BIT)
   #undef      CANTP_START_SEC_VAR_POWER_ON_INIT_32BIT
   #define START_SEC_VAR_POWER_ON_INIT_32BIT
#elif defined (CANTP_STOP_SEC_VAR_POWER_ON_INIT_32BIT)
   #undef      CANTP_STOP_SEC_VAR_POWER_ON_INIT_32BIT
   #define STOP_SEC_VAR_POWER_ON_INIT_32BIT

#elif defined (CANTP_START_SEC_VAR_POWER_ON_INIT_UNSPECIFIED)
   #undef      CANTP_START_SEC_VAR_POWER_ON_INIT_UNSPECIFIED
   #define START_SEC_VAR_POWER_ON_INIT_UNSPECIFIED
#elif defined (CANTP_STOP_SEC_VAR_POWER_ON_INIT_UNSPECIFIED)
   #undef      CANTP_STOP_SEC_VAR_POWER_ON_INIT_UNSPECIFIED
   #define STOP_SEC_VAR_POWER_ON_INIT_UNSPECIFIED

#elif defined (CANTP_START_SEC_VAR_BOOLEAN)
   #undef      CANTP_START_SEC_VAR_BOOLEAN
   #define START_SEC_VAR_BOOLEAN
#elif defined (CANTP_STOP_SEC_VAR_BOOLEAN)
   #undef      CANTP_STOP_SEC_VAR_BOOLEAN
   #define STOP_SEC_VAR_BOOLEAN

#elif defined (CANTP_START_SEC_VAR_8BIT)
   #undef      CANTP_START_SEC_VAR_8BIT
   #define START_SEC_VAR_8BIT
#elif defined (CANTP_STOP_SEC_VAR_8BIT)
   #undef      CANTP_STOP_SEC_VAR_8BIT
   #define STOP_SEC_VAR_8BIT

#elif defined (CANTP_START_SEC_VAR_16BIT)
   #undef      CANTP_START_SEC_VAR_16BIT
   #define START_SEC_VAR_16BIT
#elif defined (CANTP_STOP_SEC_VAR_16BIT)
   #undef      CANTP_STOP_SEC_VAR_16BIT
   #define STOP_SEC_VAR_16BIT

#elif defined (CANTP_START_SEC_VAR_32BIT)
   #undef      CANTP_START_SEC_VAR_32BIT
   #define START_SEC_VAR_32BIT
#elif defined (CANTP_STOP_SEC_VAR_32BIT)
   #undef      CANTP_STOP_SEC_VAR_32BIT
   #define STOP_SEC_VAR_32BIT

#elif defined (CANTP_START_SEC_VAR_UNSPECIFIED)
   #undef      CANTP_START_SEC_VAR_UNSPECIFIED
   #define START_SEC_VAR_UNSPECIFIED
#elif defined (CANTP_STOP_SEC_VAR_UNSPECIFIED)
   #undef      CANTP_STOP_SEC_VAR_UNSPECIFIED
   #define STOP_SEC_VAR_UNSPECIFIED


#elif defined (CANTP_START_SEC_VAR_FAST_NOINIT_BOOLEAN)
   #undef      CANTP_START_SEC_VAR_FAST_NOINIT_BOOLEAN
   #define START_SEC_VAR_FAST_NOINIT_BOOLEAN
#elif defined (CANTP_STOP_SEC_VAR_FAST_NOINIT_BOOLEAN)
   #undef      CANTP_STOP_SEC_VAR_FAST_NOINIT_BOOLEAN
   #define STOP_SEC_VAR_FAST_NOINIT_BOOLEAN

#elif defined (CANTP_START_SEC_VAR_FAST_NOINIT_8BIT)
   #undef      CANTP_START_SEC_VAR_FAST_NOINIT_8BIT
   #define START_SEC_VAR_FAST_NOINIT_8BIT
#elif defined (CANTP_STOP_SEC_VAR_FAST_NOINIT_8BIT)
   #undef      CANTP_STOP_SEC_VAR_FAST_NOINIT_8BIT
   #define STOP_SEC_VAR_FAST_NOINIT_8BIT

#elif defined (CANTP_START_SEC_VAR_FAST_NOINIT_16BIT)
   #undef      CANTP_START_SEC_VAR_FAST_NOINIT_16BIT
   #define START_SEC_VAR_FAST_NOINIT_16BIT
#elif defined (CANTP_STOP_SEC_VAR_FAST_NOINIT_16BIT)
   #undef      CANTP_STOP_SEC_VAR_FAST_NOINIT_16BIT
   #define STOP_SEC_VAR_FAST_NOINIT_16BIT

#elif defined (CANTP_START_SEC_VAR_FAST_NOINIT_32BIT)
   #undef      CANTP_START_SEC_VAR_FAST_NOINIT_32BIT
   #define START_SEC_VAR_FAST_NOINIT_32BIT
#elif defined (CANTP_STOP_SEC_VAR_FAST_NOINIT_32BIT)
   #undef      CANTP_STOP_SEC_VAR_FAST_NOINIT_32BIT
   #define STOP_SEC_VAR_FAST_NOINIT_32BIT

#elif defined (CANTP_START_SEC_VAR_FAST_NOINIT_UNSPECIFIED)
   #undef      CANTP_START_SEC_VAR_FAST_NOINIT_UNSPECIFIED
   #define START_SEC_VAR_FAST_NOINIT_UNSPECIFIED
#elif defined (CANTP_STOP_SEC_VAR_FAST_NOINIT_UNSPECIFIED)
   #undef      CANTP_STOP_SEC_VAR_FAST_NOINIT_UNSPECIFIED
   #define STOP_SEC_VAR_FAST_NOINIT_UNSPECIFIED

#elif defined (CANTP_START_SEC_VAR_FAST_POWER_ON_INIT_BOOLEAN)
   #undef      CANTP_START_SEC_VAR_FAST_POWER_ON_INIT_BOOLEAN
   #define START_SEC_VAR_FAST_POWER_ON_INIT_BOOLEAN
#elif defined (CANTP_STOP_SEC_VAR_FAST_POWER_ON_INIT_BOOLEAN)
   #undef      CANTP_STOP_SEC_VAR_FAST_POWER_ON_INIT_BOOLEAN
   #define STOP_SEC_VAR_FAST_POWER_ON_INIT_BOOLEAN

#elif defined (CANTP_START_SEC_VAR_FAST_POWER_ON_INIT_8BIT)
   #undef      CANTP_START_SEC_VAR_FAST_POWER_ON_INIT_8BIT
   #define START_SEC_VAR_FAST_POWER_ON_INIT_8BIT
#elif defined (CANTP_STOP_SEC_VAR_FAST_POWER_ON_INIT_8BIT)
   #undef      CANTP_STOP_SEC_VAR_FAST_POWER_ON_INIT_8BIT
   #define STOP_SEC_VAR_FAST_POWER_ON_INIT_8BIT

#elif defined (CANTP_START_SEC_VAR_FAST_POWER_ON_INIT_16BIT)
   #undef      CANTP_START_SEC_VAR_FAST_POWER_ON_INIT_16BIT
   #define START_SEC_VAR_FAST_POWER_ON_INIT_16BIT
#elif defined (CANTP_STOP_SEC_VAR_FAST_POWER_ON_INIT_16BIT)
   #undef      CANTP_STOP_SEC_VAR_FAST_POWER_ON_INIT_16BIT
   #define STOP_SEC_VAR_FAST_POWER_ON_INIT_16BIT

#elif defined (CANTP_START_SEC_VAR_FAST_POWER_ON_INIT_32BIT)
   #undef      CANTP_START_SEC_VAR_FAST_POWER_ON_INIT_32BIT
   #define START_SEC_VAR_FAST_POWER_ON_INIT_32BIT
#elif defined (CANTP_STOP_SEC_VAR_FAST_POWER_ON_INIT_32BIT)
   #undef      CANTP_STOP_SEC_VAR_FAST_POWER_ON_INIT_32BIT
   #define STOP_SEC_VAR_FAST_POWER_ON_INIT_32BIT

#elif defined (CANTP_START_SEC_VAR_FAST_POWER_ON_INIT_UNSPECIFIED)
   #undef      CANTP_START_SEC_VAR_FAST_POWER_ON_INIT_UNSPECIFIED
   #define START_SEC_VAR_FAST_POWER_ON_INIT_UNSPECIFIED
#elif defined (CANTP_STOP_SEC_VAR_FAST_POWER_ON_INIT_UNSPECIFIED)
   #undef      CANTP_STOP_SEC_VAR_FAST_POWER_ON_INIT_UNSPECIFIED
   #define STOP_SEC_VAR_FAST_POWER_ON_INIT_UNSPECIFIED

#elif defined (CANTP_START_SEC_VAR_FAST_BOOLEAN)
   #undef      CANTP_START_SEC_VAR_FAST_BOOLEAN
   #define START_SEC_VAR_FAST_BOOLEAN
#elif defined (CANTP_STOP_SEC_VAR_FAST_BOOLEAN)
   #undef      CANTP_STOP_SEC_VAR_FAST_BOOLEAN
   #define STOP_SEC_VAR_FAST_BOOLEAN

#elif defined (CANTP_START_SEC_VAR_FAST_8BIT)
   #undef      CANTP_START_SEC_VAR_FAST_8BIT
   #define START_SEC_VAR_FAST_8BIT
#elif defined (CANTP_STOP_SEC_VAR_FAST_8BIT)
   #undef      CANTP_STOP_SEC_VAR_FAST_8BIT
   #define STOP_SEC_VAR_FAST_8BIT

#elif defined (CANTP_START_SEC_VAR_FAST_16BIT)
   #undef      CANTP_START_SEC_VAR_FAST_16BIT
   #define START_SEC_VAR_FAST_16BIT
#elif defined (CANTP_STOP_SEC_VAR_FAST_16BIT)
   #undef      CANTP_STOP_SEC_VAR_FAST_16BIT
   #define STOP_SEC_VAR_FAST_16BIT

#elif defined (CANTP_START_SEC_VAR_FAST_32BIT)
   #undef      CANTP_START_SEC_VAR_FAST_32BIT
   #define START_SEC_VAR_FAST_32BIT
#elif defined (CANTP_STOP_SEC_VAR_FAST_32BIT)
   #undef      CANTP_STOP_SEC_VAR_FAST_32BIT
   #define STOP_SEC_VAR_FAST_32BIT

#elif defined (CANTP_START_SEC_VAR_FAST_UNSPECIFIED)
   #undef      CANTP_START_SEC_VAR_FAST_UNSPECIFIED
   #define START_SEC_VAR_FAST_UNSPECIFIED
#elif defined (CANTP_STOP_SEC_VAR_FAST_UNSPECIFIED)
   #undef      CANTP_STOP_SEC_VAR_FAST_UNSPECIFIED
   #define STOP_SEC_VAR_FAST_UNSPECIFIED

#elif defined (CANTP_START_SEC_CONST_BOOLEAN)
   #undef      CANTP_START_SEC_CONST_BOOLEAN
   #define START_SEC_CONST_BOOLEAN
#elif defined (CANTP_STOP_SEC_CONST_BOOLEAN)
   #undef      CANTP_STOP_SEC_CONST_BOOLEAN
   #define STOP_SEC_CONST_BOOLEAN

#elif defined (CANTP_START_SEC_CONST_8BIT)
   #undef      CANTP_START_SEC_CONST_8BIT
   #define START_SEC_CONST_8BIT
#elif defined (CANTP_STOP_SEC_CONST_8BIT)
   #undef      CANTP_STOP_SEC_CONST_8BIT
   #define STOP_SEC_CONST_8BIT

#elif defined (CANTP_START_SEC_CONST_16BIT)
   #undef      CANTP_START_SEC_CONST_16BIT
   #define START_SEC_CONST_16BIT
#elif defined (CANTP_STOP_SEC_CONST_16BIT)
   #undef      CANTP_STOP_SEC_CONST_16BIT
   #define STOP_SEC_CONST_16BIT

#elif defined (CANTP_START_SEC_CONST_32BIT)
   #undef      CANTP_START_SEC_CONST_32BIT
   #define START_SEC_CONST_32BIT
#elif defined (CANTP_STOP_SEC_CONST_32BIT)
   #undef      CANTP_STOP_SEC_CONST_32BIT
   #define STOP_SEC_CONST_32BIT

#elif defined (CANTP_START_SEC_CONST_UNSPECIFIED)
   #undef      CANTP_START_SEC_CONST_UNSPECIFIED
   #define START_SEC_CONST_UNSPECIFIED
#elif defined (CANTP_STOP_SEC_CONST_UNSPECIFIED)
   #undef      CANTP_STOP_SEC_CONST_UNSPECIFIED
   #define STOP_SEC_CONST_UNSPECIFIED

#elif defined (CANTP_START_SEC_CONST_FAST_BOOLEAN)
   #undef      CANTP_START_SEC_CONST_FAST_BOOLEAN
   #define START_SEC_CONST_FAST_BOOLEAN
#elif defined (CANTP_STOP_SEC_CONST_FAST_BOOLEAN)
   #undef      CANTP_STOP_SEC_CONST_FAST_BOOLEAN
   #define STOP_SEC_CONST_FAST_BOOLEAN

#elif defined (CANTP_START_SEC_CONST_FAST_8BIT)
   #undef      CANTP_START_SEC_CONST_FAST_8BIT
   #define START_SEC_CONST_FAST_8BIT
#elif defined (CANTP_STOP_SEC_CONST_FAST_8BIT)
   #undef      CANTP_STOP_SEC_CONST_FAST_8BIT
   #define STOP_SEC_CONST_FAST_8BIT

#elif defined (CANTP_START_SEC_CONST_FAST_16BIT)
   #undef      CANTP_START_SEC_CONST_FAST_16BIT
   #define START_SEC_CONST_FAST_16BIT
#elif defined (CANTP_STOP_SEC_CONST_FAST_16BIT)
   #undef      CANTP_STOP_SEC_CONST_FAST_16BIT
   #define STOP_SEC_CONST_FAST_16BIT

#elif defined (CANTP_START_SEC_CONST_FAST_32BIT)
   #undef      CANTP_START_SEC_CONST_FAST_32BIT
   #define START_SEC_CONST_FAST_32BIT
#elif defined (CANTP_STOP_SEC_CONST_FAST_32BIT)
   #undef      CANTP_STOP_SEC_CONST_FAST_32BIT
   #define STOP_SEC_CONST_FAST_32BIT

#elif defined (CANTP_START_SEC_CONST_FAST_UNSPECIFIED)
   #undef      CANTP_START_SEC_CONST_FAST_UNSPECIFIED
   #define START_SEC_CONST_FAST_UNSPECIFIED
#elif defined (CANTP_STOP_SEC_CONST_FAST_UNSPECIFIED)
   #undef      CANTP_STOP_SEC_CONST_FAST_UNSPECIFIED
   #define STOP_SEC_CONST_FAST_UNSPECIFIED

#elif defined (CANTP_START_CONST_PBCFG)
   #undef      CANTP_START_CONST_PBCFG
   #define START_CONST_PBCFG
#elif defined (CANTP_STOP_CONST_PBCFG)
   #undef      CANTP_STOP_CONST_PBCFG
   #define STOP_CONST_PBCFG

#elif defined (CANTP_START_CONST_PBCFG_ROOT)
   #undef      CANTP_START_CONST_PBCFG_ROOT
   #define START_CONST_PBCFG_ROOT
#elif defined (CANTP_STOP_CONST_PBCFG_ROOT)
   #undef      CANTP_STOP_CONST_PBCFG_ROOT
   #define STOP_CONST_PBCFG_ROOT
/*=========================END OF CANTP  =======================================*/
/*=======[D C M]==============================================================*/
#elif defined (DCM_START_SEC_CODE)
   #undef      DCM_START_SEC_CODE
   #define START_SEC_CODE
#elif defined (DCM_STOP_SEC_CODE)
   #undef      DCM_STOP_SEC_CODE
   #define STOP_SEC_CODE

#elif defined (DCM_START_SEC_CODE_FAST)
   #undef      DCM_START_SEC_CODE_FAST
   #define START_SEC_CODE_FAST
#elif defined (DCM_STOP_SEC_CODE_FAST)
   #undef      DCM_STOP_SEC_CODE_FAST
   #define STOP_SEC_CODE_FAST

#elif defined (DCM_START_SEC_VAR_NOINIT_BOOLEAN)
   #undef      DCM_START_SEC_VAR_NOINIT_BOOLEAN
   #define START_SEC_VAR_NOINIT_BOOLEAN
#elif defined (DCM_STOP_SEC_VAR_NOINIT_BOOLEAN)
   #undef      DCM_STOP_SEC_VAR_NOINIT_BOOLEAN
   #define STOP_SEC_VAR_NOINIT_BOOLEAN

#elif defined (DCM_START_SEC_VAR_NOINIT_8BIT)
   #undef      DCM_START_SEC_VAR_NOINIT_8BIT
   #define START_SEC_VAR_NOINIT_8BIT
#elif defined (DCM_STOP_SEC_VAR_NOINIT_8BIT)
   #undef      DCM_STOP_SEC_VAR_NOINIT_8BIT
   #define STOP_SEC_VAR_NOINIT_8BIT

#elif defined (DCM_START_SEC_VAR_NOINIT_16BIT)
   #undef      DCM_START_SEC_VAR_NOINIT_16BIT
   #define START_SEC_VAR_NOINIT_16BIT
#elif defined (DCM_STOP_SEC_VAR_NOINIT_16BIT)
   #undef      DCM_STOP_SEC_VAR_NOINIT_16BIT
   #define STOP_SEC_VAR_NOINIT_16BIT

#elif defined (DCM_START_SEC_VAR_NOINIT_32BIT)
   #undef      DCM_START_SEC_VAR_NOINIT_32BIT
   #define START_SEC_VAR_NOINIT_32BIT
#elif defined (DCM_STOP_SEC_VAR_NOINIT_32BIT)
   #undef      DCM_STOP_SEC_VAR_NOINIT_32BIT
   #define STOP_SEC_VAR_NOINIT_32BIT

#elif defined (DCM_START_SEC_VAR_NOINIT_UNSPECIFIED)
   #undef      DCM_START_SEC_VAR_NOINIT_UNSPECIFIED
   #define START_SEC_VAR_NOINIT_UNSPECIFIED
#elif defined (DCM_STOP_SEC_VAR_NOINIT_UNSPECIFIED)
   #undef      DCM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
   #define STOP_SEC_VAR_NOINIT_UNSPECIFIED

#elif defined (DCM_START_SEC_VAR_POWER_ON_INIT_BOOLEAN)
   #undef      DCM_START_SEC_VAR_POWER_ON_INIT_BOOLEAN
   #define START_SEC_VAR_POWER_ON_INIT_BOOLEAN
#elif defined (DCM_STOP_SEC_VAR_POWER_ON_INIT_BOOLEAN)
   #undef      DCM_STOP_SEC_VAR_POWER_ON_INIT_BOOLEAN
   #define STOP_SEC_VAR_POWER_ON_INIT_BOOLEAN

#elif defined (DCM_START_SEC_VAR_POWER_ON_INIT_8BIT)
   #undef      DCM_START_SEC_VAR_POWER_ON_INIT_8BIT
   #define START_SEC_VAR_POWER_ON_INIT_8BIT
#elif defined (DCM_STOP_SEC_VAR_POWER_ON_INIT_8BIT)
   #undef      DCM_STOP_SEC_VAR_POWER_ON_INIT_8BIT
   #define STOP_SEC_VAR_POWER_ON_INIT_8BIT

#elif defined (DCM_START_SEC_VAR_POWER_ON_INIT_16BIT)
   #undef      DCM_START_SEC_VAR_POWER_ON_INIT_16BIT
   #define START_SEC_VAR_POWER_ON_INIT_16BIT
#elif defined (DCM_STOP_SEC_VAR_POWER_ON_INIT_16BIT)
   #undef      DCM_STOP_SEC_VAR_POWER_ON_INIT_16BIT
   #define STOP_SEC_VAR_POWER_ON_INIT_16BIT

#elif defined (DCM_START_SEC_VAR_POWER_ON_INIT_32BIT)
   #undef      DCM_START_SEC_VAR_POWER_ON_INIT_32BIT
   #define START_SEC_VAR_POWER_ON_INIT_32BIT
#elif defined (DCM_STOP_SEC_VAR_POWER_ON_INIT_32BIT)
   #undef      DCM_STOP_SEC_VAR_POWER_ON_INIT_32BIT
   #define STOP_SEC_VAR_POWER_ON_INIT_32BIT

#elif defined (DCM_START_SEC_VAR_POWER_ON_INIT_UNSPECIFIED)
   #undef      DCM_START_SEC_VAR_POWER_ON_INIT_UNSPECIFIED
   #define START_SEC_VAR_POWER_ON_INIT_UNSPECIFIED
#elif defined (DCM_STOP_SEC_VAR_POWER_ON_INIT_UNSPECIFIED)
   #undef      DCM_STOP_SEC_VAR_POWER_ON_INIT_UNSPECIFIED
   #define STOP_SEC_VAR_POWER_ON_INIT_UNSPECIFIED

#elif defined (DCM_START_SEC_VAR_BOOLEAN)
   #undef      DCM_START_SEC_VAR_BOOLEAN
   #define START_SEC_VAR_BOOLEAN
#elif defined (DCM_STOP_SEC_VAR_BOOLEAN)
   #undef      DCM_STOP_SEC_VAR_BOOLEAN
   #define STOP_SEC_VAR_BOOLEAN

#elif defined (DCM_START_SEC_VAR_8BIT)
   #undef      DCM_START_SEC_VAR_8BIT
   #define START_SEC_VAR_8BIT
#elif defined (DCM_STOP_SEC_VAR_8BIT)
   #undef      DCM_STOP_SEC_VAR_8BIT
   #define STOP_SEC_VAR_8BIT

#elif defined (DCM_START_SEC_VAR_16BIT)
   #undef      DCM_START_SEC_VAR_16BIT
   #define START_SEC_VAR_16BIT
#elif defined (DCM_STOP_SEC_VAR_16BIT)
   #undef      DCM_STOP_SEC_VAR_16BIT
   #define STOP_SEC_VAR_16BIT

#elif defined (DCM_START_SEC_VAR_32BIT)
   #undef      DCM_START_SEC_VAR_32BIT
   #define START_SEC_VAR_32BIT
#elif defined (DCM_STOP_SEC_VAR_32BIT)
   #undef      DCM_STOP_SEC_VAR_32BIT
   #define STOP_SEC_VAR_32BIT

#elif defined (DCM_START_SEC_VAR_UNSPECIFIED)
   #undef      DCM_START_SEC_VAR_UNSPECIFIED
   #define START_SEC_VAR_UNSPECIFIED
#elif defined (DCM_STOP_SEC_VAR_UNSPECIFIED)
   #undef      DCM_STOP_SEC_VAR_UNSPECIFIED
   #define STOP_SEC_VAR_UNSPECIFIED

#elif defined (DCM_START_SEC_VAR_FAST_NOINIT_BOOLEAN)
   #undef      DCM_START_SEC_VAR_FAST_NOINIT_BOOLEAN
   #define START_SEC_VAR_FAST_NOINIT_BOOLEAN
#elif defined (DCM_STOP_SEC_VAR_FAST_NOINIT_BOOLEAN)
   #undef      DCM_STOP_SEC_VAR_FAST_NOINIT_BOOLEAN
   #define STOP_SEC_VAR_FAST_NOINIT_BOOLEAN

#elif defined (DCM_START_SEC_VAR_FAST_NOINIT_8BIT)
   #undef      DCM_START_SEC_VAR_FAST_NOINIT_8BIT
   #define START_SEC_VAR_FAST_NOINIT_8BIT
#elif defined (DCM_STOP_SEC_VAR_FAST_NOINIT_8BIT)
   #undef      DCM_STOP_SEC_VAR_FAST_NOINIT_8BIT
   #define STOP_SEC_VAR_FAST_NOINIT_8BIT

#elif defined (DCM_START_SEC_VAR_FAST_NOINIT_16BIT)
   #undef      DCM_START_SEC_VAR_FAST_NOINIT_16BIT
   #define START_SEC_VAR_FAST_NOINIT_16BIT
#elif defined (DCM_STOP_SEC_VAR_FAST_NOINIT_16BIT)
   #undef      DCM_STOP_SEC_VAR_FAST_NOINIT_16BIT
   #define STOP_SEC_VAR_FAST_NOINIT_16BIT

#elif defined (DCM_START_SEC_VAR_FAST_NOINIT_32BIT)
   #undef      DCM_START_SEC_VAR_FAST_NOINIT_32BIT
   #define START_SEC_VAR_FAST_NOINIT_32BIT
#elif defined (DCM_STOP_SEC_VAR_FAST_NOINIT_32BIT)
   #undef      DCM_STOP_SEC_VAR_FAST_NOINIT_32BIT
   #define STOP_SEC_VAR_FAST_NOINIT_32BIT

#elif defined (DCM_START_SEC_VAR_FAST_NOINIT_UNSPECIFIED)
   #undef      DCM_START_SEC_VAR_FAST_NOINIT_UNSPECIFIED
   #define START_SEC_VAR_FAST_NOINIT_UNSPECIFIED
#elif defined (DCM_STOP_SEC_VAR_FAST_NOINIT_UNSPECIFIED)
   #undef      DCM_STOP_SEC_VAR_FAST_NOINIT_UNSPECIFIED
   #define STOP_SEC_VAR_FAST_NOINIT_UNSPECIFIED

#elif defined (DCM_START_SEC_VAR_FAST_POWER_ON_INIT_BOOLEAN)
   #undef      DCM_START_SEC_VAR_FAST_POWER_ON_INIT_BOOLEAN
   #define START_SEC_VAR_FAST_POWER_ON_INIT_BOOLEAN
#elif defined (DCM_STOP_SEC_VAR_FAST_POWER_ON_INIT_BOOLEAN)
   #undef      DCM_STOP_SEC_VAR_FAST_POWER_ON_INIT_BOOLEAN
   #define STOP_SEC_VAR_FAST_POWER_ON_INIT_BOOLEAN

#elif defined (DCM_START_SEC_VAR_FAST_POWER_ON_INIT_8BIT)
   #undef      DCM_START_SEC_VAR_FAST_POWER_ON_INIT_8BIT
   #define START_SEC_VAR_FAST_POWER_ON_INIT_8BIT
#elif defined (DCM_STOP_SEC_VAR_FAST_POWER_ON_INIT_8BIT)
   #undef      DCM_STOP_SEC_VAR_FAST_POWER_ON_INIT_8BIT
   #define STOP_SEC_VAR_FAST_POWER_ON_INIT_8BIT

#elif defined (DCM_START_SEC_VAR_FAST_POWER_ON_INIT_16BIT)
   #undef      DCM_START_SEC_VAR_FAST_POWER_ON_INIT_16BIT
   #define START_SEC_VAR_FAST_POWER_ON_INIT_16BIT
#elif defined (DCM_STOP_SEC_VAR_FAST_POWER_ON_INIT_16BIT)
   #undef      DCM_STOP_SEC_VAR_FAST_POWER_ON_INIT_16BIT
   #define STOP_SEC_VAR_FAST_POWER_ON_INIT_16BIT

#elif defined (DCM_START_SEC_VAR_FAST_POWER_ON_INIT_32BIT)
   #undef      DCM_START_SEC_VAR_FAST_POWER_ON_INIT_32BIT
   #define START_SEC_VAR_FAST_POWER_ON_INIT_32BIT
#elif defined (DCM_STOP_SEC_VAR_FAST_POWER_ON_INIT_32BIT)
   #undef      DCM_STOP_SEC_VAR_FAST_POWER_ON_INIT_32BIT
   #define STOP_SEC_VAR_FAST_POWER_ON_INIT_32BIT

#elif defined (DCM_START_SEC_VAR_FAST_POWER_ON_INIT_UNSPECIFIED)
   #undef      DCM_START_SEC_VAR_FAST_POWER_ON_INIT_UNSPECIFIED
   #define START_SEC_VAR_FAST_POWER_ON_INIT_UNSPECIFIED
#elif defined (DCM_STOP_SEC_VAR_FAST_POWER_ON_INIT_UNSPECIFIED)
   #undef      DCM_STOP_SEC_VAR_FAST_POWER_ON_INIT_UNSPECIFIED
   #define STOP_SEC_VAR_FAST_POWER_ON_INIT_UNSPECIFIED

#elif defined (DCM_START_SEC_VAR_FAST_BOOLEAN)
   #undef      DCM_START_SEC_VAR_FAST_BOOLEAN
   #define START_SEC_VAR_FAST_BOOLEAN
#elif defined (DCM_STOP_SEC_VAR_FAST_BOOLEAN)
   #undef      DCM_STOP_SEC_VAR_FAST_BOOLEAN
   #define STOP_SEC_VAR_FAST_BOOLEAN

#elif defined (DCM_START_SEC_VAR_FAST_8BIT)
   #undef      DCM_START_SEC_VAR_FAST_8BIT
   #define START_SEC_VAR_FAST_8BIT
#elif defined (DCM_STOP_SEC_VAR_FAST_8BIT)
   #undef      DCM_STOP_SEC_VAR_FAST_8BIT
   #define STOP_SEC_VAR_FAST_8BIT

#elif defined (DCM_START_SEC_VAR_FAST_16BIT)
   #undef      DCM_START_SEC_VAR_FAST_16BIT
   #define START_SEC_VAR_FAST_16BIT
#elif defined (DCM_STOP_SEC_VAR_FAST_16BIT)
   #undef      DCM_STOP_SEC_VAR_FAST_16BIT
   #define STOP_SEC_VAR_FAST_16BIT

#elif defined (DCM_START_SEC_VAR_FAST_32BIT)
   #undef      DCM_START_SEC_VAR_FAST_32BIT
   #define START_SEC_VAR_FAST_32BIT
#elif defined (DCM_STOP_SEC_VAR_FAST_32BIT)
   #undef      DCM_STOP_SEC_VAR_FAST_32BIT
   #define STOP_SEC_VAR_FAST_32BIT

#elif defined (DCM_START_SEC_VAR_FAST_UNSPECIFIED)
   #undef      DCM_START_SEC_VAR_FAST_UNSPECIFIED
   #define START_SEC_VAR_FAST_UNSPECIFIED
#elif defined (DCM_STOP_SEC_VAR_FAST_UNSPECIFIED)
   #undef      DCM_STOP_SEC_VAR_FAST_UNSPECIFIED
   #define STOP_SEC_VAR_FAST_UNSPECIFIED

#elif defined (DCM_START_SEC_CONST_BOOLEAN)
   #undef      DCM_START_SEC_CONST_BOOLEAN
   #define START_SEC_CONST_BOOLEAN
#elif defined (DCM_STOP_SEC_CONST_BOOLEAN)
   #undef      DCM_STOP_SEC_CONST_BOOLEAN
   #define STOP_SEC_CONST_BOOLEAN

#elif defined (DCM_START_SEC_CONST_8BIT)
   #undef      DCM_START_SEC_CONST_8BIT
   #define START_SEC_CONST_8BIT
#elif defined (DCM_STOP_SEC_CONST_8BIT)
   #undef      DCM_STOP_SEC_CONST_8BIT
   #define STOP_SEC_CONST_8BIT

#elif defined (DCM_START_SEC_CONST_16BIT)
   #undef      DCM_START_SEC_CONST_16BIT
   #define START_SEC_CONST_16BIT
#elif defined (DCM_STOP_SEC_CONST_16BIT)
   #undef      DCM_STOP_SEC_CONST_16BIT
   #define STOP_SEC_CONST_16BIT

#elif defined (DCM_START_SEC_CONST_32BIT)
   #undef      DCM_START_SEC_CONST_32BIT
   #define START_SEC_CONST_32BIT
#elif defined (DCM_STOP_SEC_CONST_32BIT)
   #undef      DCM_STOP_SEC_CONST_32BIT
   #define STOP_SEC_CONST_32BIT

#elif defined (DCM_START_SEC_CONST_UNSPECIFIED)
   #undef      DCM_START_SEC_CONST_UNSPECIFIED
   #define START_SEC_CONST_UNSPECIFIED
#elif defined (DCM_STOP_SEC_CONST_UNSPECIFIED)
   #undef      DCM_STOP_SEC_CONST_UNSPECIFIED
   #define STOP_SEC_CONST_UNSPECIFIED

#elif defined (DCM_START_SEC_CONST_FAST_BOOLEAN)
   #undef      DCM_START_SEC_CONST_FAST_BOOLEAN
   #define START_SEC_CONST_FAST_BOOLEAN
#elif defined (DCM_STOP_SEC_CONST_FAST_BOOLEAN)
   #undef      DCM_STOP_SEC_CONST_FAST_BOOLEAN
   #define STOP_SEC_CONST_FAST_BOOLEAN

#elif defined (DCM_START_SEC_CONST_FAST_8BIT)
   #undef      DCM_START_SEC_CONST_FAST_8BIT
   #define START_SEC_CONST_FAST_8BIT
#elif defined (DCM_STOP_SEC_CONST_FAST_8BIT)
   #undef      DCM_STOP_SEC_CONST_FAST_8BIT
   #define STOP_SEC_CONST_FAST_8BIT

#elif defined (DCM_START_SEC_CONST_FAST_16BIT)
   #undef      DCM_START_SEC_CONST_FAST_16BIT
   #define START_SEC_CONST_FAST_16BIT
#elif defined (DCM_STOP_SEC_CONST_FAST_16BIT)
   #undef      DCM_STOP_SEC_CONST_FAST_16BIT
   #define STOP_SEC_CONST_FAST_16BIT

#elif defined (DCM_START_SEC_CONST_FAST_32BIT)
   #undef      DCM_START_SEC_CONST_FAST_32BIT
   #define START_SEC_CONST_FAST_32BIT
#elif defined (DCM_STOP_SEC_CONST_FAST_32BIT)
   #undef      DCM_STOP_SEC_CONST_FAST_32BIT
   #define STOP_SEC_CONST_FAST_32BIT

#elif defined (DCM_START_SEC_CONST_FAST_UNSPECIFIED)
   #undef      DCM_START_SEC_CONST_FAST_UNSPECIFIED
   #define START_SEC_CONST_FAST_UNSPECIFIED
#elif defined (DCM_STOP_SEC_CONST_FAST_UNSPECIFIED)
   #undef      DCM_STOP_SEC_CONST_FAST_UNSPECIFIED
   #define STOP_SEC_CONST_FAST_UNSPECIFIED

#elif defined (DCM_START_CONST_PBCFG)
   #undef      DCM_START_CONST_PBCFG
   #define START_CONST_PBCFG
#elif defined (DCM_STOP_CONST_PBCFG)
   #undef      DCM_STOP_CONST_PBCFG
   #define STOP_CONST_PBCFG

#elif defined (DCM_START_CONST_PBCFG_ROOT)
   #undef      DCM_START_CONST_PBCFG_ROOT
   #define START_CONST_PBCFG_ROOT
#elif defined (DCM_STOP_CONST_PBCFG_ROOT)
   #undef      DCM_STOP_CONST_PBCFG_ROOT
   #define STOP_CONST_PBCFG_ROOT
/*=========================END OF DCM  =======================================*/
#endif


#if defined (START_WITH_IF)

/*=======[M E M M A P  S Y M B O L  C H E C K]================================*/
/* @req MEMMAP007 */
/***********************memmap symbol check ***********************/
#if defined (MEMMAP_ERROR)
    #error "MemMap.h, wrong pragma command"
#endif

#endif

/*=======[E N D   O F   F I L E]==============================================*/
