/*
 * Copyright 2016-2018 NXP
 * All rights reserved.
 *
 * THIS SOFTWARE IS PROVIDED BY NXP "AS IS" AND ANY EXPRESSED OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL NXP OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
/*!
 * @file power_manager_MPC57xx.c
 *
 * @page misra_violations MISRA-C:2012 violations
 *
 * @section [global]
 * Violates MISRA 2012 Required Rule 1.3, There shall be no occurrence of
 * undefined or critical unspecified behaviour.
 * The addresses of the stack variables are only used at local scope.
 *
 * @section [global]
 * Violates MISRA 2012 Advisory Rule 8.7, External could be made static.
 * Function is defined for usage by application code.
 *
 * @section [global]
 * Violates MISRA 2012 Advisory Rule 10.5,
 * Impermissible cast; cannot cast from 'essentially unsigned' to 'essentially enum<i>'.
 * All possible values are covered by the enumeration, direct casting is used to optimize code.
 *
 * @section [global]
 * Violates MISRA 2012 Required Rule 10.3,
 * Expression assigned to a narrower or different essential type.
 * All possible values are covered by the enumeration, direct casting is used to optimize code.
 *
 * @section [global]
 * Violates MISRA 2012 Required Rule 11.6, A cast shall not be performed
 * between pointer to void and an arithmetic type.
 * The base address parameter from HAL functions is provided as integer so
 * it needs to be cast to pointer.
 *
 * @section [global]
 * Violates MISRA 2012 Advisory Rule 11.4, A conversion should not be performed
 * between a pointer to object and an integer type.
 * The base address parameter from HAL functions is provided as integer so
 * a conversion between a pointer and an integer has to be performed
 */

#include "power_manager.h"
#include "power_mc_me_hw_access.h"

/*! Timeout used for waiting to set new mode */
#define POWER_SET_MODE_TIMEOUT 200000U
/*! Timeout used for waiting to set new mode */
#define POWER_SET_PREVIOUS_MODE_TIMEOUT 250000U

/*! @brief Power manager internal structure. */
extern power_manager_state_t gPowerManagerState;

/*******************************************************************************
 * INTERNAL FUNCTIONS
 ******************************************************************************/
static status_t POWER_SYS_WaitForModeStatus(power_manager_modes_t mode);
static status_t POWER_SYS_WaitForPreModeStatus(power_manager_modes_t mode);

/*FUNCTION**********************************************************************
 *
 * Function Name : POWER_SYS_DoInit
 * Description   : Implementation-specific configuration of power modes.
 *
 * This function performs the actual implementation-specific initialization based on the provided power mode configurations.
 *END**************************************************************************/
status_t POWER_SYS_DoInit(void)
{
    uint32_t i = 0U;

    /* Disable user modes first; they will be enabled as needed */
#if FEATURE_MC_ME_HAS_TEST_MODE
    MC_ME_DisablePowerMode(MC_ME, POWER_MANAGER_TEST);
#endif /* #if FEATURE_MC_ME_HAS_TEST_MODE */
    MC_ME_DisablePowerMode(MC_ME, POWER_MANAGER_RUN1);
    MC_ME_DisablePowerMode(MC_ME, POWER_MANAGER_RUN2);
    MC_ME_DisablePowerMode(MC_ME, POWER_MANAGER_RUN3);
    MC_ME_DisablePowerMode(MC_ME, POWER_MANAGER_STOP0);
#if FEATURE_MC_ME_HAS_HALT_MODE
    MC_ME_DisablePowerMode(MC_ME, POWER_MANAGER_HALT0);
#endif /* #if FEATURE_MC_ME_HAS_HALT_MODE */
#if FEATURE_MC_ME_HAS_STANDBY_MODE
    MC_ME_DisablePowerMode(MC_ME, POWER_MANAGER_STANDBY0);
#endif /* #if FEATURE_MC_ME_HAS_STANDBY_MODE */
    /* Configure run modes */
    for (i = 0; i < gPowerManagerState.configsNumber; i++)
    {
        const power_manager_user_config_t * const config = (*gPowerManagerState.configs)[i];
        MC_ME_SetRunModeConfig(MC_ME, config);
        MC_ME_EnablePowerMode(MC_ME, config->powerMode);
    }

    return STATUS_SUCCESS;
}

/*FUNCTION**********************************************************************
 *
 * Function Name : POWER_SYS_DoDeinit
 * Description   : Implementation-specific de-initialization of power manager.
 *
 * This function performs the actual implementation-specific de-initialization.
 *END**************************************************************************/
status_t POWER_SYS_DoDeinit(void)
{
#if FEATURE_MC_ME_HAS_TEST_MODE
    MC_ME_DisablePowerMode(MC_ME, POWER_MANAGER_TEST);
#endif /* #if FEATURE_MC_ME_HAS_TEST_MODE */
    MC_ME_DisablePowerMode(MC_ME, POWER_MANAGER_RUN1);
    MC_ME_DisablePowerMode(MC_ME, POWER_MANAGER_RUN2);
    MC_ME_DisablePowerMode(MC_ME, POWER_MANAGER_RUN3);
    MC_ME_DisablePowerMode(MC_ME, POWER_MANAGER_STOP0);
#if FEATURE_MC_ME_HAS_HALT_MODE
    MC_ME_DisablePowerMode(MC_ME, POWER_MANAGER_HALT0);
#endif /* #if FEATURE_MC_ME_HAS_HALT_MODE */
#if FEATURE_MC_ME_HAS_STANDBY_MODE
    MC_ME_DisablePowerMode(MC_ME, POWER_MANAGER_STANDBY0);
#endif /* #if FEATURE_MC_ME_HAS_STANDBY_MODE */

    return STATUS_SUCCESS;
}

/*FUNCTION**********************************************************************
 *
 * Function Name : POWER_SYS_DoSetMode
 * Description   : Configures the power mode.
 *
 * This function performs the actual implementation-specific logic to switch to one of the defined power modes.
 *END**************************************************************************/
status_t POWER_SYS_DoSetMode(const power_manager_user_config_t * const configPtr)
{
    status_t returnCode = STATUS_SUCCESS;
    power_manager_modes_t currentMode = POWER_SYS_GetCurrentMode();
    /* Configure the power mode */
    switch (configPtr->powerMode)
    {
        /* Test mode */
#if FEATURE_MC_ME_HAS_TEST_MODE
        case POWER_MANAGER_TEST:
            /* Test mode can be entered from DRUN mode */
            if ((currentMode != POWER_MANAGER_DRUN) && (currentMode != POWER_MANAGER_TEST))
            {
                /* Switch the mode */
                MC_ME_SetPowerMode(MC_ME, POWER_MANAGER_DRUN);
                returnCode = POWER_SYS_WaitForModeStatus(POWER_MANAGER_DRUN);
            }
            if (returnCode == STATUS_SUCCESS)
            {
                /* Switch the mode */
                MC_ME_SetPowerMode(MC_ME, POWER_MANAGER_TEST);
                returnCode = POWER_SYS_WaitForModeStatus(POWER_MANAGER_TEST);
            }
            break;
#endif /* #if FEATURE_MC_ME_HAS_TEST_MODE */
        /* Safe mode */
        case POWER_MANAGER_SAFE:
        /* Safe mode can be entered from DRUN and RUNx mode */
            if (currentMode >= POWER_MANAGER_SAFE)
            {
                MC_ME_SetPowerMode(MC_ME, POWER_MANAGER_SAFE);
                returnCode = POWER_SYS_WaitForModeStatus(POWER_MANAGER_SAFE);
            }
#if FEATURE_MC_ME_HAS_TEST_MODE
            else
            {
                /* Current mode is test mode */
                /* The first one switches to drun mode */
                MC_ME_SetPowerMode(MC_ME, POWER_MANAGER_DRUN);
                returnCode = POWER_SYS_WaitForModeStatus(POWER_MANAGER_DRUN);
                /* Switch the safe mode */
                if(returnCode == STATUS_SUCCESS)
                {
                    MC_ME_SetPowerMode(MC_ME, POWER_MANAGER_SAFE);
                    returnCode = POWER_SYS_WaitForModeStatus(POWER_MANAGER_SAFE);
                }
            }
#endif
            break;
        /* DRUN mode */
        case POWER_MANAGER_DRUN:
            /* Switch the mode */
            MC_ME_SetPowerMode(MC_ME, POWER_MANAGER_DRUN);
            returnCode = POWER_SYS_WaitForModeStatus(POWER_MANAGER_DRUN);
            break;
        /* RUN mode */
        /* Fall-through */
        case POWER_MANAGER_RUN0:
        /* Fall-through */
        case POWER_MANAGER_RUN1:
        /* Fall-through */
        case POWER_MANAGER_RUN2:
        /* Fall-through */
        case POWER_MANAGER_RUN3:
            /* RUNx modes can be entered from DRUN,SAFE and other RUNx modes */
            if (currentMode <= POWER_MANAGER_SAFE)
            {
                MC_ME_SetPowerMode(MC_ME, POWER_MANAGER_DRUN);
                returnCode = POWER_SYS_WaitForModeStatus(POWER_MANAGER_DRUN);
            }
            if (STATUS_SUCCESS == returnCode)
            {
                /* Switch the run mode */
                MC_ME_SetPowerMode(MC_ME, configPtr->powerMode);
                returnCode = POWER_SYS_WaitForModeStatus(configPtr->powerMode);
            }
            break;
#if FEATURE_MC_ME_HAS_HALT_MODE
        case POWER_MANAGER_HALT0:
        /* Fall-through */
#endif
        case POWER_MANAGER_STOP0:
            /* HALT0 and STOP0 mode can be entered only from RUN mode */
            /* Must switch to drun mode if current mode is test or safe mode */
            if (currentMode <= POWER_MANAGER_SAFE)
            {
                MC_ME_SetPowerMode(MC_ME, POWER_MANAGER_DRUN);
                returnCode = POWER_SYS_WaitForModeStatus(POWER_MANAGER_DRUN);
            }
            if (returnCode == STATUS_SUCCESS)
            {
                if ((currentMode < POWER_MANAGER_RUN0) || (currentMode > POWER_MANAGER_RUN3))
                {
                    MC_ME_SetPowerMode(MC_ME, POWER_MANAGER_RUN0);
                    returnCode = POWER_SYS_WaitForModeStatus(POWER_MANAGER_RUN0);
                }
                if (STATUS_SUCCESS == returnCode)
                {
                    /* Switch the mode
                    * After switch this mode the core stop and wait interrupt or wakeup */
                    MC_ME_SetPowerMode(MC_ME, configPtr->powerMode);
                    returnCode = POWER_SYS_WaitForPreModeStatus(configPtr->powerMode);
                }
            }
            break;
#if FEATURE_MC_ME_HAS_STANDBY_MODE
        case POWER_MANAGER_STANDBY0:
            /* STANDBY0 mode can be entered from RUN and DRUN mode */
            if ((currentMode < POWER_MANAGER_DRUN) || (currentMode > POWER_MANAGER_RUN3))
            {
                MC_ME_SetPowerMode(MC_ME, POWER_MANAGER_DRUN);
                returnCode = POWER_SYS_WaitForModeStatus(POWER_MANAGER_DRUN);
            }
            if (STATUS_SUCCESS == returnCode)
            {
                /* Switch the STANDBY mode.
                 * After switch this mode the core stop and just reset or wake up it */
                MC_ME_SetPowerMode(MC_ME, POWER_MANAGER_STANDBY0);
            }
            break;
#endif /* #if FEATURE_MC_ME_HAS_STANDBY_MODE */
        default:
            /* invalid power mode */
            returnCode = STATUS_UNSUPPORTED;
            break;
    }

    return returnCode;
}

/*FUNCTION**********************************************************************
 *
 * Function Name : POWER_SYS_GetCurrentMode
 * Description   : Returns currently running power mode.
 *
 * Implements POWER_SYS_GetCurrentMode_Activity
 *END**************************************************************************/
power_manager_modes_t POWER_SYS_GetCurrentMode(void)
{
    power_manager_user_config_t status;
    MC_ME_GetStatus(MC_ME, &status);

    return status.powerMode;
}

/*FUNCTION**********************************************************************
 *
 * Function Name : POWER_SYS_WaitForModeStatus
 * Description   : Internal function used for waiting for mode change to finish
 * mode            The expected running mode
 *
 *END**************************************************************************/
static status_t POWER_SYS_WaitForModeStatus(power_manager_modes_t mode)
{
    status_t retCode = STATUS_MCU_TRANSITION_FAILED;
    uint32_t i = 0U;

    switch (mode)
    {
#if FEATURE_MC_ME_HAS_TEST_MODE
        case POWER_MANAGER_TEST:
        /* Fall-through */
#endif /* #if FEATURE_MC_ME_HAS_TEST_MODE */
        case POWER_MANAGER_SAFE:
        /* Fall-through */
        case POWER_MANAGER_DRUN:
        /* Fall-through */
        case POWER_MANAGER_RUN0:
        /* Fall-through */
        case POWER_MANAGER_RUN1:
        /* Fall-through */
        case POWER_MANAGER_RUN2:
        /* Fall-through */
        case POWER_MANAGER_RUN3:
            retCode = STATUS_SUCCESS;
            break;
        default:
            retCode = STATUS_UNSUPPORTED;
            break;
    }

    if (STATUS_SUCCESS == retCode)
    {
        for (i = 0U; i < POWER_SET_MODE_TIMEOUT; i++)
        {
            if (!MC_ME_ModeTransitionInProgress(MC_ME))
            {
                if (POWER_SYS_GetCurrentMode() == mode)
                {
                    break;
                }
            }
        }
    }

    if (i >= POWER_SET_MODE_TIMEOUT)
    {
        retCode = STATUS_MCU_TRANSITION_FAILED;
    }

    return retCode;
}

/*FUNCTION**********************************************************************
 *
 * Function Name : POWER_SYS_GetPreviousMode
 * Description   : This function will return previous mode where the chip jump
 * from lastest mode to current mode. The function can not return STANDBY mode
 * because register control is disabled.
 *
 * Implements POWER_SYS_GetPreviousMode_Activity
 *END**************************************************************************/
power_manager_modes_t POWER_SYS_GetPreviousMode(void)
{
    power_manager_modes_t returnMode;
    MC_ME_GetPreviousMode(MC_ME,&returnMode);

    return returnMode;
}

/*FUNCTION******************************************************************************
 *
 * Function Name : POWER_SYS_WaitForPreModeStatus
 * Description   : Internal function used for waiting for previous mode update to finish
 *                 This function only uses to check previous mode when cpu exited
                   stop or halt mode.
 * mode            The expected checking mode
 *
 *END**********************************************************************************/
static status_t POWER_SYS_WaitForPreModeStatus(power_manager_modes_t mode)
{
    status_t retCode = STATUS_MCU_TRANSITION_FAILED;;
    uint32_t i = 0U;
    power_manager_modes_t preMode = POWER_MANAGER_DRUN;

    switch (mode)
    {
        /* Fall-through */
#if FEATURE_MC_ME_HAS_HALT_MODE
        case POWER_MANAGER_HALT0:
        /* Fall-through */
#endif
        case POWER_MANAGER_STOP0:
            retCode = STATUS_SUCCESS;
            break;
        default:
            retCode = STATUS_UNSUPPORTED;
            break;
    }

    if (STATUS_SUCCESS == retCode)
    {
        for (i = 0U; i < POWER_SET_PREVIOUS_MODE_TIMEOUT; i++)
        {
            MC_ME_GetPreviousMode(MC_ME,&preMode);
            if (preMode == mode)
            {
                break;
            }
        }
    }

    if (i >= POWER_SET_PREVIOUS_MODE_TIMEOUT)
    {
        retCode = STATUS_MCU_TRANSITION_FAILED;
    }

    return retCode;
}
/*******************************************************************************
 * EOF
 ******************************************************************************/
