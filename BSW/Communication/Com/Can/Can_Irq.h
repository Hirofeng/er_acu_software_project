/*============================================================================*/
/*  Copyright (C) 2009-2011,  INFRASTRUCTURE SOFTWARE CO.,LTD.
 *  
 *  All rights reserved. This software is  property. Duplication
 *  or disclosure without  written authorization is prohibited.
 *  
 *  
 *  @file       <Can_Irq.h>
 *  @brief      <Can Irq Isr header file>
 *  
 *  <Compiler: CodeWarrior V2.7    MCU:MPC5634>
 *  
 *  @author     <bo.zeng>
 *  @date       <15-07-2013>
 */
/*============================================================================*/
#ifndef  CAN_IRQ_H
#define  CAN_IRQ_H
/*=======[R E V I S I O N   H I S T O R Y]====================================*/
/*  <VERSION>    <DATE>    <AUTHOR>    <REVISION LOG>
 *  V1.0.0       20130715  bo.zeng    Initial version
 * 
 */
/*============================================================================*/

/*=======[I N C L U D E S]====================================================*/
#include "ComStack_Types.h"
//#include "Can_Cfg.h"

#include "Can_Cfg.h"
/*=======[V E R S I O N  I N F O R M A T I O N]===============================*/
/* @req <CAN085> Source File Version Information  */
#define CAN_IRQ_H_AR_MAJOR_VERSION  2
#define CAN_IRQ_H_AR_MINOR_VERSION  4
#define CAN_IRQ_H_AR_PATCH_VERSION  0

#define CAN_IRQ_H_SW_MAJOR_VERSION  3
#define CAN_IRQ_H_SW_MINOR_VERSION  1
#define CAN_IRQ_H_SW_PATCH_VERSION  0

/*=======[E X T E R N A L   F U N C T I O N   D E C L A R A T I O N S]========*/

#define CAN_START_SEC_CODE_FAST
#include "Can_MemMap.h"

/******************************************************************************/
/*
 * Brief               <CAN A Tx/Rx (00MB) Interrupt Service Routine process>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
extern FUNC(void, CAN_CODE_FAST) Can_A_TxRxMb00ISR(void);

/******************************************************************************/
/*
 * Brief               <CAN A Tx/Rx (01MB) Interrupt Service Routine process>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
extern FUNC(void, CAN_CODE_FAST) Can_A_TxRxMb01ISR(void);

/******************************************************************************/
/*
 * Brief               <CAN A Tx/Rx (02MB) Interrupt Service Routine process>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
extern FUNC(void, CAN_CODE_FAST) Can_A_TxRxMb02ISR(void);

/******************************************************************************/
/*
 * Brief               <CAN A Tx/Rx (03MB) Interrupt Service Routine process>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
extern FUNC(void, CAN_CODE_FAST) Can_A_TxRxMb03ISR(void);

/******************************************************************************/
/*
 * Brief               <CAN A Tx/Rx (04MB) Interrupt Service Routine process>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
extern FUNC(void, CAN_CODE_FAST) Can_A_TxRxMb04ISR(void);

/******************************************************************************/
/*
 * Brief               <CAN A Tx/Rx (05MB) Interrupt Service Routine process>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
extern FUNC(void, CAN_CODE_FAST) Can_A_TxRxMb05ISR(void);

/******************************************************************************/
/*
 * Brief               <CAN A Tx/Rx (05MB) Interrupt Service Routine process>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
extern FUNC(void, CAN_CODE_FAST) Can_A_TxRxMb06ISR(void);

/******************************************************************************/
/*
 * Brief               <CAN A Tx/Rx (07MB) Interrupt Service Routine process>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
extern FUNC(void, CAN_CODE_FAST) Can_A_TxRxMb07ISR(void);

/******************************************************************************/
/*
 * Brief               <CAN A Tx/Rx (08MB) Interrupt Service Routine process>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
extern FUNC(void, CAN_CODE_FAST) Can_A_TxRxMb08ISR(void);

/******************************************************************************/
/*
 * Brief               <CAN A Tx/Rx (09MB) Interrupt Service Routine process>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
extern FUNC(void, CAN_CODE_FAST) Can_A_TxRxMb09ISR(void);

/******************************************************************************/
/*
 * Brief               <CAN A Tx/Rx (10MB) Interrupt Service Routine process>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
extern FUNC(void, CAN_CODE_FAST) Can_A_TxRxMb10ISR(void);

/******************************************************************************/
/*
 * Brief               <CAN A Tx/Rx (11MB) Interrupt Service Routine process>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
extern FUNC(void, CAN_CODE_FAST) Can_A_TxRxMb11ISR(void);

/******************************************************************************/
/*
 * Brief               <CAN A Tx/Rx (12MB) Interrupt Service Routine process>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
extern FUNC(void, CAN_CODE_FAST) Can_A_TxRxMb12ISR(void);

/******************************************************************************/
/*
 * Brief               <CAN A Tx/Rx (13MB) Interrupt Service Routine process>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
extern FUNC(void, CAN_CODE_FAST) Can_A_TxRxMb13ISR(void);

/******************************************************************************/
/*
 * Brief               <CAN A Tx/Rx (14MB) Interrupt Service Routine process>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
extern FUNC(void, CAN_CODE_FAST) Can_A_TxRxMb14ISR(void);

/******************************************************************************/
/*
 * Brief               <CAN A Tx/Rx (15MB) Interrupt Service Routine process>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
extern FUNC(void, CAN_CODE_FAST) Can_A_TxRxMb15ISR(void);

/******************************************************************************/
/*
 * Brief               <CAN A Tx/Rx (16-31 MB) Interrupt Service Routine process>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
extern FUNC(void, CAN_CODE_FAST) Can_A_TxRxMb16_31ISR(void);

/******************************************************************************/
/*
 * Brief               <CAN A Tx/Rx (32-63 MB) Interrupt Service Routine process>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
extern FUNC(void, CAN_CODE_FAST) Can_A_TxRxMb32_63ISR(void);

#if (STD_ON == CAN_CONTROLLERA_BUSOFF_INTERRUPT)
/******************************************************************************/
/*
 * Brief               <CAN A TW/RW/bus-off Interrupt Service Routine process>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
extern FUNC(void, CAN_CODE_FAST) Can_A_TwRwBusOff(void);
#endif /* STD_ON == CAN_CONTROLLERA_BUSOFF_INTERRUPT */

/******************************************************************************/
/*
 * Brief               <CAN A Error Interrupt Service Routine process>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
extern FUNC(void, CAN_CODE_FAST) Can_A_Err(void);

#if (STD_ON == CAN_WAKEUP_SUPPORT)
#if (STD_ON == CAN_CONTROLLERA_WAKEUP_INTERRUPT)
/******************************************************************************/
/*
 * Brief               <CAN A Wakeup Interrupt Service Routine process>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
extern FUNC(void, CAN_CODE_FAST) Can_A_Wakeup(void);
#endif /* STD_ON == CAN_CONTROLLERA_WAKEUP_INTERRUPT */
#endif /* STD_ON == CAN_WAKEUP_SUPPORT */

/******************************************************************************/
/*
 * Brief               <CAN C Tx/Rx (00MB) Interrupt Service Routine process>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
extern FUNC(void, CAN_CODE_FAST) Can_C_TxRxMb00ISR(void);

/******************************************************************************/
/*
 * Brief               <CAN C Tx/Rx (01MB) Interrupt Service Routine process>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
extern FUNC(void, CAN_CODE_FAST) Can_C_TxRxMb01ISR(void);

/******************************************************************************/
/*
 * Brief               <CAN C Tx/Rx (02MB) Interrupt Service Routine process>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
extern FUNC(void, CAN_CODE_FAST) Can_C_TxRxMb02ISR(void);

/******************************************************************************/
/*
 * Brief               <CAN C Tx/Rx (03MB) Interrupt Service Routine process>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
extern FUNC(void, CAN_CODE_FAST) Can_C_TxRxMb03ISR(void);

/******************************************************************************/
/*
 * Brief               <CAN C Tx/Rx (04MB) Interrupt Service Routine process>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
extern FUNC(void, CAN_CODE_FAST) Can_C_TxRxMb04ISR(void);

/******************************************************************************/
/*
 * Brief               <CAN C Tx/Rx (05MB) Interrupt Service Routine process>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
extern FUNC(void, CAN_CODE_FAST) Can_C_TxRxMb05ISR(void);

/******************************************************************************/
/*
 * Brief               <CAN C Tx/Rx (05MB) Interrupt Service Routine process>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
extern FUNC(void, CAN_CODE_FAST) Can_C_TxRxMb06ISR(void);

/******************************************************************************/
/*
 * Brief               <CAN C Tx/Rx (07MB) Interrupt Service Routine process>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
extern FUNC(void, CAN_CODE_FAST) Can_C_TxRxMb07ISR(void);

/******************************************************************************/
/*
 * Brief               <CAN C Tx/Rx (08MB) Interrupt Service Routine process>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
extern FUNC(void, CAN_CODE_FAST) Can_C_TxRxMb08ISR(void);

/******************************************************************************/
/*
 * Brief               <CAN C Tx/Rx (09MB) Interrupt Service Routine process>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
extern FUNC(void, CAN_CODE_FAST) Can_C_TxRxMb09ISR(void);

/******************************************************************************/
/*
 * Brief               <CAN C Tx/Rx (10MB) Interrupt Service Routine process>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
extern FUNC(void, CAN_CODE_FAST) Can_C_TxRxMb10ISR(void);

/******************************************************************************/
/*
 * Brief               <CAN C Tx/Rx (11MB) Interrupt Service Routine process>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
extern FUNC(void, CAN_CODE_FAST) Can_C_TxRxMb11ISR(void);

/******************************************************************************/
/*
 * Brief               <CAN C Tx/Rx (12MB) Interrupt Service Routine process>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
extern FUNC(void, CAN_CODE_FAST) Can_C_TxRxMb12ISR(void);

/******************************************************************************/
/*
 * Brief               <CAN C Tx/Rx (13MB) Interrupt Service Routine process>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
extern FUNC(void, CAN_CODE_FAST) Can_C_TxRxMb13ISR(void);

/******************************************************************************/
/*
 * Brief               <CAN C Tx/Rx (14MB) Interrupt Service Routine process>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
extern FUNC(void, CAN_CODE_FAST) Can_C_TxRxMb14ISR(void);

/******************************************************************************/
/*
 * Brief               <CAN C Tx/Rx (15MB) Interrupt Service Routine process>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
extern FUNC(void, CAN_CODE_FAST) Can_C_TxRxMb15ISR(void);

/******************************************************************************/
/*
 * Brief               <CAN C Tx/Rx (16-31 MB) Interrupt Service Routine process>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
extern FUNC(void, CAN_CODE_FAST) Can_C_TxRxMb16_31ISR(void);

#if (STD_ON == CAN_CONTROLLERC_BUSOFF_INTERRUPT)
/******************************************************************************/
/*
 * Brief               <CAN C TW/RW/bus-off Interrupt Service Routine process>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
extern FUNC(void, CAN_CODE_FAST) Can_C_TwRwBusOff(void);
#endif /* STD_ON == CAN_CONTROLLERC_BUSOFF_INTERRUPT */

/******************************************************************************/
/*
 * Brief               <CAN C Error Interrupt Service Routine process>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
extern FUNC(void, CAN_CODE_FAST) Can_C_Err(void);

#if (STD_ON == CAN_WAKEUP_SUPPORT)
#if (STD_ON == CAN_CONTROLLERC_WAKEUP_INTERRUPT)
/******************************************************************************/
/*
 * Brief               <CAN C Wakeup Interrupt Service Routine process>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
extern FUNC(void, CAN_CODE_FAST) Can_C_Wakeup(void);
#endif /* STD_ON == CAN_CONTROLLERC_WAKEUP_INTERRUPT */
#endif /* STD_ON == CAN_WAKEUP_SUPPORT */

#define CAN_STOP_SEC_CODE_FAST
#include "Can_MemMap.h"

/*=======[I N T E R N A L   F U N C T I O N   D E C L A R A T I O N S]========*/
#define CAN_START_SEC_CODE
#include "Can_MemMap.h"
/******************************************************************************/
/*
 * Brief               <This function performs Tx/Rx Interrupt Handle>
 * Param-Name[in]      <Controller - CAN controller to be error process>
 * Param-Name[in]      <startMbId  - start mailbox id>
 * Param-Name[in]      <endMbId    - end mailbox id>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
extern FUNC(void, CAN_CODE) Can_RxTxInt_Handler(uint8 Controller, uint8 startMbId, uint8 endMbId,Can_PduType ppdu);

/******************************************************************************/
/*
 * Brief               <This function performs bus-off  process>
 * Param-Name[in]      <Controller- CAN controller to be Tw/Rw/bus-off process>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
extern FUNC(void, CAN_CODE) Can_TwRwBusOff_Handler(uint8 Controller);

/******************************************************************************/
/*
 * Brief               <This function performs error process>
 * Param-Name[in]      <Controller- CAN controller to be error process>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
extern FUNC(void, CAN_CODE) Can_Error_Handler(uint8 Controller);
 
/******************************************************************************/
/*
 * Brief               <This function performs wake up  process>
 * Param-Name[in]      <Controller- CAN controller to be wake up process>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
extern FUNC(void, CAN_CODE) Can_Wakeup_Handler(uint8 Controller);

#endif  /* #ifndef  CAN_IRQ_H */

/*=======[E N D   O F   F I L E]==============================================*/



