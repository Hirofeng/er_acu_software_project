/*============================================================================*/
/*  Copyright (C) 2009-2013,  INFRASTRUCTURE SOFTWARE CO.,LTD.
 *  
 *  All rights reserved. This software is  property. Duplication
 *  or disclosure without  written authorization is prohibited.
 *  
 *  
 *  @file       Comm_dcm.h
 *  @brief      Public interfaces declared by ComM module.
 *
 *  
 *  @author     stanley
 *  @date       2013-4-7
 */
/*============================================================================*/

#ifndef COMM_DCM_H
#define COMM_DCM_H

/**************************************
 * 函数原型:  ComM_DCM_ActiveDiagnostic
 * 功能描述:  通知ComM模块进入“FULL COMMUNICATION”状态
 * 服务ID:    0x1f
 * 同步/异步: 同步
 * 可重入性:  可重入
 * 输入:      无
 * 输出:      无
 * 返回值:    无
 ************************************/
extern  void ComM_DCM_ActiveDiagnostic(void);
/**************************************
 * 函数原型:  ComM_DCM_InactiveDiagnostic
 * 功能描述:  通知ComM模块退出“FULL COMMUNICATION”状态
 * 服务ID:    0x20
 * 同步/异步: 同步
 * 可重入性:  可重入
 * 输入:      无
 * 输出:      无
 * 返回值:    无
 ************************************/
extern  void ComM_DCM_InactiveDiagnostic(void);

#ifdef __cplusplus
extern "C" {
#endif  /* __cplusplus */

/****************************** declarations *********************************/

/****************************** definitions *********************************/

#ifdef __cplusplus
}
#endif  /* __cplusplus */

#endif /* COMM_DCM_H */
