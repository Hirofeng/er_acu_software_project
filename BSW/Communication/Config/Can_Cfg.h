/*============================================================================*/
/*  Copyright (C) 2009-2014,  INFRASTRUCTURE SOFTWARE CO.,LTD.
 *  
 *  All rights reserved. This software is  property. Duplication
 *  or disclosure without  written authorization is prohibited.
 *  
 *  @file       <Can_Cfg.h>
 *  @brief      <>
 *  
 *  <MCU:MPC563x>
 *  
 *  @author     <>
 *  @date       <2014-06-24 15:12:51>
 */
/*============================================================================*/

	
#ifndef CAN_CFG_H
#define CAN_CFG_H
 
/*=======[F I L E  V E R S I O N   I N F O R M A T I O N]===============================*/
#define CAN_CFG_H_AR_MAJOR_VERSION     2
#define CAN_CFG_H_AR_MINOR_VERSION     4
#define CAN_CFG_H_AR_PATCH_VERSION     0
#define CAN_CFG_H_SW_MAJOR_VERSION     1
#define CAN_CFG_H_SW_MINOR_VERSION     0
#define CAN_CFG_H_SW_PATCH_VERSION     0
 
/*=======[R E L E A S E   V E R S I O N   I N F O R M A T I O N]===============================*/
#define CAN_VENDOR_ID                 62U         /* Replace with a symbolic name once available */
#define CAN_MODULE_ID                 80U         /* from AUTOSAR_BasicSoftwareModules.pdf */
#define CAN_INSTANCE                  0U
#define CAN_AR_MAJOR_VERSION          2U 
#define CAN_AR_MINOR_VERSION          4U 
#define CAN_AR_PATCH_VERSION          0U 
#define CAN_SW_MAJOR_VERSION          1U
#define CAN_SW_MINOR_VERSION          0U
#define CAN_SW_PATCH_VERSION          0U 

/*=======[I N C L U D E S]====================================================*/
#include "ComStack_Types.h"

/*=======[M A C R O S]========================================================*/
/*
 * Variants Class (PC, PB)
 */
#define CAN_VARIANT_PC                VARIANT_PRE_COMPILE
#define CAN_VARIANT_PB                VARIANT_POST_BUILD
#define CAN_VARIANT_CFG               CAN_VARIANT_PC

/*
 * CanGeneral
 */
#define CAN_DEV_ERROR_DETECT            STD_OFF    /* development error detection */
#define CAN_DEM_ERROR_DETECT            STD_OFF   /* public error detection */

#define CAN_HW_TRANSMIT_CANCELLATION    STD_OFF    /* transmision cancel */
#define CAN_MULTIPLEXED_TRANSMISSION    STD_OFF    /* multiplexed transmision */
#define CAN_WAKEUP_SUPPORT              STD_ON   /* wakeup */
#define CAN_VERSION_INFO_API            STD_ON    /* version info get API */

/* max. number of tight SW loops to execute while waiting for a state change */
#define CAN_TIMEOUT_DURATION            10000

#define CAN_MAINFUNCTION_BUSOFFPERIOD   100U
#define CAN_MAINFUCTION_WRITEPERIOD     100U
#define CAN_MAINFUNCTION_READPERIOD     100U
#define CAN_MAINFUNCTION_WAKEUPPERIOD   100U

/* Instance Id of the module instance */
#define CAN_INDEX                       0U

/* 
 * CanController
 */
/* Can controller id */
#define CAN_CONTROLLER_A                0

#define CAN_CONTROLLER_C                1



#define CAN_MAX_CONTROLLERS             2

/* CanBusOffProcessing */
#define CAN_CONTROLLERA_BUSOFF_INTERRUPT     STD_ON
#define CAN_CONTROLLERC_BUSOFF_INTERRUPT     STD_ON

/* CanRxProcessing */
#define CAN_CONTROLLERA_RX_INTERRUPT         STD_ON
#define CAN_CONTROLLERC_RX_INTERRUPT         STD_ON

/* CanTxProcessing */
#define CAN_CONTROLLERA_TX_INTERRUPT         STD_ON
#define CAN_CONTROLLERC_TX_INTERRUPT         STD_ON

/* CanWakeupProcessing */
#define CAN_CONTROLLERA_WAKEUP_INTERRUPT     STD_ON
#define CAN_CONTROLLERC_WAKEUP_INTERRUPT     STD_ON

#define CAN_BUSOFF_POLLING                   STD_OFF
#define CAN_RX_POLLING                       STD_OFF
#define CAN_TX_POLLING                       STD_OFF
#define CAN_WAKEUP_POLLING                   STD_OFF

/*
 * Additional Configure
 */
//#define CAN_MAX_HARDWAREOBJECTS              36
//#define CAN_MAX_HARDWAREOBJECTS              18

#define CAN_MAX_HARDWAREOBJECTS              16
#define CAN_MAX_HOHS                         36

/* Descriptors of registers and bits of the SIU module  */
#define SIU_HLT       (*( volatile uint32 *) 0xC3F909A4U)

#define SIU_HLTACK    (*( volatile uint32 *) 0xC3F909A8U)

#endif /* #define CAN_CFG_H */

/*=======[E N D   O F   F I L E]==============================================*/
