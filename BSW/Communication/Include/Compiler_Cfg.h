/*============================================================================*/
/*  Copyright (C) 2009-2011,  INFRASTRUCTURE SOFTWARE CO.,LTD.
 *  
 *  All rights reserved. This software is  property. Duplication
 *  or disclosure without  written authorization is prohibited.
 *  
 *  @file       <Compiler_Cfg.h>
 *  @brief      <Briefly describe file(one line)>
 *  
 *  <Compiler: CodeWarriar    MCU:MC9S12>
 *  
 *  @author     <chen xue hua>
 *  @date       <2013-02-27>
 */
/*============================================================================*/
#ifndef COMPILER_CFG_H
#define COMPILER_CFG_H

/*=======[R E V I S I O N   H I S T O R Y]====================================*/
/*  <VERSION>    <DATE>    <AUTHOR>    <REVISION LOG>
 *  V1.0.0       20130227  chenxuehua  Initial version
 */
/*============================================================================*/

/*=======[V E R S I O N  I N F O R M A T I O N]===============================*/
#define COMPILER_CFG_VENDOR_ID         62
#define COMPILER_CFG_MODULE_ID         0
#define COMPILER_CFG_AR_MAJOR_VERSION  2
#define COMPILER_CFG_AR_MINOR_VERSION  0
#define COMPILER_CFG_AR_PATCH_VERSION  2
#define COMPILER_CFG_SW_MAJOR_VERSION  1
#define COMPILER_CFG_SW_MINOR_VERSION  0
#define COMPILER_CFG_SW_PATCH_VERSION  0
#define COMPILER_CFG_VENDOR_API_INFIX  0

/* @req COMPILER055 @req COMPILER054 */
/*=======[M A C R O S]========================================================*/

/* @req COMPILER044 @req COMPILER040 */
/*=======[Can_MemoryAndPointerClasses]========================================*/
/* Configurable memory class for code. */
#define CAN_CODE

/* Configurable memory class for ISR code. */
#define CAN_CODE_FAST

/* 
 * Configurable memory class for all global or static variables that are never 
 * initialized. 
 */
#define CAN_VAR_NOINIT

/* 
 * Configurable memory class for all global or static variables that are 
 * initialized only after power on reset. 
 */
#define CAN_VAR_POWER_ON_INIT

/*
 * Configurable memory class for all global or static variables that are 
 * initialized after every reset. 
 */
#define CAN_VAR

/* 
 * Configurable memory class for all global or static variables that will 
 * be accessed frequently. 
 */
#define CAN_VAR_NOINIT_FAST

/* 
 * Configurable memory class for all global or static variables that have at 
 * be accessed frequently. 
 */
#define CAN_VAR_POWER_ON_INIT_FAST

/* 
 * Configurable memory class for all global or static variables that have at 
 * be accessed frequently. 
 */
#define CAN_VAR_FAST 

/* Configurable memory class for global or static constants. */
#define CAN_CONST

/*
 * Configurable memory class for global or static constants that will be 
 * accessed frequently.
 */
#define CAN_CONST_FAST

/* Configurable memory class for global or static constants in post build. */
#define CAN_CONST_PBCFG

/*
 * Configurable memory class for pointers to applicaiton data(expected to be 
 * in RAM or ROM)passed via API.
 */
#define CAN_APPL_DATA

/* 
 * Configurable memory class for pointers to applicaiton constants(expected to
 * be certainly in ROM,for instance point of Init-function)passed via API.
 */
#define CAN_APPL_CONST

/* 
 * Configurable memory class for pointers to applicaiton functions(e.g. call 
 * back function pointers).
 */
#define CAN_APPL_CODE


/*=======[CanIf_MemoryAndPointerClasses]========================================*/
#define CANIF_CODE

#define CANIF_CODE_FAST

#define CANIF_VAR_NOINIT

#define CANIF_VAR_POWER_ON_INIT

#define CANIF_VAR

#define CANIF_VAR_NOINIT_FAST

#define CANIF_VAR_POWER_ON_INIT_FAST

#define CANIF_VAR_FAST 

#define CANIF_CONST

#define CANIF_CONST_FAST

#define CANIF_CONST_PBCFG

#define CANIF_APPL_DATA

#define CANIF_APPL_CONST

#define CANIF_APPL_CODE


/* @req COMPILER044 @req COMPILER040 */
/*=======[OSEKCOM_MemoryAndPointerClasses]====================================*/
/* Configurable memory class for code. */
#define COM_CODE

/* Configurable memory class for ISR code. */
#define COM_CODE_FAST

/* 
 * Configurable memory class for all global or static variables that are never 
 * initialized. 
 */
#define COM_VAR_NOINIT

/* 
 * Configurable memory class for all global or static variables that are 
 * initialized only after power on reset. 
 */
#define COM_VAR_POWER_ON_INIT

/*
 * Configurable memory class for all global or static variables that are 
 * initialized after every reset. 
 */
#define COM_VAR

/* 
 * Configurable memory class for all global or static variables that will 
 * be accessed frequently. 
 */
#define COM_VAR_NOINIT_FAST

/* 
 * Configurable memory class for all global or static variables that have at 
 * be accessed frequently. 
 */
#define COM_VAR_POWER_ON_INIT_FAST

/* 
 * Configurable memory class for all global or static variables that have at 
 * be accessed frequently. 
 */
#define COM_VAR_FAST

/* Configurable memory class for global or static constants. */
#define COM_CONST

/*
 * Configurable memory class for global or static constants that will be 
 * accessed frequently.
 */
#define COM_CONST_FAST

/* Configurable memory class for global or static constants in post build. */
#define COM_CONST_PBCFG

/*
 * Configurable memory class for pointers to applicaiton data(expected to be 
 * in RAM or ROM)passed via API.
 */
#define COM_APPL_DATA

/* 
 * Configurable memory class for pointers to applicaiton constants(expected to
 * be certainly in ROM,for instance point of Init-function)passed via API.
 */
#define COM_APPL_CONST

/* 
 * Configurable memory class for pointers to applicaiton functions(e.g. call 
 * back function pointers).
 */
#define COM_APPL_CODE

/*=======[CanSM_MemoryAndPointerClasses]========================================*/
#define CANSM_CODE

#define CANSM_CODE_FAST

#define CANSM_VAR_NOINIT

#define CANSM_VAR_POWER_ON_INIT

#define CANSM_VAR

#define CANSM_VAR_NOINIT_FAST

#define CANSM_VAR_POWER_ON_INIT_FAST

#define CANSM_VAR_FAST 

#define CANSM_CONST

#define CANSM_CONST_FAST

#define CANSM_CONST_PBCFG

#define CANSM_APPL_DATA

#define CANSM_APPL_CONST

#define CANSM_APPL_CODE

/*=======[NmIf_MemoryAndPointerClasses]========================================*/
#define NM_CODE

#define NM_VAR_NOINIT

#define NM_VAR_POWER_ON_INIT

#define NM_VAR

#define NM_CONST

#define NM_APPL_DATA

#define NM_APPL_CONST

#define NM_APPL_CODE

/*=======[OsekNm_MemoryAndPointerClasses]========================================*/

#define OSEKNM_CODE


#define OSEKNM_VAR_NOINIT

#define OSEKNM_VAR_POWER_ON_INIT

#define OSEKNM_VAR

#define OSEKNM_CONST

#define OSEKNM_APPL_DATA

#define OSEKNM_APPL_CONST

#define OSEKNM_APPL_CODE


/***********[CanTp_MemoryAndPointerClasses]**********************************************/
#define CANTP_CODE

#define CANTP_CODE_FAST

#define CANTP_VAR_NOINIT

#define CANTP_VAR_POWER_ON_INIT

#define CANTP_VAR

#define CANTP_VAR_NOINIT_FAST

#define CANTP_VAR_POWER_ON_INIT_FAST

#define CANTP_VAR_FAST

#define CANTP_CONST

#define CANTP_CONST_FAST

#define CANTP_CONST_PBCFG

#define CANTP_APPL_DATA

#define CANTP_APPL_CONST

#define CANTP_APPL_CODE

/***********[DCM_MemoryAndPointerClasses]**********************************************/
#define DCM_CODE

#define DCM_CODE_FAST

#define DCM_VAR_NOINIT

#define DCM_VAR_POWER_ON_INIT

#define DCM_VAR

#define DCM_VAR_NOINIT_FAST

#define DCM_VAR_POWER_ON_INIT_FAST

#define DCM_VAR_FAST

#define DCM_CONST

#define DCM_CONST_FAST

#define DCM_CONST_PBCFG

#define DCM_APPL_DATA

#define DCM_APPL_CONST

#define DCM_APPL_CODE

/***********[ECUM_MemoryAndPointerClasses]**********************************************/
#define EcuM_CODE

/*=======[ComM_MemoryAndPointerClasses]========================================*/
#define COMM_CODE

#define COMM_CODE_FAST

#define COMM_VAR_NOINIT

#define COMM_VAR_POWER_ON_INIT

#define COMM_VAR

#define COMM_VAR_NOINIT_FAST

#define COMM_VAR_POWER_ON_INIT_FAST

#define COMM_VAR_FAST 

#define COMM_CONST

#define COMM_CONST_FAST

#define COMM_CONST_PBCFG

#define COMM_APPL_DATA

#define COMM_APPL_CONST

#define COMM_APPL_CODE




/* ---------------------------------------------------------------------------*/
/*                   MCU                                                      */
/* ---------------------------------------------------------------------------*/

#define MCU_CODE                	   /* API functions                       */
#define MCU_CODE_FAST                  /* */

#define MCU_VAR_NOINIT                 /* */
#define MCU_VAR_POWER_ON_INIT          /* */
#define MCU_VAR                        /* */
#define MCU_VAR_NOINIT_FAST            /* */
#define MCU_VAR_POWER_ON_INIT_FAST     /* */
#define MCU_VAR_FAST                   /* */

#define MCU_CONST                      /* Data Constants                      */
#define MCU_CONST_PBCFG                /* */
#define MCU_FAST                       /* */

#define MCU_APPL_CODE                  /* callbacks of the Application        */
#define MCU_APPL_CONST                 /* Applications' ROM Data              */
#define MCU_APPL_DATA                  /* Applications' RAM Data              */

/* ---------------------------------------------------------------------------*/
/*                   GPT                                                      */
/* ---------------------------------------------------------------------------*/
#define GPT_CODE                	   /* API functions                       */
#define GPT_CODE_FAST                  /* */

#define GPT_VAR_NOINIT                 /* */
#define GPT_VAR_POWER_ON_INIT          /* */
#define GPT_VAR                        /* */
#define GPT_VAR_NOINIT_FAST            /* */
#define GPT_VAR_POWER_ON_INIT_FAST     /* */
#define GPT_VAR_FAST                   /* */

#define GPT_CONST                      /* Data Constants                      */
#define GPT_CONST_PBCFG                /* */
#define GPT_FAST                       /* */

#define GPT_APPL_CODE                  /* callbacks of the Application        */
#define GPT_APPL_CONST                 /* Applications' ROM Data              */
#define GPT_APPL_DATA                  /* Applications' RAM Data              */


/* ---------------------------------------------------------------------------*/
/*                   WDG                                                      */
/* ---------------------------------------------------------------------------*/

#define WDG_CODE                	   /* API functions                       */
#define WDG_CODE_FAST                  /* */

#define WDG_VAR_NOINIT                 /* */
#define WDG_VAR_POWER_ON_INIT          /* */
#define WDG_VAR                        /* */
#define WDG_VAR_NOINIT_FAST            /* */
#define WDG_VAR_POWER_ON_INIT_FAST     /* */
#define WDG_VAR_FAST                   /* */

#define WDG_CONST                      /* Data Constants                      */
#define WDG_CONST_PBCFG                /* */
#define WDG_FAST                       /* */

#define WDG_APPL_CODE                  /* callbacks of the Application        */
#define WDG_APPL_CONST                 /* Applications' ROM Data              */
#define WDG_APPL_DATA                  /* Applications' RAM Data              */


/* ---------------------------------------------------------------------------*/
/*                   WDGIF                                                    */
/* ---------------------------------------------------------------------------*/
#define WDGIF_CODE                	   /* API functions                       */
#define WDGIF_CODE_FAST                  /* */

#define WDGIF_VAR_NOINIT                 /* */
#define WDGIF_VAR_POWER_ON_INIT          /* */
#define WDGIF_VAR                        /* */
#define WDGIF_VAR_NOINIT_FAST            /* */
#define WDGIF_VAR_POWER_ON_INIT_FAST     /* */
#define WDGIF_VAR_FAST                   /* */

#define WDGIF_CONST                      /* Data Constants                    */
#define WDGIF_CONST_PBCFG                /* */
#define WDGIF_FAST                       /* */

#define WDGIF_APPL_CODE                  /* callbacks of the Application      */
#define WDGIF_APPL_CONST                 /* Applications' ROM Data            */
#define WDGIF_APPL_DATA                  /* Applications' RAM Data            */


/* ---------------------------------------------------------------------------*/
/*                   PORT                                                     */
/* ---------------------------------------------------------------------------*/
#define PORT_CODE                	   /* API functions                       */
#define PORT_CODE_FAST                  /* */

#define PORT_VAR_NOINIT                 /* */
#define PORT_VAR_POWER_ON_INIT          /* */
#define PORT_VAR                        /* */
#define PORT_VAR_NOINIT_FAST            /* */
#define PORT_VAR_POWER_ON_INIT_FAST     /* */
#define PORT_VAR_FAST                   /* */

#define PORT_CONST                      /* Data Constants                      */
#define PORT_CONST_PBCFG                /* */
#define PORT_FAST                       /* */

#define PORT_APPL_CODE                  /* callbacks of the Application        */
#define PORT_APPL_CONST                 /* Applications' ROM Data              */
#define PORT_APPL_DATA                  /* Applications' RAM Data              */



/* ---------------------------------------------------------------------------*/
/*                   DIO                                                      */
/* ---------------------------------------------------------------------------*/
#define DIO_CODE                	   /* API functions                       */
#define DIO_CODE_FAST                  /* */

#define DIO_VAR_NOINIT                 /* */
#define DIO_VAR_POWER_ON_INIT          /* */
#define DIO_VAR                        /* */
#define DIO_VAR_NOINIT_FAST            /* */
#define DIO_VAR_POWER_ON_INIT_FAST     /* */
#define DIO_VAR_FAST                   /* */

#define DIO_CONST                      /* Data Constants                      */
#define DIO_CONST_PBCFG                /* */
#define DIO_FAST                       /* */

#define DIO_APPL_CODE                  /* callbacks of the Application        */
#define DIO_APPL_CONST                 /* Applications' ROM Data              */
#define DIO_APPL_DATA                  /* Applications' RAM Data              */


/* ---------------------------------------------------------------------------*/
/*                   MEMIF                                                    */
/* ---------------------------------------------------------------------------*/
#define MEMIF_CODE                	   /* API functions                       */
#define MEMIF_CODE_FAST                  /* */

#define MEMIF_VAR_NOINIT                 /* */
#define MEMIF_VAR_POWER_ON_INIT          /* */
#define MEMIF_VAR                        /* */
#define MEMIF_VAR_NOINIT_FAST            /* */
#define MEMIF_VAR_POWER_ON_INIT_FAST     /* */
#define MEMIF_VAR_FAST                   /* */

#define MEMIF_CONST                      /* Data Constants                   */
#define MEMIF_CONST_PBCFG                /* */
#define MEMIF_FAST                       /* */

#define MEMIF_APPL_CODE                  /* callbacks of the Application     */
#define MEMIF_APPL_CONST                 /* Applications' ROM Data           */
#define MEMIF_APPL_DATA                  /* Applications' RAM Data           */



/* ---------------------------------------------------------------------------*/
/*                   FEE                                                      */
/* ---------------------------------------------------------------------------*/
#define FEE_CODE                	   /* API functions                       */
#define FEE_CODE_FAST                  /* */

#define FEE_VAR_NOINIT                 /* */
#define FEE_VAR_POWER_ON_INIT          /* */
#define FEE_VAR                        /* */
#define FEE_VAR_NOINIT_FAST            /* */
#define FEE_VAR_POWER_ON_INIT_FAST     /* */
#define FEE_VAR_FAST                   /* */

#define FEE_CONST                      /* Data Constants                      */
#define FEE_CONST_PBCFG                /* */
#define FEE_FAST                       /* */

#define FEE_APPL_CODE                  /* callbacks of the Application        */
#define FEE_APPL_CONST                 /* Applications' ROM Data              */
#define FEE_APPL_DATA                  /* Applications' RAM Data              */



/* ---------------------------------------------------------------------------*/
/*                   PWM                                                      */
/* ---------------------------------------------------------------------------*/
#define PWM_CODE                	   /* API functions                       */
#define PWM_CODE_FAST                  /* */

#define PWM_VAR_NOINIT                 /* */
#define PWM_VAR_POWER_ON_INIT          /* */
#define PWM_VAR                        /* */
#define PWM_VAR_NOINIT_FAST            /* */
#define PWM_VAR_POWER_ON_INIT_FAST     /* */
#define PWM_VAR_FAST                   /* */

#define PWM_CONST                      /* Data Constants                      */
#define PWM_CONST_PBCFG                /* */
#define PWM_FAST                       /* */

#define PWM_APPL_CODE                  /* callbacks of the Application        */
#define PWM_APPL_CONST                 /* Applications' ROM Data              */
#define PWM_APPL_DATA                  /* Applications' RAM Data              */



/* ---------------------------------------------------------------------------*/
/*                   SPI                                                      */
/* ---------------------------------------------------------------------------*/
#define SPI_CODE                	   /* API functions                       */
#define SPI_CODE_FAST                  /* */

#define SPI_VAR_NOINIT                 /* */
#define SPI_VAR_POWER_ON_INIT          /* */
#define SPI_VAR                        /* */
#define SPI_VAR_NOINIT_FAST            /* */
#define SPI_VAR_POWER_ON_INIT_FAST     /* */
#define SPI_VAR_FAST                   /* */

#define SPI_CONST                      /* Data Constants                      */
#define SPI_CONST_PBCFG                /* */
#define SPI_FAST                       /* */

#define SPI_APPL_CODE                  /* callbacks of the Application        */
#define SPI_APPL_CONST                 /* Applications' ROM Data              */
#define SPI_APPL_DATA                  /* Applications' RAM Data              */

/* ---------------------------------------------------------------------------*/
/*                   SCI                                                      */
/* ---------------------------------------------------------------------------*/
#define SCI_CODE                	   /* API functions                       */
#define SCI_CODE_FAST                  /* */

#define SCI_VAR_NOINIT                 /* */
#define SCI_VAR_POWER_ON_INIT          /* */
#define SCI_VAR                        /* */
#define SCI_VAR_NOINIT_FAST            /* */
#define SCI_VAR_POWER_ON_INIT_FAST     /* */
#define SCI_VAR_FAST                   /* */

#define SCI_CONST                      /* Data Constants                      */
#define SCI_CONST_PBCFG                /* */
#define SCI_FAST                       /* */

#define SCI_APPL_CODE                  /* callbacks of the Application        */
#define SCI_APPL_CONST                 /* Applications' ROM Data              */
#define SCI_APPL_DATA                  /* Applications' RAM Data              */


/* ---------------------------------------------------------------------------*/
/*                   ADC                                                      */
/* ---------------------------------------------------------------------------*/
#define ADC_CODE                	   /* API functions                       */
#define ADC_CODE_FAST                  /* */

#define ADC_VAR_NOINIT                 /* */
#define ADC_VAR_POWER_ON_INIT          /* */
#define ADC_VAR                        /* */
#define ADC_VAR_NOINIT_FAST            /* */
#define ADC_VAR_POWER_ON_INIT_FAST     /* */
#define ADC_VAR_FAST                   /* */

#define ADC_CONST                      /* Data Constants                      */
#define ADC_CONST_PBCFG                /* */
#define ADC_FAST                       /* */

#define ADC_APPL_CODE                  /* callbacks of the Application        */
#define ADC_APPL_CONST                 /* Applications' ROM Data              */
#define ADC_APPL_DATA                  /* Applications' RAM Data              */



/* ---------------------------------------------------------------------------*/
/*                   ICU                                                      */
/* ---------------------------------------------------------------------------*/
#define ICU_CODE                	   /* API functions                       */
#define ICU_CODE_FAST                  /* */

#define ICU_VAR_NOINIT                 /* */
#define ICU_VAR_POWER_ON_INIT          /* */
#define ICU_VAR                        /* */
#define ICU_VAR_NOINIT_FAST            /* */
#define ICU_VAR_POWER_ON_INIT_FAST     /* */
#define ICU_VAR_FAST                   /* */

#define ICU_CONST                      /* Data Constants                      */
#define ICU_CONST_PBCFG                /* */
#define ICU_FAST                       /* */

#define ICU_APPL_CODE                  /* callbacks of the Application        */
#define ICU_APPL_CONST                 /* Applications' ROM Data              */
#define ICU_APPL_DATA                  /* Applications' RAM Data              */


/* ---------------------------------------------------------------------------*/
/*                   FLS                                                      */
/* ---------------------------------------------------------------------------*/
#define FLS_CODE                	   /* API functions                       */
#define FLS_CODE_FAST                  /* */

#define FLS_VAR_NOINIT                 /* */
#define FLS_VAR_POWER_ON_INIT          /* */
#define FLS_VAR                        /* */
#define FLS_VAR_NOINIT_FAST            /* */
#define FLS_VAR_POWER_ON_INIT_FAST     /* */
#define FLS_VAR_FAST                   /* */

#define FLS_CONST                      /* Data Constants                      */
#define FLS_CONST_PBCFG                /* */
#define FLS_FAST                       /* */

#define FLS_APPL_CODE                  /* callbacks of the Application        */
#define FLS_APPL_CONST                 /* Applications' ROM Data              */
#define FLS_APPL_DATA                  /* Applications' RAM Data              */

/* ---------------------------------------------------------------------------*/
/*                   DMA                                                      */
/* ---------------------------------------------------------------------------*/
#define DMA_CODE                	   /* API functions                       */
#define DMA_CODE_FAST                  /* */

#define DMA_VAR_NOINIT                 /* */
#define DMA_VAR_POWER_ON_INIT          /* */
#define DMA_VAR                        /* */
#define DMA_VAR_NOINIT_FAST            /* */
#define DMA_VAR_POWER_ON_INIT_FAST     /* */
#define DMA_VAR_FAST                   /* */

#define DMA_CONST                      /* Data Constants                      */
#define DMA_CONST_PBCFG                /* */
#define DMA_FAST                       /* */

#define DMA_APPL_CODE                  /* callbacks of the Application        */
#define DMA_APPL_CONST                 /* Applications' ROM Data              */
#define DMA_APPL_DATA                  /* Applications' RAM Data              */

/* ---------------------------------------------------------------------------*/
/*                   EEP                                                      */
/* ---------------------------------------------------------------------------*/
#define EEP_CODE                	   /* API functions                       */
#define EEP_CODE_FAST                  /* */

#define EEP_VAR_NOINIT                 /* */
#define EEP_VAR_POWER_ON_INIT          /* */
#define EEP_VAR                        /* */
#define EEP_VAR_NOINIT_FAST            /* */
#define EEP_VAR_POWER_ON_INIT_FAST     /* */
#define EEP_VAR_FAST                   /* */

#define EEP_CONST                      /* Data Constants                      */
#define EEP_CONST_PBCFG                /* */
#define EEP_FAST                       /* */

#define EEP_APPL_CODE                  /* callbacks of the Application        */
#define EEP_APPL_CONST                 /* Applications' ROM Data              */
#define EEP_APPL_DATA                  /* Applications' RAM Data              */
#endif /* end of COMPILER_CFG_H */

/*=======[R E V I S I O N   H I S T O R Y]====================================*/
/*  <VERSION>    <DATE>    <AUTHOR>    <REVISION LOG>
 *  V1.0         20130227  chenxuehua  Initial version
 */
/*=======[E N D   O F   F I L E]==============================================*/
